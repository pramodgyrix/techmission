<?php
$CI = & get_instance();	
$controller = $CI->router->class;
$method = $CI->router->method;
?>
<?php if(!$unset_bulk_delete || !$unset_bulk_publish || !$unset_bulk_publish){?>
<style>
.highlight{background-color:#E02222 !important;}
</style>
<script type="text/javascript">
$(function () {
	//CHECK ALL BOXES
	$('.checkall').click(function () {		
		if($(this).is(':checked')){
			$('input[name="custom_delete"]').prop( "checked", true );
		}else{
			$('input[name="custom_delete"]').attr('checked', false);
		}
	});
});

function delete_selected(action){


	if (confirm("Are you sure to perform this action?")){	
			
		var list = "";
		jQuery('input[name=custom_delete]').each(function() {     
			if (this.checked) {
				//remove selection rows
				if(action=='delete'){
					jQuery('#custom_tr_'+this.value).remove();
				}else{
					jQuery('#custom_tr_'+this.value).find('td').addClass('highlight');
					setTimeout(function(){jQuery('#custom_tr_'+this.value).find('td').removeClass('highlight');}, 2000);								
				}
				//create list of values that will be parsed to controller
				list += this.value + '|';
			}
		});
		
		
		
		//send data to delete
		if(list !=''){
			 
			jQuery.post(bulk_action_url, { selection: list, action: action, table_name: basic_db_table_name, field_name: status_db_field_name, primary_key:primary_key}, function(data) {
				alert(data);
				$('.ajax_refresh_and_loading').closest('.flexigrid').find('.filtering_form').trigger('submit');
			});
		}
    }else{
		return false;		
	}
	
}

</script>
<?php } ?>
<?php  
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	$column_width = (int)(80/count($columns));
	
	if(!empty($list)){
?><div class="bDiv" >
		<table cellspacing="0" cellpadding="0" border="0" id="flex1">
		<thead>
			<tr class='hDiv'>
            	<?php if(!$unset_bulk_delete || !$unset_bulk_publish || !$unset_bulk_publish){?>
            	<th style="padding:3px 5px 0px !important;" valign="middle"><input type="checkbox" class="checkall" /></th>
                <?php }?>
				<?php foreach($columns as $column){?>
                <th width='<?php echo $column_width?>%'>
					<div class="text-left field-sorting <?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?><?php echo $order_by[1]?><?php }?>" 
						rel='<?php echo $column->field_name?>'>
						<?php echo $column->display_as?>
					</div>
				</th>
				<?php }?>
				<?php if(!$unset_delete || !$unset_edit || !empty($actions)){?>
				<th align="left" abbr="tools" axis="col1" class="" width='20%'>
					<div class="text-right">
						<?php echo $this->l('list_actions'); ?>
					</div>
				</th>
				<?php }?>
			</tr>
		</thead>		
		<tbody>
<?php foreach($list as $num_row => $row){ ?>        
		<?php
		$temp_string = $row->delete_url;
		$temp_string = explode("/", $temp_string);
		$row_num = sizeof($temp_string)-1;
		$rowID = $temp_string[$row_num];
		?>
		<tr  <?php if($num_row % 2 == 1){?>class="erow"<?php }?> id="custom_tr_<?=$rowID?>">
            <?php if(!$unset_bulk_delete || !$unset_bulk_publish || !$unset_bulk_publish){?>
            <td style="padding:3px 5px 0px !important;" valign="middle"><input type="checkbox" name="custom_delete" value="<?=$rowID?>" /></td>
            <?php }?>
			<?php foreach($columns as $column){?>
            <td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
				<div style="width: 100%;" class='text-left'><?php echo $row->{$column->field_name}; ?></div>
			</td>
			<?php }?>			
            <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
			<td align="left" width='20%'>
				<div class='tools'>
                	<?php 
					if(!empty($row->action_urls)){						
						foreach($row->action_urls as $action_unique_id => $action_url){ 
							$action = $actions[$action_unique_id];							
					?>
							<a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action" title="<?php echo $action->label?>"><?php 
								if(!empty($action->image_url))
								{
									//echo '<img src="'.$action->image_url.'" alt="'.$action->label.'" />';
									echo '<i class="'.$action->image_url.'"></i> '.$action->label;
								}else{
									echo $action->label;
								}
							?></a>
					<?php }
					}
					?>			
					<?php if(!$unset_delete){?>
                    	<a href="<?php echo $row->delete_url?>" title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row">
                        	<!--<span class='delete-icon'></span>-->
                        	<button type="button" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                        </a>
                    <?php }?>
                    <?php if(!$unset_edit){?>
						<a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button"><!--<span class='edit-icon'></span>--><button type="button" class="btn btn-primary"><i class="fa fa-edit"></i></button></a>
					<?php }?>
					<?php if(!$unset_read){?>
						<a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button" <?php if(($controller=='properties')||($controller=='myproperties')){echo 'target="_blank"';}?>><!--<span class='read-icon'></span>--><button type="button" class="btn btn-info"><i class="clip-expand"></i></button></a>
					<?php }?>									
                    <div class='clear'></div>
				</div>
			</td>
			<?php }?>
		</tr>
<?php } ?>        
		</tbody>
		</table>
	</div>
<?php }else{?>
	<br/>
	&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $this->l('list_no_items'); ?>
	<br/>
	<br/>
<?php }?>	
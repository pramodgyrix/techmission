var messageFormValidator = function () {
    // function to initiate Validation Sample 2
	var runValidator = function () {
        var form = $('#messageComposeForm');
        var errorHandler = $('.errorHandler', form);
        var successHandler = $('.successHandler', form);
		
        $.validator.addMethod("getEditorValue", function (){            
            if ($("#msgbody").val() != "" && $("#msgbody").val() != "<br>") {                
                return true;
            } else {
                return false;
            }
        }, 'Please specify your message.');
		
		$.validator.addMethod("multiemail",	function(value, element){
				var email = value.split(/[;,]+/); // split element by , and ;
				valid = true;
				for (var i in email) {
					value = email[i];
					valid = valid && jQuery.validator.methods.email.call(this, $.trim(value), element);
				}
				return valid;
			}, jQuery.validator.messages.multiemail);
		
        form.validate({
            errorElement: "span", // contain the error msg in a small tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.hasClass("ckeditor")) {
                    error.appendTo($(element).closest('.form-group'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
				recipienties:{
					required: true,
					multiemail: true
				},
                subject: {
                    minlength: 2,
                    required: true
                },
                message: 'getEditorValue'
            },
            messages:{
				recipienties: {
                    required: "Please specify recipient email address",
                    multiemail: "The Recipient email address must be in the format of name@domain.com"
                },
                subject: "Please specify your subject of message",
				message: "Please specify your Message"
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler.hide();
                errorHandler.show();
            },
            highlight: function (element) {

                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                successHandler.show();
                errorHandler.hide();
                // submit form
                form.submit();
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {           
            runValidator();
        }
    };
}();
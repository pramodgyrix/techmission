(function(){
	  var dataAPI = base_url+"messages/recent_mail";
	  $.getJSON(dataAPI,{}).done(function(result){
		  //alert(data.total);
		 var rsc = $('.recently_unread_mail');
		  var total = result.total;		  
		  var dataRecords = result.data;
		  var content = '';
		  $('.total_badge_mail').text(total);	
		  $('.total_mail').text('You have '+total+' messages');
		  if(total > 0){
			rsc.find('ul').html('');		  
			  $.each(dataRecords, function(i, data){
				var sender = (data.name=='')?data.name:data.sender_email;
				content = '<li>';
				content += '    <a target="_blank" href="'+base_url+'messages/messageDetail/'+data.message_id+'">';
				content += '        <div class="clearfix">';
				content += '            <div class="thread-image circle-img">';
				content += '                <img class="circle-img" src="'+data.profile_image+'" style="width:40px; height:40px;">';
				content += '            </div>';
				content += '            <div class="thread-content">';            
				content += '                <span class="author">'+sender+'</span>';
				content += '                <span class="preview">'+data.subject+' - '+data.message+'</span>';
				content += '                <span class="time">'+data.send_date+'</span>';
				content += '            </div>';
				content += '        </div>';
				content += '    </a>';
				content += '</li>';			
				rsc.find('ul').append(content);
			  });
		   }
		});
})();

(function(){
	$('#mobile-search').on('click', function(){
		$('#basic-search-form').toggle();
	});
	
	if ($('#mobile-search').is(':visible')){
		$('.hideonmobile').hide();
	}
})();
var validateEmail = function(email) { 
	if(typeof email === 'undefined' || !email){
		return false;
	};

    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    return re.test(email);

}
var validateName = function(name) { 
	if(typeof name === 'undefined' || !name){
		return false;
	};

    return name;

}
var validateContactNumber = function(no) { 
	if(typeof no === 'undefined' || !no){
		return false;
	};

    var re = /^[0-9-+]+$/;

    return re.test(no);

}

var subscribe_email = function(){
	  $('#msg_success').hide();
	  var email = $('#subscribe').val();
	
		if (validateEmail(email)) {
			  $.ajax({  
				  type : "POST",
				  url  : base_url+"ajax/subscribe",
				  data : {'email': email},
			
					success: function(result){
					 var result_data = JSON.parse(result);
					 var result_class = result_data.class;
					 var result_message = result_data.message;
					 var data = '<div class="'+result_class+'">'+result_message+'<button data-dismiss="alert" class="close">×</button></div>';
					 $('#msg_success').show().html(data);
					}
				 });
	  }else{
		  var data = '<div class="alert alert-danger">Please enter a valid email address<button data-dismiss="alert" class="close">×</button></div>';
		  $('#msg_success').show().html(data);
		  return false; 
	 }
}
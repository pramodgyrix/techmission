$(document).ready(function(){		 
 initialize();
 
   $('#address').focusout(function(){
      initialize();
       var country = $.trim($('select[name="country"]').children("option:selected").text());
		var state   =   $.trim($('select[name="state"]').children("option:selected").text())
		var city = $.trim($('select[name="city"]').children("option:selected").text());
		var address = document.getElementById("address").value;
		codeAddress(address +', '+city +', '+state+', '+country);
	  
  });
	 });
	 	
var map;
var geocoder = new google.maps.Geocoder();
var autocomplete = {};
var autocomplete;
var markericon = site_url+'assets/js/gmap/map-pointer.png';

var markers = [];
var marker = '';

 function initialize() {
    var latlng = new google.maps.LatLng(22.7195687, 75.85772580000003); 
    var mapOptions = {
      zoom: 8,
      center: latlng,
	  icon: markericon
    }
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	 initializeAutocomplete();
	 codeAddress();
	 
  }
  
  function initializeAutocomplete() {
  autocomplete = new google.maps.places.Autocomplete(
    document.getElementById('address'),
    {
      types: ['geocode']
    }
  );
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    fillInAddress();
  });
}

function fillInAddress() {
  var place = autocomplete.getPlace();
  //alert(place);
  console.log(place);/*
  var latitude = place.geometry.location.lat();
  var longitude = place.geometry.location.lng();
  $('#latitude').val(latitude);
  $('#longitude').val(longitude);
  var formatted_address = place.formatted_address;*/
  codeAddress();
}

  $(function(){
  
  });
  
  var from_input_name = { street_number: 'short_name', route: 'long_name', locality: 'long_name', administrative_area_level_1: 'short_name', country: 'short_name', postal_code: 'short_name' };

    function codeAddress(searchAddress) {  
    var address = document.getElementById('address').value;
	var address = (searchAddress) ? searchAddress : address;
	     if (address == '') {
           // alert("Address can not be empty!");
            return;
        }
		
    geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) 
      {
          clearMarkers();
		  
		  var lat = results[0].geometry.location.lat();
          var lng = results[0].geometry.location.lng();
          $("#latitude").val(results[0].geometry.location.lat());
          $("#longitude").val(results[0].geometry.location.lng());
		  
		  
		  if(results[0].address_components.length != ''){	
			for (var i = 0; i < results[0].address_components.length; i++) {
					var addressType = results[0].address_components[i].types[0];
					if (typeof from_input_name[addressType] !== 'undefined') {
					   var val = results[0].address_components[i][from_input_name[addressType]];
						$('.'+addressType).val(val);
						//console.log(val);
					}
					
				}
			 }
 
          map.setCenter(results[0].geometry.location);
          marker = new google.maps.Marker({
          map: map, 
          position:results[0].geometry.location,
		  icon: markericon,
		  title: 'drag city address',
          draggable: true

        });
		markers.push(marker);
        google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
        });
     
      } 
    });
  }
  
   function geocodePosition(pos) {
    geocoder.geocode({
      latLng: pos
    }, function(responses) {
      if (responses && responses.length > 0) {
      
      document.getElementById('address').value=responses[0].formatted_address;
	  
	  
	    $("#latitude").val(responses[0].geometry.location.lat());
          $("#longitude").val(responses[0].geometry.location.lng());
		  
		  
      } else {
        alert('Cannot determine address at this location.');
      }
    });
  }
  
    function clearMarkers() {

  if (markers) {

    for (var i = 0; i < markers.length; i++) {

      markers[i].setMap(null);

    }

  }

  markers = new Array();

}

 // google.maps.event.addDomListener(window,'load', initialize);
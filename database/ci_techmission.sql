-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2016 at 12:33 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ci_techmission`
--
CREATE DATABASE IF NOT EXISTS `ci_techmission` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ci_techmission`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `contact_no` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `remark` text NOT NULL,
  `country_id` int(10) NOT NULL,
  `branch_id` int(10) NOT NULL,
  `industries` varchar(200) NOT NULL,
  `physical_country` int(10) NOT NULL,
  `physical_city` varchar(100) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_address` text NOT NULL,
  `company_website` varchar(100) NOT NULL,
  `company_email` varchar(150) NOT NULL,
  `created_by` int(10) NOT NULL COMMENT 'user_id',
  `updated_by` int(10) NOT NULL COMMENT 'user_id',
  `created_date` int(10) NOT NULL,
  `updated_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: inactive,1: active',
  PRIMARY KEY (`id`),
  KEY `fk_account_country_id` (`country_id`),
  KEY `fk_account_branch_id` (`branch_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1037 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(1, 'Gyrix infolab', 'A B Road Sigapore', 'sg_65_81234568', 'pramod.jain@gyrix.in', 'sg_65_81234569', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 1, 1, '1', 192, 'Sigapore', '', '', 'http://www.lockernlock.com/sms', '', 2, 0, 1477030967, 0, 1),
(2, 'aaa', '', 'sg_65_2345666', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(3, 'Bukit Batok Secondary School', '50 Bukit Batok West Avenue 8\r\rSingapore 658962', 'sg_65_65663121', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(4, 'Katoen Natie Singapore Jurong Pte Ltd', 'No 1 Banyan Place\r\rJurong Place\r\rSingapore 627841', 'sg_65_98514918', '', 'sg_65_64192501', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(5, 'MedTech Engineering Services Pte Ltd', '1093 Lower Delta Road \r\r#02-16\r\rSingapore 169204', 'sg_65_98006044', '', 'sg_65_62708680', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(6, 'Kalidasa Investment Pte Ltd', 'La Pizziola\r\r19 Kalidasa Avenue \r\rSingapore 789398', 'sg_65_91479485', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(7, 'Semiconductor Technologies & Instruments Pte Ltd', 'Blk 25 #02-03\r\rKallang Avenue \r\rSingapore 339416', 'sg_65_96930369', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(8, 'Mactech Engineering & Trading Pte Ltd', 'Blk 9005 Tampines Street 93\r\r#02-228 Tampines Industrial Park Singapore 528839', 'sg_65_81648263', '', 'sg_65_67820190', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(9, 'DSTA PC09-FACMGT/JOINT', 'Blk 320 Gombak Drive\r\rSingapore 669647', 'sg_65_97842871', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(10, 'Tan Teck Meng', '', 'sg_65_98453427', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(11, 'BMTC HQ', '', 'sg_65_91788046', '', 'sg_65_65100018', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(12, 'Fisher Hostel LLP', '127 Tyrwhitt Road\r\rSingapore 207551\r\rAttn:  Mr James Ang', 'sg_65_98152780', '', 'sg_65_62978835', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(13, 'Sentosa 4D Magix & Cine Blast Pte Ltd', '51B Imbiah Road\r\rSentosa\r\rSingapore 099708\r\rAttn  :  Mr Rajkumar / Asst Manager ( Technical Service )', 'sg_65_96406524', '', 'sg_65_62743933', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(14, 'Sentosa 4D Magix ^ Cine Blast Pte Ltd', '51B Imbiah Road\r\rSentosa\r\rSingapore 099708\r\rAttn  :  Mr Rajkumar\r\rAssistant Nanager ( Technical Service)', 'sg_65_96406524', '', 'sg_65_62743933', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(15, 'Singapore Pools (Private) Limited', '', 'sg_65_98453427', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(16, 'Skyline Luge Sentosa', '45 Siloso Beach Walk \r\rSentosa\r\rSingapore 099003\r\rAttn :  Ms Sarah Tan \r\rRetail Sales Manager', 'sg_65_83667655', '', 'sg_65_62730906', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(17, 'Securite Associates Pte Ltd', '', 'sg_65_93889698', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(18, 'DB&B Pte Ltd', '3791 Jalan Bukit Merah  #07-15, S159471', 'sg_65_84247488', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(19, 'Ramen Play Pte Ltd', '', 'sg_65_91017117', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(20, 'Teknor Apex Asia Pacific Pte Ltd', '', 'sg_65_98226163', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(21, 'Zebra Technologies Asia Pacific', '5 Changi North Way Level 3\r\rAgility Building \r\rSingapore 498771\r\rAttn  :  Mr Bernard Tan \r\rReconfiguration Supervisor', 'sg_65_98245329', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(22, 'HEB-ASHRAM', '30 Durban Road\r\rSingapore 759642\r\rAttn :  Mr Alvin Dawson\r\rSocial Worker', 'sg_65_91258184', '', 'sg_65_67533727', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(23, 'ID Inc Interiors Pte Ltd', '', 'sg_65_98637432', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(24, 'Comfort Delgro Engineering', '205 Braddell Road\r\rSingapore 579701\r\rAttn :  Mr Ahmad Jumaedi Bin Ngari / Tazi Maintainence', 'sg_65_98764559', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(25, 'CISCO Recall Total Information Management Pte Ltd', 'CISCO Recall Centre\r\r28 Quality Road\r\rSingapore 618828\r\rAttn  :  Mr Joseph Leow', 'sg_65_96641003', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(26, 'Hard Rock Cafe', '', 'sg_65_96611416', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(27, 'Hougang Mall', '90 Hougang Avenue 10, #05-21 \r\r\r\rHougang Mall', 'sg_65_64889610', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(28, 'Lilliputt Pte Ltd', '902 East Coast Parkway Blk B\r\r#03-05, Playground @ Big Splash\r\rS 449874\r\r\r\r60/40', 'sg_65_63489606', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(29, 'Moon Shine Entertainment', '1 Claymore Drive, #02-11\r\rOrchard Rower Rear Block Apartment\r\rS 229594\r\r\r\r70/30', 'sg_65_98513169', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(30, 'Singapore Botanic Garden', '1 Cluny Road\r\rSingapore Botanic Garden\r\rS 259569', 'sg_65_98378463', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(31, 'SICEC', '5 Temasek Boulevard #07-03 \r\rSuntec Tower 5\r\rS 039594', 'sg_65_92396186', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(32, 'Singapore Canoe Federation', 'MachRitches Reservoir Paddle Lodge', 'sg_65_93838409', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(33, 'Ipanema', 'Strumms Enterainment\r\r\r\r400 Orchard Road, #02-43\r\rOrchard Towers S 238875\r\r\r\r50/50', 'sg_65_68874824', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(34, 'Ba-Li-Ba', 'Strumm''s Holding Pte Ltd\r\r\r\r400, Orchar Road, #01-100\r\rOrchard Towers, S 238875\r\r\r\r70/30', 'sg_65_94597779', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(35, 'Blowfish Entertainment', 'Strumm''s Entertainment\r\r\r\r360 Orchard Road, #B1-00\r\rInternational Building, S 238869\r\r\r\r70/30', 'sg_65_67376036', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(36, 'Shangri-la Night Club', '189 Selegie Road, #B2-01 ton 08\r\rSelegie Centre, S 188332', 'sg_65_63394138', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(37, 'Ten & Hans Trading', 'Kisok@MacRitches Reservoir,\r\rOff Thomson Road\r\r\r\r70/30', 'sg_65_93809955', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(38, 'Underwater World Singapore Pte Ltd', '80 Siloso Road, Sentosa\r\rS 098969\r\r\r\r60/40', 'sg_65_93842648', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(39, 'Wave House Sentosa (Singapore)', '36 Siloso Beach Walk\r\rSentosa, S 099007', 'sg_65_96912422', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(40, 'Zirca Mega Club', 'Cannery Leisure Pte  Ltd\r\r\r\rBlk C, #01-02 to 05 & 02-01 to 08\r\rThe Cannery, Clarke Quay,\r\rRiver Valley Rd, S 179022', 'sg_65_83663024', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(41, 'Doll House', '30 Victor Street, #B1-05', 'sg_65_91641213', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(42, 'Kidz Mania', '1020 East Coast Parkway, #01-02', 'sg_65_91467546', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(43, 'Swiss Cottage SS', '', 'sg_65_65637173', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(44, 'Hill grove SS', '3 Bukit Batok Street 34', 'sg_65_92716993', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(45, 'Grand Hollywood Disco & Pub Pte Ltd', '3A River Valley Road, #03-01\r\rMerchant''s court', 'sg_65_98587277', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(46, 'Ngee Ann Poly', '535 Clementi Road', 'sg_65_64606600', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(47, 'Funan Digital Life Mall', 'CaptiaMalls Asia Ltd\r\r\r\r109 North Bridge Road\r\r#01-15A Funan DigitalLife Mall', 'sg_65_63327852', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(48, 'Club Avatar Pte Ltd', '6 Raffles Boulevard Marine Square\r\r#01-16, Marina Mandarin Hotel', 'sg_65_91818609', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(49, 'Attica Pte Ltd', '3A River Valley Road\r\r#01-03 Clarke Quay \r\rSingapore 179020\r\r\r\rAttn  :  Ms Linda', 'sg_65_98751247', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(50, 'ARMF ll (Liang Court) Pte Ltd', '177 River Valley Road\r\r#05-17/18', 'sg_65_63372495', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(51, 'Club Oceanz', '', 'sg_65_94510451', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(52, 'Polliwogs East Coast Pte Ltd', '1020 East Coast Parkway\r\r#01-02, S 449878', 'sg_65_91467546', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(53, 'Jcube The Ring', 'CapitalMalls Asia Limited\r\r\r\r2 Jurong East Central 1\r\r#05-01, S 609731', 'sg_65_66842139', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(54, 'Gardens by the Bay', '18 Marina Gardens Drive\r\rS 018953', 'sg_65_96177164', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(55, 'Club Impact Entertainment Pte Ltd', '2 Serangoon Road\r\r#02-26/27, The Verge\r\rS 218227', 'sg_65_98988104', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(56, 'New Sensation', '1 Sophia Road, #03-50\r\rPeace Centre\r\rS 228149', 'sg_65_92283901', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(57, 'Palnet Paradise Pte Ltd', '177 River Valley Road\r\rShopping Centre S 179030', 'sg_65_91914680', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(58, 'Kampung Siglap Mosque Management Board', '451 Marine Parade Road\r\rS 449283', 'sg_65_62437060', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(59, 'Jeanz Lives', 'No.2, Serangoon Road\r\r#01-24/25 (Verge)', 'sg_65_98312084', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(60, 'Polliwogs Robertson Walk Private Limited', '11 Unity Street\r\r#02-18/19 Robertson Walk\r\rS 237995', 'sg_65_64421805', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(61, 'Accesstech Engineering Pte Ltd', '', 'sg_65_97456383', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056795, 0, 1),
(62, 'I-Bridge Design', '144 Rangoon Road\r\rSingapore 218421\r\rAttn :  Ms Wendy Neo', 'sg_65_97651775', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(63, 'MCST No :  3346', 'Montview\r\r63 Mount Sinai Drive\r\rSingapore 277116', 'sg_65_62199349', '', 'sg_65_62197663', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(64, 'ISG Asia Pte Ltd', '', 'sg_65_98391653', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(65, 'LHN Group Pte Ltd', '', 'sg_65_96353400', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(66, 'ab', '', 'sg_65_98485855', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 6, 1477056796, 1477361083, 1),
(67, 'Pietrasanta', '', 'sg_65_97295053', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(68, 'Kimerbly Lee Tan', '', 'sg_65_60108209666', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(69, 'The 3rio', '6 Oxford Street \r\rSingapore 798409\r\rAttn : Mr Anson Chua', 'sg_65_98399388', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(70, 'The Gelato Connection Pte Ltd', '8A Admiralty Street \r\r#04-37 Food Xchange\r\rSingapore 757437\r\rAttn :  Mr Dylan Ooi', 'sg_65_96226457', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(71, 'Kim Leng Office Equipment Pte Ltd', '260 Jalan Besar \r\rSingapore 208935\r\rAttn  :  Mr Casey Tan', 'sg_65_96644069', '', 'sg_65_62279236', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(72, 'Kwong Wai Shiu Hospital', '', 'sg_65_91795996', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(73, 'ISS M&E Pte Ltd', '', 'sg_65_97359664', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(74, 'Derrick Loh', '', 'sg_65_91914391', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(75, 'Breadtalk Pte Ltd', '171 Kampong Ampat #05-01 to 06 \r\rKA Food Link\r\rS368330', 'sg_65_97998053', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(76, 'DH Contracts Pte Ltd', '19 Tannery Lane \r\rLevel 3\r\rLiao Ning Building \r\rSingapore 347781\r\rAttn :  Ms Jace Chiun', 'sg_65_94504071', '', 'sg_65_68481340', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(77, 'Hawker Pacific Asia Pte Ltd', '720 West Camp Road \r\rSeletar Aerospace Park, Singapore 797520', 'sg_65_90711611', '', 'sg_65_66817874', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(78, 'Park 22 Hotel', '22 Teck Lim Road \r\rSingapore 088392\r\rAttn  :  Mr Ng Aung San\r\rTel   :  62255565\r\rFax   :  62255603\r\remail :  ngaungsan@park22hotel.com', 'sg_65_93680786', '', 'sg_65_62255603', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(79, 'Miracles Design Consultants Pte Ltd', '67 Ubi Crescent #03-06 \r\rTechniques Centre \r\rSingapore 408560\r\rAttn :  Mr Wayne Phan \r\rHP   :   9322 6792\r\rTel  :   6747 5789\r\rFax  :   6747 3206\r\rwayne@miraclesdesign.com.sg', 'sg_65_93226792', '', 'sg_65_6747 3206', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(80, 'Novotel Singapore Clarke Quay Hotel', '177A River Valley Road \r\rSingapore 179031', 'sg_65_98228806', '', 'sg_65_6433 8868', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(81, 'Khim Huat', 'NULL', 'sg_65_83222340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(82, 'Country Foods Pte Ltd', '', 'sg_65_83222340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(83, 'Intership Ltd c/o', '1 Scotts Road\r\r#16-06 Shawcentre\r\rSingapore 228208\r\rAttn :  Mr Daniel Bruh\r\rLogistic and Purchasing Manager', 'sg_65_9722 8770', '', 'sg_65_68363745', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(84, 'NSL OILCHEM WASTE MANAGEMENT PTE LTD', '', 'sg_65_90219659', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(85, 'Juronghealth (Alexandra Hospital)', '', 'sg_65_81821185', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(86, 'J.J. Ricson (S) Pte Ltd', '37 Defu Lane 10\r\r#04-75 Defu Industrial Estate\r\rSingapore 539214', 'sg_65_96261806', '', 'sg_65_62812587', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(87, 'Daiman Johor Jaya Sports Complex Berhad', 'No 1 Jalan Dedap 3, Taman Johor Jaya, 81100 Johor Bahru, Johor Darul Takzim, West Malaysia', 'sg_65_60167711592', '', 'sg_65_6073552077', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(88, 'Personal_Mulyadi', 'JL. Alaydrus No 65-67\r\rJakarta 10130,\r\r\r\rIndonesia', 'sg_65_62816819100', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(89, 'Shung Design Pte Ltd', '', 'sg_65_9145 6452', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(90, 'Central Star (Singapore) Pte Ltd', 'Blk 9006 Tampines Industrial Park A\r\r#01-206 Tampines St 93\r\rSingapore 528840', 'sg_65_9675 2316', '', 'sg_65_6785 3166', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(91, 'Shin Yong Construction Pte Ltd', 'Blk 3017A #01-15\r\rUbi Road 1\r\rSingapore 408709\r\rAttn :  Mr Chris \r\remail :  chris@shinyong.com.sg', 'sg_65_97718163', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(92, 'Two K Renovation Work', '34, Jalan GU 2/8, Taman Garing Utama, 48000 Rawang.', 'sg_65_165507599', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(93, 'Cooking Art Industries', 'Blk 3017, Bedok North St 5\r\r#06-34, Gourmet East Kitchen\r\rSingapore 486121', 'sg_65_81995273', '', 'sg_65_62441812', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(94, 'SAFTI ( MI )', '', 'sg_65_98206027', '', 'sg_65_98206027', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(95, 'J&Fong Deco Pte Ltd', '', 'sg_65_9856 8316', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(96, 'Ms Zhao', '', 'sg_65_90178160', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(97, 'WILLTÉCH ASIA PTE LTD', 'Lot 2, Oasis, Johor Port, \r\r81700 Pasir Gudang, Johor', 'sg_65_125631285', '', 'sg_65_72524907', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(98, 'Mount Elizabeth', '3 Mount Elizabeth Hospital\r\rSingapore 228510', 'sg_65_91148793', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(99, 'Krekka Creation Sdn Bhd', 'Unit 3A-09 Block D, Kelana Square Jalan SS7/26,47301 Petaling Jaya', 'sg_65_174038938', '', 'sg_65_378809804', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(100, 'Progress Bio Pharma Sdn Bhd', 'Lot 5016 Jalan Air Hitam \r\r83700 Yong Peng \r\rJohor', 'sg_65_127100576', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(101, 'Design Eternity', 'Blk 55 Marine Terrace\r\r#04-02\r\rSingapore 440055\r\rAttn :   Mr BK Tan \r\rHP   :   91083038', 'sg_65_91083038', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(102, 'Prestige Workshop Pte Ltd', '', 'sg_65_94230866', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(103, 'Completion Products Pte Ltd', 'No 13 Tuas Avenue 6\r\rSingapore 639301\r\rAttn :   Ms Jacinta \r\rTel  :   68978970 ( Ext 212 )\r\rjacinta@completionproducts.com', 'sg_65_98175763', '', 'sg_65_68978570', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(104, 'Asia Square', '8 Marina View\r\rAsia Square Tower 1\r\r#07-05 Singapore 018960\r\rAttn : Ms Joyce Wong \r\rHP   : 98558033\r\remail :  Joyce.Wong@ap.jll.com', 'sg_65_98558033', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(105, 'COMO Hotels and Resorts', 'COMO House \r\r6B Orange Grove Road \r\rSingapore 258332\r\rAttn  : Ms Catherine Low \r\rProcurement Manager\r\rHP  :   8121532\r\remail :   catherine.low@comohotels.com', 'sg_65_81215232', '', 'sg_65_63041485', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(106, 'LTA Sports Club', '', 'sg_65_9125 7837', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(107, 'Forest Adventure Pte Ltd', 'Bedok Reservoir Park\r\rAttn :  Mr Dennis \r\rHP   :  8100 7420\r\rdennis@forestadventure.com.sg', 'sg_65_8100 7420', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(108, 'KLP Sales & Services', 'No 496, Jalan Jambu Gajus,\r\rJinjang Selatan,\r\r52000 Kuala Lumpur', 'sg_65_123190121', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(109, 'DTZ Facilities & Engineering (S ) Limited', '2 Stadium Walk\r\rSingapore 397691\r\rAttn :  Mr Ibrahim Daud\r\rAsst Fire Health Safety & Security Manager\r\rTel  :  63409258 \r\rFax  :  63445903\r\ribrahim.daud@sportshub.com.sg', 'sg_65_91864331', '', 'sg_65_63445903', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(110, 'NPS International School', '', 'sg_65_9159 2128', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(111, 'Halex (M) Sdn Bhd', 'No. 9, Jalan Taruka, \r\rTampoi Industrial Estate, \r\r81200, Johor Bahru, Johor, Malaysia.', 'sg_65_167722627', '', 'sg_65_72370276', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(112, 'Cheers Holdings ( 2004 ) Pte Ltd', '82 Genting Lane \r\r#02-08 Media Centre\r\rSingapore 349567\r\rAttn :  Mr Derek Tan \r\rAlliance & Operations Support Manager\r\remail : derek.tan@cheersstore.com', 'sg_65_96678667', '', 'sg_65_65520161', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(113, 'Cosmek Link Sdn Bhd', 'Lot 895, Jalan Masjid Kanchong Darat, 42700 Banting, Kuala Langat, Selangor', 'sg_65_192459911', '', 'sg_65_331879729', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(114, 'Modular Space Pte Ltd', 'One Commonwealth, \r\rLevel 6 Unit 10, \r\rCommonwealth Avenue \r\rSingapore 149544\r\rMain: 6570 5700 \r\rFax: +65 6570 3900', 'sg_65_9819 4540', '', 'sg_65_6570 3900', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(115, 'Pustaka Union', '1959 L-M Jalan Stadium,\r\r05100 Alor Setar, Kedah', 'sg_65_174721718', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(116, 'Club Luxy LLP', '177 River Valley Road \r\rLiang Court \r\rSingapore 179030\r\rAttn  :  Ms Daisy Morgan \r\rHP    :  93266055\r\remail :  daisy_morgan@hotmail.com', 'sg_65_93266055', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(117, 'Sunray Woodcraft Construction Pte Ltd', 'SUNRAY BUILDING\r\r9 Sungei Kadut Street 3 Singapore 729143\r\rAttn :  Ms Cherrie Cel\r\rQuantity Surveyor\r\rTel  : 65662311\r\rFax  : 65653332 \r\rdela.cruz@sunray.com.sg', 'sg_65_91138700', '', 'sg_65_65653332', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056796, 0, 1),
(118, 'Castello Design Asia Pte Ltd', '25 Mandai Estate #01-02\r\rInnovation Place Tower 1\r\rSingapore 729930\r\rAttn :  Ms Doreen \r\rTel:  6363 6566 / HP:9873 4468\r\rFax: 6363 1715', 'sg_65_9859 3969', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(119, 'Bruliz Resources', 'No 19, Jalan Opera E, U2/E, 40000 TTDI Shah Alam, Malaysia', 'sg_65_133312418', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(120, 'Teguh Satria Enterprise', 'No. 1-1-14 Gugusan Dedap, \r\rJalan Kenyalang SBJ 11/1, \r\r47810 Petaling Jaya, \r\rSelangor', 'sg_65_177862721', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(121, 'Mineware Metal Works & Industrial Supply', 'No. 24, Jalan Rumbia 20,\r\rTaman Daya,\r\r81100 Joihor Bahru, Johor', 'sg_65_127858887', '', 'sg_65_73549789', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(122, 'NDT Instruments Pte Ltd', '50 Ubi Avenue 3\r\r#05-20\r\rFrontier \r\rSingapore 408866\r\rTel :  65710663\r\rFax :  65710669\r\rjtlim@ndt-instruments.com', 'sg_65_98185548', '', 'sg_65_65710669', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(123, 'Michael Backpackers', '', 'sg_65_98192203', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(124, 'Freshmart Singapore Pte Ltd', '', 'sg_65_96195560', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(125, 'KOYO-SHA Co., Ltd', 'KOYO-SHA Bldg., No 48-5\r\rHigashi Nippori 5-chome\r\rArakawa-ku \r\rTokyo 116-0014\r\rJAPAN \r\rAttn :  Mr Kosuke Miura\r\rTel  :  (03)38058375\r\rFax  :  (03)38058369\r\rkosuke-miura@koyo-sha.co.jp', 'sg_65_8030857441', '', 'sg_65_338058369', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(126, 'SEF Interior(s) Pte Ltd', '152 Ubi Ave 4 #03-01 Singapore 408826', 'sg_65_91796752', '', 'sg_65_62883688', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(127, 'Lab Store Ptre Ltd', '8A Admiralty Street \r\r#02-03\r\rFood Xchange @ Admiralty\r\rSingapore 757437\r\rAttn  :  Mr Derrick Tan', 'sg_65_91072629', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(128, 'SCS Singapore Pte. Ltd.', '2 Tuas South St 2, #02-01 , Singapore 637895', 'sg_65_68628687', '', 'sg_65_68614958', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 8, 1477056797, 1477368255, 1),
(129, 'La Pizzaiola Pte Ltd', '15/3 Jalan Riang\r\rSpore 358987\r\rAttn : Mr Rajah\r\rMobile : 9147 9485\r\rEmail : gendrajah@yahoo.com', 'sg_65_91479485', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(130, 'Accolade Solutions', '', 'sg_65_9746 1531', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(131, 'Upside Motion', '100 Turf Club Road\r\r#01-02L/X\r\rSingapore 287992\r\rAttn :  Ms Hwee Sze\r\rHP   :  97985469\r\remail :  sze@upsidemotion.com', 'sg_65_97985469', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(132, 'SINOCard Technologies Pte Ltd', '111 North Bridge Road\r\r#27-01/02 Peninsula Plaza\r\rSingapore 179098\r\rTel / Fax :  62537851\r\rsales@sincocard-technologies.com', 'sg_65_90184356', '', 'sg_65_62537851', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(133, 'Personal_Leow Siew Kee', '', 'sg_65_90698118', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(134, 'Bishan Home for the Intellectually Disabled', '6 Bishan Street 13\r\rSingapore 579 798\r\rAttn :  Mr Edmund Mak \r\redmund_mak@bishanhome.org.sg', 'sg_65_91503658', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(135, 'Aura Indonesian Massage Pte Ltd', '17 Anamalai Avenue \r\rAttn :  Ms Gee \r\rHP :  98504661', 'sg_65_98504661', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(136, 'Jubilee Church', '', 'sg_65_98231466', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(137, 'El-Incor Design', 'YS-One, 1 Yishun St 23, \r\r#05-42\r\rSingapore 768441\r\rAttn   Mr Bernard Lee \r\rDesign Consultant \r\rM  : +65 9625 0976 \r\rT  : +65 6257 3751 \r\rF  : +65 6257 3917\r\rEmail: bernard.lee@el-incor.com', 'sg_65_96250976', '', 'sg_65_62573917', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(138, 'Smart Kleen Corporation', '21,Jalan Puteri 5/7,\r\rBandar Puteri 47100 Puchong, Selangor.\r\rAttn: Mr Chester Au Yong\r\rTel: 012-653 6323', 'sg_65_012 653 6323', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(139, 'Amaris-Bugis Singapore', '21 Middle Road\r\rSingapore 188931 \r\rPhone +65 6337 7100\r\rMobile: +65 9697 4834\r\rrozlansamadi@amarishotel.com', 'sg_65_9697 4834', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(140, 'Aldrin', '', 'sg_65_82988693', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(141, 'Green Kiwi Pte Ltd', '280a Lavender Street \r\rSingapore 338800\r\rAttn :  Mr Kevin Hynds\r\rHP   :  96889255\r\remail :   owner@greenkiwi.com.sg', 'sg_65_96889255', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(142, 'Big Hotel (Singapore) Pte Ltd', '200 Middle Road\r\r#B1-01\r\rSingapore 188980', 'sg_65_98441414', '', 'sg_65_6349 5909', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(143, 'Paramount Bed Asia Pacific Pte Ltd', '50 Raffles Place #22-03\r\rSingapore Land Tower \r\rSingapore 048623\r\rAttn :  Mr Yusuke Miyoshi\r\rTel  :  62209750\r\rmiyoshi@paramount.com.sg', 'sg_65_97232223', '', 'sg_65_62209571', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(144, 'Takasago International ( Singapore ) Pte Ltd', '', 'sg_65_91734733', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(145, 'Space Scope Pte Ltd', '112A Tanjong Pagar Road\r\rS088528', 'sg_65_9848 4663', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(146, 'Olivine Capital Pte Ltd', '180 Rangoon Road\r\rSingapore 218442\r\rAttn :  Mr Steven Ong \r\rTel  :  63965522\r\rFax  :stevenong@cherryloft.com', 'sg_65_96158800', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(147, 'Takasago International ( Singapore ) Pte  Ltd', '25 Sunview Road\r\r\r\rAttn  :  Mr John SC Ho\r\rSenior Supply Chain Manager\r\rHP :   91734733\r\remail :   john_ho@takasago.com', 'sg_65_91734733', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(148, 'The Black Swan', '19 Cecil Street\r\r#01-01\r\rSingapore 049704\r\r\r\rAttn   :  Ms Nurashykin Jamil\r\rnura@tanjongbeachclub.com\r\rhp     :  90660753', 'sg_65_90660753', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(149, 'Prima Ferries Pte Ltd', '1 Maritime Square (Lobby D)\r\rHarbourfront Centre\r\r#03-60\r\rSingapore 099253', 'sg_65_98455992', '', 'sg_65_6276 6511', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(150, 'Green River Energy Sdn Bhd', '111 Lrg Perindustrian Bkt Minyak 11\r\rTmn Perindustrian Bkt Minyak\r\r14000 Bkt Mertajam\r\rPenang\r\rAttn: Mr Chew Shiu Wei\r\rTel:012-4290060', 'sg_65_124290060', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(151, 'Omnisense Systems', '18 Kaki Bukit Road 3\r\r#04-16 Entrepreneur Business Centre\r\rSingapore 415978\r\r\r\rTel  :  6844 3191\r\rFax  :  6844 3190\r\rHP   :  9115 7924\r\rck.teng@omnisense-systems.com', 'sg_65_91157924', '', 'sg_65_6844 3190', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(152, 'Radiance Physiofit Pte Ltd', '6 Temasek Boulevard \r\r#43-01 Suntec Tower 4\r\rSingapore 038986\r\r\r\rAttn :   Mr En / Director\r\rHP   :   94753168\r\ren@radiancephysiofit.com.sg', 'sg_65_94753168', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(153, 'Royal Lake Club', 'Taman Tasek Perdana\r\rJalan Cenderamulia\r\rOff Jalan Parlimen\r\rAttn: Mr Alex Loke\r\rTel: 03 2698 7878', 'sg_65_03 2698 9889', '', 'sg_65_03 2698 9889', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(154, 'St Andrew Community Hospital', '', 'sg_65_90280576', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(155, 'Serangoon Gardens Country Club', '', 'sg_65_9847 4509', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(156, 'One Paradise Pte Ltd', '', 'sg_65_8522 5638', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(157, 'Green Apple Foot Spa', '', 'sg_65_9328 8777', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(158, 'Kolej Perkembangan Awal Kanak-Kanak', 'Ground Floor, Podium Block,\r\rSeason Square,\r\rNo.1, Jalan PJU 10/3C,\r\rDamansara Damai,\r\r47830 Petaling Jaya,\r\rSelangor Darul Ehsan\r\rAttn: Ms Izwain binti Mohd Zaid\r\rTel: 03 6156 0230 ( ext: 110 )', 'sg_65_03 6156 0230', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(159, 'SMM Pte Ltd', '', 'sg_65_97862884', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(160, 'Pico Art International Pte Ltd', 'Pico Creative Centre\r\r20 Kallang Avenue\r\rSingapore 339411\r\rAttn :  Ms Jessie Woo \r\rDID  :  6290 5612\r\rHP   :  9239 5291\r\rFax  :  6290 5764\r\rjessie.woo@sg.pico.com', 'sg_65_9239 5291', '', 'sg_65_6290 5764', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(161, 'Immigration & Checkpoints Authority', 'Attn  :  Mr Jack Aw Yong Kheng Soon\r\rBuilding Section\r\rDD: (65) 6391 6046 \r\rHP (65) 9138 7958\r\rFax: (65) 6298 0837\r\rAw_Yong_Kheng_Soon@ica.gov.sg', 'sg_65_9138 7958', '', 'sg_65_6298 0837', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(162, 'C.P.Vietnam Corporation', '', 'sg_65_1639888482', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(163, 'Singapore Health Services Pte Ltd', '168 Jalan Bukit Merah\r\r#16-01 Surbana One\r\rS(150168)\r\rAttn : Mr Daniel Wong Emergency Preparedness\r\rDID  : 6377-7598\r\rFax  : 6377-2504  daniel.wong.m.t@singhealth.com.sg', 'sg_65_96788390', '', 'sg_65_63772504', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(164, 'Wolero Pte Ltd.', '65 Airport Boulevard\r\rChangi Airport Teminal 3, \r\r#B1-18\r\rSingapore 819663\r\r\r\rAttn    :   Ms Agnes Kua \r\rHP      :   97927200\r\remail   ;   agnes@wolero.com', 'sg_65_9792 7200', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(165, 'OP3 International Pte Ltd', '', 'sg_65_9171 9492', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(166, 'Tanglin Trust School Ltd', '95 Portsdown Road\r\rSingapore 139299', 'sg_65_85338780', '', 'sg_65_67703121', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(167, 'F1 Recreation Gym Management Pte Ltd', '20 Sin Ming Lane, #01-57/58/59 \r\rMidview City, Singapore 573968', 'sg_65_9697 5722', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(168, 'Rubix Interior', '', 'sg_65_90694928', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(169, 'Leica Instruments Singapore Pte Ltd', '12 Teban Gardens Crescent\r\rSingapore 608924\r\r\r\rAttn : Mr Azman\r\rHp   : 9846 2341\r\rEmail: azman.hassan@leica-microsystems.com', 'sg_65_98462341', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(170, 'Osvaldo Family''s Kitchen Pte Ltd', '', 'sg_65_9297 4151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(171, 'Methods Pte Ltd', '5001, Beach Road\r\r#08-56,\r\rGolden Mile Complex\r\rSingapore 199588\r\rHP  : 92956996\r\rFax : 62197882 \r\r\r\r\r\r\r\rBR,\r\r\r\rMatt Ng\r\rMethods Pte Ltd\r\rH: 9295 6996\r\rF: 6219 7882', 'sg_65_92956996', '', 'sg_65_62197882', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(172, 'Fisher Hostel Pte Ltd', '127 Tyrwhitt Road\r\rSingapore 207551\r\r\r\rAttn :  Mr James Ang \r\rHP   :  98152780\r\rcontact@fisherhostel.com', 'sg_65_98152780', '', 'sg_65_62978835', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(173, 'TyreQueen Pte Ltd', '61 Ubi Avenue 2 #-07-01\r\rAuotmobile Megamart\r\rSingapore 408898\r\r\r\rAttn :  Mr Darren Tan\r\rContact: 81331123\r\rdarren.lifeunited@gmail.com', 'sg_65_81331123', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(174, 'Garuda Maintenance Facility Aero Asia, PT', '', 'sg_65_98232143', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(175, 'Up Dog Studio', '', 'sg_65_98890782', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(176, 'heng Yew Trading Pte Ltd', '234 Balestier Road \r\rSingapore 329698\r\rAttn : Mr Tan \r\rTel  : 6252 1026 / 62526622\r\rFax  : 6253 7911\r\rHP   :  93820893', 'sg_65_93820893', '', 'sg_65_62537911', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(177, 'Kheng Yew Trading Pte Ltd', '234 Balestier Road \r\rSingapore 329698\r\rAttn :  Mr Tan \r\rTel  :  6252 1026/6252 6622\r\rFax  :  6253 7911\r\rhp   :  93820893\r\rsales@khengyew.com.sg', 'sg_65_93820893', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(178, 'Tung Lok Catering', '20 Bukit Batok Crescent \r\r#11-05/06/07\r\rEnterprise Centre\r\rSingapore 658080\r\rAttn :  Ms Vivien Hooi \r\rHP   :  97338160\r\rvivienhooi@tunglok.com', 'sg_65_97338160', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(179, 'PT Garuda Indonesia ( Persero) Tbk', 'Singapore Changi Airport\r\rTerminal 3\r\r65 Airport Boulevard #01-10\r\rSingapore 819663\r\rAttn :  Mr Gani Wahyudin \r\rHP   :   97580506\r\rgani@garudaindonesia.com.sg', 'sg_65_97580506', '', 'sg_65_65420307', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(180, 'Orchard KTV', 'Attn : Mr Melvin Lee\r\rHP   : 9455 4933\r\rFax  : 6733 9477\r\rEmail: melvinlsk@gmail.com', 'sg_65_94554933', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(181, 'Media Corp', '', 'sg_65_91693323', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(182, 'Applied Teamwork Pte Ltd', '', 'sg_65_9325 0144', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(183, 'Magnificent Seven Corporation Pte Ltd', '462 Tagore Industrial Avenue . Singapore 787831\r\r\r\rAttn  : Mr Patrick Tan\r\rProject Manager \r\rTel   : 64542388 .  .\r\rHP    : 98732297', 'sg_65_98732297', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(184, 'The Interior Library', '', 'sg_65_91394793', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(185, 'Nan Hua High School', '41 Clementi Avenue 1\r\rSingapore 129956\r\rAttn :  Ms Meggy Oon \r\rYel  :  68731734\r\rHP   :  91754627\r\roon_lay_hong_meggy@moe.edu.sg', 'sg_65_91754627', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(186, 'Build Five Pte Ltd', '', 'sg_65_92702178', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(187, 'National Heart Centre', '', 'sg_65_96620307', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(188, 'sky pilates pte ltd', '541 orchard road\r\r#05-03 liat towers\r\rsingapore 238881\r\rAttn  :   Ms Kimberly Oh \r\rStudio Manager\r\rtel  :   61007597\r\rhp   :   98336670\r\rfax  :   62350515\r\rkimberly@skypilates.com.sg', 'sg_65_98336670', '', 'sg_65_62350515', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(189, 'Melvin Wong', '', 'sg_65_9828 3198', '', 'sg_65_6278 2759', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(190, 'Pacific Radiance Limited', '6 Temasek Boulevard\r\r#43-01 Suntec Tower Four\r\rSingapore 038 986\r\rAttn :  Mr Melvin Wong\r\rSnr Admin/ Facilities Executive\r\rTel  :   238 8881\r\rHP   :   9828 3198\r\rDID  :   6597 5930\r\rFax  :   6278 2759\r\rmelvin.wong@pacificradiance.com', 'sg_65_9828 3198', '', 'sg_65_6278 2759', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(191, 'Sunset Bay Group Pte Ltd', '590 Toa Payoh East\r\rSingapore ( 319134 )\r\rAttn : Russell \r\rhp   : 91809452\r\rrussell.headech@gmail.com', 'sg_65_91809452', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(192, 'PUJANGGA KREATIF SDN BHD', 'No 89-2, Jalan 3/76D, Desa Pandan, 55100 Kuala Lumpur\r\rAttn: Mr Ahmad Farid Bin Naupa\r\rTel: 0123302784', 'sg_65_123302784', '', 'sg_65_392846703', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(193, 'Life Technologies Holdings Pte Ltd', 'Blk 33 Marsiling Industrial Estate Road 3\r\r#07-06\r\rSingapore 73925\r\rAttn :  Mr Ang Kok Kiong \r\rSenior Manager \r\rkokkiong.ang@lifetech.com', 'sg_65_97300391', '', 'sg_65_63688182', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(194, 'Hyper Island', 'Hyper Island\r\r\r\rAttn :  Ms Zainon Samsudin\r\rStudent Administration Manager, Singapore\r\rHP  : 91143746\r\rzainon.samsudin@hyperisland.com', 'sg_65_91143746', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(195, 'Urban Office Concept Pte Ltd', 'Blk. 1023 Yishun Ind Park A\r\r#02-07\r\rSingapore 768762\r\r\r\rAttn :   Mr Harris K Lee\r\rTel  :   67559055\r\rharris@urbanoc.com.sg', 'sg_65_83067959', '', 'sg_65_67559074', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(196, 'Naumi Hotel', '', 'sg_65_9750 7815', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(197, 'Libraco Services (S) Pte Ltd', 'Sanjeev \r\rTel  : 67784651\r\rFax  : 67790954\r\rhp   : 92227725\r\rsanjeev.ramk@gmail.com', 'sg_65_92227725', '', 'sg_65_67790954', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(198, 'Anthony Ma', '', 'sg_65_8388 7789', '', 'sg_65_6746 0092', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(199, 'L&RUI Concept Group Pte Ltd', '457 Macpherson Road \r\rLevel 7 L&RUI d’ gallery \r\rSingapore 368175\r\r\r\rtel  : 6538 3738 (ext. 717)  fax  : 6538 3731 \r\rHP   : 9673 2448    \r\rceline@lruid.com', 'sg_65_9673 2448', '', 'sg_65_6538 3731', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(200, 'NUS Students'' Arts and Social Sciences Club', '', 'sg_65_9641 3199', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056797, 0, 1),
(201, 'Focus Action Sdn Bhd', 'No 6 Jalan SS3/31,\r\rUniversity Garden,\r\r47300 Petaling Jaya,\r\rSelangor Darul Ehsan,\r\rMalaysia.\r\rAttn  :  Mr See Ching Peng\r\rManaging Director \r\rTel   :  +603-78777070\r\rFax   :  +603-78738636\r\rsee@drlocker.com', 'sg_65_60123956969', '', 'sg_65_60378738636', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(202, 'Yuen Jing Construction Pte Ltd', 'Blk 157 Bedok South Avenue 3\r\r#13-575\r\rSingapore 460157\r\r\r\rAttn : Mr Lim Teck Chuan \r\rHP   : 96953383\r\rlim@yuenjing.com.sg', 'sg_65_96953383', '', 'sg_65_62436270', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(203, 'Hong Jun Construction Pte Ltd', '1 Bukit Batok Crescent\r\rWCEGA Plaza #06-05\r\rSingapore 658064\r\r\r\rAttn :  Ms Jane Wong\r\rProject Manager\r\rHP    : 96821827\r\rTel   : 66597933\r\rFax   : 66597055', 'sg_65_96821827', '', 'sg_65_66597055', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(204, 'Khoo Teck Puat Hospital', '90 Yishun Central\r\rSingapore 768828\r\rhp : 97576247\r\rshastrie.ismie.eideal@alexandrahealth.com.sg', 'sg_65_97576247', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(205, 'Kali Majapahit International', '', 'sg_65_9638 4747', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(206, 'China Classic Pte Ltd', '', 'sg_65_82994093', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(207, 'TY Contracts & Services', 'No.  65 Sims Avenue \r\r#05-03\r\rYi Xiu Factory Building \r\rSingapore 387418\r\rAttn  :  Mr Tony Yeo \r\rHP    :   96818587', 'sg_65_96818587', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(208, 'Meggitt Aerospace Asia Pacific Pte Ltd', '', 'sg_65_9652 6512', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(209, 'ST Logistics Pte Ltd', '5 Clementi Loop \r\rSingapore 129813\r\r\r\rAttn :  Mr Patrick Tan \r\rVice President / Business Development \r\rpatrick.tan@stlogs.com', 'sg_65_86888133', '', 'sg_65_64697582', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(210, 'ST Engineering and Technology Solutions Pte Ltd', '5 Clementi Loop \r\rSingapore 129816\r\r\r\rAttn :  Mr Sezhiyan\r\rProject Manager\r\rTel  :   64628795\r\rFax  :  64628851\r\rHP   :  91865642', 'sg_65_91865642', '', 'sg_65_64628851', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(211, 'Heraeus Materials Singapore Pte Ltd', 'Blk 5002 Ang Mo Kio Avenue 5\r\r#05-10 TECHplace II\r\rSingapore 569871', 'sg_65_81337642', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(212, 'Singtel', '', 'sg_65_9826 1922', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(213, 'TIDC Pte Ltd', '75 Duxton Road #02-01\r\rSingapore 089534', 'sg_65_98509659', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(214, 'Eastern Harvest Foods Singapore Pte Ltd', '', 'sg_65_9138 4040', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(215, 'Personal_Irene', '', 'sg_65_96809090', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(216, 'Atrium Interior Pte Ltd', '', 'sg_65_9678 2491', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(217, 'St Andrew''s Community Hospital', '8 Simei Street 3\r\rSingapore 529895\r\rAttn :  Ms Ong Seok Peng \r\rProcurement\r\rTel 65861023\r\rseokpeng_ong@sach.org.sg', 'sg_65_96523759', '', 'sg_65_65861100', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(218, 'ST Electronics (Training & Simulation System) Pte Ltd', '10 Ang Mo Kio Street 65\r\nTechpoint #04-18\r\nSingapore 569059\r\n\r\n\r\n\r\nSingapore 569059', 'sg_65_97626165', 'chiong.hocksoon@stee.stengg.com', 'sg_65_64812674', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', 'www.stee.stengg.com', '', 1, 11, 1477056798, 1477306674, 1),
(219, 'Edmund Yeo', '', 'sg_65_91197737', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(220, 'Imperial Treasure Restaurant Group Pte Ltd', 'No. 36 Sin Ming Lane, Midview City, Singapore 573956', 'sg_65_91712978', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(221, 'PURNAMA BERSATU SDN BHD', 'No. 11, Kaw Perindustrian Kecil Soi,\r\r25150 Kuantan,\r\rPahang \r\rAttn: Mr Ahmad\r\rTel: 0199508412 / 09 5342382', 'sg_65_199508412', '', 'sg_65_09 534 2384', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(222, 'Vivafit 67 Beach Road', '67 Beach Road, Bulkhaul #0202', 'sg_65_9452 5167', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(223, 'Victory Fitness', 'A-02-08 & A-03-08 \r\rStarParc Point, Jalan Taman Ibukota\r\rSetapak, 53300 KL\r\rAttn: Ms Julie\r\rTel: 012 7418566', 'sg_65_127418566', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(224, 'Crab in da bag', '', 'sg_65_9796 6078', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(225, 'DENE N TEAM SDN BHD', '62, Level 1, Jalan TPP 1/19,\r\rTaman Perindustrian Puchong, Batu 12, Jalan Puchong, 47100 Puchong, Selangor\r\rAttn: Ms Natalia Lai \r\rTel: 012 2074607 / 03 8060 1166', 'sg_65_012 2074607', '', 'sg_65_03 8060 0222', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(226, 'TETRIX (KLANG ) SDN BHD', '302 Ground Floor, Jalan Batu Unjur 7, Taman Bayu Perdana, 41200 Klang, Selangor\r\rAttn: Mr Alan Phoon\r\rTel: 019 3320988/ 03 33230988', 'sg_65_019 3320988', '', 'sg_65_03 33234988', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(227, 'National Equestrian Centre', 'National Equestrian Park \r\r100 Jalan Mashhor\r\rSingapore 299177\r\r\r\rAttn : Ms Titien Irvianty   (Administrative Manager)', 'sg_65_9753 8739', '', 'sg_65_6256 6709', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(228, 'Gucci Singapore Pte Ltd', '390 Orchard Road\r\r#07-01 Palais Renaissance\r\rSingapore 238 871', 'sg_65_82232580', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(229, 'Personal_Serena', '', 'sg_65_93669889', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(230, 'Club Cin Cin', '', 'sg_65_97622696', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(231, 'SAMADHI RETREATS', 'Suite 6.01B, Level 6 (South Block), The AmpWalk,\r\r218 Jalan Ampang, 50450 Kuala Lumpur\r\rAttn: Colin Tan\r\rTel: 012 791 0228 / 0321660601', 'sg_65_012 791 0228', '', 'sg_65_321660604', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(232, 'Automatic Locker (Thailand) Co.,Ltd.', 'Automatic Locker (Thailand) Co.,Ltd.\r\r9 Soi Narathiwasrajanakarin 26,\r\rChongnonsi, Yannawa, Bangkok 10120 Thailand\r\rAttn : Mr Thavatchai \r\rTel. +6626811811\r\rFax. +6626811771\r\rMobile: +66870001007\r\rmuseum@asianet.co.th', 'sg_65_66870001007', '', 'sg_65_6626811771', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(233, 'Mahkota Technologies Sdn Bhd', 'Attn : Mr. Khairuddin', 'sg_65_019 214 3636', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(234, 'Perform Industries Pet Ltd', 'Blk 5 Upper Aljunied Lane\r\r#01-26\r\rSingapore 360005\r\rAttn :  Mr Peh Teck Kian \r\rManaging Director \r\rhp   :  97368683\r\rpeh118@performindustries.com', 'sg_65_97368683', '', 'sg_65_62863435', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(235, 'S.Rajaratnam School of International Studies', 'Nanyang Technological University\r\rNanyang Avenue, Blk, S4, Level B4\r\rSingapore 639798', 'sg_65_94382177', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(236, 'Urban Habitat Design Pte Ltd', '', 'sg_65_97497232', '', 'sg_65_62201776', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(237, 'APLHA ENERGY', 'No A-20-1, Lagoonview Condo, Jalan Universiti, 46150 Bandar Sunway, Selangor\r\rAttn: Ms Nurul Mahirah\r\rTel: 017 2580 087', 'sg_65_017 2580 087', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(238, 'ALPHA ENERGY', 'No A-20-1, Lagoonview Condo, Jalan Universiti, 46510 Bandar Sunway, Selangor\r\rAttn: Ms Nurul Mahirah\r\rTel: 017 2580 087', 'sg_65_017 2580 087', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(239, 'Vancon LLP', '111 North Bridge Road\r\r#05-32\r\rPeninsula Plaza\r\rSingapore 179 098', 'sg_65_9487 8724', '', 'sg_65_6333 4636', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(240, 'City Harvest Church', '8 Temasek Boulevard 3\r\r#08-01\r\rSingapore 038988\r\rAttn  :  Ms Zhang Ting Ting hp    :  92336641\r\remail :  zhangtingting@chc.org.sg', 'sg_65_92336641', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(241, 'Ken', '', 'sg_65_92971991', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(242, 'Ecopave Pte Ltd', '10 Woodlands Ind Park E3\r\rSingapore 757788\r\rAttn : Mr Lim Sak \r\rHP   : 90616677\r\rlimjay77@singnet.com.sg', 'sg_65_90616677', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(243, 'Bluprint.id Pte Ltd', '122 Eunos Avenue 7\r\r#03-04\r\rRichfield Industrial Centre\r\rSingapore 409575\r\rAttn :  Mr Ken\r\rHP   :  92971991\r\rken@bluprinid.com', 'sg_65_92971991', '', 'sg_65_66364258', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(244, 'Heraeus Materials Malaysia Sdn Bhd', 'NO. 6 Jalan I-Park 1/1\r\rKawasan Perindustrian I-park\r\r81000 Bandar Indahpura\r\rKulaijaya', 'sg_65_197615560', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(245, 'Revogym Sdn Bhd', '2nd Floor, No 2-2, Jalan Tasik Utama 7, Medan Tasik Niaga Lake Field, Sg Besi, Kuala Lumpur\r\rAttn: Mr Safwan Alwi\r\rTel: 0172339034', 'sg_65_172339034', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(246, 'Impiana KLCC Hotel', 'Impiana KLCC Hotel\r\r13 Jalan Pinang,\r\r50450 Kuala Lumpur\r\rAttn: Ms Alice Foo\r\rTel: 03 21471022\r\rFax: 03 21451303', 'sg_65_03 21471022', '', 'sg_65_03 21451303', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(247, 'Hilti Pte Ltd', '', 'sg_65_9887 6813', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(248, 'Sree Ramar Temple', '', 'sg_65_9159 4779', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(249, 'Ademco ( Fr East ) Pte Ltd', '315 Outram Road\r\r#08-03 Tan Boon Liat Building \r\rSingapore 169074\r\rAttn :  Mr Xavier Joseph/ Mr Eddie Koh \r\rDID  :  63053036\r\rFax  :  62241221\r\rXavier.joseph@ademcosecurity.com\r\reddie.koh@ademcosecurity.com', 'sg_65_91852934', '', 'sg_65_62241221', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(250, 'Asian Blending Pte Ltd', '', 'sg_65_9066 5260', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(251, 'Da Lian Gourmet Pte Ltd', 'No 9 Geylang Lorong 21A\r\r#09-00\r\rSingapore 388428\r\r\r\rAttn :  Mr Gerald Ng \r\rHP   :  96411011\r\rDalian.traditionalnoodles@gmail.com', 'sg_65_96411011', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(252, 'Soju Kuala Lumpur', 'Lot GF001, 001A &001B, Ground Floor, Federal Arcade, Annex Federal Hotel, 55100, Jalan Bukit Bintang, Kuala Lumpur\r\rAttn: Mr Robert Raymond\r\rTel: 014 7315600', 'sg_65_147315600', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(253, 'St Andrew Secondary School', '15 Francis Thomas Drive\r\rSingapore 359342\r\rAttn :  Mrs Carol Yap\r\rHP   :  96391979\r\rchua_oon_cheng_carol@moe.edu.sg', 'sg_65_96391979', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(254, 'SingGas ( LPG) Pte Ltd', 'No 31 Defu Lane 9\r\rSingapore 539271\r\r\r\rAttn :  Ms Siti Noraini Omar\r\rCustomer Service Executive\r\rsiti-noraini.omar@singgas.com.sg', 'sg_65_96937744', '', 'sg_65_63820350', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(255, 'Blue cross Thong Kheng Home', 'No 201 Jurong East Avenue 1\r\rSingapore 609791\r\rTel  :  6560 2022\r\rFax  :  6560 2797\r\rHP   :  90080828\r\rMr Steven Tan steventan@thongkheng.org.sg', 'sg_65_90080828', '', 'sg_65_65602797', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(256, 'Kudeta Sg Pte Ltd', 'Marina Bay Sands Skypark\r\rSkyPark at Marina Bay Sands North Tower\r\r1 Bayfront Avenue\r\rSingapore 018971\r\r\r\rAttn : Ms Abby', 'sg_65_9001 22530', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(257, 'Ace Design and Contract Pte Ltd', '10 Anson Road #10-06 International Plaze\r\rSingapore 079903\r\r\r\rAttn  :  Mr Shane Lo\r\rHP    :  84826443\r\rshanelo1981@gmail.com', 'sg_65_84826443', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(258, 'Microtac Technology Pte Ltd.', '71 Ubi Crescent, \r\rExcalibur centre, \r\r#01-01/02\r\rSingapore 408571. \r\rTel : 62977713\r\rFax : 62977793\r\rsk.teh@microtac.com.sg', 'sg_65_96942731', '', 'sg_65_62977793', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(259, 'Alpha One Lockers Pte Ltd', '1 Bukit Batok Crescent #06-55 Wcega Plaza Singapore 658064', 'sg_65_9730 5289', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(260, 'Rakanda Enterprise Sdn Bhd', 'No. 18, 20, 22, 22A-G, Jalan Semenyih Sentral 6, Taman Semenyih Sentral, 43500, Semenyih, Selangor\r\rAttn: Ms The\r\rTel: 03 8723 2799', 'sg_65_03 8723 2799', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056798, 0, 1),
(261, 'Hou Hua Development Co.Sdn Bhd', 'No 90-A, Jalan Watson, 42000 Port Klang, Selangor\r\rAttn: Mr Patrick Teoh\r\rTel: 019 317 0672', 'sg_65_019 317 0672', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(262, 'KS Distribution ( Malaysia ) Sdn Bhd', 'Suite 3A-3A, Block L, \r\rNo.2 Jalan Solaris,\r\rSolaris Mont’ Kiara,\r\r50480 Kuala Lumpur\r\rAttn: Mr Nurrahmad Shah\r\rTel: 03 6203 0080', 'sg_65_03 6203 0080', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(263, 'Forvis Construction & Engineering Pte Ltd', '', 'sg_65_97936159', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(264, 'Singapore National Eye Centre', '11 Third Hospital Avenue, Singapore 168751\r\r\r\rAttn : Mr Patrick Ng', 'sg_65_81218554', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(265, 'CASH', 'Deliver to :  \r\rMelrose Home \r\r503 Clementi Road \r\rSingapore 599488\r\rAttn :  Mr Gabriel \r\rHp   :  96641668', 'sg_65_96641668', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(266, 'FCS SUPPLY', 'FCS SUPPLY\r\rNo 3/225, Jalan Udang Galah, Taman Sri Segambut, 52000 Kuala Lumpur\r\rAttn: Mr Azly\r\rTel: 012 942 0286', 'sg_65_012 942 0286', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(267, 'Chameleon Events Pte Ltd', '1 Nanson Rd, #03-11 Gallery Hotel, Singapore 238909', 'sg_65_97770710', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(268, 'Macwinn Techno Singapore', '53 Ubi Avenue 1 #05-01\r\rPaya Ubi Industrial Park\r\rSingapore 408934\r\r\r\rAttn : Mr Vincent Neo', 'sg_65_96869156', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(269, 'UFIT Pte Ltd', '87-88 Amoy Street \r\rSingapore 069907', 'sg_65_81235069', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(270, 'Assumption Pathway School', '30 Cashew Road Singapore 679697', 'sg_65_90762823', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(271, 'Club Infinitude Pte Ltd', '407 Havelock Road\r\r#03-00 Furama Riverfront \r\r( Annex Block )\r\rSingapore 169634\r\r\r\rAttn : Mr David Chia/Mr Raymond \r\rdavickchia@yahoo.com.sg', 'sg_65_90675738', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(272, 'Thomas Wang', '', 'sg_65_98540101', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(273, 'RSS Endurance', '', 'sg_65_93494507', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(274, 'BSO Pte Ltd', 'Attn : Mr Ivan Loh\r\r\r\r153B Rochor Road, Bugis Village\r\rSingapore 188428', 'sg_65_9655 7855', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(275, 'Asia Furniture Co Pte Ltd', 'Blk 1767 Geylang Bahru \r\r#01-05\r\rKallang Distripark \r\rSingapore 339702\r\rAttn :  Ms Nancy Soh \r\rTel  :  67473366\r\rFax  :  67420186\r\rnancy.soh@afc.com.sg', 'sg_65_91385333', '', 'sg_65_67420186', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(276, 'HELP International School', 'Annexe Office, BZ-2, Pusat Bandar Damansara, 50490 Kuala Lumpur\r\rAttn: Ms Patricia\r\rTel: 03 2711 2000 Ext: 1738', 'sg_65_173908351', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(277, 'SHINJUSHIRO TRADING & SERVICES', 'No 38, Jalan Aman Perdana 1D/KU5, Taman Aman Perdana, 40150 Klang, Selangor\r\rAttn: Mr Alvin Ng\r\rTel: 012 660 2277', 'sg_65_012 660 2277', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(278, 'The Cake Shop Pte Ltd', '', 'sg_65_8268 4868', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(279, 'Union Services (Singapore) Pte Ltd', '', 'sg_65_91571182', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(280, 'RSH (Singapore) Pte Ltd', '190 Macpherson Road	\r\rWisma Gulab Level 2	\r\rSingapore 348548		\r\rAttn :  Mr William Oh	\r\rMobile : 8699 5340	\r\remail : williamoh@rsh.com.sg', 'sg_65_8699 5340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(281, 'Anglican High School', '600 Upper Changi Road\r\rSingapore 487012', 'sg_65_98214730', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(282, 'Ramanpreet Singh', '', 'sg_65_93494507', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(283, 'Eutex International Group', '20 Pioneer Crescent\r\r#08-01 to #08-05\r\rWest Park BizCentral\r\rSingapore 628555\r\rAttn   :  Ms Yvonne loh \r\rOffice : +65.6871.4300\r\rMobile : +65.9450.2927\r\rFax    :  +65.6684.1605', 'sg_65_94502927', '', 'sg_65_66841605', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(284, 'Canberra Secondary School', '', 'sg_65_9733 7309', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(285, 'LCH Paper Marketing', '8A, Jalan Lengkongan Brunei Off Jalan Pudu 55100 KL', 'sg_65_012 654 1659', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(286, 'Nitro Entertainment Pte Ltd', '114 Middle Road \r\rLee Kai House \r\r#4-00\r\rSingapore 188971\r\rAttn :  Mr Andrew \r\rHP   :  90904449', 'sg_65_90904449', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(287, 'R E & S Enterprises Pte Ltd', '32 Tai Seng Street,\r\r#07-01 RE&S Building, \r\rSingapore 533972\r\r\r\rAttn : Mr Chee Hwee', 'sg_65_9737 6993', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(288, 'Ngai Chin Construction Pte Ltd', '59 Sungei Kadut Loop \r\rSingapore 729490\r\rAttn :  Mr Leong Wai Kit\r\rD: 6389 9887\r\rF: 6389 9889\r\rM: 9003 6660\r\rleongwaikit@ngaichin.com.sg', 'sg_65_90036660', '', 'sg_65_6389 9889', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(289, 'Acme Monaco Asia Pte Ltd', '', 'sg_65_96932023', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(290, 'Blue Lotus Restaurant Pte Ltd', 'Blue Lotus Chinese Eating House\r\r31 Ocean Way \r\r#01-13 Quayside Isle \r\rSingapore 098375\r\rAttn :   Ms Chloe Chan\r\rHP   :   97299370\r\rchloe@bluelotus.com.sg', 'sg_65_97299370', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(291, 'Skylace Learning Hub Pte Ltd', 'Blk 4 Toa Payoh Lorong 7\r\r#01-125 Singapore 310004', 'sg_65_96868112', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(292, 'Sansui', '', 'sg_65_9737 0711', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(293, 'Personal_Ms Cheong Wai Yin', '18 Sundridge Park Road\r\rS358149\r\r\r\rAttn : Ms Cheong Wai Yin\r\rHP : 9677 1001\r\rEmail : waiyin7@gmail.com', 'sg_65_9677 1001', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(294, 'Sigma 3 Pte Ltd', '', 'sg_65_94569940', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(295, 'Omni Aquatic Supplies Pte Ltd', '147 Pandan Loop Singapore 128347\r\r\r\rAttn : Ms Xueqi', 'sg_65_8268 4868', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(296, 'Crown Construction Pte Ltd', '55 Loyang Wy \r\rSingapore 508748\r\r\r\rAttn :  Mr Bernard Ho \r\rHP   :  81909860\r\rBernard@crownconstruction.com.sg', 'sg_65_81909860', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(297, 'KK''s Baggage Store Service', 'Wawasan Plaza,\r\rWisma San Hin,\r\rBlock 2-A\r\rKota Kinabalu \r\rPoscode 89000\r\rSabah\r\rAttn: Mr Ray Ibrahim\r\rTel: 0105968383', 'sg_65_105968383', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(298, 'Compass Hospitality', 'Compass Hospility\r\r\r\rAttn : Mr. Wong', 'sg_65_126899190', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(299, 'Advance Kapt Services Pte Ltd', 'No 24 Jalan Tukang \r\rJurong Industrial Estate\r\rSingapore 619258\r\r\r\rAttn :  Mr Terence Ang \r\rTel  :  62650183\r\rhp   :  91853223\r\radvancekapt@hotmail.com', 'sg_65_91853223', '', 'sg_65_62650183', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(300, 'BIG THREE TRADING', '7-G, Jalan Puteri 2/2C, Bandar Puteri 47100 Puchong,Selangor\r\rAttn: Mr Low Shiann Yuan\r\rTel: 012 2202025', 'sg_65_122202025', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(301, 'G Peak Engineering Pte. Ltd', '6001 Beach Road\r\r#01-20, Golden Mile Tower\r\rSingapore 199589\r\rAttn :   Mr Ng Yen Shih\r\rHP   :   9169 2503\r\rE1 yennshih@gpeak.com.sg\r\rE2 yennshih@gmail.com', 'sg_65_9169 2503', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(302, 'Lian Hup Seng Furniture Contractor Company', 'Attn :  Mr GP Lim \r\rHP   :  98534023\r\rgp_lim@hotmail.com', 'sg_65_9858 4023', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(303, 'Swiss-Bake Pte Ltd', '348 Jalan Boon Lay \r\rSingapore 619529\r\rAttn :   Mr Josef Stahlin \r\rSenior Operations / Development Manager \r\rTel  :   62660292\r\rjosef@swiss-bake.com.sg\r\rFax  :   62666098', 'sg_65_92370206', '', 'sg_65_62666098', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(304, 'Lian Hup Seng Furniture', '10 Soon Lee Road\r\rSingapore 628074\r\r\r\rAttn :  Mr G P Lim\r\rHP   :  98584023', 'sg_65_98584023', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(305, 'Sri Vairavimada Kaliamman Temple', '', 'sg_65_98342840', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(306, 'ST Electronics (Data Centre Solutions) Pte Ltd', '', 'sg_65_96648071', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(307, 'JK Furniture & Decoration', '23 Defu Lane 4 (Unit 11)\r\rSingapore 539421\r\r\r\rAttn : Mr Sunny Neo\r\rTel : 6858 2782', 'sg_65_9749 1643', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(308, 'Nara Cuisine Pte Ltd', '', 'sg_65_9297 7711', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(309, 'Kosho Singapore Pte Ltd', '205 River Valley rd\r\r#01-76 UE Square\r\rSingapore 238274\r\r\r\rAttn : Mr Tatsu \r\rHP : 9664 0511', 'sg_65_96640511', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(310, 'Pacific Bookstores Singapore', '73 Ayer Rajah Crescent\r\r#03-01/09\r\rSingapore 139952\r\rAttn  :   Ms Sally \r\rHP    :   91773346\r\rserene@pacificbookstores.com', 'sg_65_91773346', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(311, 'Avios Solution Pte Ltd', '21 Toh Guan Road East\r\r#05-19 Toh Guan Centre\r\rSingapore 608609', 'sg_65_9730 5289', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(312, 'Pathlight School ( Campus 1 )', 'No 5 Ang Mo Kio Avenue 10\r\rSingapore 569739\r\rAttn :  Mr Patrick Teo\r\rHP   :  92961363\r\rpatrick.teo@pathlight.org.sg', 'sg_65_92961363', '', 'sg_65_64593397', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(313, 'Pathlight School ( Campus 2 )', 'No 6 Ang Mo Kio Street 44\r\rSingapore 569253\r\rAttn :  Mr Patrick Teo \r\rHp   :  92961363\r\rpatrick.teo@pathlight.org.sg', 'sg_65_92961363', '', 'sg_65_64593397', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(314, 'HG Metal', '', 'sg_65_97973842', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(315, 'Wildlife Reserves Singapore', '80 Mandai Lake Road\r\rSingapore 729826\r\r\r\rAttn : Ms Geraldine (Restaurant Manager)\r\r\r\rDDI : 6365 8392\r\rHP : 8168 4151\r\rEmail : benjerry.szg@wrs.com.sg', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(316, 'Empire Hotel', 'Jalan SS16/1, 47500 Subang Jaya, Selangor Darul Ehsan\r\rAttn: Ms Sabariah Binti Sabki\r\rTel: 03 55651299/ 012 7756279', 'sg_65_012 7756279', '', 'sg_65_03 5565 1287', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(317, 'Hillview Camp', '', 'sg_65_90029504', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(318, 'Advocators Education Pte Ltd', '', 'sg_65_9199 2273', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(319, 'CHANEL MALAYSIA', 'LC-G06 & LC-G07, Ground Level, Suria KLCC, Jalan Ampang, 50088, Kuala Lumpur\r\rAttn: Ms Cynthia Lim\r\rTel: 03 2785 9200 / 012 2390815', 'sg_65_012 239 0815', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(320, 'LUPRIM SDN BHD', 'Level 33, Box 123, \r\r10 Jalan P.Ramlee \r\rKuala Lumpur 50250 \r\rAttn: Ms Cynthia Lim\r\rTel: 03 2785 9200 / 012 239 0815', 'sg_65_6012 239 0815', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(321, 'Jabatan Kejuruteraan Bioperubatan Fakulti', 'Kejuruteraan University Malaya 50603, Kuala Lumpur\r\rAttn: Mr Mohd Hanafi Zainal Abidin\r\rTel: 013 2098 660', 'sg_65_013 2098 660', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(322, 'KOLEJ MARA BANTING', 'Bukit Changgang, 42700 Banting, Selangor\r\rAttn: Mr Mohd Razali Mohd Muhajir\r\rTel: 016 2337557', 'sg_65_016 2337557', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056799, 0, 1),
(323, 'Sin Boon Beng Printing Sdn Bhd', 'No 5384 Lorong Kilang 13, Kaw Perindustrian Rengan Tupai 34000 Taiping, Perak\r\rAttn: Ms Chua\r\rTel: 012 5232204', 'sg_65_012 5232204', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(324, 'Macbuild Construction Pte Ltd', '', 'sg_65_82634273', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(325, 'Sengkang Health Pte Ltd', '167 Jalan Bukit Merah \r\r#06-10 Connection One Tower 5\r\rSingapore 150167', 'sg_65_9176 9631', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(326, 'HUP JOO HIN DEVELOPMENT SDN BHD', 'Lot 1735/1736, Jalan Wira 2/1, Taman Wira Jaya, 72000 Kuala Pilah,Negeri Sembilan\r\rAttn: Mr Tan Jiang Nam\r\rTel: 012 6188969/06 4818839', 'sg_65_012 6188969', '', 'sg_65_06 4818837', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(327, 'Sigma.3 Pte Ltd', '53 Kim Keat Road\r\r#03-02 Mun Hean Building\r\rSingapore 328823\r\rAttn :  Mr Michael Tan\r\rProject Manager \r\rTel  :  64717171 Ext 751\r\rFax  :  64717170\r\rhp   :  96788990\r\rmichael.tan@sigma3.com.sg', 'sg_65_9678 8990', '', 'sg_65_64717170', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(328, 'Calmic Trading Company', '101 Kitchener Rd, \r\r#02-40\r\rSingapore 208511\r\r\r\rAttn  : Mr David Yeong \r\rTel   : 6294 2697\r\rHP    : 9626 9506\r\rFax   : 6292 2020\r\rcalmictrad@singnet.com.sg', 'sg_65_9626 9506', '', 'sg_65_6292 2020', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(329, 'Jack Lockers', '18 Graham Road\r\rMENORA  WA  6050\r\rWESTERN AUSTRALIA\r\r\r\rAttn  :  Ms Christina Jack \r\rtinajack@bigpond.com', 'sg_65_0415 382 541', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(330, 'Club De Colors Pte Ltd', '', 'sg_65_9090 4449', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(331, 'RS Components Pte Ltd', '31 Tech Park Crescent\r\rSingapore 638040\r\r\r\rAttn : Mr Ian Soo (Operations - Admin/Coordinator)\r\r\r\rTel : 6865 3406', 'sg_65_97648113', '', 'sg_65_68653286', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(332, 'ZOOM Salon Pte Ltd', '6 Raffles Boulevard #B1-13/14 Marine Square\r\rSingapore 039594\r\r\r\rAttn :  Ms Catherine \r\rhp   :  83660213', 'sg_65_83660213', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(333, 'Paradise Group Holdings Pte Ltd', '', 'sg_65_94558651', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(334, 'Becker Industrial Coatings (M) Sdn  Bhd', 'No. 3 & 5, Jalan Anggerik Mokara 31/54 \r\rSeksyen 31, Kota Kemuning \r\r40460 Shah Alam, Selangor \r\rAttn: Ms Stacey Tee\r\rTel: 03 51227540', 'sg_65_016 322 0016', '', 'sg_65_03 5122 7542', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(335, 'CBRE Pte Ltd', '10 Eunos Road 8, #02-37\r\rSingapore 408600\r\r\r\rAttn : Mr Gabriel Chia (Complex Manager)\r\rHP : 97755744', 'sg_65_97755744', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(336, 'Lockerfellas Pte Ltd', 'Blk 4012 Ang Mo Kio Avenue 10\r\rAng Mo Kio Techplace 1\r\r#01-14\r\rSingapore 569628\r\rAttn  :   Mr Kelvin Khoo \r\rHP    :   81278274', 'sg_65_81278274', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(337, 'C & L Enterprise', '20 Jalan Dian 8\r\rTaman Munsyi Ibrahim\r\r81200 Johor Bahru \r\rJohor', 'sg_65_167028965', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(338, 'Tradecom Services Pte Ltd', '3 Shenton Way\r\r#09-08 Shenton House\r\rSingapore 068805\r\r\r\rAttn : Mr Tan Eng Boo\r\rTel : 6224 0036', 'sg_65_98363369', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(339, 'Jones Lang Lasalle Ppty Consultants Pte Ltd', '238A Thomson Road #20-01/10, Novena Square Tower A \r\rS307684\r\r\r\rAttn : Mr Rommel\r\rHP : 9831 8620', 'sg_65_98318620', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(340, 'Ms Rose/ Mr Omar', '', 'sg_65_196592801', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(341, 'A.D.I Construction & Engineering Pte Ltd', 'Blk 123 Bukit Merah Lane 1 \r\r#04-114\r\rSingapore 150123\r\rTel   :   62721400\r\rFax   :   62720413\r\rasaraf@adiengineeringllp.com', 'sg_65_91267502', '', 'sg_65_62720413', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(342, 'FXL Innovation Pte Ltd', '520 Sims Avenue , #01-07, Fuyuen Court, Singapore 387580\r\r\r\rAttn : Ms Fida', 'sg_65_93350961', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(343, 'Aspacio Design Associates Pte Ltd', 'Attn :  Ms Samyukta Bankal\r\r        Project Designer\r\rHp   : 9106 3002\r\rTel  : 6339 0681 ext 21\r\rFax  : 6338 6892\r\rsamyukta-ankal@aspacio.com.sg', 'sg_65_9106 3002', '', 'sg_65_6338 6892', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(344, 'Taka Hardware & Engineering Pte Ltd', '46, Tannery Lane \r\rSingapore 347793\r\r\r\rAttn   : Mr Andy Lee \r\rDID    : 68420782\r\rMobile : 93852129\r\rFax    : 68424282\r\randy@takahardware.com.sg', 'sg_65_93852129', '', 'sg_65_68424282', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(345, 'Punj Lloyd Pte. Ltd, Singapore', 'Attn :  Mr Vipul Sharma\r\rProject Controls \r\rHP   : 97361152\r\rTel  : 6933 4992\r\rFax  : 6565 9880\r\rvipulsharma@punjlloyd.com', 'sg_65_97361152', '', 'sg_65_6565 9880', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(346, 'Superb Hostel', '', 'sg_65_96699990', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(347, 'Personal_Shanmugam', '', 'sg_65_94568350', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(348, 'Kai Hospitality Pte Ltd', 'Attn : Mr Dave Tung\r\rHP : 9029 8394', 'sg_65_90298394', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(349, 'Faith Community Baptist Church', '', 'sg_65_98274837', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(350, 'All Inn Pte Ltd', 'No 2 Jalan Pinang \r\rSingapore 199150\r\rAttn :  Mr Ronnie Tarn\r\rHP   :  96699990', 'sg_65_96699990', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(351, 'KORU WELLNESS', '25A & 27A, JALAN MURNI 11, TAMAN MURNI, 83000 BATU PAHAT, JOHOR\r\rATTN: MR CARSON NG\r\rTEL: 016 7110800', 'sg_65_167110800', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(352, '191 Squadron', 'Attn :  Mr Aylwin\r\rHp   :  93801268\r\rFax  :  65444378', 'sg_65_93801268', '', 'sg_65_65444378', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(353, 'Chatsworth International School', '', 'sg_65_8118 4662', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(354, 'Adnex Interior Solution Sdn Bhd', '49-1, Jalan SS18/6,\r\r47500 Subang Jaya,\r\rSelangor\r\rAttn: Ms Li Ying\r\rTel: 016 328 1500 / 03 5611 6677', 'sg_65_016 328 1500', '', 'sg_65_03 5611 7766', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(355, 'ASM ENTERPRISE', 'No 4, Jalan Lorong SS 5C/1F\r\rKelana Jaya, 47301 Petaling Jaya\r\rAttn: Mr Adi Saiful\r\rTel: 010 221 6858', 'sg_65_010 221 6858', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(356, 'HMS Far East Pte Ltd', '12 Tuas Road\r\rSingapore 638486\r\rAttn   :  Mr Jack Ngui\r\rCustomer Account Manager\r\rTel    : +65 6276 7890\r\rDirect : +65 6849 5142\r\rFax    : +65 6265 1365\r\rMobile : +65 9651 2115\r\rJackNgui@hmsfareast.com', 'sg_65_9651 2115', '', 'sg_65_6265 1365', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(357, 'REG SDN BHD', '221B, One City C-1A-08, USJ 25\r\rSelangor\r\rAttn: Mr Gan\r\rTel: 012 257 4295', 'sg_65_012 257 4295', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(358, 'Royal Sungei Ujong Club', '2A, Jalan Dato Kelana Ma''amor\r\r70700 Seremban\r\rAttn: Ms Devi\r\rTel: 06 7630104/ 012 6311979', 'sg_65_126311979', '', 'sg_65_67621915', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(359, 'FELKEN CREATION', 'No 56-1, Jalan Puteri 5/1\r\rBandar Puteri, 47100 Puchong\r\rAttn: Mr Stanley Chin\r\rTel: 0126166988', 'sg_65_126166988', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(360, 'Manuli Fluiconnecto Pte Ltd', '53 Gul Drive \r\rSingapore 629497\r\r\r\rAttn :  Mr Andrew Lek \r\rTel  :  67909191\r\rFax  :  67909292\r\randrew.lek@fluiconnecto.com.sg', 'sg_65_98551181', '', 'sg_65_67909292', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(361, 'TATWAI Enterprise Pte Ltd', '20 Woodlands Industrial Park E1\r\rAdmiralty Industrial Park\r\rSingapore 757739\r\r\r\rTel  : 6362 9222\r\rFax  : 6367 0020\r\rHP   : 83180188\r\rlesliewong@tatwai.com.sg', 'sg_65_83180188', '', 'sg_65_6367 0020', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(362, 'EK Menthod', '11 Woodlands Close\r\r#06-03  Woodlands 11\r\rSingapore 737853\r\rAttn  :  Ms Moeaye\r\r         Marketing Director\r\rhp    :   98760625\r\rekmenthod.com.sg', 'sg_65_98760625', '', 'sg_65_64528816', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(363, 'Devo Design', '215 Henderson Road\r\rHenderson Industrial Park,\r\r#03-06\r\rSingapore 15955\r\r\r\rAttn  :  Ms Roselle Montilla\r\rTel   :  6377 0080\r\rrosellem@devodesign.com.sg', 'sg_65_8468 4592', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(364, 'Lucky Square Pte Ltd', '2 Leonie Hill Road, #01-01, Leonie Condotel\r\rSingapore 239192  \r\rAttn :  Mr Sam Chan\r\rProperty Officer | Leasing Operations | Corporate Real Estate Business Group\r\r\r\rTel    : 6735 3395\r\rMobile : 9090 1149\r\rFax    : 6735 3396', 'sg_65_9090 1149', '', 'sg_65_6735 3396', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(365, 'BIBLE SOCIETY OF SINGAPORE', '7 Armenian Street\r\rBible House\r\rSingapore 179932\r\rTel  : 6337 3222 Ext 161\r\rFax  : 6337 3036\r\rHp   : 9765 9381  \r\rjerry.tan@bible.org.sg', 'sg_65_9765 9381', '', 'sg_65_63373036', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(366, 'Recall Total Information Management Pte Ltd', 'Recall Total Information Management Pte Ltd\r\rRecall Centre 3\r\r28 Quality Road\r\rSingapore 618828\r\rAttn :  Mr CHIA Hock Chee\r\rOperations Executive, Recall Digital Services\r\rt: (65) 6262 5612\r\rm: (65) 8201 3668\r\re: hockchee.chia@recall.com.sg', 'sg_65_8201 3668', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(367, 'OFFICE-TECH & Supplies', 'Lot 2059, Jalan Cattleya 2\r\rMiri, Sarawak, Malaysia\r\rAttn: Ms Mee Hua\r\rTel: 012 8773044 / 085 655005', 'sg_65_012 8773044', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(368, 'Wangsa Citra Pte Ltd', 'Blk 12 Teck Whye Lane\r\r#03-214\r\rSingapore 680012\r\rJames Augustine Qin\r\rHP :  90682486\r\rwangsacitrapteltd@yahoo.com', 'sg_65_90682486', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(369, 'Crowne Plaza Hotels & Resorts', '75 Airport Boulevard\r\rSingapore 81966\r\rAttn :   Mr Abdul Rahman Yacob\r\rTel  :   68235430\r\rhp   :   91055850\r\rabdulrahman.yacob@ihg.com', 'sg_65_91055850', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(370, 'Schneider Electric', 'Global Supply Chain Singapore Human Resource \r\r46 Penjuru Lane, \r\rC & P Hub 3 Level 4, Singapore 609206 \r\rAttn   : Ms Adonica Lee \r\rPhone  : 6480 3688\r\rMobile : 9852 0959\r\rFax    : 6265 1151    adoncia.lee@schneider-electric.com     \r\rAddress: \r\r*** Please consider the environment before printing this e-mail', 'sg_65_9852 0959', '', 'sg_65_62651151', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(371, 'NY Laundry Pte Ltd', '4 Woodlands Loop \r\rSingapore 738204\r\rAttn :  Ms Quek Sock Chen\r\rHP   :  97805419\r\rqueksc@nylaundry.com.sg', 'sg_65_97805419', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(372, 'Ergoworld Pte Ltd', '53 Serangoon North Avenue 4\r\r#03-01\r\rSingapore 555852\r\rAttn :  Ms Grace Chen\r\rTel  :  62968488 Ext 122\r\rhp   :  967963606\r\rgracechen@ergoworld.com.sg', 'sg_65_967963606', '', 'sg_65_67538488', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(373, 'PLATINUM INDUSTRIAL SUPPLY & ENGINEERING SDN BHD', 'No 3A-1, Jalan KP 1/8, Taman Kajang Prima, 43000 Kajang, Selangor\r\rAttn: Mr Kit\r\rTel: 012 3515 575', 'sg_65_123515575', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(374, 'QMAS TRADING', 'No C, 210B, Block C, Jalan PJU 10/2A, Damansara Damai, Sungai Buloh, 47830 Petaling Jaya, Selangor\r\rAttn: Mr Wan Mohd Rozi/ Mr Faizal\r\rTel: 0133060337 / 0172504076', 'sg_65_133060337', '', 'sg_65_03 6157 2237', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(375, 'Beautylite Interiors Sdn Bhd', 'No 16, Jalan 5/108c, Taman Sg Besi, 57100 Kuala Lumpur\r\rAttn: Mr Raymond Goh\r\rTel: 016 261 2285', 'sg_65_162612285', '', 'sg_65_03 7983 6890', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(376, 'Primitives Woodworks Pte Ltd', '48 Defu Lane 9 \r\rSingapore ( 539288 )\r\rMobile : +65 8183 9765\r\rDID : +65 6858 5271\r\rFax : +65 6858 5291', 'sg_65_8183 9765', '', 'sg_65_6858 5291', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(377, 'INNOZEN SDN BHD', 'No 96, jalan BP 5/11, Bandar Bukit Puchong, 47100 Puchong,\r\rSelangor\r\rAttn: Tan Bee Ching\r\rTel: 03 8068 1728', 'sg_65_123657728', '', 'sg_65_03 80681729', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(378, 'Sleepy Kiwi Pte Ltd', '55 Bussorah Street\r\rSingapore 199471', 'sg_65_96889255', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(379, 'TNT Express Worldwide (M) Sdn Bhd', '17B Floor Menara PKNS,Jalan Yong Shook Lin, \r\r46050 ,Selangor\r\rAttn: Ms Brenda Lee\r\rTel: 037962 3132', 'sg_65_03 7962 3132', '', 'sg_65_03 7962 3380', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(380, 'Prime Evolue Singapore Pte. Ltd', 'Prime Evolue Singapore Pte Ltd\r\r31 Tembusu Crescent, \r\rJurong Island\r\rSingapore 627614\r\r\r\rAttn: Mr Sofyan Wongsodihardjo\r\rManager, Production\r\r\r\rHp    : +65 9618 1217', 'sg_65_9618 1217', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(381, 'Akademik Suite', 'Akademik Suite,\r\rJalan Austin Heights Utama,\r\rTaman Mount Austin, 81100 Johor Bahru', 'sg_65_73009146', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(382, 'TOYS''R''US (SINGAPORE) PTE LTD', '315, Outram Road, \r\r#14-09\r\rTan Boon Liat Building\r\rSingapore 169074\r\rAttn :  Mr Eric Tui\r\rEric.Tui@toysrus.com', 'sg_65_82889330', '', 'sg_65_62221095', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(383, 'Singapore Zoological Gardens', '80 Mandai Lake Road \r\rSingapore 729826 \r\rDID    :(65) 63608636\r\rHP     :(65) 91724999\r\rS/HP   :(65) 97224699\r\rFAX    :(65) 63658032\r\rbanquet.zoo@wrs.com.sg', 'sg_65_91724999', '', 'sg_65_63658032', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(384, 'ROTA Elite Sdn Bhd', 'Suite C-4-1, Tower C, Wisma Goshen\r\rNo. 5, Jalan 4/83A, Off Jalan Pantai Baharu\r\r59200 Kuala Lumpur\r\rAttn: Ms Amzar Bin Abdul Rahman\r\rTel: 03 2287 3711', 'sg_65_03 2287 3711', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(385, 'Azen Manufacturing Pte Ltd', 'No. 8 Tuas Link One\r\rSingapore 638593\r\rAttn : Ms Alice Ong \r\rTel  : 6261 0277\r\rFax  : 6261 7785\r\ralice@azen.com.sg', 'sg_65_96229684', '', 'sg_65_6261 7785', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(386, 'LF Interior Pte Ltd', 'No. 10 Kaki Bukit Road 2\r\r#03-10 First East Centre\r\rSingapore 417868\r\rAttn :  Mr L.K Tan ( Xiao Qiang)\r\rTel  :  66048107\r\rFax  :  66048607\r\rrecept@lfinterior.com.sg', 'sg_65_98219958', '', 'sg_65_66048607', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(387, 'CHEAH & CHEAH BROTHERS SDN BHD', 'Suite 8.01, Level 8, Menara Binjai, No 2, Jalan Binjai,50450 Kuala Lumpur\r\rAttn: Mr Cheah Heen Goh\r\rTel: 017 854 1119', 'sg_65_017 854 1119', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(388, 'BINASAT SDN BHD', 'No 17 & 19, Jalan Bukit Permai Utama 3\r\rTaman Bukit Permai, Cheras,56100 Kuala Lumpur\r\rAttn: Mr Gora Ashraf\r\rTel: 0123199681', 'sg_65_123199681', '', 'sg_65_342961881', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(389, 'Nara Cuisine Pte. Ltd.', '#03-07, Westgate\r\r3 Gateway Drive\r\rSingapore 608532\r\rAttn :  Mr Tommy Ang \r\rHP   :  92977711\r\rtommy@narathai.com.sg', 'sg_65_92977711', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(390, 'SP eCommerce Pte Ltd', '107 Eunos Avenue 3\r\r#06-02\r\rSingapore 409837\r\rAttn :  Ms Sherine Teo \r\reCommerce\r\rhp   :  9762 2556\r\rsherineteo@singpost.com', 'sg_65_97622556', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(391, 'Federal Express Services (M) Sdn Bhd', '25 Jalan Delima 1/3\r\rSubang Hitech Industrial Park\r\rBatu 3, Shah Alam\r\r40000 Selangor\r\rAttn: Ms Linda Cheah\r\rTel: 0123731933', 'sg_65_123731933', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(392, 'Z L Construction Pte Ltd', '756 Upper Serangoon Rd, #02-13B, Upper Serangoon S.C, Singapore 534626\r\r\r\rAttn :  Ryan Liu Yicheng \r\rHP   :  96248059\r\rryanyicheng@gmail.com', 'sg_65_96248059', '', 'sg_65_6280 0995', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(393, 'Mr Abdul Gani Bin Syed', '', 'sg_65_196592801', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(394, 'Eden School', '101 Bukit Batok West Ave 3 S(659168)\r\rAttn:  Mr Jason Quek\r\rOperations Executive\r\rHP  : 91887785\r\rDID : 65925634\r\rTel : 62657400\r\rFax : 62659400\r\rjasonquek@edenschool.edu', 'sg_65_91887785', '', 'sg_65_62659400', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(395, 'SJK (C) CHUNG KWOK', 'Jalan Merpati, 50350 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur\r\rAttn: Ms Joanne\r\rTel: 0122528617', 'sg_65_122528617', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(396, 'Red dot', '123 bedok south \r\rRd \r\rSingaore\r\ratnn :  ....A\r\rtel   : \r\rFax   :', 'sg_65_123455678', '', 'sg_65_675454', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(397, 'The Odd Fellows Pte Ltd', '603 Ang Mo Kio ave 5,\r\r#01-2661 \r\rSingapore 560603\r\rAttn: June Lew\r\rTel: 96710250', 'sg_65_96710250', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(398, 'Salpac (S) Pte Ltd', '19 Tukang Innovation Drive\r\r#02-01, Greenhub \r\rSingapore 618301\r\rM : (65) 9632 3783\r\rO : (65) 6362 6116\r\rF  : (65) 6362 3113\r\rE  : chris@salpac.com', 'sg_65_9632 3783', '', 'sg_65_6362 3113', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(399, 'BSBH Office and Industrial Supplies.', 'Blk 928 Hougang Street 91\r\r#07-55\r\rSingapore 530928\r\rAttn :  Mr Steve\r\rhp   :  86148335\r\rbsbhsteve@gmail.com', 'sg_65_86148335', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(400, 'Makeway Pte Ltd', '47 Sungei Kadut Loop\r\rSingapore 729512\r\r\r\rAttn : Jamie Low\r\rHP : 9618 3234', 'sg_65_9618 3234', '', 'sg_65_6269 5946', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(401, 'Henderson Secondary School', '100 Henderson Road\r\rSingapore 159544\r\r\r\rAttn : Mr Liau Thiam Huat\r\rHP : 9799 1828\r\rliau_thiam_huat@moe.edu.sg', 'sg_65_9799 1828', '', 'sg_65_6273 6037', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(402, 'Inner Space ID Pte. Ltd.', '3 Irving Road \r\r#01-02 Irving Ind. Bldg.\r\rSingapore 369522\r\rAttn :  Mr Matthew Sa\r\rTel  :  68361490\r\rFax  :  62855121\r\rHp   :  96930206\r\rinnerspv@singnet.com.sg', 'sg_65_96930206', '', 'sg_65_62855121', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(403, 'K Entertainment Pte Ltd', 'St James Power Station \r\rOld Toxic ( beside Neverland ) \r\r\r\rAttn :  Mr Fairuz\r\rhp   :  87275476\r\rfairuz.cc@gmail.com', 'sg_65_87275476', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(404, 'AFSYS Pte Ltd', '4190 Broadway Plaza \r\r#01-02\r\rSingapore 569841\r\r\r\rAttn :  Mr Dan Ho \r\rhp   :  96961648 \r\rdan.ho@anytimefitness.', 'sg_65_96961648', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(405, 'Hai Sing Catholic School', '9, Pasir Ris Drive 6\r\rSingapore 519421\r\r\r\rAttn : Mr Ting Koh Ling\r\rHP : 9337 2496\r\rEmail : ting_koh_ling@moe.edu.sg', 'sg_65_9337 2496', '', 'sg_65_6582 2543', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(406, 'YMD (S) Pte Ltd', '24, Sungei Kadut Way\r\rSingapore 728780\r\r\r\rAttn :  Mr Ong Yean Shen \r\rTel    : 6368 3335\r\rFax    : 6368 6626\r\rHP     : 9297 7686\r\rongys@ymd.com.sg', 'sg_65_9297 7686', '', 'sg_65_6368 6626', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(407, 'Idstudioworks Pte Ltd', '122, Eunos Ave 7, \r\rRichfield Industrial Centre, #08-01, \r\rSingapore 409575\r\r\r\rAttn : Mr Tim Lavente\r\rMobile : 91088530\r\rtimlavente@hotmail.com', 'sg_65_91088530', '', 'sg_65_64451811', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(408, 'Chang Cheng Group Pte Ltd', '31 Ubi Crescent \r\rSingapore 408583\r\rAttn : Mr James Hang Hock Siew \r\rAccountant \r\rTel  :  65015273\r\rFax  :  68481370\r\rjameshang@changcheng.com.sg', 'sg_65_97777544', '', 'sg_65_68481370', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(409, 'Elsie’s Kitchen Catering Services Pte. Ltd.', '21 Second Chin Bee Road \r\rSingapore 618780\r\r\r\rAttn  :  Ms Rachel Ang \r\rHR Director \r\rTel   :  62884457  \r\rDirect:  65933744\r\rHP    :  9735 5671\r\rrachel@elsiekitchen.com.sg', 'sg_65_9735 5671', '', 'sg_65_62888794', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(410, 'Cocoa Island by COMO,', 'Makunufushi, South MalAc Atoll\r\rThe Republic of the Maldives\r\rTel    : +9606652255\r\rFAX    : +9606640493\r\rMobile : +9607932255\r\rraja.muthu@comohotels.com', 'sg_65_9607932255', '', 'sg_65_9606640493', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(411, 'Symrise Asia  Pacific Pte Ltd', '226 Pandan Loop\r\rSingapore 128412\r\r\r\rAttn : Mr Lee Yie How\r\rHp.  : 96334086', 'sg_65_96334086', '', 'sg_65_68720265', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(412, 'Lexon Furniture & Construction Pte Ltd', 'Block 3023 Ubi Road 3 #05-20\r\rSingapore 408663                        Attn :  Mr Leong \r\rTel  :  6742-2226\r\rFax  :  6742-4431\r\rH/P  :  8222-9039\r\rtkleong@lexon.com.sg', 'sg_65_82229039', '', 'sg_65_67424431', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(413, 'Home Oasis LLP', 'Block.302 Serangoon Ave. 2\r\r#12-286\r\rSingapore (550302)\r\rReg No: T12LL2184F\r\rAttn :  Mr Francis \r\rTel  :  98456054\r\rthewhiteoasis@yahoo.com', 'sg_65_98456054', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(414, 'Amitabha Buddhist Society (S)', 'N0 2 Lorong 35 Geylang\r\rSingapore 387934\r\r\r\rAttn :  Mr Tan Peng Kwang \r\rHP   :  96868006\r\rbuilding@amtb.org.sg', 'sg_65_96868006', '', 'sg_65_67444774', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(415, 'Lockdown Pte Ltd', '6 Eu Tong Sen Street\r\r#02-32\r\rThe Central \r\rSingapore 059817\r\rAttn  :  Mr Jonathan Ye \r\rHP    :  91701201\r\rjonathan13579@hotmail.com', 'sg_65_91701201', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(416, 'Megatec Technology Private Limited', '29 Joo Koon Road, \r\rSingapore 628983\r\r\r\rAttn : Ms Ivy Lee\r\rHP : 8383 8669', 'sg_65_8383 8669', '', 'sg_65_6862 3801', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(417, 'RMG Rent-A-Car Pte Ltd', '25 Hoot Kiam Road\r\rSingapore 249407\r\r\r\rAttn : Ms Jesling Ang\r\rSales Manager\r\rHP : 9760 6607', 'sg_65_97606607', '', 'sg_65_6235 5256', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056800, 0, 1),
(418, 'Everluck Electrical Service', 'Blk 2, Defu Lane 10 #03-503\r\rSingapore 539183\r\r\r\rAttn : Mr Laurance\r\rHP : 9738 7075', 'sg_65_9738 7075', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(419, 'Just Education Learning Centre (Sengkang)', 'Block 308 Anchorvale Road #01-09\r\rSingapore 540308\r\r\r\rAttn : Mr Philip Wan\r\rHP : 9147 2255', 'sg_65_9147 2255', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(420, 'Spatula Bakery', '87 Frankel Avenue \r\rSingapore 458215 \r\r\r\rAttn : Ms Anna Majeed\r\rHP : 9817 2533', 'sg_65_98172533', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(421, 'SD Group Asia Pte Ltd', '79 Anson Road #11-03\r\rSingapore 079906\r\r\r\rAttn : Mr Leslie Sim\r\rHP : 8553 5645', 'sg_65_8553 5645', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(422, 'North West Interior Design Pte Ltd', '246 Macpherson Rd #01-01\r\rBetime Bldg\r\rSingapore 348578\r\r\r\rAttn : Ms Jammie Goh\r\rHP : 9383 0398', 'sg_65_9383 0398', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(423, 'Studio ALT Pte Ltd', '25 Tannery Lane,\r\r#06-25\r\rSingapore 347786\r\rAttn :   Mr Algene Tan/Principal Designer', 'sg_65_91007045', '', 'sg_65_62825241', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(424, 'Rosso Vino Pte Ltd', '15 Merbau Road\r\rSingapore 239032\r\r\r\rAttn : Mr Adaldo Salavtore/ Restaurant Manager\r\rhp   :   94499542\r\rsalvatoreadaldosg@gmail.com', 'sg_65_94499542', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(425, 'Opto Precision Pte Ltd', '', 'sg_65_98631924', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(426, 'LG BEAN MANUFACTURER PTE LTD', 'Blk 1009 Aljunied Avenue 4\r\r#01-32/34 \r\rSingapore 389910\r\r\r\rAttn : Mr Jimmy Lim\r\rHP : 9654 4998', 'sg_65_96544998', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(427, 'Casa Tartufo Restaurant', '33 Erskine Road\r\rThe Scarlet Hotel (entrance on Ann Siang rd) \r\rSingapore 069333\r\r\r\rAttn : Mr Alex\r\rTel : 6836 4647  Mobile : 8518 8200', 'sg_65_8518 8200', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(428, 'Bruce Fitness', '20 Aljunied Road\r\r#01-04/05/06\r\rSingapore 389805\r\r\r\rAttn : Mr Bruce\r\rHP : 9619 9034', 'sg_65_96199034', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(429, 'GoMobile Security Solutions Pte Ltd', 'Blk 165 Bukit Merah Central\r\r#07-3685 \r\rSingapore 150165 \r\r\r\rAttn : Mr Daud Sam\r\rOperations Director\r\rHP : 9724 3944 \r\rTel : 6299 4410 \r\rFax : 6299 5380\r\rEmail : daud@gomobile.com.sg', 'sg_65_93412374', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(430, 'Neo Garden Catering Pte Ltd', '1 Enterprise Road \r\rUnit 1\r\rSingapore 629813\r\r\r\rAttn :   Ms Tris Thio \r\rhp   :   85114389\r\rtris.thio@neogroup.com.sg', 'sg_65_85114389', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(431, 'National Healthcare Group Polyclinics', 'Attn :  Mr Soh Chun Kang\r\rDID  : (65) 64966663\r\rHP   : (65) 96272546\r\rFax  : (65) 64966753\r\rChun_Kang_SOH@nhgp.com.sg', 'sg_65_96272546', '', 'sg_65_64966753', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(432, 'Synergy FMI Pte Ltd', 'NUH Medical Centre\r\rSingapore 119082\r\r\r\rAttn : Mr Faraaz Hussain\r\rHP : 9382 5756', 'sg_65_93825756', '', 'sg_65_68343172', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(433, 'Mandarin Gardens Management Office', '5 Siglap Road, #01-43\r\rSingapore 448908\r\r\r\rAttn : Mr Wesley Liaw\r\rHP : 8228 4237', 'sg_65_8228 4237', '', 'sg_65_64442188', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(434, 'CIR Resources Holdings Pte Ltd', '45 Kallang Pudding Road \r\r#07-03 Alpha Building \r\rSingapore 349317\r\r\r\rAttn : Mr Adam Tong Ching\r\rTel : 65730363  Hp : 98336723', 'sg_65_98336723', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(435, 'Motherland Electronic Hardware Pte Ltd', 'Blk 23 Sin Ming Road #01-25\r\rSingapore 570023\r\r\r\rAttn : Mr Brandon \r\rTel : 6456 6912\r\rHP  : 9027 0562', 'sg_65_90270562', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(436, 'QXY Resources Pte Ltd', '28A Penjuru Close\r\r#01-04\r\rSingapore 609129\r\r\r\rAttn : Mr Richard Tike\r\rQuantity Surveyor\r\rTel: 6515 0980\r\rHP : 9621 1854', 'sg_65_9621 1854', '', 'sg_65_6515 0981', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(437, 'People’s Association c/o Potong Pasir Constituency Office', '6 Potong Pasir Ave 2, \r\rSingapore 358361\r\r\r\rAttn : Mr Muhd Muhaimin\r\rConstituency Management Executive\r\rTel : 6280 1182 \r\rHP  : 8163 1256\r\rMuhd_Muhaimin_NGALIMAN@pa.gov.sg', 'sg_65_8163 1256', '', 'sg_65_6287 7655', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(438, 'Encounter', '302 Jalan Besar #B1-01 \r\rSingapore 208963\r\r\r\rAttn : Ms Phyllis Chua\r\rHp : 9191 8825', 'sg_65_9191 8825', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(439, 'ASTI ENGINEERING PTE LTD', 'Block 5000 Ang Mo Kio Ave 5, Techplace II #04-06, Singapore 569870', 'sg_65_8118 9567', '', 'sg_65_64842022', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 8, 1477056801, 1477367076, 1),
(440, 'Hua Fu Xuan', '15 Jalan Tepong #05-02\r\rJurong Food Hub\r\rSingapore 619336\r\r\r\rAttn : Mr Lee\r\rHP : 92370354', 'sg_65_92370354', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(441, 'World Clean Facility Services Pte Ltd', '4 Jalan Bukit Ho Swee #01-158\r\rSingapore 162004\r\r\r\rAttn : Mr Chin Chwee Hwa\r\rTel  : 62718995\r\rFax  : 62717573\r\rHp   : 96698388', 'sg_65_96698388', '', 'sg_65_62717573', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(442, 'Palm & Oak Properties Pte Ltd', '32 Seah Street, \r\rSingapore 188388\r\r\r\rAttn : Ms Annie \r\rHp : 97490912', 'sg_65_97490912', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(443, 'Inn Interior Enterprise Pte Ltd', '34 Toh Guan Road East\r\r#01-26 Enterprise Hub\r\rSingapore 608579\r\r\r\rAttn : Mr A.B Lee\r\rTel  : 6316 5471 \r\rHp   : 9786 7176\r\rinn.interior@gmail.com', 'sg_65_9786 7176', '', 'sg_65_6316 5470', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(444, 'Kunyi (Singapore) Pte Ltd', '27A Jurong Port Road Blk 1 #01-07 \r\rSingapore 619101\r\r\r\rAttn : Mr Benjamin Lin\r\rTel  : 62612862\r\rHp   : 97873454', 'sg_65_97873454', '', 'sg_65_62615609', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(445, 'AB Chemical & Engineering Pte Ltd', '2 Ang Mo Kio Street 64,\r\rEcon Industrial Building, #04-02\r\rSingapore 569084\r\r\r\rAttn : Mr Eric Quek\r\rTel: +65 6752 2887\r\rFax: +65 6752 2813\r\rMobile: +65 90727127', 'sg_65_90727127', '', 'sg_65_6752 2813', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(446, 'AMKFSC Community Services Ltd', 'Attn : Mr Hong Shao Wei\r\rHp   : 9698 9808', 'sg_65_96989808', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(447, 'St. Andrew''s Cathedral', 'Danny Chooi', 'sg_65_97884894', '', 'sg_65_6339 1197', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(448, 'Young Dancers Academy Pte Ltd', '170 Ghim Moh Road\r\r#06-08\r\rSingapore 279621\r\r\r\rAttn :  Ms Lisa Latip \r\rhp   :  97545134\r\ryoungdancersacademy@yahoo.com', 'sg_65_97545134', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(449, 'Boldwin Immanuel Pte Ltd', '42 IMBIAH road , \r\r#01-02/04 Sentosa\r\rSingapore 099701\r\r\r\rAttn : Mr Raymond\r\rHP : 9229 3277', 'sg_65_9229 3277', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(450, 'Diamond Kitchen', 'Attn : Mr Devan\r\rHP   : 9662 7600\r\rdevan.ang@diamondkitchen.com.sg', 'sg_65_9662 7600', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(451, 'TPI Interior Design', '', 'sg_65_97939599', '', 'sg_65_62591514', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(452, 'Spageddies Italian Kitchen', 'Attn : Mr Henry\r\rHP   : 9274 8885\r\rEmail: henry@spagaddies.com.sg', 'sg_65_92748885', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(453, 'Rotork Controls (Singapore) Pte Ltd', '426 Tagore Industrial Avenue, Singapore 787808\r\r\r\rAttn : Mr Peter Chia\r\rTel  : +65 64600560 \r\rDID  : +65 64600582 \r\rHP   : +65 96406238\r\rEmail: peter.chia@rotork.com', 'sg_65_98710637', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(454, 'Norland Pte Ltd', '1 Woodlands Industrial Park E9\r\rSingapore 757703\r\r\r\rAttn : Irfan Iskandar B Anuar\r\r       (CE Day Shift Tech)\r\rHP : 9784 7825', 'sg_65_9784 7825', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(455, 'Algaetech International Sdn Bhd', 'PT 5517,Technology Park Malaysia,Bukit Jalil\r\r57000, Kuala Lumpur\r\rAttn: Ms Noor Azuar Asmah\r\rTel: 019 259 1054', 'sg_65_019 259 1054', '', 'sg_65_03 8994 1662', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(456, 'LKM Engineering & Industrial Supply', '60 Kaki Bukit Place\r\r#06-06\r\rSingapore 415979\r\rAttn :  Mr Eddie Lim \r\rhp   :  97537402', 'sg_65_97537402', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(457, 'KINGSMEN EXHIBITS PTE LTD', 'Kingsmen Creative Centre\r\r3 Changi South Lane \r\rSingapore 486118 \r\r\r\rAttn : Nurul Asyiqin \r\rTel  : 6880 0452\r\rHP   : 9695 9412', 'sg_65_96959412', '', 'sg_65_68800468', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(458, 'SYSMA ENERGY PTE LTD', '28 Gul Avenue \r\rSingapore 629668\r\r\r\rAttn  : Mr Ong Zhong Kai\r\rTel   : 6863 3863\r\rMobile: 9828 4056\r\rEmail : zhongkai@sysmaenergy.com.sg', 'sg_65_9828 4056', '', 'sg_65_6863 3343', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(459, 'Gloria Foods Pte Ltd', 'C/O Baja Fresh Mexican grill\r\r#01-03 , 9 Bras Basah Road\r\rSingapore 189559\r\r\r\rAttn : Ms Jaya\r\rHP   : 8157 4137\r\rEmail: jaya@gloriafoods.com.sg', 'sg_65_8157 4137', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(460, 'JR Foods Pte Ltd', 'No. 8, Senoko South Road, \r\r#02-00, Singapore 758095\r\r\r\rAttn : Mr Thomas Wong\r\rTel  : 6288 4111', 'sg_65_9658 2333', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(461, 'Bukit Batok Clubhouse', '91 Bukit Batok West Avenue 2\r\rSingapore 659206\r\rAttn :  Mr Ishak \r\rhp   :  91889090\r\rbbpool@csc.sg', 'sg_65_91889090', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(462, 'SecuriState', 'No. 20, Sin Ming Lane,\r\r#05-66 Singapore 573968\r\r\r\rAttn : Mr Mohammad Faizal \r\rOperations Manager\r\rMaritime Division\r\rDID : +65 6684 5650 / 5652\r\rFax : +65 6684 5651\r\rHP  : +65 9794 4373', 'sg_65_9794 4373', '', 'sg_65_6684 5651', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(463, 'Gracehaven', 'The Salvation Army\r\r\r\rAttn : Mr David Moses Heng\r\rOperations Supervisor\r\r\r\rHP  : 93851997\r\rTel : 65802273\r\rFax : 62841479', 'sg_65_93851997', '', 'sg_65_62841479', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(464, 'K H Healthy Pte Ltd', 'Blk 204 Bedok North St 1 \r\r#01-417/419\r\rSingapore 460204\r\r\r\rAttn : Mr Heng Khee Huat\r\rTel  : 9696 6658\r\rEmail: khhengheng@yahoo.com', 'sg_65_9696 6658', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(465, 'St Luke’s Hospital', 'Bukit Batok Street 11 \r\rSingapore 659674\r\rAttn :  Ms Myat Su\r\rAdmin Assistant Rehabilitation Services Division2\r\rTel : 65632281 ext 296\r\rDID : 68953296\r\rFax : 65618205\r\rHP  : 92994067\r\rE:myatsu@slh.org.sg', 'sg_65_92994067', '', 'sg_65_65618205', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(466, 'Molly Malone''s Irish Pub', '56 Circular Road, \r\rSingapore 049411\r\r\r\rAttn : Mr Carlo\r\rHP   : 9128 4207\r\rEmail: carlo@theexchange.com.sg', 'sg_65_91284207', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(467, 'B K Civil & Construction Pte Ltd', '48, Toh Guan Road East,\r\r#06-153 Enterprise Hub\r\rSingapore 608586\r\rAttn :  Mr Arikrishnan (Ari) \r\rTel  : 64678878\r\rFax  : 64678858\r\rHP   : 97597846\r\rbkmail@bkcivil.com.sg', 'sg_65_97597846', '', 'sg_65_64678858', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(468, 'Wee', '', 'sg_65_8335 6368', '', 'sg_65_6582 2135', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(469, 'HS Xpress Pte Ltd', '1, Benoi Rd\r\rSingapore 629875\r\r\r\rAttn : Mr Wee\r\rTel  : 6582 5645\r\rFax  : 6582 2135\r\rHp   : 8335 6368\r\rEmail: wee@hsxpress.com.sg', 'sg_65_8335 6368', '', 'sg_65_6582 2135', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(470, 'Fatfish Internet Group Ltd', '71 Ayer Rajah Cresent #02-15,\r\rSingapore 139951\r\r\r\rAttn : Mr KW Lau\r\rHP   : 93203588\r\rEmail: lau@fatfishgroup.com', 'sg_65_93203588', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(471, 'Pollux Properties Ltd.', '391A Orchard Road\r\r#08-07 Ngee Ann City Tower A\r\rSingapore 238873\r\r\r\rAttn : Ms Evie Zheng\r\rDID  : 6922 0336 (Ext:336)\r\rFax  : 6922 0338\r\rHP   : 8622 2800', 'sg_65_8622 2800', '', 'sg_65_6922 0338', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(472, 'Heng Long Leather', '50 Defu Lane 7\r\rSingapore 539356\r\r\r\rAttn : Ms Doris Ong \r\rEmail: dorisong@henglong.com', 'sg_65_91380369', '', 'sg_65_6284 8209', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(473, 'IN-INTERIOR PTE LTD', 'Attn : Mr John\r\rHP   : 96536732\r\rEmail: in_interior@singnet.com.sg', 'sg_65_96536732', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(474, 'Zenith Effectum LLP', '758 Yishun Street 72\r\r#07-452 Singapore 760758\r\r\r\rAttn : Ms Rita Farida \r\rHp   : 81197636 \r\rEmail: liev_ta@hotmail.com', 'sg_65_81197636', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(475, 'RESCO Surfaces Private Limited', '50 Kallang Pudding Road\r\r#08-06\r\rSingapore 349326\r\rAttn :  Mr Joseph YEO \r\rHp   :  93860073\r\rjy.resco@gmail.com', 'sg_65_93860073', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(476, 'The Audience Motivation Company Asia', 'Attn : Ms Lee Hui Ling\r\rTel  : 67350785\r\rFax  : 67357827\r\rHP   : 96538740\r\rEmail: huiling@amcasia.com', 'sg_65_96538740', '', 'sg_65_67357827', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(477, 'The Body Clinic', '10 Anson Road, Tanjong Pagar\r\rInternational Plaza, #18-11\r\rSingapore 079903 \r\r\r\rAttn : Mr Kavin\r\rHP   : 9109 8035\r\rEmail: kavin@thebodyclinic.com.sg', 'sg_65_91098035', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(478, 'Arch & Build Concept Private Limited', 'Block 1 Thomson Road\r\r#03-336R\r\rSingapore 300001\r\r\r\rAttn :  Mr Dennis Leong \r\rManaging Director \r\rTel  :  63522242\r\rFax  :  62537079\r\rym@abconcept.com.sg', 'sg_65_93391212', '', 'sg_65_62537079', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056801, 0, 1),
(479, 'Parawan Music Lounge', '646, Geylang Road\r\rSingapore 389576\r\r\r\rAttn : Mr Goh\r\rHP   : 9005 2653\r\rEmail: goh123@hotmail.com', 'sg_65_90052653', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(480, 'THE BIG OZ LLP', '8A Admiralty Street \r\rSingapore 757437\r\r\r\rAttn : Ms Angie\r\rHP   : 9228 8213\r\rEmail: bigoz_sg@yahoo.com.sg', 'sg_65_92288213', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(481, 'MINDEF', '325 Sembawang Road \r\rSingapore 757758\r\r\r\rAttn : Mr Leong Hon Wai, Camp Commandant\r\rHP   : 9633 3787\r\rEmail: leokaiser0@yahoo.com.sg', 'sg_65_96333787', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(482, 'Climb Central Pte Ltd', '#B1-01 Kallang Wave\r\r1 Stadium Place S(397628)\r\r\r\rAttn : Mr Christopher Xie\r\rHp   : (+65) 9297 6314\r\rTel  : (+65) 6702 7902\r\rEmail: chris@climbcentral.sg', 'sg_65_9297 6314', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(483, 'M Moser Associates', '8 Shenton Way, #02-01, AXA Tower, \r\rSingapore 068811\r\r\r\rAttn : Mr Halim Wahab (Senior Designer)\r\rDID  : 6438 4088 \r\rHP   : 9229 1479\r\rEmail: HalimW@mmoser.com / halim.wahab@gmail.com', 'sg_65_9229 1479', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(484, 'The Loco Group Pte Ltd', 'Super Loco Mexican Restaurant Bar\r\rThe Quayside, 60 Robertson Quay #01-13\r\rSingapore 238252\r\r\r\rAttn : Mr Jamie Amir\r\rHP   : (+65) 86911676\r\rTel  : (+65) 62358900\r\rEmail: jamie@super-loco.com', 'sg_65_86911676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(485, 'Turbo Solutions Pte Ltd', '53 Tuas View Loop\r\rSingapore 637703\r\r\r\rAttn : Mr Preyeash. C\r\rHP   : 9826 7009\r\rEmail: ts.tech@turbosolutions247.com / ts.finance@turbosolutions247.com', 'sg_65_98267009', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(486, '3 Petits Croissants Pte Ltd', '8 Chang Charn Road #02-01, \r\rLink (THM) Building, \r\rSingapore 159637 \r\r\r\rAttn : Mr Francis\r\rHP   : 96850996', 'sg_65_96850996', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(487, 'Personal_Mrs Png', '233 Paya Lebar Road\r\r#19-03 Le Crescendo\r\rSingapore 409044\r\r\r\rAttn : Mrs Png\r\rHP   : 90172398\r\rEmail: nicolepng1999@yahoo.com.sg', 'sg_65_90172398', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(488, 'Catalunya Pte Ltd', 'Fullerton Pavilion\r\r100 Beach Road\r\rShaw Tower #15-05/06\r\rSingapore 189702\r\r\r\rAttn : Ms Abby Cheong\r\rHP   : +65 90012253 \r\rEmail: m90012253@gmail.com/ abby@tonyspizza.sg', 'sg_65_90012253', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(489, 'Gan Hup Lee (1999 ) Pte Ltd', '16 Chin Bee Avenue \r\rSingapore 619939\r\r\r\rAttn :  Mr Kang \r\rHP   :  97339118\r\rhakleong.kang@ganhuplee.com', 'sg_65_97339118', '', 'sg_65_67410129', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(490, 'Anytime Fitness Jurong Gateway', '130 Jurong Gateway Road #01-235\r\rSingapore 600130\r\r\r\rAttn : Ms Andrea Bell (Club Owner)\r\rTel  : 65668481\r\rHP   : 83635076\r\rEmail: JurongGateway@anytimefitness.sg', 'sg_65_83635076', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(491, 'Three Management Pte Ltd', 'Attn : Ms Shirley\r\rHP   : 9875 3372\r\rEmail: contact@hotelnuve.com', 'sg_65_98753372', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(492, 'Chin Guan Hong (Singapore) Pte Ltd', '23 Fishery Port Road, \r\rSingapore 619738\r\r\r\rAttn : Mr Yio Jin Xian\r\rGeneral Manager\r\rTel  : 6265 2552 / 2752\r\rHP   : 97731460\r\rEmail: yiojinxian@chinguanhong.com', 'sg_65_97731460', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(493, 'Bay Hotel Singapore', '50 Telok Blangah Road\r\rSingapore 098828\r\r\r\rAttn : Ms Racquel Yap \r\rProcurement Officer\r\rTel  : 6818 6673\r\rHP   : 9385 9533\r\rEmail: procurement@bayhotelsingapore.com', 'sg_65_9385 9533', '', 'sg_65_6271 7628', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(494, 'CLUB DE COLOUR', '114 Middle Road, #09-00\r\rSingapore 188971\r\r\r\rAttn : Mr Vince\r\rHP   : 8282 5517\r\rEmail: vince_weikiat@hotmail.com', 'sg_65_8282 5517', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(495, 'Climb Asia Pte Ltd', 'Attn : Mr Lim June Pein (Director)\r\rHp: (65) 9437 0934\r\rTel: (65) 6292 7701\r\rFax: (65) 6292 2281', 'sg_65_9437 0934', '', 'sg_65_6292 2281', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(496, 'Enterprise Trading Services', '27 Jalan Malu Malu\r\rSingapore 769644\r\r\r\rAttn : Mr Chan Chee Wah\r\rHP : 65-91376525\r\rFax : 65-67551780\r\rEmail : ets.chan@gmail.com', 'sg_65_91376525', '', 'sg_65_67551780', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(497, 'Singapore Food Industries Pte Ltd', '234 Pandan Loop\r\rSingapore 128422\r\r\r\rAttn : Mr Soe Thein\r\rHP   : 98356675\r\rEmail: soe_thein@sats.com.sg', 'sg_65_98356675', '', 'sg_65_6774 1639', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(498, 'Clean Solutions Pte Ltd', '55 Ayer Rajah Cres, \r\r#02-23/24, Singapore 139949\r\r\r\rAttn : Mr Ooi Teong Yew\r\rTel  : 64710880 \r\rHP   : 90277882 \r\rEmail: ooi_teongyew@cleansolutions.com.sg', 'sg_65_9027 7882', '', 'sg_65_64723578', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(499, 'Dozo Restaurant Pte Ltd', 'Attn : Ms Jen\r\rHP   : 9850 5132\r\rEmail: info@dozo.com.sg', 'sg_65_98505132', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(500, 'Force Readiness Squadron', 'Blk 211 Tuas Naval Base Pioneer Sector 2\r\r\r\rAttn : ME2 William Quan \r\rTel  : 68642829 \r\rHP   : 91471025\r\rEmail: williamquan@hotmail.com', 'sg_65_91471025', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(501, 'Anytime Fitness', '23 Serangoon Central\r\rNex #04-73/74/75/75A\r\rSingapore 556083\r\r\r\rAttn : Mr Stewart HIne\r\rHP   : 9022 7856\r\rEmail: stewart.hine@anytimefitness.sg', 'sg_65_9022 7856', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(502, 'Chee Fatt Co. Pte Ltd', '54 Tanjong Penjuru \r\rSingapore 609035\r\r\r\rAttn : Mr Mike Lee\r\rTel  : 6294 2066\r\rHP   : 8660 7066\r\rEmail: leekl@cheefatt.com.sg', 'sg_65_8660 7066', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(503, 'Planhouse', 'Blk 304 Woodlands Street 31 \r\r#01-101, Singapore 730304\r\r\r\rAttn : Mr Rodney\r\rHP   : 98585597\r\rEmail: rodneycheam@planhouse.com.sg', 'sg_65_98585597', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(504, 'Yamato Asia Pte Ltd', '223 Mountbatten Road \r\r#01-07/08\r\rSingapore 398008\r\rTel  :  66327400\r\rFax  :  66048677\r\rHP   :  96280134\r\rrichard@yamatoasia.com', 'sg_65_96280134', '', 'sg_65_66048677', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(505, 'Soon Teck Furniture Centre Sdn Bhd', 'No 15-23, Lorong Bendahara 56A,\r\rTaman Mewah Ria, Off Jalan Raja Nong, 41000 Klang, Selangor\r\rAttn: Ms Lee Fang\r\rTel: 0172481366', 'sg_65_172481366', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(506, 'S - I Engineering Pte Ltd', '48 Toh Guan Road East\r\r#04-145, Enterprise Hub\r\rSingapore 608586\r\r\r\rAttn : Mr Marvin Chan\r\rTel  :  6425 2308\r\rHP   : 9685 5561\r\rEmail: marvin@fami.com.sg', 'sg_65_9685 5561', '', 'sg_65_6569 2398', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(507, 'Super Employment Specialist', 'Attn : Ms May \r\rHP   : 9180 5511\r\rEmail: michaelteo88@yahoo.com.sg', 'sg_65_9180 5511', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(508, 'Civil Service Club@Changi', 'No 2 Netheravon Road\r\rSingapore 508503\r\rAttn  :   Mr William Yuen \r\rManager,  Operations\r\r(Changi Clubhouse)\r\rTel   :   63916894\r\rFax   :   62926894\r\rhp    :   96524716\r\rwilliamyuen@csc.sg', 'sg_65_96524716', '', 'sg_65_62926894', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(509, 'Amore Fitness', 'Attn : Mr Kian Ong\r\rHP   : 9858 2520\r\rEmail: kianong@amorefitness.com', 'sg_65_9858 2520', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(510, 'The White House International Preschool Pte Ltd', '644 Dunearn Road\r\rSingapore 289627\r\r\r\rAttn : Mr Edmund Ng\r\rHp   : 98282809\r\rEmail: edmund@thewhitehouse.com.sg', 'sg_65_98282809', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(511, 'Ideagraph Pte Ltd', '5 Kaki Bukit Road 1, Eunos Technolink, #01-07, S415936\r\r\r\rAttn : Ms Melanie Loh\r\rHP   : 9150 6920\r\rTel  : 6842 2303\r\rFax  : 6842 2937\r\rEamil: melanie.loh@ideagraph.com.sg', 'sg_65_9150 6920', '', 'sg_65_6842 2937', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(512, 'Tee Hai Chem Pte Ltd', '18 Tuas Link 1 Tee Hai Building	\r\rSingapore 638599\r\r\r\rAttn : Mr Kelvin Ong\r\rLogistics Supervisor 	\r\rHP   : 9182 0151\r\rEmail: KelvinOng@teehaichem.com.sg', 'sg_65_9101 9509', '', 'sg_65_6863 1319', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(513, 'Eden Beauty Spa', 'Attn : Mr Kelvin\r\rHP: 97496106\r\rEmail: klcrx@yahoo.com.sg', 'sg_65_97496106', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(514, 'Bake Mission Pte Ltd', 'No. 8 Senoko South Road\r\r#03-03 Singapore 758095\r\r\r\rAttn : Ms Rachel Foo\r\rTel  : 6758 8955\r\rHP   : 8189 2589\r\rEmail: rachel.foo@bakemission.com.sg', 'sg_65_8189 2589', '', 'sg_65_6758 8956', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(515, 'Hengs ATF Pte Ltd', '60 Jurong West Street 91\r\rSingapore 649040\r\r                                                    Attn : Mr Heng Khee Huat\r\rTel : 9696 6658\r\rEmail: khhengheng@yahoo.com', 'sg_65_9696 6658', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(516, 'Crest Secondary School', '561 Jurong East Street 24\r\rSingapore 609561\r\r\r\rAttn : Mr Edward Neo\r\rTel  : 6899 2779 ext 107\r\rHP   : 9630 3276\r\rEmail: edward_neo@crestsec.edu.sg', 'sg_65_9630 3276', '', 'sg_65_68992668', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(517, 'AVIN INTEGRATED SERVICES PTE. LTD.', '355 Upper Paya Lebar Road\r\r#03-13 The Vue\r\rSingapore 534958\r\r\r\rAttn : Mr Vincent Ting \r\rMobile: (65) 9863 7892\r\rTel/Fax: (65) 6284 3352\r\rEmail: vincentting@singnet.com.sg', 'sg_65_9863 7892', '', 'sg_65_6284 3352', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(518, 'Qin Spa', 'Attn : Ms Tina\r\rHP : 9185 1285\r\rEmail : tina_wang82@yahoo.com', 'sg_65_9185 1285', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(519, 'Blends Pte Ltd', '3 Soon Lee Street \r\r#03-21 Pioneer Junction \r\rSingapore 627606\r\r\r\rAttn : Ms Jessica Gomez\r\rHp  : 9114 5266\r\rTel : 6264 738\r\rFax : 6264 2737\r\rEmail : jessica@blends.com.sg', 'sg_65_91145266', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(520, 'Jack Morton Worldwide', '40A Orchard Road \r\r#07-01 MacDonald House \r\rSingapore 238838 \r\r\r\rAttn : Ms Winnie Young\r\rProduction Coordinator\r\rTel  : 6499 8819\r\rHP   : 9643 7309\r\rEmail: winnie_young@jackmorton.com.sg', 'sg_65_9643 7309', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(521, 'AMORE FITNESS PTE LTD', '1 Commonwealth Lane,\r\r#02-28, One Commonwealth, Singapore 149544.\r\rDID : 63089816\r\rDIV : 63089806\r\rTEL : 63363822\r\rFAX : 63089838\r\rKianOng@AmoreFitness.com', 'sg_65_82238222', '', 'sg_65_63089838', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(522, 'Sri Srinivasa Perumal Temple', '397 Serangoon Road \r\rSingapore 218123\r\r\r\rAttn : Mr Soma\r\rHP   : 9853 9967\r\rEmail: sspt@heb.gov.sg', 'sg_65_98539967', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(523, 'TRINET TECHNOLOGIES PTE LTD', '21 Woodlands Close\r\r#07-12 Primz Bizhub\r\rSingapore 737854\r\r\r\rAttn : Ms Jennifer Sia\r\rTel  : 67497811\r\rHP   : 97570770\r\rEmail: tonhwee@trinet.com.sg', 'sg_65_97570770', '', 'sg_65_67497838', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(524, 'Pace Building Services Pte Ltd', 'Attn : Mr Chow Yew Hoong\r\rHP   : 9384 2074\r\rEmail: chowyewhoong@gmail.com', 'sg_65_93842074', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(525, 'S.F. Express (Singapore) Pte Ltd', '12 Arumugam Road, \r\rLion Industrial Building B, #05-02, \r\rSingapore 409958\r\r\r\rAttn : Mr Kenneth Lee\r\rSenior Human Resource & Admin Executive\r\rTel  : (65) 6603 0655  \r\rHP   : (65) 8323 8025  \r\rFax  : (65) 6603 0699\r\rEmail:Kennethlee@sf-express.com', 'sg_65_8323 8025', '', 'sg_65_6603 0699', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(526, 'Changi General Hospital', 'Attn : Ms Adeline Ong\r\rSenior Executive, Facilities Management \r\rDID  : 6850 4673   \r\rHP   : 8121 1406 \r\rEmail: adeline_ong@cgh.com.sg', 'sg_65_81211406', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(527, 'Combat Security', 'Attn : Mr Matthew\r\rTel  : 6291 4928\r\rHP   : 9073 8858\r\rEmail: matthew@cisa-pl.com', 'sg_65_90738858', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(528, 'Hong Leong Corporation Holdings Pte Ltd', '178 Paya Lebar Road, #01-01\r\rS(409303)\r\r\r\rAttn: Mr Alvin Tan\r\rTel: 6749 0588\r\rHP: 9858 8722\r\rEmail: alvintan@hlcorp.com.sg', 'sg_65_9858 8722', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(529, 'Silat Road Sikh Temple', '8 Jalan Bukit Merah\r\rSingapore 169541\r\r\r\rAttn : Mr Gurmukh Singh Khaira\r\rHP   : 90165331\r\rEmail: gurmukh@sikhs.org.sg', 'sg_65_90165331', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(530, 'Chong Pang Huat Holdings Pte Ltd', '8A Admiralty Street \r\r#04-27 Food Xchange @ Admiralty\r\rSingapore 757437 \r\r\r\rAttn : Mr Sim Kiang Huat\r\rEmail: tachuanconstruction@yahoo.com.sg', 'sg_65_93676363', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(531, 'Singapore OPPO Electronics Pte Ltd', '62 Ubi Road 1 #04-01\r\rOxley Bizhub 2\r\rSingapore 408734\r\r\r\rAttn : Mr Kelvin Ng\r\rHP   : 9650 8287\r\rEmail: kelvin.ng@oppo.com.sg', 'sg_65_9650 8287', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(532, 'Macjack Contract Service Pte Ltd', '61, Ubi Road 1 \r\r#03-34 Oxley Bizhib\r\rSingapore 408727\r\r\r\rAttn : Ms Jcqueline Barbara\r\rTel  : 6702 3913 \r\rHP   : 8187 2428\r\rEmail: macjack78@yahoo.com', 'sg_65_81872428', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(533, 'Mac Education Solutions', '625 Senja Road\r\r#26-152\r\rSingapore 670625\r\r\r\rAttn : Mr Caleb\r\rHP   : 8186 4895\r\rEmail: caleb@macedusolutions.com', 'sg_65_81864895', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(534, 'UWCSEA East Campus', '1 Tampines street 73\r\rSingapore 528704\r\r\r\rAttn : Mr Shahril Bin Zainuddin\r\rFacilities Department\r\rTel  : 63055344 (Main)\r\rHP   : 90676034\r\rEmail: shahriluwc@gapps.uwcsea.edu.sg', 'sg_65_90676034', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(535, 'Two Bakers', 'Attn : Ms Erica Yap\r\rHP   : 9068 6498\r\rEmail: erica.yap@two-bakers.com', 'sg_65_90686498', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(536, 'DJ Bakery', '6 Woodlands Terrace,\r\rSingapore 738431\r\r\r\rAttn : Ms Jin Kan\r\rHP   : 97423374\r\rEmail: djbakery@hotmail.com', 'sg_65_97423374', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(537, 'The Seafood Company Pte Ltd', 'Attn : Ms Xiu Zhen\r\rHP   : 9295 0777\r\rEmail: xiuzhen@foodbuddyz.com.sg', 'sg_65_92950777', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(538, 'Food Partner Singapore', 'Attn : Mr Loo\r\rHP   : 9025 6022\r\rEmail: foodpartnersingapore@gmail.com', 'sg_65_90256022', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(539, 'Welead Pte Ltd', '81 Sungei Kadut Drive\r\rSingapore 729565\r\r\r\rAttn : Mr Chay Wah Keong\r\rTel  : 6368 3633\r\rFax  : 6365 4002\r\rHP   : 9817 9760\r\rEmail: chaywk@welead.com.sg', 'sg_65_98179760', '', 'sg_65_63654002', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(540, 'MAccanic Sud Singapore Pte Ltd', '4 Battery Road\r\rBank of China Building, #25-01\r\rSingapore 049908\r\r\r\rAttn : Mr Daniel Wee\r\rHP   : 9737 0523\r\rEmail: daniel.wee@mecanicsud-singapore.com', 'sg_65_9737 0523', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(541, 'Mobile Workforce Solution', '1557 Keppel Road,\r\r#03-13/15\r\rSingapore 089066\r\rAttn :  Mr Louis Lee\r\rDirector\r\rHP   : 96585357\r\rTel  : 6271 2752\r\rDid  : 6225 3171\r\rFax  : 6225 3141\r\rlouislee@mobilewfs.com.sg', 'sg_65_96585357', '', 'sg_65_62253141', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(542, 'Jwo Holdings Pte Ltd', '21A Lorong Liput\r\rHolland Village\r\rSingapore 277733\r\r\r\rAttn : Mr Julien Perrinet\r\rTel  : 6291 9727\r\rHP   : 8198 9951\r\rEmail: julien@2amlab.org\r\r\r\r15/2/15 - 2amlab moved to ION Orchard Level 4, #04-12,', 'sg_65_81989951', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(543, 'Casa Italia Pte Ltd', 'Jurong Food Hub\r\r15 Jalan Tepong\r\r#02-15\r\rSingapore 619336\r\rAttn: Mr Shane Ee Yeo/\r\r      Graphic Designer\r\rTel : 62673061\r\rHP  : 92348176\r\rshane@casaitalia1962.com', 'sg_65_92348176', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(544, 'Happy Willow Pte Ltd', '1 Fusionopolis Way\r\r#B2-06 Connexis Tower  \r\rSingapore 138632\r\r\r\rAttn : Ms Lyn Tan\r\rHP   : 9337 7666\r\rEmail: lyntan18@gmail.com', 'sg_65_9337 7666', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(545, 'Chettiars'' Temple Society', 'Sri Layan Sithi Vinayagar Temple \r\r73 Keong Saik Road, \r\rSingapore 089167\r\r\r\rAttn : Mr Natarajan \r\rHP : 8348 5572\r\rTel : 6221 4853 \r\rFax : 6221 1728\r\rEmail : lsvtemple@singnet.com.sg', 'sg_65_83485572', '', 'sg_65_6221 1728', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(546, 'B-Construction Pte Ltd', 'Blk 1022 Tai Seng Ave, \r\r#05-3530\r\rTai Seng Industrial Estate\r\rSingapore 534415\r\r\r\rAttn : Ms Joyce Yap \r\rHp : 84187542\r\rEmail : joyce.n1c8@hotmail.com', 'sg_65_84187542', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(547, 'SYSTMZ PTE LTD', '1 Bukit Batok Crescent\r\r#09-07\r\rWCEGA Plaza\r\rSingapore 658064\r\rAttn   : Mr Joo Kwan \r\rDirector,  Projects \r\rOffice : 68617149\r\rFax    : 64650656\r\rMobile : 81386555\r\rjookwan@systmz.sg', 'sg_65_81386555', '', 'sg_65_64650656', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(548, 'Ngee Ann Secondary School', '1 Tampines Street 22\r\rSingapore 529283\r\r\r\rAttn : Ms Patricia Goh Poh Hong \r\rHP: 9821 7020\r\rEmail : patricia_goh@ngeeannsec.edu.sg', 'sg_65_9821 7020', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(549, 'Inuka Cafe @ Zoo Entrance', 'Singapore Zoological Gardens\r\r80 Mandai Lake Road\r\rSingapore 729826', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(550, 'Chawang Bistro @ Zoo Entrance', 'Singapore Zoological Gardens\r\r80 Mandai Lake Road\r\rSingapore 729826', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056802, 0, 1),
(551, 'Ah Meng Kitchen @ Zoo Entrance', 'Singapore Zoological Gardens\r\r80 Mandai Lake Road\r\rSingapore 729826', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(552, 'Casa Italia @ Zoo Entrance', 'Singapore Zoological Gardens\r\r80 Mandai Lake Road\r\rSingapore 729826', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(553, 'Mandai Mart @ Zoo Entrance', 'Singapore Zoological Gardens\r\r80 Mandai Lake Road\r\rSingapore 729826', 'sg_65_81684151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(554, 'Orange Home Nursing Homes Pte Ltd', 'Attn :  Ms Pauline Tan \r\rTel  :   64994699', 'sg_65_96930396', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(555, 'Cyber Cars Prime Workshop Pte Ltd', 'Attn : Mr Terence Tan\r\rHP : 9117 7182\r\rEmail : terenceewtan@yahoo.com', 'sg_65_91177182', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(556, 'MindChamps P5 & 6 Success Learning Centre @ USQ Pte Ltd', '101 Thomson Road, \r\rUnited Square #04-23/25 \r\rSingapore 307591\r\r\r\rAttn : Ms Mary Lau\r\rHP : 9380 3022\r\rEmail : marylau@mindchamps.org', 'sg_65_9380 3022', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(557, 'MindChamps P5 & 6 Success Learning Centre @ TSV Pte Ltd', '1 Vista Exchange Green, \r\rThe Star Vista #B1-15 \r\rSingapore 138617\r\r\r\rAttn : Ms Mary Lau\r\rHP : 9380 3022\r\rEmail : marylau@mindchamps.org', 'sg_65_9380 3022', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(558, 'Sentosa Leisure Management Pte Ltd', '33 Allanbrooke Road\r\rSingapore 099981\r\r\r\rAttn : Ms Charmaine Hoo Chia Mien\r\rHP   : 9116 6861\r\rEmail: charmaine_hoo@sentosa.com.sg', 'sg_65_91166861', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(559, 'Everbliss Cleaning & Services LLP', 'Blk 463B, Sembawang Drive\r\r#17-385\r\rSingapore 752463\r\r\r\rAttn : Mr Victor Foo\r\rHP : 96714336\r\rEmail : everblisscleaning@gmail.com', 'sg_65_96714336', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(560, 'The Snack Culture Company Pte Ltd', 'Attn : Ms Kimberly \r\rHP : 9338 4788\r\rEmail : kimberly@snackculture.com', 'sg_65_93384788', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(561, 'Ang Chin Moh Funeral Directors Pte Ltd', '53 Ubi Ave 1\r\r#01-56 Paya Ubi Industrial Park\r\rSingapore 408934\r\r\r\rAttn : Ms Jess Tham\r\rTel : 6747 7103 (ext 812)\r\rHP : 9323 2241\r\rFax : 6747 7322\r\rEmail : jess.tham@angchinmoh.com.sg', 'sg_65_9323 2241', '', 'sg_65_6747 7322', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(562, 'Lian Shing Construction Co Pte Ltd', '216 Tagore Lane, \r\rSingapore 787598\r\r\r\rAttn : Mr Chen Jun Long\r\rHP : 9744 1521\r\rEmail : junlong.chen@lianshing.com.sg', 'sg_65_9744 1521', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(563, 'Deluxe Lido Palace Pte Ltd', '317 Outram Road\r\r#05-01\r\rConcorde Shopping Centre\r\rSingapore 169075\r\rAttn  :  Mr Chris \r\rHP    :  81286902\r\rboonegoh@gmail.com', 'sg_65_81286902', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(564, 'Yew Tee Residences', 'Sub-MC 2, MCST 3516\r\r\r\rChoa Chu Kang North 6, \r\rBlk 25, #03-16\r\rSingapore 689580\r\r\r\rAttn : Mr Jack Tiang\r\rTel : 6314 9923 \r\rHP  : 9270 7243 \r\rEmail : ma@yewteeresidences.sg', 'sg_65_9270 7243', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(565, 'D''trax Design Private Limited', '51 Anson Road\r\rAnson Centre #08-51\r\rSingapore 079904\r\r\r\rAttn : Mr Ronald Goh Choon Meng\r\rHP : 90011489\r\rEmail : ronald@dtrax.com.sg', 'sg_65_90011489', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(566, 'Huang He Logistics Pte. Ltd.', 'Attn : Mr Mohd Yusuf\r\rHP : 8200 1899\r\rEmail : yusuf_huanghelog@yahoo.com', 'sg_65_82001899', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(567, 'Kingsville Pacific Pte Ltd', 'Attn : Mr Herman\r\rHP : 9633 0843\r\rEmail : herman@kingsville.com.sg', 'sg_65_96330843', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(568, 'Skyline Media Co., Ltd', 'Floor 7, Abacus Tower, 58 Nguyen Dinh Chieu Street, DaKao Ward, \r\rDistrict 1, \r\rHo Chi Minh City, Vietnam\r\rTel   : +84862915818\r\rFax   : +84862915819\r\rMobile: +84935952368\r\rcong.vu@skyline.vn', 'sg_65_84935952368', '', 'sg_65_84862915819', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(569, 'Eco & Sons Pte Ltd', '2 Woodlands Sector 1\r\r#05-17\r\rSpectrum 1\r\rSingapore 738068\r\rAttn :  Mr Elvin Goh\r\rhp   :  92373701\r\relvin@ecosons.com.sg', 'sg_65_92373701', '', 'sg_65_68620550', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(570, 'The Cat Museum', '8 Purvis Street \r\r#02-01/02\r\rSingapore 188587\r\rAttn  :  Ms Preetha Damo \r\rhp    :  90686407\r\rpree@artofvoice.com', 'sg_65_90686407', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(571, 'TDA Design Pte Ltd', 'Attn : Ms Esther\r\rHP : 9826 6758\r\rEmail : tree.dim.design@gmail.com', 'sg_65_9826 6758', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(572, 'Straits Express Restaurant & Bar', '1 Stadium Place\r\r#01-24/28 & 01-K13/K17\r\rSingapore 397628\r\r\r\rAttn : Mr Riezal Satli\r\rTel : 6339 0280\r\rFax : 6334 6944\r\rHP  : 8434 5672\r\rEmail : riezal35@yahoo.com.sg', 'sg_65_84345672', '', 'sg_65_63346944', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(573, 'Delco Art Interior Pte Ltd', 'No 6, Kallang Way 5 \r\rSingapore 349025\r\r\r\rAttn  : Ms Nerelyn S. Bolisay\r\rProject Secretary \r\rMobile: +65 9827 9449 \r\rTel   : +65 6284 9558 \r\rFax   : +65 6744 0230  \r\rEmail : nerelyn@delcoart.com.sg', 'sg_65_9827 9449', '', 'sg_65_6744 0230', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(574, 'Kasturi Technology Pte Ltd', '37 Lorong 23 Geylang\r\r#07-05 Yu Li Industrial Building\r\rSingapore 388371\r\r\r\rAttn : Mr Wong Moses\r\rHP : 9154 4698\r\rEmail : wong.moses@kasturi.com.sg', 'sg_65_9154 4698', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(575, 'One Stop Builders', 'Attn : Mr Terence\r\rHP : 97267967\r\rEmail : terence@one-stop-builders.com.sg', 'sg_65_97267967', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(576, 'I-deal House Pte Ltd', '780 Upper Serangoon Road,\r\r#01-04 Choon Kim House,\r\rSingapore 534649.\r\r\r\rAttn : Mr Derrick Ong SK\r\rProject Manager \r\rHp : 9852 9655\r\rEmail : derrick76@hotmail.sg', 'sg_65_98529655', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(577, 'Fortune Food Mfg Pte Ltd', '348 Jalan Boon Lay\r\rSingapore 619529\r\r\r\rAttn : Mr Bernard Ong\r\rHP : 96313956\r\rEmail : benard@fortunefood.com.sg', 'sg_65_96313956', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(578, 'Methodist Girls’ School', '11 Blackmore Drive \r\rSingapore 599986 \r\r\r\rAttn : Mr Yeoh Yong Kee\r\rDID : 6796 2417  \r\rH/P : 9654 8120 \r\rFax : 6462 4166\r\rEmail : yeoh_yong_kee@moe.edu.sg', 'sg_65_9654 8120', '', 'sg_65_6462 4166', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(579, 'BREWER''S COFFEE PTE LTD', '1 Senoko Avenue \r\r#05-08 FoodAxis@Senoko\r\rSingapore 758297\r\rAttn : Mr Kevin Zheng\r\rTel. : 65560880\r\rFax. : 65560330\r\rHP   : 91852135\r\rkevin@brewers.com.sg', 'sg_65_91852135', '', 'sg_65_65560330', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(580, 'JJ Medic Pte Ltd', 'Attn: Ms Joanne\r\rHP: 8222 8522\r\rEmail: jchan@jjmedic.com', 'sg_65_82228522', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(581, 'Uniquely Secret', 'No. 1021 Upper Serangoon Road, \r\r#02-01 Singapore 534759\r\r\r\rAttn: Ms Perlin Chan\r\rHP: 9326 8882\r\rEmail: uniquely.secret@gmail.com', 'sg_65_93268882', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(582, 'TreeBox Solutions Pte Ltd', '71 Ayer Rajah Crescent, #06-03, Singapore 139951\r\r\r\rAttn: Andrew Lee\r\rProject Director\r\rHP: 9877 3613\r\rTel: 6873 2886\r\rFax: 6873 2996\r\rEmail: andrew@treeboxsolutions.com', 'sg_65_98773613', '', 'sg_65_68732996', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(583, 'Mercury Backpackers'' Hostel', '57 Lavender Street\r\rSingapore 338714\r\r\r\rAttn : Mr Andy\r\rHP : 9367 7722\r\rEmail : andy161172@hotmail.com / admin@mercurypac.com', 'sg_65_93677722', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(584, 'Litaflex Plastics Mfg Pte Ltd', 'No 3307, Jalan Tanjung 27/4,\r\rKawasan Perindustrian Indahpura,\r\r81000 Kulaijaya, Johor', 'sg_65_6598301687', '', 'sg_65_6567591121', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(585, 'Scientific Procurement (S) Pte Ltd', '1 Bukit Batok Crescent\r\rWCEGA Plaza #07-21\r\rSingapore 658064\r\r\r\rAttn: Mr Max \r\rHP: 96536507\r\rTel: 6570 0518\r\rFax: 6570 0538\r\rEmail: max@sps-sg.com', 'sg_65_96536507', '', 'sg_65_6570 0538', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(586, 'Ping Yi Secondary School', '61 Chai Chee Street\r\rSingapore 468980\r\r\r\rAttn : Mr Rosman Bin Atan\r\rOperations Manager\r\rrosman_atan@moe.edu.sg\r\rTel : 6245 4037 \r\rHP : 9176 1928\r\rEmail : rosman_atan@moe.edu.sg', 'sg_65_91761928', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(587, 'SENNEX DESIGN  PROFESSIONALS', '315 Outram Road #06-10\r\rTan Boon Liat Building\r\rSingapore 169074\r\r\r\rAttn : Mr Kyan Wong\r\rProject Manager\r\rM: +65 9671 5789\r\rT: +65 6327 9489\r\rF: +65 6399 2343\r\rkyan@sennexconsultants.com', 'sg_65_9671 5789', '', 'sg_65_6399 2343', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(588, 'Sennex Consultants Pte Ltd', '315 Outram Road #06-10\r\rTan Boon Liat Building\r\rSingapore 169074\r\r\r\rAttn : Mr Kyan Wong\r\rProject Manager\r\rM: +65 9671 5789\r\rT: +65 6327 9489\r\rF: +65 6399 2343\r\rkyan@sennexconsultants.com', 'sg_65_9671 5789', '', 'sg_65_6399 2343', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(589, 'Singapore University of Technology and Design', '8 Somapah Road\r\rSingapore 487372\r\r\r\rAttn : Mr Andrew Ang\r\rSenior Manager\r\rHP   : 9383 3486\r\rTel  : 6303 6600\r\rFax  : 6779 5161\r\rEmail: andrewang@sutd.edu.sg', 'sg_65_93833486', '', 'sg_65_67795161', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(590, 'ZYCC SINGAPORE PTE. LTD.', '80 Marine Parade Road \r\r#11-06 Parkway Parade \r\rSingapore 449269\r\r\r\rAttn : Mr Ryusuke Kurumada? Division Manager\r\rTel  : 6348 9981   \r\rHP   : 8533 8811 \r\rFax  : 6348 9983\r\rEmail: kurumada@zycc.com.sg', 'sg_65_8533 8811', '', 'sg_65_6348 9983', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(591, 'Acromec Engineers Pte Ltd', '4 Kaki Bukit Avenue 1, \r\r#06-03 Kaki Bukit Industrial Estate, \r\rSingapore 417939\r\r\r\rAttn : Mr Dylan Eng\r\rHP : 92282607\r\rTel : 6743 1300\r\rFax : 6743 1159\r\rEmail : dylan.eng@acromec.com', 'sg_65_92282607', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(592, 'Sembawang Community Club', '2125 Sembawang Road, \r\rSingapore 758528 \r\r\r\rAttn : Mr Muhammad Bin Ahmad \r\rHead (Community Arts and Culture) \r\rHP  : 9049 4955\r\rTel : 6758 4183\r\rFax : 752 7388  \r\rEmail : Muhammad_AHMAD@pa.gov.sg', 'sg_65_90494955', '', 'sg_65_6752 7388', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(593, 'The Feline Bridal Pte Ltd', 'Attn : Ms Rachel\r\rHP : 9145 0630\r\rEmail : rachel@thefelinebridal.com', 'sg_65_91450630', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(594, 'Wong Fong Engineering Works (1988) Pte Ltd', '79 Joo Koon Circle\r\rSingapore 629107\r\r\r\rAttn : Mr CK Lim\r\rHP : 9066 2817\r\rEmail : cklim@wongfong.com', 'sg_65_90662817', '', 'sg_65_6861 3230', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(595, 'Centrillion Pte Ltd', '50 Tuas Ave 11\r\r02-42 Tuas Lot\r\rSingapore 639107\r\r\r\rAttn: Ms Hooi Lee\r\rHp: 8223 6718\r\rTel: 6659 5393\r\rFax: 6570 1489\r\rEmail: hl.yap@centrillion.com.sg', 'sg_65_8223 6718', '', 'sg_65_6570 1489', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(596, 'SingHealth Residency', 'KK Women’s and Children’s Hospital, \r\rChildren Tower Level 4, GME Office, \r\r100 Bukit Timah Road \r\rSingapore 229899\r\r\r\rAttn : Ms Belinda Huang Linghui \r\rSnr Program Executive, OBGYN Residency\r\rDID : 6394 8483 \r\rHP  : 9842 5257\r\rEmail: Belinda.huang.l.h@singhealth.com.sg', 'sg_65_9842 5257', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(597, 'Trinity Christian Centre', '', 'sg_65_91444428', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(598, 'Lian Hup Seng Furniture Contractor Co.', '253 Jurong East Street 24, \r\rSingapore 600253\r\r\r\rAttn : Mr Lim \r\rHP : 9858 4023\r\rEmail : gplim@lianhupseng.com', 'sg_65_98584023', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(599, 'MC Design & Solutions', 'Blk 3007 Ubi Road 1 #03-438        Singapore 408701\r\r\r\rAttn : Mr Mike Khoo\r\rHP : 8613 2440\r\rEmail : mikekhoo@mcds.com.sg', 'sg_65_8613 2440', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(600, 'Nicholas Ng', 'Cluster B Management Office \r\r984 Upper Changi Road North \r\rSingapore 506969\r\rAttn  :  Mr Nicholas Ng \r\rDeputy Superintendent of Prisons\r\rOC Information Communication & Technology', 'sg_65_94759457', '', 'sg_65_65467641', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(601, 'Singapore Prison Service', 'Cluster B Management Office \r\r984 Upper Changi Road North \r\rSingapore 506969\r\rAttn :  Mr Nicholas Ng \r\rDeputy Superintendent of Prisons\r\rOC Information Communication & Techhnology \r\rTel :   65465981\r\rFax :   65467641\r\rNg_Keng_Kun@pris.gov.sg', 'sg_65_94759457', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(602, 'Wilsin Office Furniture (S) Pte Ltd', '10 Tagore Drive,\r\rSingapore 787625\r\r\r\rAttn : Mr Andrew Tay\r\rDDI : 6430 0179 \r\rTel : 6292 0123\r\rFax : 6391 0123\r\rEmail : andrew@wilsin.com.sg', 'sg_65_90045213', '', 'sg_65_6391 0123', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(603, 'Designhub International Pte Ltd', '1 Commonwealth Lane \r\r#08-11, One Commonwealth\r\rSingapore 149544\r\r\r\rAttn : Mr Daniel Ang\r\rInterior Designer\r\rTel : +65 6272 2422\r\rFax : +65 6274 2511\r\rHP  : +65 9844 1346\r\rEmail : danielang@designhubintl.com', 'sg_65_9844 1346', '', 'sg_65_6274 2511', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(604, 'First Avenue Interiors', '1 Kaki Bukit Ave 3, #08-16 \r\rSingapore 416087 \r\r\r\rAttn : Mr Alex Leng\r\rHP : 9615 3828\r\rTel: 6745 0256\r\rFax: 6743 3178\r\rEmail : firstave@singnet.com.sg', 'sg_65_96153828', '', 'sg_65_67433178', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(605, 'MediaCorp TV Singapore Pte Ltd', 'Attn : Ms Eng Guan Rong \r\rAssistant Producer\r\rHP : +65 85227200\r\rEmail : EngGuanRong@mediacorp.com.sg', 'sg_65_85227200', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(606, 'British American Tobacco (S) Pte Ltd', 'Attn : Mr Tay \r\rHP : 9615 8861\r\rEmail : tay_chiap_how@bat.com', 'sg_65_9615 8861', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(607, 'Leong Guan Food Manufacturer Pte Ltd', 'Leong Guan Food Trading Pte Ltd\r\r\r\rAttn : Mr Yan Heng\r\rQA/QC Officer\r\rTel: +65 67547911 Ext 131    \r\rFax: +65 67561531    \r\rHP : +65 83168699   \r\rEmail: qaqc@leong-guan.com', 'sg_65_83168699', '', 'sg_65_67561531', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(608, 'Lead Interiors Pte Ltd', 'No. 7 Toh Guan Road East\r\r#08-16/17 Alpha Industrial Building \r\rSingapore 608599\r\r\r\rAttn : Ms Karen Seet\r\rSales Manager\r\rTel: 6562 2838\r\rFax: 6562 5868\r\rMobile: 8113 4516\r\rEmail: karenseet@leadinteriors.com.sg', 'sg_65_8113 4516', '', 'sg_65_6562 5868', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(609, 'Diamond Sky Jewellery Pte Ltd', 'No. 2 Kallang Ave \r\r#01-01 to 01-06\r\rCT Hub 1 Singapore 339407\r\r\r\rAttn : Mr Leonard Teoh\r\rHP  : 9633 3032\r\rTel : 6444 1898\r\rFax : 6636 1071\r\rEmail : leotok6488@yahoo.com.sg', 'sg_65_96333032', '', 'sg_65_6636 1071', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(610, 'AsiaMedic Limited', 'Attn : Ms Kellyn\r\rHP   : 9642 7736\r\rEmail: kellyn@asiamedic.com.sg', 'sg_65_96427736', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(611, 'JNE', 'Jl. Tomang Raya No. 11\r\rJakarta Barat 11440\r\rIndonesia \r\rAttn :   Mr Johari Zein \r\rManaging Director', 'sg_65_62816928337', '', 'sg_65_62215671413', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(612, 'J-PLAN ASSOCIATES PTE LTD', '6  Sungei Kadut Way\r\rSingapore 728786\r\r\r\rAttn : Ms Esther Soo\r\rHp: 8511 7331  \r\rTel: 6382 2676  \r\rFax: 6382 2896 \r\rEmail: esther@jplan.com.sg', 'sg_65_8511 7331', '', 'sg_65_6382 2896', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(613, 'GOODILICIOUS CORPORATION', 'Attn : Ms Angie Liao\r\rHP : 8102 2288\r\rangie@goodilicious-sg.com', 'sg_65_8102 2288', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(614, 'Amas Tan', '', 'sg_65_90115515', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(615, 'Progreso Networks (S) Pte Ltd', '67 Ubi Road 1 \r\rOxley Bizhub #07-10\r\rSingapore 408780\r\r\r\rAttn : Mr Victor Tang\r\rHP: 97351700\r\rEmail: victor@progreso.com.sg', 'sg_65_97351700', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(616, 'SingaporeBowling Pte Ltd', '100 Tyrwhitt Road \r\r#02-05\r\rJalan Besar Swimming Complex\r\rSingapore 207542\r\r\r\rAttn  :  Mr Amas Tan \r\rHP    :  90115515\r\ramas@singaporebowling.org.sg', 'sg_65_90115515', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056803, 0, 1),
(617, 'SUNTRONIC DESIGN AND CONSTRUCTION PTE LTD', '11 Upper Wilkie Road\r\rSingapore 228120\r\r\r\rAttn : Ms Jane Wong\r\rProject Manager\r\rHP : 96821827\r\rEmail : jane.wong@suntronicdc.com', 'sg_65_96821827', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(618, 'AD interior Pte Ltd', 'Attn : Mr Jimmy\r\rHp : 96853434\r\rEmail : 8411jimmy@gmail.com', 'sg_65_96853434', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(619, 'i.Dezign Pte Ltd', 'Block 1003 Toa Payoh Industrial Park\r\r#03-1521\r\rSingapore 319075\r\r\r\rAttn : Ms Linna Lim\r\rDirector\r\rHp  : 9118 7972\r\rTel : 6358 2757\r\rEmail : linna@idezign.com.sg', 'sg_65_9118 7972', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(620, 'Sans Services Enterprise LLP', '186 Woodlands Industrial Park E5\r\r#04-01H\r\rSingapore 757515\r\r\r\rAttn : Mr Edward Ang\r\rHp   : 91884610\r\rEmail: sansservices@gmail.com', 'sg_65_91884610', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(621, 'Brunches Pte Ltd', '96 Rangoon Road\r\rSingapore 218381\r\r\r\rAttn : Ms Gina Lau\r\rHP : 9738 9040\r\rEmail : gina_lau@hotmail.com', 'sg_65_97389040', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(622, 'ThermoFisher Scientific', 'Blk 33, Marsiling Industrial Estate Road 3, #07-06, \r\nSingapore 739256\r\n', 'sg_65_63629300', 'SengChieh.Lee@thermofisher.com', 'sg_65_63688182', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', 'www.lifetechnologies.com', '', 1, 11, 1477056804, 1477299560, 1),
(623, 'YDL CONSTRUCTION PTE LTD', 'Blk 5 Ang Mo Kio Ind. Park 2A, \r\r#06-01 AMK Tech II, \r\rSingapore 567760\r\r\r\rAttn : Ms Kris Yap\r\rTel : 6886 9091 / 6481 9585         \r\rFax : 6886 9023         \r\rHP  : 9862 2832\r\rEmail: kris.yap@ydl.com.sg', 'sg_65_9862 2832', '', 'sg_65_6886 9023', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(624, 'TechGems Engg & Construction Pte Ltd', '12 Benoi Place, \r\rSingapore 629932\r\r\r\rAttn : Mr Rajesh\r\rHP   : 9339 9408\r\rTel  : 6515 4998 \r\rEmail: rajesh@techgems.com.sg', 'sg_65_9339 9408', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(625, 'Angsana Primary School', '3 Tampines Street 22 \r\rSingapore 529366\r\r\r\rAttn : Mr Muhamad Hirwan \r\rOperations Manager\r\rHP   : 9742 5461\r\rTel  : 6783 0427\r\rFax  : 6784 5293\r\rEmail: muhamad_hirwan@moe.edu.sg', 'sg_65_97425461', '', 'sg_65_67845293', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(626, 'Tokyo Salon & Education Pte Ltd', '113 Devonshire Road  \r\rSingapore 239878\r\r\r\rAttn : Ms Yuki Ito', 'sg_65_82331786', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(627, 'Tamashii Sake & Liquor Pte Ltd', '80 south bridge road \r\rGolden castle building \r\r#01-01 S(058710)\r\r\r\rAttn: Patrick Tan', 'sg_65_96861002', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(628, 'Nanyang Primary School', '52 King''s Road \r\rSingapore 268097\r\r\r\rAttn : Mr Saravanan\r\rHP   : 9171 3860\r\rTel  : 6467 2677 /6468 6913\r\rFax  : 6468 6913\r\rEmail: Saravanan_sadanandom@nyps.edu.sg', 'sg_65_91713860', '', 'sg_65_64686913', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(629, 'Ingersoll Rand SEA Pte Ltd', '42 Benoi Road \r\rSingapore 629903\r\r\r\rAttn : Mr Raymond Chew Ho Kiat \r\rEHS Leader \r\rTel  : +65 6860 6735 \r\rHP   : +65 8126 3730 \r\rho_kiat_chew@irco.com', 'sg_65_8126 3730', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(630, 'Solvay Specialty Chemicals Asia Pacific Pte Ltd', '1 Biopolis Drive \r\r#05-01/06 AMNIOS\r\rSingapore 138622\r\r\r\rAttn : Ms Chong KitChing \r\rDID  : 6394 3243\r\rFax  : 6394 3376\r\rHP   : 9113 0828\r\rEmail: kitching.chong@solvay.com', 'sg_65_91130828', '', 'sg_65_6394 3376', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(631, 'Yuan Food Holdings Pte Ltd', 'Attn : Ms Fong Yee\r\rHP   : 8688 8412\r\rEmail: fongyee@yfh.com.sg', 'sg_65_86888412', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(632, 'IS Design Pte Ltd', 'Attn : Mr Alvin\r\rHP   : 9831 6848\r\rEmail: isdesignpl@yahoo.com.sg', 'sg_65_9831 6848', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(633, 'SG Hardware Link LLP', 'Ang Mo Kio Industrial Park 2\r\rBlk 5054 #01-1117\r\rSingapore 569557\r\r\r\rAttn : Mr Alvin\r\rManager\r\rHP   : 9477 5548\r\rEmail: sghardwarelink@gmail.com', 'sg_65_94775548', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(634, 'Ar-Rayyan Corperation Pte. Ltd.', '34 Joo Chiat Rd\r\rSingapore 427364\r\r\r\rAttn : Ms Sheril \r\rAdministrative Assistant \r\rTel  : 6858 0298\r\rHP   : 8437 8421\r\rEmail: sheril_alis@hotmail.com', 'sg_65_8437 8421', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(635, 'SP PowerAssets Ltd', '10 pasir Panjang Road\r\r#03-01 Mapletree Business City\r\rSingapore 117438\r\r\r\rAttn : Mr Chiam Heng Suan\r\rPr Engineer (Distribution Control & Customer Service Section)\r\rTel  : 6272 2806 / 9389 5729\r\rFax  : 6775 1071', 'sg_65_93895729', '', 'sg_65_67751071', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(636, 'Gom Projects', '35 Kallang Pudding Road, \r\r#02-07\r\rSingapore 349314\r\r\r\rAttn : Mr David\r\rHP   : 9182 7486\r\rEmail: gomprojects@hotmail.com', 'sg_65_91827486', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(637, 'Gamersaurus Rex', '259A Upper Thomson Rd\r\rSingapore 574386\r\r\r\rAttn : Mr Stevn\r\rHP   : 9456 4867\r\rEmail: contact@gamersaurusrex.com', 'sg_65_9456 4867', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(638, 'Havelock Racing', 'Attn  : Mr Kenneth Yeo\r\rHP    : 9146 4994\r\rEmail : havelockracing@gmail.com', 'sg_65_9146 4994', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(639, 'Fong Soon Electrical Service', 'Blk 329 Ubi Avenue 1\r\r#10-623 \r\rSingapore 400329\r\r\r\rAttn : Mr Ong Poh Soon\r\rHP   : 9069 4647', 'sg_65_9069 4647', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(640, 'Chica Linda Corporate Office', '328 North Bridge Road, \r\r#03-06 Raffles Shopping Arcade, \r\rSingapore 188719\r\r\r\rAttn  : Mr Alex\r\rHP    : 96623786\r\rEmail : amcbride@garchahotels.com', 'sg_65_96623786', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(641, 'Eighteen by Three', 'No.3 Stanley Street\r\rSingapore 068722\r\r\r\rAttn  : Mr Kash Randhawa\r\rGeneral Manager\r\rTel   : 9127 5926\r\rEmail : k.kaur@18by3.com', 'sg_65_9127 5926', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(642, 'Gap Year Hostel', '322 Lavender Street\r\rSingapore 338821\r\r\r\rAttn  : Mr Dennis\r\rHp    : 9018 0943\r\rEmail : dennis@gapyearhostel.com', 'sg_65_90180943', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(643, 'Design Gallery Pte Ltd', '120 Lower Delta Road, \r\rCentex Centre #08-01/02 \r\rSingapore 169208\r\r\r\rAttn : Mr Erix Leong\r\rHP : 9795 9075\r\rEmail : erixleong@designgallery.com.sg', 'sg_65_97959075', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(644, 'Mr Lee', '', 'sg_65_97667299', '', 'sg_65_65444378', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(645, 'Republic of Singapore Navy', 'Attn  :  Mr Lee\r\rhp    :  97667299\r\rfax   :  65444378', 'sg_65_97667299', '', 'sg_65_65444378', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(646, 'SINGA PURE PTE LTD', '30 Tai Seng Street \r\r#07-02 Bread Talk Building IHQ \r\rSingapore 534013\r\r\r\rAttn : Mr Hugo David \r\rHp : 8523 7860\r\rEmail : 13hugodavid@gmail.com', 'sg_65_85237860', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(647, 'Icatcher Productions Pte Ltd', '1 Yishun Industrial St 1\r\rA Posh Biz Hub \r\rSingapore 768160', 'sg_65_91845361', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(648, 'SP PowerGrid Ltd', 'As agent for and on behalf of PowerGas Ltd \r\r\r\rAttn : Mr Muhammad Shahid\r\rGas Facilities Operations & Maintenance (GFOM)\r\rDID : 6316 2676\r\rHP  : 9651 9201\r\rEmail : muhdshahidmj@singaporepower.com.sg', 'sg_65_9651 9201', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(649, 'D Square Projects Pte Ltd', '8 Kaki Bukit Ave 4\r\rPemier @ Kaki Bukit\r\r#01-41/42/43\r\rSingapore 415875\r\rAttn :   Mr Jackson Seet \r\rhp   :   96697500\r\renquiry@dsquare.com.sg', 'sg_65_96697500', '', 'sg_65_67489568', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(650, 'Tang KL Engrg Pte Ltd', 'Attn :  Mr Tang \r\rhp   :  93891925\r\rengrg.tang@gmail.com', 'sg_65_93891925', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(651, 'Milk & Honey Gelato', 'Attn : Mr Edwin\r\rHP : 98380687\r\rEmail : edwin@milkhoney.sg', 'sg_65_98380687', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(652, 'Hd CLSA', 'HQ Basic Military Training Centre\r\r21 Pulau Tekong Besar\r\rSingapore 508450 \r\r\r\rAttn : Mr Gary Kwang', 'sg_65_91375037', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(653, '1A CATERING PTE LTD', '3015 Bedok North St 5 \r\r#02-09, Singapore 486350\r\r\r\rAttn : Mr Ashley Ow Hong Chan (Manager)\r\rTel : 6226 2226 \r\rFax : 6441 4363 \r\rHP :  9822 6193\r\rEmail : ashley.ow@me.com', 'sg_65_9822 6193', '', 'sg_65_6441 4363', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(654, 'ARKRAY & Partners Pte. Ltd.', 'Singapore Laboratory\r\r31 Biopolis Way, \r\r#05-50 Nanos, \r\rSingapore 138669\r\r\r\rAttn : Mr Masashi TSUKADA\r\rManager, Senior scientist Team 7, R&D division\r\rTel : +65 6779 1933 \r\rHP  : +65 9157 0586\r\rEmail : tsukadam@arkray.co.jp', 'sg_65_9157 0586', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(655, 'Renaissance Hospitality Pte Ltd', '69/70 Bussorah Street, \r\rSingapore 199482\r\r\r\rAttn : Mr Sumeet Singla\r\rHP : 9227 2346\r\rEmail : singlas@fabbricca.sg', 'sg_65_9227 2346', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(656, 'Uniconnect Systems Pte Ltd', '15 Kaki Bukit Road 4\r\r#01-49\r\rSingapore 417808\r\r\r\rAttn : Ms Sandy Tok\r\rDirector\r\rTel  : 68760880\r\rFax  : 68760050\r\rHP   : 82509966\r\rsandytok@uniconnectsys.asia', 'sg_65_82509966', '', 'sg_65_68760050', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(657, 'YHS (Singapore) Pte Ltd', '3 Senoko Way \r\rSingapore 758057\r\r\r\rAttn : Mr Ng Kah Yip\r\rLogistics Manager \r\rDID : (+65) 6849 6797\r\rH/P : (+65) 9154 5755\r\rEmail : kahyipng@yeos.com', 'sg_65_9154 5755', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(658, 'THE DESIGN PRACTICE PTE LTD', '387 Joo Chiat Road,\r\rUnit 04-01, The Modules,\r\rSingapore 427623\r\r\r\rAttn: Mr Raymond Kua\r\rDesign Manager\r\rMobile: 9836 8281\r\rTel: 6337 6478  \r\rFax: 6336 2879\r\rEmail: raymond@thedesignpractice.sg', 'sg_65_9836 8281', '', 'sg_65_6336 2879', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(659, 'Personal_Mr Mohammed Haikal Roslan', 'Attn : Mr Mohammed Haikal Roslan\r\rHP : 9852 1511\r\rEmail : mohammed.haikal.roslan@gmail.com', 'sg_65_9852 1511', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(660, 'Mind and Body Health Consultancy Pte Ltd', '482 Serangoon Road \r\r#02-01 Singapore 218149\r\r\r\rAttn : Mr Koo Hui Ern\r\rHP : 9618 6934\r\rEmail : iankool@gmail.com', 'sg_65_9618 6934', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(661, 'PNC Singapore Traders Pte Ltd', 'Blk 34 Upper Cross Street,  #02-150\r\r\r\rMr Cherwyn Tan\r\rMarketing / Sale\r\rHP @ 98503655 \r\rwyn.pnc@gmail.com', 'sg_65_98503655', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(662, 'Xcel Industrial Supplies Pte Ltd', 'Blk 192 Pandan Loop \r\r#04-26 Pantech Business Hub\r\rSingapore 128381\r\r\r\rAttn : Mr Daryl Ong\r\rSales & Marketing Executive\r\rTel : 6777 1131  \r\rFax : 6777 5502  \r\rHP  : 9106 4895\r\rEmail : daryl@xcelindustrial.com', 'sg_65_9106 4895', '', 'sg_65_6777 5502', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(663, 'Converge Asia Pte Ltd', '20 Toh Guan Road \r\r#06-00 CJ Korea Express Building \r\rSingapore 6088839\r\r\r\rAttn : Mr Samsuri Matrady\r\rTel : 6799 8455\r\rHP  : 9170 1481\r\rEmail : samsuri.matrady@converge.com', 'sg_65_9170 1481', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(664, 'Pentair Valves And Controls Singapore Pte Ltd', '45 Tuas Ave 9\r\rSingapore 639189\r\r\r\rAttn : Mr Andrew Chua\r\rSnr EHS Specialist\r\rTel : 6869 8903\r\rHP  : 9730 0756\r\rFax : 6861 9598\r\randrew.chua@pentair.com', 'sg_65_9730 0756', '', 'sg_65_6861 9598', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(665, 'TRADEWINDS Food Industries Pte Ltd', '1002 Tai Seng Avenue\r\r#01-2534\r\rSingapore 534409\r\r\r\rAttn : Mr Alexson Quek\r\rHP : 8116 4242\r\rTel : 6385 8385\r\rFax : 6343 7370\r\rEmail : alexson@tradewindsfood.com.sg', 'sg_65_81164242', '', 'sg_65_63437370', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(666, 'All Saints Home', 'Attn : Mr Max Singh\r\rSenior Procurement Executive\r\rHP : 97240174\r\rEmail : max.singh@allsaintshome.org.sg', 'sg_65_97240174', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(667, 'MindChamps Reading & Writing @ TM Pte Ltd', '4 Tampines Central 5 \r\r#05-06/07 Tampines Mall\r\rSingapore 529510\r\r\r\rAttn : Ms Mary Lau\r\rHP : 9380 3022\r\rEmail : marylau@mindchamps.org', 'sg_65_9380 3022', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(668, 'Treasure Electronic System', 'Blk 235 Tampines St 21\r\r#08-523 Singapore 521235\r\r\r\rAttn : Mr Alan Aw\r\rHP : 9732 2386\r\rEmail : Treasureest@yahoo.com', 'sg_65_9732 2386', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(669, 'Fedelco Enterprise LLP', 'Attn : Mr Darren Teng\r\rHP : 9101 6601\r\rdarrenteng360@hotmail.com', 'sg_65_9101 6601', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(670, 'TLC Medical Practice Pte Ltd', '9 Scotts Road \r\r#011-05 Scotts Medical Center\r\rSingapore 228210\r\r\r\rAttn :  Ms Evelyn Chan\r\rhp   :  97880200 \r\rTel  :  63392432\r\rFax  :  63392431\r\reve@drgl.com', 'sg_65_97880200', '', 'sg_65_63392431', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(671, 'Zien Furniture (S) Pte Ltd', 'Attn : Mr Ray Ng\r\rHP : 9107 5121\r\rEmail : zienrayng@gmail.com', 'sg_65_91075121', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(672, 'Francois Marine Services Pte Ltd', '12, Joo Koon Crescent, \r\rSingapore 629013\r\r\r\rAttn : Ms Lucianna Yvonne Go\r\rPurchasing Officer\r\rDDI : 6727 2192 \r\rHP  : 9237 2399 \r\rEmail: Lucianna.Go@francoismarine.com', 'sg_65_9237 2399', '', 'sg_65_6776 8122', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(673, 'The Management Corporation Strata Title Plan No. 2342', 'TAGORE BUILDING\r\r6 Tagore Drive #B1-10 \r\rSingapore 787623\r\r\r\rAttn : Mr Tham Wen Fun \r\rHP : 9150 0080\r\rEmail : mcst2342@singnet.com.sg', 'sg_65_91500080', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(674, 'SHIRAZ FNB PTE LTD', 'Restaurant Manager\r\r\r\rAttn : Mr Vincent Goh\r\rTel : 6334 2282 \r\rHP  : 9652 0165\r\rFax : 6336 5857\r\rEmail : vincent@shirazfnb.com', 'sg_65_9652 0165', '', 'sg_65_6336 5857', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(675, 'Qool Enviro Pte Ltd', '41 Tuas View Place\r\rLinkpoint Place\r\rSingapore 637885\r\r\r\rAttn :  Mr Wyman Tan \r\rhp   :  93896699\r\rWymantan@qoolenviro.com', 'sg_65_93896699', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(676, 'Cactus Art Design and Furnishing Pte Ltd', '33 Ubi  Avenue 3      \r\r#05-40  Vertex  A      \r\rSingapore 408868  \r\r\r\rAttn : Ms Lily Gimfil   \r\rProject Designer\r\rHp  : 8268 0852\r\rTel : 6774 4515\r\rFax : 6773 5691\r\rEmail : lily@cactusart.com.sg', 'sg_65_8268 0852', '', 'sg_65_6773 5691', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(677, 'VisionTech Engrg Pte Ltd', 'A subsidiary of ST Aerospace Ltd)\r\r540 Airport Road\r\rSingapore 539938\r\rAttn : Mr Yong Teck \r\rTel  : 63806924\r\rFax  : 62876164\r\ryongteck@stengg.com', 'sg_65_92213467', '', 'sg_65_62876164', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(678, 'ST Aerospace', 'Attn : Mr Paramjit Singh\r\rHP : 9820 6140\r\rEmail : paramjitsingh@stengg.com', 'sg_65_98206140', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(679, 'JFORTE SPORTAINMENT PTE LTD', '26 Tai Seng Street\r\r#07-01, Singapore 534057\r\r\r\rAttn : Ms Maureen Chua, BBM\r\rSenior Vice President\r\rHR & Corporate Affairs\r\rDID : 6718 0505     \r\rHP  : 9650 8168\r\rFax : 6718 0501\r\rEmail : maureenchua@jlasia.com.sg', 'sg_65_9650 8168', '', 'sg_65_6718 0501', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056804, 0, 1),
(680, 'ISS M&E Pte. Ltd.', 'Block 8 Level 1 \r\rSingapore General Hospital \r\rSingapore 169608\r\r\r\rAttn : Mr Lionel Ng \r\rOperations Manager\r\rTel : 6321 4797 \r\rHP  : 9450 7899  \r\rFax : 6224 3302 \r\rEmail : lionel.ng@sg.issworld.com', 'sg_65_9450 7899', '', 'sg_65_6224 3302', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(681, 'Kakis Bistro & Bar', '', 'sg_65_96534252', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(682, 'Cofely FMO Pte Ltd', '108 Pasir Panjang Road \r\r#03-13 Golden Agri Plaza\r\rSingapore 118535\r\r\r\rAttn : Mr Anthony SIEW\r\rFacilities Manager\r\rDID : 6303 6699\r\rHP  : 9668 2331\r\rEmail : Anthony.SIEW@cofely.com.sg', 'sg_65_9668 2331', '', 'sg_65_6261 2608', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(683, 'DTZ Facilities & Engineering (S) Limited', 'Ang Mo Kio Police Division Headquarters \r\r51 Ang Mo Kio Avenue 9 #02-05 \r\rSingapore 569784\r\r\r\rAttn : Mr TOMY HO\r\rTechnical Executive\r\rFacility Management\r\rTel : 6218 1131 \r\rHP  : 9816 0159\r\rFax : 6218 1131\r\rEmail : tomy.ho@dtz.com', 'sg_65_9816 0159', '', 'sg_65_6218 1131', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(684, 'Nithia Anand', '', 'sg_65_98261922', '', 'sg_65_62862617', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(685, 'abc pte ltd', '', 'sg_65_12345678', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(686, 'Singapore Technologies Electronics Limited', '24 Ang Mo Kio Street 65\r\rSingapore 569061\r\r\r\rAttn : Mr Fu JianYong\r\rDID : 6413 8840\r\rHP  : 9325 2524\r\rEmail : fu.jianyong@stee.stengg.com', 'sg_65_93252524', '', 'sg_65_6482 4615', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(687, 'Garena Online Pte Ltd', '', 'sg_65_9858 5543', '', 'sg_65_6227 8691', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(688, 'Clickcell Technologies Pte Ltd', '797 Geylang Road, \r\r#03-01 Singapore 389679\r\r\r\rAttn : Mr CK Fong\r\rHP : 9006 9724\r\rEmail : ckfong@theclickcell.com', 'sg_65_9006 9724', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(689, 'Harvest Link International Pte Ltd', '71 Geylang Lorong 23\r\r#08 – 01 THK Building\r\rSingapore 388386\r\r\r\rAttn : Mr Roel Allan Jabines\r\rBusiness Development Manager\r\rHp  : 9338 6982\r\rTel : 6745 2203\r\rEmail : allan_jabines@harvestlink.com', 'sg_65_9338 6982', '', 'sg_65_6748 3280', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(690, 'De''Riya Services Pte Ltd', 'Attn : Mr Nuraidi\r\rHP : 8539 1097\r\rEmail : noor.nuraidi@live.co.uk', 'sg_65_85391097', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(691, 'Health Capital Pte Ltd', 'Attn : Ms Shio Peng\r\rHP : 8102 6377\r\rEmail : healthcapital@anytimefitness.sg', 'sg_65_81026377', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(692, 'Personal_Mr Simonn', 'No 5, Jalan Masjid, \r\r#04-10 Kembangan Court, \r\rSingapore 418924\r\r\r\rAttn : Mr Chia Simonn\r\rHP : 9660 8093\r\rEmail : chiasi@hotmail.com', 'sg_65_9660 8093', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(693, 'Merge O+R Pte Ltd', 'No. 81, Ubi Avenue 4\r\r#05-01 UB.1\r\rSingapore 408830\r\r\r\rAttn : Ms Shally Rosales\r\rInterior Designer\r\rTel : 6604 9909\r\rFax : 6604 9939\r\rHp : 9784 6333\r\rEmail: Shally@mergeotr.com', 'sg_65_9784 6333', '', 'sg_65_6604 9939', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(694, 'Drama Box Ltd', '14A - C\r\rTrengganu Street\r\rSingapore 058468\r\r\r\rAttn : Mr Toh Wee Peng\r\rHP : 9230 1004\r\rEmail : thwpng@gmail.com', 'sg_65_9230 1004', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(695, 'Living Room Office Systems Pte Ltd', '621 Aljunied Road #04-02\r\rLipo Building\r\rSingapore 389834\r\r\r\rAttn : Mr Albert\r\rTel : 6382 6800\r\rHP  : 9686 0455\r\rEmail : albert.tan@livingrms.com', 'sg_65_9686 0455', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(696, 'Bank of China Limited', '4 Battery Road \r\rBOC Building \r\rSingapore 049908 \r\r\r\rAttn : Mr Lee Jong Neng\r\rGeneral Administration Department\r\rTel : 6412 8388 \r\rFax : 6533 1129 \r\rHP  : 9011 5973\r\rEmail : jnlee_sg@mail.notes.bank-of-china.com', 'sg_65_90115973', '', 'sg_65_65331 129', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(697, '5115 Pte Ltd', '411 River Valley Road\r\rSingapore 248309\r\r\r\rAttn : Mr Han Chek Kwang\r\rHP : 9800 5637\r\rTel : 6732 5115\r\rEmail : enquiry@breadwerks.sg', 'sg_65_9800 5637', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(698, 'Dinelike Pte Ltd', 'Attn : Mr Toshiyuki Tohekura\r\rAssistant Director\r\rHP : 9050 2055\r\rEmail : tohekura@dinelike.sg', 'sg_65_9050 2055', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(699, 'Art Heritage Singapore Pte Ltd', 'Fort Canning Arts Centre, \r\r5 Cox Terrace \r\rSingapore 179620\r\r\r\rAttn : Ms Tian Hui Shi\r\rSupervisor, Ticketing and Client Services\r\rHP  : 9189 5863 \r\rTel : 6883 1588\r\rEmail : huishi.tian@pinacotheque.com.sg', 'sg_65_91895863', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(700, 'Bauerfeind Singapore Pte Ltd', 'Attn : Ms Eunice Goh \r\rSales Support Executive\r\rTel : 6396 3497\r\rHP  : 9694 6611\r\rFax : 6295 5062\r\rEmail : eunice.goh@bauerfeind.com', 'sg_65_9694 6611', '', 'sg_65_6295 5062', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(701, 'Joo Hwa Food Manufacturer', 'Blk 15 Woodland Loop\r\r#01-15\r\rSingapore 738322\r\r\r\rAttn : Mr Thomas\r\rHP : 9190 2288\r\rEmail : joohwafood@gmail.com', 'sg_65_9190 2288', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(702, 'Select Service Partner (S) Pte Ltd.', '60 Airport Boulevard \r\rChangi Airport Terminal 2 \r\r#036-093, P.O.Box 1050\r\rSingapore 819643\r\r\r\rAttn    :  Mr Ng Kian Tak\r\rOperation Manager\r\rMobile  :  93394654\r\rDirect  :  65464605\r\rSwitch  :  65464605\r\rFax     :  65464616  \r\rngkiantak@ssp.com.sg', 'sg_65_93394654', '', 'sg_65_65464616', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(703, 'Ecoplus Manufacturing Pte Ltd', '5 Yishun Industrial Street 1\r\r#07-07 North Spring Bizhub\r\rSingapore 768161\r\r\r\rAttn : Mr Mansell Lam \r\rHP : 9799 4588\r\rTel : 6367 7595\r\rEmail : mansell@ecoplus-mfg.com', 'sg_65_9799 4588', '', 'sg_65_6367 0787', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(704, 'SingNet Pte Ltd', '31B Exeter Road\r\rComcentre II\r\rSingapore 239733\r\r\r\rAttn : Ms Kee Hwee Yee\r\rHP : 9658 5799', 'sg_65_8612 3029', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(705, 'Kimly Supplier', '100 Jalan Sultan\r\r#05-36 Sultan Plaza\r\rSingapore 199001\r\r\r\rAttn : Mr Tan Kim Lee\r\rHP : 9755 2787\r\rEmail : kimlysup@singnet.com.sg', 'sg_65_9755 2787', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(706, 'TC Import & Export Pte Ltd', '42 Senoko Drive \r\rSingapore 758226 \r\r\r\rAttn : Ms Soh Li Yun (Manager)\r\rHp : 9684 4696\r\rTel : 6758 8585 \r\rFax : 6758 3228 \r\rEmail : liyun@tcimpexp.com.sg', 'sg_65_9684 4696', '', 'sg_65_6758 3228', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(707, 'West Pharmaceutical Services Singapore Pte Ltd', '15 Joo Koon Circle\r\rSingapore 629046\r\r\r\rAttn : Mr Praveen \r\rSenior Production Engineer (Compounding)\r\rHP : 8583 9697\r\rEmail : Praveen.Kv@westpharma.com', 'sg_65_85839697', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(708, 'LST Contract & Services Pte Ltd', '48 Toh Guan Rd East, \r\rSingapore 608586\r\r\r\rAttn : Mr Kelvin\r\rHP: 9749 0032\r\rEmail : kelvinlow@lstgroup.com.sg', 'sg_65_9749 0032', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(709, 'Premium Blenders Pte Ltd', '22 Tuas Avenue 12\r\rSingapore 639040\r\r\r\rAttn : Mr V.Saminathan\r\rHP : 9885 5385\r\rEmail : Saminathan@anbros.com.sg', 'sg_65_98855385', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(710, 'Apexid Pte Ltd', '1 Commonwealth Lane \r\r#07-13 One Commonwealth \r\rSingapore 149544\r\r\r\rAttn : Mr Ng Kok Leng\r\rProject Executive\r\rHP :  9382 2468\r\rDID:  6690 1681\r\rEmail : ngkokleng@apexid.com.sg', 'sg_65_9382 2468', '', 'sg_65_6659 5889', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(711, 'Seng Soon Huat Construction Pte Ltd', 'No32 Sungei Kadut Way \r\rTeambuild Industrial Building #03-06 \r\rSingapore 728787\r\r\r\rAttn : Mr Wes Ng \r\rProjects Manager\r\rHp : 9646 2159\r\rTel: 6269 7083\r\rFax: 6368 5323 \r\rEmail : wes@sshprojects.com.sg', 'sg_65_96462159', '', 'sg_65_63685323', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(712, 'Food Services (Sodexo)', 'National University Hospital\r\r5 Lower Kent Ridge Road, \r\rMain Building 1, Basement 1, Singapore 119074\r\r\r\rAttn :  Ms Mani GANDHAM\r\rQHSE Officer- NUH\r\rTel :  67725020 \r\rHP  :  81499580\r\rgandham.MANI@sodexo.com', 'sg_65_81499580', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(713, 'Sodexo Singapore Pte Ltd (NUH)', 'Sodexo Singapore Pte Ltd (NUH)\r\r1 Genting Link, #08-01, Perfect One Building, Singapore 349518\r\rAttn : Mani GANDHAM\r\rQHSE Officer- NUH\r\rTel  : 67438998\r\rHp   : 81499580\r\rFax  : 67452232\r\rgandham.MANI@sodexo.com', 'sg_65_81499580', '', 'sg_65_67452232', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(714, 'The Westin Singapore', '12 Marina View\r\rAsia Square Tower 2\r\rSingapore 018961\r\rTel : 69226851    \r\rHp  : 98949919\r\rMurali.Sebrenmani@westin.com', 'sg_65_98949919', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(715, 'Republic Motor Pte Ltd', '123 Kaki Bukit Avenue 1\r\r#01-01\r\rSingapore 415996\r\r\r\rAttn : Mr Jimmy / Mdm Jean \r\rTel  : 68425880\r\rhp   : 83333387\r\rjean@republicmotor.com', 'sg_65_83333387', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(716, 'Xtreme Fitness', '12 west coast walk \r\rsingapore 127157\r\r\r\rAttn : Mr Shawn Karthik\r\rHP : 8522 6746\r\rEmail : karthiksgnr@gmail.com', 'sg_65_85226746', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(717, 'Eat at Seven Pte Ltd', '3 Temasek Boulevard,\r\r#03-310/311/312/313/314/315/316 & 307\r\rSuntec City Mall, Singapore 038983\r\r\r\rAttn : Mr Erich \r\rTel : 6339 1440', 'sg_65_90604430', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(718, 'Hoo Huat Engineering Pte Ltd', 'No 2, Pioneer Sector Walk,\r\rSingapore 627828\r\r\r\rAttn : Ms Jasmine Koh\r\rHP : 8606 2678\r\rEmail : jasmine@hoohuat.com.sg', 'sg_65_86062678', '', 'sg_65_6558 8698', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(719, 'Gateway Electrical & Construction Pte Ltd', 'Attn :  Mr Raj \r\rHP   :  87005044\r\rgateway.electrical@yahoo.com.sg', 'sg_65_87005044', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(720, 'Sun City Maintenance Pte Ltd', '1H Yong Siak Street, \r\rSingapore 168641\r\r\r\rAttn : Mr Nigel Ho\r\rHP : 9763 9039\r\rEmail : nigel@sun-city.com.sg', 'sg_65_97639039', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(721, 'Maxunion Pte Ltd', 'No 12, Tannery  Lane \r\r#07-01, Singapore 347775\r\r\r\rAttn : Ms Linda Chan\r\rTel : 6747 0002  \r\rFax : 6844 6580\r\rEmail : maxunion0701@gmail.com / linda@maxunion.com.sg', 'sg_65_6747 0002', '', 'sg_65_6844 6580', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(722, 'Twin Disc (Far East) Pte Ltd', '6 Tuas Avenue 1\r\rSingapore 639491\r\r\r\rAttn : Mr Steven Koo\r\rService Manager\r\rTel : 6267 0034 \r\rFax : 6264 2080\r\rHP  : 9072 2222\r\rEmail : koo.steven@twindisc.com', 'sg_65_9072 2222', '', 'sg_65_6264 2080', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(723, 'Nisel Construction & Engineering Pte Ltd', '308A, Anchorvale Road\r\r#02-02\r\rSingapore 541308\r\rAttn :  Mr Raj\r\rHP   :  87005044\r\rniselconstruction@gmail.com', 'sg_65_87005044', '', 'sg_65_64894483', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(724, 'SPACElogic Pte. Ltd.', 'Blk 30 Kallang Place \r\r#07-04/05 \r\rSingapore 339159\r\r\r\rAttn : Ms Geraldine Cheah\r\rTel : +65 6745 1733\r\rFax : +65 6745 0733\r\rMobile : +65 9846 2316\r\rEmail : geraldine.cheah@spacelogic.com.sg', 'sg_65_9846 2316', '', 'sg_65_6745 0733', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(725, 'Phoon Huat & Co (Pte) Ltd', '231A Pandan Loop\r\rSingapore 128419\r\r\r\rAttn : Ms Candy Foo\r\rDID : 6771 1272\r\rFAX : 6289 2664\r\rEmail : candy.foo@phoonhuat.com', 'sg_65_6771 1272', '', 'sg_65_6289 2664', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(726, 'CESCO Pipeline Technologies Pte Ltd', 'No.4 Sixth Lok Yang Road\r\rSingapore 628102\r\r\r\rAttn : Mr Ong Wei Jie\r\rHP : 8716 6804\r\rTel: 6267 0850\r\rFax: 6266 4297\r\rEmail : weijie@cesco.com.sg', 'sg_65_8716 6804', '', 'sg_65_62664297', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(727, 'Dantech Solutions', 'Attn : Mr Danny Foong\r\rHP : 9663 8788\r\rEmail : dannyfoong@dantechsolutions.com.sg', 'sg_65_9663 8788', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(728, 'Benelux Flowers & Food Pte Ltd', '245 Pandan Loop\r\rSingapore 128428\r\r\r\rAttn : Ms Christine Tan\r\rTel : 6779 6890\r\rFax : 6779 7203\r\rHP  : 9889 3389\r\rEmail :  christine@beneluxproduce.com', 'sg_65_98893389', '', 'sg_65_67797203', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(729, 'FG FOOD INDUSTRIES PTE LTD', 'Attn : Mr Muhd Hanas\r\rAsst Factory Manager\r\rHP  : 9021 7401\r\rEmail : hanas@firstgourmet.com', 'sg_65_90217401', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(730, 'Alsco Pte Ltd', '9 Second Chin Bee Road, Singapore 618776\r\r\r\rAttn : Ms Ros Tan ?QA Manager?\r\rTel : 6455 0150 \r\rFax : 6455 0152 \r\rHP  : 9459 1343\r\rEmail : Ros@alsco.com.sg', 'sg_65_9459 1343', '', 'sg_65_6455 0152', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(731, 'Kang Wei Jye', '', 'sg_65_9735 5655', '', 'sg_65_6491 5099', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(732, 'Onion Restaurant & Bar Pte Ltd', 'Attn : Mr Wong cc\r\rHP : 9237 9500\r\rEmail : onion_james@seasonal.com.sg', 'sg_65_92379500', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(733, 'Hwa Seng Furniture Pte Ltd', '312 Geylang Road, \r\rSingapore 389351\r\r\r\rAttn : Ms Mary\r\rHP : 9661 6822\r\rTel : 6744 0312\r\rEmail : hsfpl@singnet.com.sg', 'sg_65_9661 6822', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(734, 'AvePoint Singapore Pte Ltd', '10 Collyer Quay, \r\r#17-01/04 Ocean Financial Centre, \r\rSingapore 049315\r\r\r\rAttn : Ms Niharika Iyengar\r\rHR & Operations Assistant \r\rTel : 6692 9028 Ext (817)\r\rHP : 8728 8995 \r\rEmail : niharika.iyengar@avepoint.com', 'sg_65_8728 8995', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(735, 'OCBC Property Services Private Limited', 'Attn : Mr Francis Chew\r\rFacilities Manager\r\rOCBC CAMPUS\r\rP M D – I & R\r\rDID : 6216 3691     \r\rFax : 6535 3216\r\rMobile : 8288 6498\r\rEmail : chewkcf@ocbcproperty.com.sg', 'sg_65_288 6498', '', 'sg_65_6535 3216', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(736, 'Righturn Dezign Pte Ltd', '54 Serangoon North Ave 4\r\r#03-03 Cyberhub North \r\rSingapore 555854\r\r\r\rAttn : Ms Tennie Tan\r\rHP : 9767 2267\r\rEmail : righturndezign@gmail.com', 'sg_65_97672267', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(737, 'Tian Eng Dragon & Lion Dance Centre', '113 Eunos Avenue 3\r\r#04 - 11 \r\rGordon Industrial Building\r\rSingapore 409838\r\rAttn :  Mr Henry Neo \r\rLion Dance Coach \r\rhp   :  93870416\r\rtianeng@tianeng,com.sg', 'sg_65_93870416', '', 'sg_65_68445809', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(738, 'Chemicrete Enterprises Pte Ltd', '515 Yishun Industrial Park A, \r\rSingapore 768737\r\r\r\rAttn : Mr Steven Soh\r\rHP: 96547301\r\rEmail : steven@chemicrete.com.sg', 'sg_65_96547301', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(739, 'PATHLIGHT SCHOOL (CAMPUS 2)', '6, Ang Mo Kio Street 44 \r\rSingapore 569253 \r\r\r\rAttn : Mr Fazli Yahya\r\rAsst Executive\r\rOperation & Estate Management\r\rHp : 9750 0122\r\rTel : 6592 0513 \r\rFax : 6592 0514\r\rEmail : fazli.yahya@pathlight.org.sg', 'sg_65_9750 0122', '', 'sg_65_6592 0514', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(740, 'SOL IDesign Pte Ltd', 'Blk 25 Ghim Moh Link \r\r#01-18 \r\rSingapore 270025\r\rAttn: Mr David Kek/Director  \r\rhp  : 92718872\r\rdavidkek@sol-idesign.com', 'sg_65_92718872', '', 'sg_65_68750510', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(741, 'Holiday Inn Express Singapore Orchard Road', '20 Bideford Road, \r\rSingapore 229921\r\r\r\rAttn : Ms Katerina Danilenko\r\rGuest Service Leader\r\rTel : 6690 3199\r\rHP  : 9674 7524\r\rFax : 6690 3190\r\rEmail : ekaterina.danilenko@ihg.com', 'sg_65_9674 7524', '', 'sg_65_6690 3190', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(742, 'S M C Food 21 Pte Ltd', 'No 1 Chin Bee Crescent\r\rSingapore 619890\r\r\r\rAttn : Mr Leow Tong Hoe\r\rOperation Manager\r\rHP : 9172 8870\r\rTel: 6264 7672\r\rFax: 6264 7673\r\rEmail : leowtonghoe@smcfood21.com', 'sg_65_91728870', '', 'sg_65_62647673', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(743, 'LEGOLAND Malaysia Resort', '7, Jalan Legoland, Bandar Medini,, 79250 Nusajaya, Johor\r\rAttn: Mr Ahmad Zaki Alias\r\rTel: 075978862', 'sg_65_75978862', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(744, 'Beacon Primary School', '36 Bukit Panjang Road\r\rSingapore 679944\r\r\r\rMr Tan Choon Hee/OM\r\rHP @ 92974423', 'sg_65_92974423', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(745, 'Yuzu Japanese Restaurant Pte Ltd', '6A Lorong Mambong\r\rSingapore 277673\r\r\r\rAttn : Ms Tang\r\rHP : 8282 9939\r\rEmail : val.tang@hotmail.com', 'sg_65_82829939', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(746, 'SPACE MATRIX DESIGN CONSULTANTS PTE LTD', '306 Tanglin Road, \r\rPhoenix Park Office Campus\r\rSingapore 247973\r\r\r\rAttn : Mr Louis Tay \r\rProject Manager \r\rHP : 9799 5822\r\rTel : 6423 9516   \r\rFax : 6222 8916\r\rEmail : louis.tay@spacematrix.com', 'sg_65_9799 5822', '', 'sg_65_6222 8916', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(747, 'Gourment East Kitchen', '3017 Bedok North Street 5, \r\r#03-05\r\rSingapore 486121\r\r\r\rAttn : Mr Jason Hu\r\rHP : 9181 9984', 'sg_65_9181 9984', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(748, 'Epulas Pte Ltd', '302 Tanglin Road \r\r\r\rAttn :  Mr Jose Alonso \r\rhp   :  81271411\r\rjose@binomio.sg', 'sg_65_81271411', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(749, 'SynerBuild Pte Ltd', 'SynerBuild Pte Ltd\r\rNo 30 Shaw Road\r\r#01-06\r\rRoche Building\r\rSingapore 367957\r\rPhone : 67412330\r\rFax   : 67412330\r\rMobile: 96333550 \r\rjc.ho@synerbuild.com.sg', 'sg_65_96333550', '', 'sg_65_67412330', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056805, 0, 1),
(750, 'Zheng Sheng Pte Ltd', 'Zheng Sheng Pte Ltd\r\r61 Ubi Road 1 #04-16\r\rOxley Bizhub\r\rSingapore 408727\r\rAttn  :  Mr Shawn Hoh \r\rhp    :  86888210\r\rmzhoh10@gmail.com', 'sg_65_86888210', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(751, 'Upsite  Ptd Ltd', '8, Joo Chiat Terrace\r\rSingapore 427181\r\rAttn :  Mr Daniel Tay', 'sg_65_98764827', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(752, 'Jasa Investigation & Security SVS Pte Ltd', '135 Jurong East Street 13, Singapore 600135\r\r\r\rAttn : Mr Magen\r\rHP : 8177 9587\r\rEmail : sutd@jasainv.com', 'sg_65_8177 9587', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(753, 'Personal_Mr Shyam', '248 Lorong Chuan\r\r#21-04 Chuan Park\r\rSingapore 556747\r\r\r\rAttn : Mr Shyam\r\rHP : 9073 1518\r\rEmail : shyam.s.nair@gmail.com', 'sg_65_9073 1518', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(754, 'Unearthed Productions', '2 Kallang Pudding Road\r\r#01-03 Mactech Building\r\rSingapore 349307 \r\r\r\rAttn : Mr Stanley See\r\rExperience Management, Special Project\r\rHP : 9295 2905\r\rEmail : Stanley@unearthedproductions.com.sg', 'sg_65_92952905', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(755, 'EE SWAN ELECTRICAL ENGINEERING & PLUMBING SERVICE', '1 Yishun St 23 YS-ONE #03-46 \r\rSingapore 768441', 'sg_65_96363776', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(756, 'Aryan ( SEA ) Pte Ltd', 'Zara Liat Tower\r\r541 Orchard Road\r\rLiat Tower #02-02\r\rSingapore 238801\r\rAttn : Mr William Oh\r\rMobile : 8699 5340\r\remail : williamoh@rsh.com.sg', 'sg_65_8699 5340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(757, 'Cold Storage Singapore 1983 Pte Ltd', '21, Tampines North Drive 2, \r\r#03-01 Singapore 528765\r\rTel : 6891 8805 \r\rFax : 6784 9108\r\rMobile : 9668 0539\r\remail : kschng@coldstorage.com.sg', 'sg_65_9668 0539', '', 'sg_65_6784 9108', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(758, 'Maitat Construction Pte Ltd', '135 Middle Road #02-07\r\rBylands Building\r\rSingapore 188975\r\r\r\rAttn : Mr Lim Teck Chuan\r\rHP : 9695 3383\r\rEmail : maitat.sales@gmail.com', 'sg_65_9695 3383', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(759, 'Kelvin & Frank Reid Pte Ltd', '20 Malacca Street,\r\r #09-00 Malacca Centre, \r\rSingapore 048979\r\r\r\rAttn : Mr Mark Alnas \r\rInterior Designer\r\rDID : 6508 7138\r\rTel : 6221 7750\r\rHP  : 8220 7389\r\rEmail : mark@kelvinfrankreid.com', 'sg_65_8220 7389', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(760, 'Chinatown Tai Chong Kok Confectionery Hue Kee', 'Blk 1006 Aljunied Ave 5\r\r#01-2/4\r\rSingapore 389887\r\r\r\rAttn : Mr Tham\r\rHP : 9807 1677\r\rEmail : chinatownhuekee@yahoo.com.sg', 'sg_65_9807 1677', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(761, 'Orchard Hotel Singapore', '442 Orchard Road, \r\rSingapore 238879\r\r\r\rAttn : Mr Victor Kwan\r\rAssistant Purchasing Manager\r\rDID : 6739 6547 \r\rFax : 6739 6703 \r\rHP  : 9684 9797\r\rEmail : victor.kwan@millenniumhotels.com', 'sg_65_9684 9797', '', 'sg_65_6739 6703', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(762, 'Konica Minolta Business Solutions Asia Pte Ltd', '10 Teban Gardens Crescent Singapore 608923\r\r\r\rAttn :  Mr Clarence Tai \r\rBusiness Operations Manager\r\rDID  :  68958630\r\rHP   :  97610376\r\rFax  :  65619879\r\rclarence.tai@konicaminolta.com', 'sg_65_97610376', '', 'sg_65_65619879', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(763, 'Bell Fitness', '501 West Coast Drive \r\r#01-302, Singapore 120501\r\r\r\rAttn : Ms Andrea Bell (Club Owner)\r\rTel : 6566 8481\r\rHP  : 8266 8442\r\rEmail : Andrea.Bell@anytimefitness.sg', 'sg_65_8363 5076', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(764, 'SPH Magazines Pte Ltd', '82 Genting Lane, \r\rLevel 7 Media Centre, \r\rSingapore 349567\r\r\r\rAttn : Ms Karen Wong\r\rAssistant Manager, Marketing | Men''s Health\r\rDID : 6319 5819\r\rHP  : 9847 5132\r\rEmail : karenwqy@sph.com.sg', 'sg_65_98475132', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(765, 'Third Element Pte. Ltd.', '9 Yishun Ind. St. 1\r\r#04-72 Yishun North Spring\r\rSingapore 768163\r\r\r\rAttn : Mr Elvin Tan\r\rProject Manager\r\rHP : 9723 2298\r\rTel : 6710 4227\r\rFax : 6710 4327 \r\rEmail : elvin@thirdelement.biz', 'sg_65_9723 2298', '', 'sg_65_6710 4327', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(766, 'T B C TRANSPORTATION & TRADING (PTE) LTD', '102E Pasir Panjang Road \r\r#04-02 CitiLink \r\rSingapore 118529\r\r\r\rAttn : Mr Tino\r\rHP : 9125 9377\r\rEmail : tino.r@tbc.com.sg', 'sg_65_9125 9377', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(767, 'Diva Models Singapore', '2A Stanley Street\r\rSingapore 068721\r\r\r\rAttn : Ms Rowena Foo', 'sg_65_98156943', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(768, 'The Salvation Army', 'Attn : Mr Elwin\r\rHP : 8298 5522\r\rEmail : elwin_leong@SMM.salvationarmy.org', 'sg_65_8298 5522', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(769, 'Delifrance Singapore Wholesale', '"206 Pandan Loop \r\r#04-01 (Seawaves Building) \r\rSingapore 128397\r\r\r\rAttn : Mr Simon Solinas\r\rSales & Marketing Manager\r\rTel : 6779 1402 \r\rHP : 9877 8669 \r\rEmail : ssolinas@delifrance.com"', 'sg_65_9877 8669', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(770, 'Stereo Electronics Pte Ltd', 'Attn : Mr Cyrus Tan \r\rOperation Manager \r\rHP: 9672 3672\r\rEmail : management@stereo.com.sg', 'sg_65_9672 3672', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(771, 'Weissach Motors Private Limited', '8 Kaki Bukit Avenue 4\r\r#02-19 Premier @ Kaki Bukit\r\rSingapore 415875\r\r\r\rAttn : Mr Teoh Song Teik\r\rHP : 9761 4507\r\rTel : 6341 6428\r\rEmail : weissachmotors@gmail.com', 'sg_65_97614507', '', 'sg_65_63416428', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(772, 'UTERO MANAGEMENT PTE LTD', '15 Hong Kong street\r\rThe Novelty House #03-01\r\rSingapore 059658	\r\r			\r\rAttn : Ms Mila Troncoso	\r\rProject Executive\r\rHP : 9632 4340	\r\rEmail :	mila@lec.sg', 'sg_65_9632 4340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(773, 'CPG Consultants Pte ltd', '238B Thomson Road, \r\rSingapore 307685\r\r\r\rAttn : Mr Saurabh Bhagra\r\rDID : 6357 5213\r\rHP  : 8606 4800\r\rEmail : saurabh.bhagra@cpgcorp.com.sg', 'sg_65_8606 4800', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(774, 'Personal_Mr Edwin Lee', 'Attn : Mr Edwin Lee\r\rHP : 9766 4533\r\rEmail : sit@edwiin.com', 'sg_65_9766 4533', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(775, 'Cerulean Pte Ltd', '10 Peirce Road \r\rSingapore 248529\r\r\r\rAttn : Madam Ho Juat Keng\r\rHP : 9850 8898\r\rFax : 6471 2998', 'sg_65_852 9019 9025', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(776, 'Natron Industries Pte Ltd', 'Blk 18 Sin Ming Lane\r\r#04-02 Midview City\r\rSingapore 573960\r\r\r\rAttn : Mr Keane Lin\r\rHP : 9115 9509\r\rTel : 6659 8863\r\rEmail : keane.lin@natron.com.sg', 'sg_65_91159509', '', 'sg_65_66598836', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(777, 'Project Creative Pte Ltd', 'Attn : Ms Keith Toh\r\rProject Executive\r\rHP : 8200 0121\r\rEmail : kt@projectcreative.sg', 'sg_65_8200 0121', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(778, 'Ritual Pte Ltd', '11 North Canal Road\r\r#03-01 Singapore 048824', 'sg_65_8522 8179', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(779, 'Culinaryon Pte Ltd', '#04-63, Tower 2, \r\rOne Raffles Place, \r\rSingapore 048616 \r\r\r\rAttn : Mr Kelvin Ng\r\rHP : 9386 0900\r\rEmail : kelvin@culinaryon.sg', 'sg_65_9386 0900', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(780, 'Burnt Ends Restaurant', '20 Teck Lim Road\r\rSingapore 088391\r\r\r\rAttn : Mr Fabio Fazely\r\rRestaurant Assistant Manager\r\rHP : 8495 2818\r\rEmail : fazely@yahoo.com', 'sg_65_84952818', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(781, 'TAG TEAM', 'Attn : Mr CARL De Leon\r\r“Team Strategist”\r\rAssistant Manager\r\rHP : 9645 5810\r\rEmail : carl@tagteaminc.sg', 'sg_65_96455810', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(782, 'Living Water Resources Pte Ltd', '705 Sims Drive #03-06 \r\rShun Li Industrial Complex \r\rSingapore 387384\r\r\r\rAttn : Mr Joseph Tan\r\rHP : 9815 0037\r\rEmail : livingwaterresources@gmail.com', 'sg_65_98150037', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(783, 'ALPHA GEN PTE LTD', 'Block 72 Loewen Road \r\r#01-07 \r\rSingapore 248848\r\r\r\rAttn : Mr Tong Wei Shiang\r\rHP : 9622 0735\r\rEmail : mettalzero@yahoo.com', 'sg_65_9622 0735', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(784, 'VLV Pte Ltd', '3A Merchant’s Court,\r\rRiver Valley Road #01-02,\r\rSingapore (179020)\r\r\r\rAttn : Ms Grace Lam\r\rRestaurant Manager\r\rDID : 6661 0198\r\rHP : 9051 3466\r\rFax : 6333 4101\r\rEmail : glam@vlv.life', 'sg_65_9667 8732', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(785, 'Tony’s Pizza Pte Ltd (Au Kah Lok)', '200 Victoria Street \r\r#01-68A, Singapore 188021\r\r\r\rAttn : Ms Celine Yang\r\rPurchasing Manager\r\rDID : 6730 8295 \r\rHP : 9667 8732 \r\rEmail: cyan@catalunya.sg', 'sg_65_9667 8732', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(786, 'ST Electronics Limited', '24 Ang Mo Kio Street 65 \r\rSingapore 569061\r\r\r\rAttn : Ms Candy Wong\r\rHP : 9066 2343\r\rEmail : wonghx@stee.stengg.com', 'sg_65_9066 2343', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(787, 'CMS Engineering Pte Ltd', '10 Admiralty Street\r\r#04-36 Northlink Building\r\rSingapore 757695\r\r\r\rAttn : Mr Govin \r\rHP : 9272 4257\r\rEmail : govincms@yahoo.com', 'sg_65_92724257', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(788, 'Work Central Offices Pte Ltd', '190 Clemenceau Ave \r\r#06-01 Singapore 239924\r\r\r\rAttn : Ms Hosanna Marjorie (Operations Manager)\r\rDID : 6708 8211\r\rHP  : 8333 8212\r\rEmail : marjorie@workcentral.com.sg', 'sg_65_8333 8212', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(789, 'Covenant Evangelical Faith Church', 'Attn : Mr Henry Samuel\r\rHP : 9784 8361\r\rEmail : henry.samuel@cefc.org.sg', 'sg_65_97848361', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(790, 'Peak Engineering & Consultancy Pte Ltd', 'Blk 9005 Tampines Street 93 \r\r#01-228 Singapore 528839 \r\r\r\rAttn : Mr Lee Yao Tang \r\rProject Manager \r\rTel : 6472 2819 \r\rFax : 6474 4614 \r\rMobile: 9762 1450\r\rEmail : ytlee@peakengrg.com', 'sg_65_9762 1450', '', 'sg_65_6474 4614', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(791, 'LF Logistics', 'Attn : Mr Ronald Chor \r\rManager - Procurement \r\rTel: 6357 6338 \r\rHP : 9767 3360\r\rFax: 6898 9350\r\rEmail: ronaldchor@lflogistics.com', 'sg_65_9767 3360', '', 'sg_65_6898 9350', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(792, 'Anchor Green Primary School', '31 Anchorvale Drive, \r\rSingapore 544969\r\r\r\rAttn : Ms Jeal Leow\r\rOperation Manager\r\rHP : 9651 4923\r\rEmail : leow_chen_cheng@moe.edu.sg', 'sg_65_96514923', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(793, 'RT Kleen Services', 'Attn : Mr Robert\r\rHP : 9068 3668\r\rEmail : rthexeira@gmail.com', 'sg_65_9068 3668', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(794, 'Red Bull Singapore Pte Ltd', 'Attn : Mr Harresh Krishnan\r\rHP : 9630 8290\r\rEmail : harresh.krishnan@sg.redbull.com', 'sg_65_9630 8290', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(795, 'Pearson Education South Asia Pte Ltd', '9 North Buona Vista Drive\r\r#13-01 The Metropolis Tower One\r\rSingapore 138588\r\rAttn : Ms Wita Tan\r\rTel : 6225 2027 Mobile : 9777 0199\r\rFax : 6224 6293', 'sg_65_9777 0199', '', 'sg_65_6224 6293', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(796, 'MAXXUS PTE LTD', '150 Ubi Ave 4\r\r#05-07/08 Ubi Biz-Hub\r\rSingapore 408825\r\r\r\rAttn : Mr Yen Yoon\r\rTel : 6483 3639\r\rFax : 6483 2629\r\rHP  : 9002 4857\r\rEmail : yen.yoon@maxxus.com.sg', 'sg_65_9002 4857', '', 'sg_65_6483 2629', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(797, 'Verve Holdings Pte Ltd', '8 Lock Road \r\rGillman Barracks \r\rSingapore 108936\r\r\r\rAttn : Ms Belle	\r\rHP : 9773 0267\r\rEmail : belle@verve.sg', 'sg_65_9773 0267', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(798, 'DUCTILE PTE. LTD.', 'Attn : Mr Cheok Kai Weei \r\rHP : 9857 4550\r\rEmail : kaiweei@dpf.sg', 'sg_65_98574550', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(799, 'Teguh', '', 'sg_65_111111', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(800, 'PT. Indogreen World Cakrawala', 'Komplek SP Plaza blok B. No 1-2\r\rBatam, Indonesia\r\rAttn :  Mr Teguh\r\rTelp. +62778395757', 'sg_65_111111', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056806, 0, 1),
(801, 'MP Biomedicals Asia Pacific Pte Ltd', 'Attn : Mr Vipin\r\rHP : 8268 8665\r\rEmail : vipin.prasad@mpbio.com', 'sg_65_8268 8665', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(802, 'Blue Sky Events', 'Attn : Mr Jonathan Wong\r\rHP : 9177 5114\r\rTel :  6731 5945\r\rEmail : jonathan.wong@singaporegp.sg', 'sg_65_91775114', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(803, 'Stuttgart Auto Pte Ltd', '12 Sungei Kadut Avenue\r\rSingapore 729648\r\r\r\rAttn : Ms Kylie Tan\r\rHP : 9329 3778\r\rEmail : kylietan@eurokars.com.sg', 'sg_65_9329 3778', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(804, 'The Coffee Connection Pte Ltd', '420 North Bridge Road\r\r#02-12\r\rNorth Bridge Centre \r\rSingapore 188727\r\rAttn :  Ms Justina Tan \r\rHP   :  98756932\r\rtcc29@rockymaster.com.sg', 'sg_65_98756932', '', 'sg_65_1111', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(805, 'Team Synergy Pte Ltd', 'Blk 4012 Ang Mo Kio Ave 10\r\r#07-03 TechPlace 1\r\rSingapore 569628\r\r\r\rAttn : Mr Bob Loo\r\rHP : 9151 1110\r\rEmail : bob@teamsynergy.com.sg', 'sg_65_91511110', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(806, 'WeBarre Pte Ltd', '23 Keppel Bay View, #37-71,\r\rSingapore 098414\r\r\r\rAttn : Ms Linda Lim\r\rHP : 9115 7771\r\rEmail : linda@webarre.com', 'sg_65_9115 7771', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(807, 'Asian De Tours', 'Attn : Ms Hilda \r\rHP : 9772 2071\r\rEmail : hilda.cheung@asiandetours.com', 'sg_65_9772 2071', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(808, 'Singapore Management University', 'Attn : Ms Joanne Wong\r\rTel : 98281941\r\rEmail : joannewong@smu.edu.sg', 'sg_65_98281941', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(809, 'DODO MARKETING PTE LTD', '14 Senoko Way\r\rSingapore 758035\r\r\r\rAttn : Ms KYORENE ANG\r\rSenior Executive\r\rH/P : 9696 4833\r\rDID : 6852 8330  \r\rFAX : 6754 4030  \r\rEmail : kyorene@thongsiek.com', 'sg_65_9696 4833', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(810, 'The Ice Cream & Cookie Co. Pte. Ltd.', '5 Burn Road, #01-01\r\rSingapore 369972\r\r\r\rAttn : Ms Natasha\r\rHP : 9733 8379\r\rEmail : info@icecreamcookieco.com', 'sg_65_97338379', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(811, 'Anglo-Chinese Junior College', '25 Dover Close East \r\rSingapore 139745\r\r\r\rAttn : Mr David DSOUZA \r\rTel : 9478 9292\r\rEmail : David_DSOUZA@schools.gov.sg', 'sg_65_67750511', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(812, 'MPL Woodworks LLP', '27 Woodlands Industrial Park E1\r\r#04-10 Singapore 757718\r\r\r\rAttn : Mr Peter Pee\r\rHP : 94507458\r\rTel : 63140762\r\rFax : 63140782\r\rEmail : mplcwp@gmail.com', 'sg_65_94507458', '', 'sg_65_63140782', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(813, 'Changi Airport Group (S) Pte Ltd', 'P.O.Box 168\r\rSingapore Changi Airport\r\rSingapore 918146\r\r\r\rAttn: Ms Yvonne Sin, AES\r\rFinance Division, Expenditure Section\r\rTel : 6541 2554\r\rFax : 6545 7072   \r\rEmail : yvonne.sin@changiairport.com', 'sg_65_6541 2554', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(814, 'Forward Land Pte Ltd', 'Marine Parade Road\r\r#21-01 Parkway Parade\r\rSingapore 443263\r\r\r\rAttn : Ms Geraldine\r\rTel : 6664 0734\r\rEmail : geraldine@hotel81.com.sg', 'sg_65_6664 0734', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(815, 'MUSE Music Club', 'Parklane Shopping Mall, \r\r35 Selegie Road, #06-01\r\rSingapore 188307\r\r\r\rAttn : Mr Heng \r\rHP : 9168 6161\r\rEmail : yewcheeheng@gmail.com', 'sg_65_9168 6161', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(816, 'Bukit Panjang Govt High School', '7 Choa Chu Kang Avenue 4\r\rSingapore 689809\r\r\r\rAttn : Mr Sam Goh Khiang Hong Operations Manager\r\rTel : 6769 1031\r\rFax : 6762 6576\r\rEmail : goh_khiang_hong@moe.edu.sg', 'sg_65_6769 1031', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(817, 'Electrical & Mobile Pte Ltd', '18 Kaki Bukit Road 3\r\r#03-13\r\rEntrepreneur Business Centre\r\rSingapore 415978\r\rAttn : Ms Ding \r\rhp   : 94699904\r\relectricalmobile@yahoo.com.sg', 'sg_65_94699904', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(818, 'Bonafides Beaute Pte Ltd', '100 Tras Street \r\r#05-01\r\rSingapore 079027\r\r\r\rAttn : Ms Sylvonne Ng\r\rBiz Development Executive\r\rTel : 6737 2828\r\rEmail : sylvonne@bonafidesbeaute.com', 'sg_65_6737 2828', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(819, 'Sephora JEM', '50 Jurong Gateway Road, \r\rJurong East Mall #01-42/56-57\r\rSingapore 608548\r\r\r\rAttn : Ms Elaine Neo \r\rStore Manager \r\rDID : 66945528\r\rFax : 66943459 \r\rEmail : man1783@sephora.sg', 'sg_65_66945528', '', 'sg_65_66943459', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(820, 'Techworks Building & Material Pte Ltd', '8 Kaki Bukit Avenue 4\r\r#06-25\r\rSingapore 415875\r\rAttn : Mr Tony Ng \r\rTel  : 63455416\r\rtechworsbm@gmail.com', 'sg_65_96630676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(821, 'Lighthouse Evangelism', '22 Kaki Bukit Place\r\rSingapore 416200\r\rTel : 6744 9848', 'sg_65_97468808', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(822, 'Kuek Kim Construction', 'Attn : Mr Padmarajan R\r\rCivil engineer\r\rHP : 9355 2405\r\rEmail : roseyouu@gmail.com', 'sg_65_93552405', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(823, 'Heart of God Church', 'Attn : Mr Ang Wei Jie\r\rHP : 93858234\r\rEmail : Angweijie7@gmail.com', 'sg_65_93858234', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(824, 'PlusOne Asia Pte Ltd', '70 Ubi Crescent, \r\r#01-09 Ubi TechPark, \r\rSingapore 408570 \r\r\r\rAttn : Mr Jimmy Ang\r\rAccounts Executive\r\rAccounts Department\r\rHP : 9278 7589 \r\rTel : 6842 1238 Ext: 623 \r\rFax : 6842 1136\r\rEmail : jimmy@plus1asia.com', 'sg_65_9278 7589', '', 'sg_65_6842 1136', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(825, 'Deliciae Hospitality Management Pte Ltd', '5 Duxton Hill\r\rSingapore 089591\r\r\r\rAttn : Mr Alastair Fern\r\rDeputy CFO\r\rHP : 8518 4688\r\rTel: 6690 7512\r\rFax: 6224 5247\r\rEmail :   alastair.fern@deliciae.sg', 'sg_65_8518 4688', '', 'sg_65_6224 5247', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(826, 'Jones Lang LaSalle Property Consultants Pte Ltd', 'Level 38, Republic Plaza, \r\rSingapore\r\r\r\rAttn : Ms Manda Brook\r\rNational Property Manager Singapore/Malaysia \r\rHP : 9838 7585 \r\rEmail : manda.brook@ap.jll.com', 'sg_65_9838 7585', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(827, 'DMxchange Builder Pte Ltd', '8 Jalan Kilang Barat \r\r#01-03\r\rCentral Link \r\rSingapore 159351\r\rAttn  :  Mr Paiman \r\rProject Director \r\rTel   :  62238768\r\rFax   :  62270811\r\rpaiman@dmxchange.com', 'sg_65_91818155', '', 'sg_65_62270811', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(828, 'Ferrari Logistics Singapore Pte Ltd', '7 Kaki Bukit Road 1, \r\r#02-05/06 Eunos Technolink, \r\rSingapore 415937\r\r\r\rAttn : Ms Caren Tay\r\rTel : 65475560\r\rEmail : caren.tay@ferrarigroup.net', 'sg_65_65475560', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(829, 'Kino Office Furniture Pte Ltd', '10 Anson Road \r\r#02-13/14/15 International Plaza\r\rSingapore 079903\r\r\r\rAttn : Ms Brina\r\rTel : 6225 2244\r\rEmail : sales@kino.com.sg', 'sg_65_6225 2244', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(830, 'The MCST Plan No 2197', 'APM Property Management Pte Ltd\r\r3 Temasek Boulevard, \r\r#B1-65 Suntec City Mall, \r\rSingapore 038983\r\r\r\rAttn : Ms Hazel Lim\r\rContracts Administration & Procurement\r\rDDI : 6825 2893 \r\rTel : 6295 2888\r\rEmail : purchasing@apmasia.com.sg', 'sg_65_6825 2893', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(831, 'Catering Solutions Pte Ltd', 'No.96 Tuas South Boulevard\r\rSingapore 637055\r\rAttn :  Mr Mahenthiran \r\rHP   :  82666061\r\rmahen@cateringsolutions.com.sg', 'sg_65_82666061', '', 'sg_65_63699271', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(832, 'Vitablend Asia Pacific Pte Ltd', '1 Senoko Avenue\r\r#01-06/07\r\rSingapore 758297\r\r\r\rAttn : Mr Dyven\r\rHP : 96282788\r\rEmail : si@vitablend.com.sg', 'sg_65_96282788', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(833, 'Rolls-Royce Singapore Pte Ltd', '1 Seletar Aerospace Crescent, \r\rSingapore 797575\r\r\r\rAttn : Mr Tan Ming Kwang\r\rPurchasing Operations Centre - Buyer\r\rAsia Pacific\r\rDID : 6240 3297\r\rH/p : 9386 9037\r\rEmail : MingKwang.Tan@Rolls-Royce.com', 'sg_65_9386 9037', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(834, 'GLOBAL PROCUREMENT MANAGEMENT PTE LTD', '21 Merchant Road \r\r#07–01, Singapore 058267\r\r\r\rAttn : Ms Wendy Kiew\r\rDirector of Procurement\r\rTel : 6634 1062\r\rDID : 6812 1729\r\rFax : 6634 1077 \r\rH/P : 9272 9313 \r\rEmail : wendy.kiew@metroglobal.net', 'sg_65_9272 9313', '', 'sg_65_6634 1077', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(835, 'Jasema Marketing', 'Blk 1013 Geylang East Ave 3\r\r#03-154 Singapore 389728\r\r\r\rAttn : Mr Joseph Yeo\r\rHP : 9689 6048\r\rTel: 6745 1581\r\rFax: 6745 1282\r\rEmail : jsm.asean@gmail.com', 'sg_65_9689 6048', '', 'sg_65_6745 1282', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(836, 'ST Electronics (Info-Software Systems) Pte. Ltd.', 'Attn : Mr Michael Yeo \r\rTel : 6679 8730\r\rFax : 6679 8899\r\rEmail : michaelyeo@stee.stengg.com', 'sg_65_6679 8730', '', 'sg_65_6679 8899', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(837, 'Snorre Food Pte Ltd', '25 Fishery Port Road,  \r\rSingapore 619739\r\r\r\rAttn : Mr Niels Graae\r\rProduction Manager\r\rTel : 6538 3303\r\rDID : 6420 0296\r\rHP  : 9178 1058\r\rFax : 6538 0104\r\rEmail : niels.graae@snorrefood.com.sg', 'sg_65_9178 1058', '', 'sg_65_6538 0104', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(838, 'Housing & Development Board', 'Attn : Ms Bee Tin SEAH \r\rTel : 6490 1097\r\rEmail : SEAH_Bee_Tin@hdb.gov.sg', 'sg_65_6490 1097', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(839, 'Lim Luan Seng Foods Industries Pte Ltd', 'Blk 26 Defu Lane 10 \r\r#01-178 Singapore 539207\r\r\r\rAttn : Ms Seow Fun\r\rTel : 62830022\r\rEmail : seowfun@limluanseng.com', 'sg_65_62830022', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(840, 'Blasco Groups Pte Ltd', 'Attn : Mr Huang\r\rHP : 9161 9466\r\rEmail : whuang@dearchi.com.sg', 'sg_65_9161 9466', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(841, 'Evolve Mixed Martial Arts', '181 Orchard Rd #06-01 \r\rOrchard Central\r\rSingapore 238896\r\r\r\rAttn : Mr Mohammad Rizal\r\rOperations Exexcutive\r\rTel : 65364556\r\rHP : 91115269\r\rEmail : m.rizal@evolve-mma.com', 'sg_65_65364556', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(842, 'Changi AirBase', 'Attn :  Mr Jeevan\r\rFax  :  65864230', 'sg_65_1111111', '', 'sg_65_65864230', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(843, 'Queenstown Citizens'' Consultative Committee', 'C/O Queenstown Community Centre\r\r365 Commonwealth Avenue\r\rSingapore 149732\r\r\r\rAttn: Alex Tan', 'sg_65_8484 5505', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(844, 'National Cancer Centre', '11 Hospital Drive\r\rSingapore 169610\r\rAttn : Mr Zulkifli Bin Hasan\r\rSenior Engineering Technician\r\rFacility Management\r\rTel : 64368222 / 63721548 \r\rMobile: 9826 5069', 'sg_65_9826 5069', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(845, 'Jin Shui Tong Reflexology', '316 Bedok Road\r\rSingapore 469481\r\r\r\rAttn : Mr Raymond\r\rHP : 9339 5393\r\rEmail : jinshuitong@yahoo.com', 'sg_65_9339 5393', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(846, 'Club Harman LLP', '177 River Valley Road\r\r#01-28 Liang Court \r\rSingapore 179030\r\rAttn :  Mr Steven \r\rhp   :  96348478', 'sg_65_96348478', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(847, 'CRIMSON ID PTE LTD', '6 Ubi View, #04-01, \r\rKai Lim Building, \r\rSingapore 408544\r\r\r\rAttn : Ms Gladi\r\rTel : 6333 3086\r\rFax : 6333 0386\r\rEmail :  crimson.interior@gmail.com', 'sg_65_6333 3086', '', 'sg_65_6333 0386', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(848, 'That''s D Wash', 'Blk 744, Bedok Reservior Road\r\r#01-3057\r\rSingapore 470744\r\r\r\rAttn : Mr Johnny\r\rHP : 9125 5000\r\rEmail : ThatsDWash@gmail.com', 'sg_65_91255000', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(849, 'Nail Palace (TB) PTE LTD', '3 Tai Seng Drive, #03-01, \r\rSingapore 535216\r\r\r\rAttn : Ms Valerie Tan\r\rMarcom Executive\r\rTel : 6286 2831   \r\rFax : 6286 4811\r\rEmail : nailpalacedesign@gmail.com', 'sg_65_6286 2831', '', 'sg_65_6286 4811', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(850, 'Golden Beach Seafood Paradise', '2 Andover Road, \r\rSingapore 509984\r\r\r\rAttn : Mr Michael Teo\r\rTel : 6542 7720\r\rHP : 9652 7720\r\rEmail : goldenbeach_rest@yahoo.com', 'sg_65_9652 7720', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(851, 'Food From The Heart', '130 Joo Seng Road,\r\r#03-01, Singapore 368357\r\r\r\rAttn : Ms Jeneve Lim \r\rSenior Manager\r\rTel : 6280-4483 ext 102\r\rHP : 9232 4606\r\rFax : 6280 4498  \r\rEmail : jeneve@foodheart.org', 'sg_65_9232 4606', '', 'sg_65_6280 4498', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(852, 'Sengkang Central Constituency Office', 'Sengkang Community Club\r\rSengkang Community Hub \r\r2 Sengkang Square \r\r#01-01  Singapore 545025\r\r\r\rAttn : Ms Loi Tsae Shin \r\rDeputy Constituency Director \r\rDID : 63125400 \r\rFax : 63128696 \r\rEmail : LOI_Tsae_Shin@pa.gov.sg', 'sg_65_63125400', '', 'sg_65_63128696', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(853, 'Saint Julien Pte Ltd', '27 Scotts road\r\rSingapore 228222\r\r\r\rAttn : Mr Julien BOMPARD \r\rChef / Director\r\rTel : 6284 0238 \r\rMobile: 9088 9301\r\rEmail : julien@cuisineservice.com.sg', 'sg_65_9088 9301', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(854, 'Kriste Designs', '33A HongKong Street\r\rSingapore 059672\r\r\r\rAttn : Ms Gigi\r\rHP : 9617 6504\r\rEmail : gig@kriste.com.sg', 'sg_65_96176504', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056807, 0, 1),
(855, 'Personal_Ms Perlin Xia', 'Attn : Ms Perlin Xia\r\rTel : 6505 5612\r\rEmail : perlin.xia@parkroyaljotels.com', 'sg_65_6505 5612', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(856, 'Golden Village', 'Attn : Ms Deborah Kang\r\rHP : 96208453\r\rEmail : deborahkong@goldenvillage.com.sg', 'sg_65_96208453', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(857, 'Certis CISCO Protection Services Pte Ltd', '10A Science Centre Road\r\rSingapore 609079\r\r\r\rAttn : Mr Raphael Chen\r\rOperations Manager, Protection Services (KINs)\r\rCertis CISCO Protection & Enforcement Services (CPE)\r\rHP : +65 9784 6427\r\rFax: +65 6743 8843\r\rE-mail: raphaelr_chen@certissecurity.com', 'sg_65_9784 6427', '', 'sg_65_6743 8843', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(858, 'Anytime Fitness Greenwich V', 'Attn : Ms Evonne Quek\r\rClub Owner \r\rTel : 9121 0333\r\rEmail : evonnequek@gmail.com', 'sg_65_9121 0333', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(859, 'DEVO Design & Contracts Pte Lt', '215 Henderson Road\r\rHenderson Industrial Park,\r\r#03-06 Singapore 159554\r\r\r\rAttn : Ms Roselle Montilla\r\rProject Leader\r\rTel : 6377 0080\r\rHP : 8468 4592\r\rEmail : roselle@devodc.com.sg', 'sg_65_6377 0080', '', 'sg_65_8468 4592', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(860, 'Devo Design & Contracts Pte Ltd', '27A Jalan Besar, \r\rSingapore 208796\r\r\r\rAttn : Ms Roselle Montilla\r\rProject Manager\r\rHP : 8468 4592 \r\rDID : 6291 3393 \r\rTel : 6291 3363 \r\rEmail : roselle@devodc.com.sg', 'sg_65_8468 4592', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(861, 'Changi Naval Base', 'Attn :  ME2 Kumar\r\rHP   :  93373721\r\rshivaani3657@gmail.com', 'sg_65_93373721', '', 'sg_65_65444378', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(862, 'Burlinson Dental Surgery', '697 East Coast Road \r\r#01-01\r\rSingapore 459060\r\r\r\rAttn : Mr Graeme Burlinson\r\rTel: 62413088\r\rEmail : gnburlin@hotmail.com', 'sg_65_62413088', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(863, 'Ministry of Defence', 'Attn : Ms Anna Chye\r\rDefence Psychology Dept\r\rHP : 9008 1083 \r\rTel : 6373 1566\r\rEmail : DefPsyDep_VAC@defence.gov.sg', 'sg_65_90081083', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(864, 'Asia Genomics Pte Ltd', '65A Temple Street \r\rSingapore 058610\r\r\r\rAttn : Ms Deborah Wong\r\rOperations Manager \r\rTel : +65 6336 2050\r\rHP : +65 9646 0095\r\rFax : +65 6220 3718\r\rEmail : deborah@asia-genomics.com', 'sg_65_9646 0095', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(865, 'Global Link Partners Pte Ltd', 'Attn : Mr Daisuke Ise\r\rTel : 65439812\r\rEmail : ise@glp-sg.com', 'sg_65_65439812', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(866, 'KH Fitness Pte Ltd', '9 Tampines St 32, \r\r#02-01, Tampines Mart \r\rSingapore 529286\r\r\r\rAttn : Mr Khee Huat Heng\r\rHP : 96966658\r\rEmail : khhengheng@yahoo.com.sg', 'sg_65_96966658', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(867, 'ECOLAB PTE LTD', '21 Gul Lane\r\rSingapore 629416\r\r\r\rAttn : Mr Wong Ming Lui \r\rProduction Specialist\r\rDID : 6505 6854  \r\rEmail : MingLui.Wong@ecolab.com', 'sg_65_6505 6854', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(868, 'CWT Logistics Pte Ltd', '38 Tanjong Penjuru, \r\rSingapore 609039 \r\r\r\rAttn : Mr Johari Bin Jantan\r\rTel : 63078086 \r\rHP : 96521575\r\rFax : 6266 3087\r\rEmail : jjohari@sg.logistics.cwtlimited.com', 'sg_65_96521575', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(869, 'Novo Pte Ltd', '190 Macpherson Road\r\rWisma Gulab\r\rSingapore 348548\r\rAttn : Mr William Oh\r\rMobile : 8699 5341\r\remail : williamoh@rsh.com.sg', 'sg_65_8699 5340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(870, 'CRIMSONLOGIC PTE LTD', '31 Science Park Road,\r\rThe Crimson,\r\rSingapore 117611\r\r\r\rAttn : Ms Ong Lay Kheng\r\rHP : 92821173\r\rEmail : laykheng@crimsonlogic.com', 'sg_65_92821173', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(871, 'Nature 2000 Pte Ltd', 'Attn : Ms Joanne\r\rTel : 68481123\r\rEmail : nature2000@singnet.com.sg', 'sg_65_68481123', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(872, 'Yoe Seng Hardware & Industrial Supply', 'Blk 809 French Road\r\r#04-154 Kitchener Complex\r\rSingapore 200809\r\r\r\rAttn : Mr Jimmy Leow\r\rTel: 62913593 \r\rFax: 62932198\r\rMobile: 96632670\r\rEmail : yoeseng1@singnet.com.sg', 'sg_65_96632670', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(873, 'G & J Goldsmiths & Jewellery Pte Ltd', '4 Buffalo Road\r\rSingapore 219781\r\r\r\rAttn : Ms San San\r\rTel : 62923909\r\rEmail : gjgoldsmiths@singnet.com.sg', 'sg_65_62923909', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(874, 'Cocoon Studio Pte Ltd', '315 Outram Road \r\r#02-07 Tan Boon Liat Building\r\rSingapore 169074\r\r\r\rAttn : Madam Linda Oei\r\rHP : 97864430\r\rEmail : lindaoei@cocoonstudio.com.sg', 'sg_65_97864430', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(875, 'Asian Geographic Magazines Pte Ltd', '20 Bedok South Road, \r\rSingapore 469277 \r\r\r\rAttn : Mr Victor Ow\r\rSenior Manager\r\rProduction & Traffic\r\rTel : +65 6298 3241 \r\rFax : +65 6291 2068 \r\rHP : +65 97466322 \r\rEmail : victor@uw360.asia', 'sg_65_97466322', '', 'sg_65_6291 2068', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(876, 'PLANAR ONE & ASSOCIATES PTE LTD', 'No. 9 Senoko Loop \r\rSingapore 758172\r\r\r\rAttn : Mr Dave David \r\rQuantity Surveyor\r\rTel : 6273 1217/ 3106 2207 \r\rFax : 6272 6196\r\rHP : 8223 3570\r\rEmail : davedavid@planarone.com.sg', 'sg_65_8223 3570', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(877, 'MCST Plan No. 3230', '959 Bukit Timah Road \r\r#B1 -26 The Nexus \r\rSingapore 589654\r\r\r\rAttn : Mr Erik Seet \r\rProperty Officer\r\rDID : 6468 1953 \r\rFax : 6468 8903\r\rEmail : the_nexus@singnet.com.sg', 'sg_65_6468 1953', '', 'sg_65_6468 8903', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(878, 'VIC Interior Pte Ltd', 'Attn : Mr Anthony Tan\r\rHP : 9139 7666\r\rEmail : sales@vic.com.sg', 'sg_65_9139 7666', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(879, 'REDICON TEC (S) PTE LTD', 'Attn : Mr Richard Sng\r\rHP : 9664 7557\r\rEmail :  richard@redicon.com.sg', 'sg_65_9664 7557', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(880, 'Conqueror Express Pte Ltd', '237 Pandan Loop\r\r#08-03 Westech Building Singapore 128424\r\rAttn :  Mr Muhammed Ibrahim Syed\r\rhp   :  85111574\r\rmuhdisyed@gmail.com', 'sg_65_85111574', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(881, 'Platinum Yoga Pte Ltd', 'Attn : Mr Anil Kumar\r\rHP : 9237 0225\r\rEmail : anilkumar@platinumyoga.com', 'sg_65_9237 0225', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(882, 'Presbyterian Eldercare @ Potong Pasir', 'Block 108 Potong Pasir Ave 1\r\r#01-488 \r\rSingapore 350108\r\r\r\rAttn : Ms Katherine \r\rTel : 62889674\r\rEmail : wmcs.pec@hotmail.com', 'sg_65_62889674', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(883, 'SIS’88 Pte Ltd', '34, Jurong Port Road\r\rSingapore 619107\r\r\r\rAttn : Mr William Heng\r\rFacility & Maintenance Manager\r\rDID : 66604512 \r\rMobile : 91826423\r\rEmail : william.heng@sis88.com.sg', 'sg_65_91826423', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(884, 'Vector Aerospace Asia Pte Ltd', '100 Seletar Aerospace View\r\rSingapore 797507\r\r\r\rAttn : Ms Carin Tan \r\rOffice Administrator\r\rTel:  +65 6715 2112\r\rFax:  +65 6570 3018\r\rEmail: carin.tan@vectoraerospace.com', 'sg_65_6715 2112', '', 'sg_65_6570 3018', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(885, 'Fortune Food Manufacturing Pte Ltd', '348 Jalan Boon Lay \r\rSingapore 619529\r\r\r\rAttn : Mr Jerico Quek\r\rProduction Manager\r\rDID : 6577 3131\r\rCell : 9748 6066\r\rEmail: jericoquek@fortunefood.com.sg', 'sg_65_9748 6066', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(886, 'MTrade', '11 Woodlands Close\r\r#05-04 \r\rSingapore 737853\r\rAttn : Mr Mark Tan\r\rTel : 6339 1878\r\rMobile : 9769 1377\r\remail : mtradenovelty@gmail.com', 'sg_65_9769 1377', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(887, 'Ms Valencia Khoo', '', 'sg_65_9776 0640', '', 'sg_65_6836 1193', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(888, 'Parkway Dental Practice', '(Parkway Dental Pte Ltd)\r\r9 Scotts Road \r\r#12-02 Pacific Plaza\r\rSingapore 228210  \r\rAttn : Ms Valencia Khoo\r\rTel : 6836 9808 Mobile: 9776 0640 \r\rFax : 6836 1193', 'sg_65_9776 0640', '', 'sg_65_6836 1193', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(889, 'MC CARE SINGAPORE PTE LTD', '21 Woodlands Close, \r\r#01-08 Primz Bizhub \r\rSingapore 737854\r\r\r\rAttn : Ms Christine \r\rHP : 8468 5133\r\rEmail : kiwi_9260@hotmail.com', 'sg_65_8468 5133', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(890, 'BioCon Solutions Pte Ltd', 'The Alpha Industrial Building\r\r7, Toh Guan Road East \r\r#08-03\r\rSingapore 608599\r\r\r\rAttn : Mr Sankar \r\rProject Engineer\r\rHP : +65 96301458\r\rTel: +65 63371344\r\rFax: +65 65626290\r\rEmail : sankar@bioconsolutions.com', 'sg_65_96301458', '', 'sg_65_65626290', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(891, 'Procomp Printset Pte Ltd', '57 Loyang Drive, \r\rAnnex Building Level 3, \r\rSingapore 508968\r\r\r\rAttn : Mr Patrick Lim\r\rDID?: 6580 2695\r\rMobile?: 9681 2886\r\rEmail?: patrick@procomp.com.sg', 'sg_65_9681 2886', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(892, 'Marina Bay Sands Pte Ltd', '10 Bayfront Avenue, \r\rSingapore 018956 \r\r\r\rAttn : Ms Cillia Ong\r\rProcurement Manager, Procurement & Supply Chain\r\rDID : 6688 0502 \r\rMobile : 8292 0014 \r\rEmail : cillia.ong@marinabaysands.com', 'sg_65_8292 0014', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(893, 'Inmark Sg Pte Ltd', '31 Jurong Port Road\r\r#05-2728\r\rSingapore 619115\r\r\r\rAttn : Mr Jason\r\rHP : 9459 4682\r\rEmail : jasonT@inmarkinc.com', 'sg_65_9459 4682', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(894, 'Hua Yu Wee Seafood Restaurant', '462 Upper East Coast Road\r\rSingapore 466508\r\r\r\rAttn : Ms Poh\r\rHP : 9780 9595\r\rEmail : huayuwee@hotmail.com', 'sg_65_9780 9595', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(895, 'HongYi TCM Healthcare Pte Ltd', '16 Arumugam Rd\r\r#01-03 Lion Building D\r\rSingapore 409961\r\r\r\rTel : 6284 0897', 'sg_65_6284 0897', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(896, 'Cliftons', 'Lvl 11, Finexis Building,\r\r108 Robinson Rd, Singapore\r\rAttn :  Mr Billy Ooi \r\rHp   :  91122617\r\rBilly.Ooi@Cliftons.com', 'sg_65_91122617', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(897, 'Red House At The Quayside Pte Ltd', '#01-14 The Quayside\r\r60 Robertson Quay\r\rSingapore 238252\r\r\r\rAttn : Mr Chee Meng\r\rHP : 9107 5698\r\rTel : 6336 6048\r\rEmail : suppliers@redhouseseafood.com', 'sg_65_9107 5698', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(898, 'D''apres Nous D&B Pte Ltd (DNDB)', '114 Lavender street, \r\r#03-62 CT HUB 2\r\rSingapore 338729\r\r\r\rAttn : Ms Adity Dhingra\r\rInterior Deisgner\r\rTel : 6385 0195\r\rFax : 6385 0197\r\rEmail : Adity@dn-db.com', 'sg_65_6385 0195', '', 'sg_65_6385 0197', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(899, 'ASSA ABLOY Hospitality Pte Ltd', '133 New Bridge Road\r\r#24-01 Chinatown Point \r\rSingapore 059413\r\r\r\rAttn : Ms Stephanie SOH\r\rProject Sales Manager\r\rTel : +65 6305 7692\r\rMobile : +65 9225 0199\r\rEmail : Stephanie.Soh@assaabloy.com', 'sg_65_9225 0199', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(900, 'Brands Bridge Pte Ltd', '40 Jalan Pemimpin\r\rTat Ann Building, #02-01\r\rSingapore 577185\r\r\r\rAttn : Ms Grace Chua\r\rHP : 9729 1089\r\rEmail : grace.chua@brandsbridge.com', 'sg_65_9729 1089', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(901, 'Deco-Base Enterprise Pte Ltd', '32 Changi South Street 1 \r\rSingapore 486768   \r\r\r\rAttn : Ms Monica\r\rQuantity Surveyor        \r\rTel : 62836311 \r\rFax : 62850792  \r\rEmail : monica@deco-base.com', 'sg_65_62836311', '', 'sg_65_62850792', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(902, 'San Yu Adventist School', '299 Thomson Road\r\rSingapore 307652\r\rHP  : 91180855\r\rTel : 62566840 ext 120\r\rFax : 62566842\r\rmurtinsantim@syas.edu.sg', 'sg_65_91180855', '', 'sg_65_62566842', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(903, 'Roe Singapore Pte Ltd', '32 Ang Mo Kio Industrial Park 2\r\r#04-14 Sing Industrial Complex\r\rSingapore 569510\r\r\r\rAttn : Ms Jasmine Tan \r\rHP : 94381611\r\rTel : 67440990\r\rFax : 67450090\r\rEmail : jasmine@roesin.com.sg', 'sg_65_94381611', '', 'sg_65_67450090', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(904, 'The British Club', 'Attn : Ms Theresa Po\r\rAdmin Officer\r\rTel : +65 64101165\r\rFax : +65 6467 5263\r\rEmail : TheresaPo@britishclub.org.sg', 'sg_65_64101165', '', 'sg_65_6467 5263', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(905, 'Singapore Table Tennis Association', '297C, Toa Payoh Lorong 6\r\rSingapore 319389\r\r\r\rAttn : Mr Eddy Tay\r\rHP : 81188810\r\rEmail : eddy_tay@stta.org.sg', 'sg_65_81188810', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(906, 'TMG Projects Pte Ltd', '26 Defu Lane 7\r\rSingapore 539345\r\r\r\rAttn : Mr Fu Yih Shyang \r\rTel  : 62860933\r\rFax  : 62869133\r\ryihshyang@tmg.com.sg', 'sg_65_85044109', '', 'sg_65_62869133', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(907, 'TJS PTE. LTD.', '19 Tanjong Penjuru, \r\rSingapore 609021\r\r\r\rAttn : Mr Richard Soh\r\rHP : 9824 5459\r\rEmail : ct.soh@chemstationasia.com', 'sg_65_98245459', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(908, 'Sunprop Property And Asset Management Consultants Pte Ltd', 'As Managing Agent of Sutton Place\r\rFor & On Behalf of MCST 1605\r\r24 Farrer Road,\r\rSingapore 268829\r\r\r\rAttn : Mr Steve Chow\r\rProperty Officer\r\rTel : 6467 8652\r\rEmail : mcst1605@gmail.com', 'sg_65_6467 8652', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(909, 'Apex Sealing Technologies Pte Ltd', '23 Neythal Road, \r\rBlock P #01-01A \r\rSingapore 628588\r\r\r\rAttn : Ms Darren Lim \r\rMarketing Manager\r\rPurchasing & Supply Department\r\rHP : 9090 9498 \r\rEmail : marketing@apexsealing.com.sg', 'sg_65_9090 9498', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(910, 'Plan3 Design & Build Pte Ltd', '150 Orchard Road \r\r#08-18 Orchard Plaza \r\rSingapore 238841\r\r\r\rAttn : Mr Goldie Goh\r\rHP : 9835 4903\r\rEmail : goldie@plan3db.com', 'sg_65_9835 4903', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(911, 'GTED-X PTE LTD', 'Attn : Ms Carol Siar\r\rHP : 9683 9337\r\rEmail : siarcarol@gmail.com', 'sg_65_96839337', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(912, 'Alpha Formation Pte Ltd', '59 Ubi Avenue 1 \r\r#04-11 Bizlink Centre\r\rSingapore 408938\r\r\r\rAttn : Mr Raymond Poh\r\rProject Director\r\rTel : 6841 1008\r\rH/P : 9222 0343\r\rEmail : alphaformation555@gmail.com', 'sg_65_9222 0343', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(913, 'ROYAL EDLEN PTE LTD', '944 Bendemeer Road\r\r#06-06\r\rSingapore 339943\r\r\r\rAttn : Mr Roy/ Ms Yan Ru\r\rHP : 97947643 (Roy) / 81252479 (Yan Ru)\r\rEmail : chuayanru@gmail.com', 'sg_65_81252479', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(914, 'Beach Road Hotel (1886) Ltd', 'Raffles Hotel Singapore \r\r1 Beach Road \r\rSingapore 189673 \r\r\r\rAttn: Director of Purchasing (Group)', 'sg_65_6431 5431', '', 'sg_65_6431 5435', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(915, 'MINDS – Tampines Training & Development Centre', 'Blk 267 Tampines Street 21\r\r#01-09\r\rSingapore 520267\r\r\r\rAttn : Ms Poonam\r\rTel : 6260 5886     \r\rFax : 6260 5686\r\rEmail : poonam.ttdc@minds.org.sg', 'sg_65_6260 5886', '', 'sg_65_6260 5686', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(916, 'Singapore National Eye Centre ( SNEC )', '11 Third Hospital Avenue\r\rSingapore 168751\r\r1St Storey, BMS Room\r\r\r\rAttn : Mr Muhd Nur Wagiyoh ( Operations - Maintenance )\r\rTel : 6322 9343   \r\rFax : 6322 9345\r\rHp : 91005214\r\rEmail : muhammad@cornerstonefm.com.sg', 'sg_65_91005214', '', 'sg_65_6322 9345', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(917, 'Nam Aik Builders Pte Ltd', 'Attn : Mr Onion\r\rProject Manager \r\rH/P: 8611 3926\r\rEmail : y.onion@hotmail.com', 'sg_65_8611 3926', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(918, 'Cleantec Engineering (S) Pte. Ltd.', '201 Woodlands Avenue 9, \r\r#04-05\r\rSingapore 738955\r\r\r\rAttn : Ms Geraldine\r\rTel : 62541151\r\rEmail : geraldine@cleantec.com.sg', 'sg_65_62541151', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(919, 'Assisi Hospice', '820 Thomson Rd\r\rSingapore 574623\r\r\r\rAttn :  Mr Andy Tham Chee Leong \r\rHP   :  98753130\r\randy.tham.cl@assisihospice.org.sg', 'sg_65_98753130', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(920, 'Sensuri Coffee Pte Ltd', 'Attn : Ms Suriyanti Ng\r\rHP : 9366 9107\r\rEmail : suriyanti.ng@gmail.com', 'sg_65_93669107', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(921, 'Cheers Holdings (2004) Pte Ltd', '1 Joo Koon Circle,\r\r#13-01 FairPrice Hub,\r\rSingapore 629117', 'sg_65_97671527', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(922, 'Extra Space Singapore Holdings Pte Ltd', '301 Boon Keng Road \r\r#08-02\r\rSingapore 339779\r\rAttn :  Ms Susanna HO \r\rOperations Director\r\rDID  :  6771 3118\r\rhp   :  98169914\r\rsusannaho@extraspace.com.sg', 'sg_65_98169914', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(923, 'Orange Clove Catering', '1 Kaki Bukit Road \r\r#05-03/04\r\rEnterprise One \r\rSingapore 416236\r\rAttn :  Ms Low Sue Fun \r\rTel  :  65930072\r\rhp   :  97374523\r\rsuefun.low@orangeclove.com.sg', 'sg_65_97374523', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(924, 'Toppan Forms (S) Pte Ltd', '41, Joo Koon Circle \r\rSingapore 629065 \r\r\r\rAttn : Mr Ray Kow\r\rAdmin & HR Department\r\rTel : 6862 3811\r\rDID : 6868 9281\r\rFax : 6861 8817\r\rEmail : raykow@tfsg.com.sg', 'sg_65_6868 9281', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056808, 0, 1),
(925, 'GK MEDICAL GROUP PTE LTD', '235 Jalan Besar Road\r\rSingapore 208909\r\r\r\rAttn : Ms Mandy Ng\r\rH/P : 9061 1729\r\rDID : 6297 3303\r\rEmail : mandy@gkaesthethics.com', 'sg_65_9061 1729', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(926, 'Yishun Community Hospital Pte Ltd', 'c/o 90 Yishun Central	\r\rSingapore 768828	\r\r\r\rAttn : Ms Regina Cheong\r\rMaterials Management Department	\r\rTel : 6602 3321	\r\rEmail : cheong.regina.ps@alexandrahealth.com.sg', 'sg_65_6602 3321', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(927, 'HR Law Academy Pte Ltd', '190 Clemenceau Avenue, #02-31 Singapore Shopping Centre, Singapore 239924', 'sg_65_6334 3177', '', 'sg_65_6334 3179', 'Initial upload imported from Old system', 1, 1, '48', 192, '', '', '', '', '', 1, 8, 1477056809, 1477360639, 1),
(928, 'LIV STUDIO', 'Attn : Mr Joel Lin\r\rHP : 8777 2255\r\rEmail : lin.zl.joel@gmail.com', 'sg_65_8777 2255', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(929, 'PrintPix Pte Ltd', 'Blk 164 Kallang Way\r\r#01-05/06\r\rSingapore 349248\r\r\r\rAttn : Mr Dave Ong\r\rSales Manager\r\rTel : 6297 4248\r\rHp : 9785 2279\r\rEmail : dave@printpix.com.sg', 'sg_65_9785 2279', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(930, 'East Marine Pte Ltd', 'Attn : Mr Alex \r\rHP : 9654 0307\r\rEmail : Alex@eastmarine.com.sg', 'sg_65_9654 0307', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(931, 'NKB PTE LTD', '89 Short Street,\r\r#09-01 Golden Wall Centre,\r\rSingapore 188216\r\r\r\rAttn : Mr Kannan\r\rH/P : 98004264\r\rEmail : kannan@nkbpl.com.sg', 'sg_65_98004264', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(932, 'Heretze Pte Ltd', 'Attn : Mr Aug Leo\r\rHP : 93683838', 'sg_65_93683838', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(933, 'Green Button Pte Ltd', 'Attn : Mr William Goh\r\rHP : 8118 5733\r\rEmail : william.goh@greenbutto.com.sg', 'sg_65_8118 5733', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(934, 'Genesis Performance Center', 'Attn : Ms Eunice \r\rHP: 97289898\r\rEmail : genesisemails@gmail.com', 'sg_65_97289898', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(935, 'KURAMATHIISLAND RESORT', 'KURAMATHIISLAND RESORT\r\rUniversal Resorts, 39 Orchid Magu, Male’ 20213, Maldives \r\r\r\rAttn  : Mr IbrahimAli\r\rResident Manager\r\rTel   : +960 666 0527\r\rDirect: +960666 0141\r\rMob   : +960 777 1140\r\rFax   : +960 666 0556\r\ribrahim.ali@kuramathi.com', 'sg_65_9607771140', '', 'sg_65_9606660556', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(936, 'SpaceVox Design Pte Ltd', 'Attn : Ms Marichi \r\rTel : 62555196\r\rEmail : marichi@voxdesign.com.sg', 'sg_65_62555196', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(937, 'TG25 Pte Ltd', '500 Old Choa Chu Kang Rd\r\rSingapore 698924\r\r\r\rAttn : Mr Peter Fong\r\rHp: 94566881\r\rEmail : peter.fong@stlodge.sg', 'sg_65_94566881', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(938, 'Singapore Press Holdings Ltd', '1000 Toa Payoh North, News Centre, Singapore 318994 \r\r\r\rAttn :  Mr Philip Tan CS\r\rHuman Resource | Sports & Leisure Club\r\rTel  : 6319 1470\r\rphilipt@sph.com.sg', 'sg_65_11111', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(939, 'Singapore SportsHub', 'Public Private Partnership (PPP)\r\r1 Stadium Drive #02-01 National \r\rStadium Singapore\r\rSingapore 397629\r\r\r\rAttn : Ms Sneha Nagaokar\r\rSenior Assistant Engineer\r\rDirect : 66539239 \r\rMobile : 81895275\r\rEmail : sneha.nanasaheb@cushwake.com', 'sg_65_81895275', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(940, 'GSAFE Pte Ltd', 'Blk 3018 Bedok Nth St.5\r\r#01-11 Eastlink\r\rSingapore 486132\r\r\r\rAttn : Mr Joseph Lim\r\rHP : 97308943\r\rEmail : joseph@gsafe.com.sg', 'sg_65_97308943', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(941, 'Select Group Limited', '24A Senoko South Road \r\rSingapore 758099\r\r\r\rAttn : Ms Jocelyn \r\rHP : 82236978\r\rEmail : hillstreet@select.com.sg', 'sg_65_82236978', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(942, 'ACP CONSTRUCTION  PTE LTD', '19 Tanglin Road\r\r#12-01 Tanglin Shopping Centre \r\rSingapore 247909 \r\r\r\rAttn : Mr Kevin Teo \r\rSenior QS\r\rTel : +65 67357865\r\rFax : +65 67356005\r\rEmail : kevin@acplcpl.com', 'sg_65_67357865', '', 'sg_65_67356005', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(943, 'KIM HENG MARINE AND OILFIELD PTE LTD', 'NO.9 PANDAN CRESCENT\r\rSINGAPORE 128465\r\r\r\rAttn: Ms Jeliane Tan\r\rHP: 9838 3887\r\rTEL: 6773 9610 / 6773 9611\r\rFAX: 6779 0552\r\rEMAIL : jeliane.tan@kimheng.com.sg', 'sg_65_9838 3887', '', 'sg_65_6779 0552', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(944, 'Singapore Jamco Services Pte Ltd', 'No. 9 Loyang Way\r\rKrislite Building #04-01\r\rSingapore 508722\r\r\r\rAttn : Ms Nalini\r\rMaterials – Purchaser\r\rTel : 6718 6578\r\rEmail : sjs_purchasing@sjamco.com.sg', 'sg_65_6718 6578', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(945, 'Grandwork Interior Pte Ltd', '7, Sungei Kadut Street 3\r\rGrandwork Building\r\rSingapore 729142\r\r\r\rAttn : Ms Inez Goh\r\rTel : 6732 7320\r\rFax : 6732 8460\r\rEmail : inezgoh@grandworkinterior.com', 'sg_65_67327320', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(946, 'Sonoma Enterprise Pte Ltd', '15 Jalan Tepong, #05-09/08,\r\rJurong Food Hub,\r\rSingapore 619336\r\r\r\rAttn : Ms Liu\r\rHP : 91375289\r\rEmail : ldh1964@hotmail.com', 'sg_65_91375289', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(947, 'SAHE Food Enterprise Pte Ltd', '52 Senoko Drive	\r\rSingapore 758233\r\r\r\rAttn : Mr Chua Kang Tor\r\rTel : 6483 2550	\r\rFax : 6482 3730	\r\rHP : 9876 8441\r\rEmail : chuakangtor@gmail.com', 'sg_65_9876 8441', '', 'sg_65_6482 3730', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(948, 'Oerlikon Contraves Pte Ltd', 'A Rheinmetall Defence Company\r\r12 Tuas Ave 7\r\rSingapore 639267\r\r\r\rAttn : Mr Clement Liew\r\rTel : 6416 0186\r\rHP : 9295 2295\r\rEmail : clement.liew@rheinmetall.sg', 'sg_65_9295 2295', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(949, 'Hong Jun Contruction Pte Ltd', '1 Bukit Batok Crescent\r\rWCEGA Plaza #06-05\r\rSingapore 658064\r\r\r\rAttn : Ms Cherrie Chay\r\rTel : 66597933\r\rFax : 66597055\r\rEmail : admin@hongjun.com.sg', 'sg_65_66597933', '', 'sg_65_66597055', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(950, 'National University Hospital Pte Ltd', '5 Lower Kent Ridge Road, \r\rservice Block, Basement 1. \r\rSingapore 119074 \r\r\r\rAttn : Ms Clara Chua \r\rMaterial Management Department (MMD)\r\rDID : 6772 2104\r\rFax : 6779 5528\r\rEmail : clara_chua@nuhs.edu.sg', 'sg_65_6772 2104', '', 'sg_65_6772 2104', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(951, 'Woodlands Zone-2 RC', 'Blk 846, Woodlands Ave 4,\r\r#01-612\r\rSingapore 730846\r\r\r\rAttn : Ms Carol Koh\r\rTel : 63689938', 'sg_65_63689938', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(952, 'Seaquest Marine Systems Pte Ltd', '27 Woodlands Industrial Park E1\r\r#01-01 Hiangkie Industrial Building\r\rSingapore 757718\r\r\r\rAttn : Mr Sivaganesh (Project Engineer)\r\rTel : 6767 9662 \r\rFax : 6767 9693 \r\rHp :  9781 5490\r\rEmail : sivaganesh@seaquestmarine.com.sg', 'sg_65_9781 5490', '', 'sg_65_6767 9693', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(953, 'id.inc Interiors Pte. Ltd.', '62 Ubi Rd 1, Oxley Biz Hub 2, #09-11,  Singapore 408734', 'sg_65_63201687 ', '', 'sg_65_65382808', 'Initial upload imported from Old system', 1, 1, '15', 192, '', '', '', '', '', 1, 8, 1477056809, 1477375546, 1),
(954, 'The Beacon Room Pte Ltd', 'No 6 Raffles Boulevard \r\r#01-04/05 Marina Square\r\rSingapore 039594\r\r\r\rAttn : Mr Wandi Sani\r\rBar manager\r\rHP : 98229622\r\rEmail : wandisani@gmail.com', 'sg_65_98229622', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(955, 'Siemens Pte Ltd', 'Attn : Mr Santhosh Kumar\r\rHP : 84217852\r\rEmail : santhoshkumar.sankar@siemens.com', 'sg_65_84217852', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(956, 'THK Nursing Home', '20 Jalan Eunos \r\rSingapore 419494 \r\r\r\rAttn : Mr Calvin I. Yeo \r\rFacilities Executive (Operation)\r\rTel : 6742 4429 \r\rFax : 6547 5507 \r\rHP : 92762936\r\rEmail : calvinyeo@thknh.org.sg', 'sg_65_92762936', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(957, 'ABS PTE LTD', '12323232', 'sg_65_7777', '', 'sg_65_3322', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(958, 'Fadely Enterprises & Construction Pte Ltd', '120 Lower Delta Road \r\r#13-01 Cendex Centre \r\rSingapore 169208\r\r\r\rAttn : Ms Jane\r\rHP : 90707061\r\rTel: 62733091\r\rFax: 62733191\r\rEmail : jinghui.chan119@gmail.com', 'sg_65_90707061', '', 'sg_65_62733191', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(959, 'Begonia Pte Ltd', 'Attn : Mr Hussain Muhammad\r\rHP : 9765 7795\r\rEmail : huss8in@hotmail.com', 'sg_65_9765 7795', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(960, 'B H S Kinetic Pte Ltd', '10 Pandan Road \r\rSingapore 609258\r\r\r\rAttn : Mr Sean Soh \r\rHuman Resource Executive\r\rTel : 6261 2665 \r\rFax : 6262 0334\r\rEmail : sean.soh@bhskinetic.com', 'sg_65_6261 2665', '', 'sg_65_6262 0334', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(961, 'BRINK’S SINGAPORE PTE LTD', '1 Kaki Bukit Road 1\r\r#02-33 Enterprise One\r\rSingapore 415934\r\r\r\rAttn : Ms Celine Lam\r\rSenior Executive\r\rAdmin & Procurement\r\rTel : 6591 7777 Ext 98\r\rHP  : 9298 4911\r\rFax : 6591 7778\r\rEmail :  PoiWeei.Lam@BrinksGlobal.com', 'sg_65_9298 4911', '', 'sg_65_6591 7778', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(962, 'Star Shield Pte Ltd', '100 Pasir Panjang Road \r\r#02-10\r\rSingapore 118518\r\r\r\rAttn : Mr Vinson Lee\r\rHP : 97250782\r\rEmail : vinson.lee@starshield.sg', 'sg_65_97250782', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(963, 'Nippon Paint (S) Co Pte Ltd', '1 First Lok Yang Road\r\rJurong. Singapore 629728\r\r\r\rAttn : Ms Mary Ng\r\rTechnical Dept\r\rTel : (65) 6319 7167\r\rFax : (65) 6268 7731\r\rEmail : maryng@nipponpaint.com.sg', 'sg_65_6319 7167', '', 'sg_65_6268 7731', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(964, 'Adeco Interior Pte Ltd', '6D Mandai Estate\r\r#07-04 M-Space\r\rSingapore 729938\r\r\r\rAttn : Mr Andy\r\rHP : 9663 2184\r\rEmail : andyt229@gmail.com', 'sg_65_96632184', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(965, 'Pikasa Builders', '80 Kaki Bukit Industrial Terrace\r\rSingapore 416160\r\r\r\rAttn : Ms Jaymie Royce C. Lavarez\r\rQuantity Surveyor\r\rTel : 67479865\r\rFax : 67478655\r\rEmail : jaymie@pikasabuilders.com', 'sg_65_67479865', '', 'sg_65_67478655', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(966, 'GS Engineering & Construction Corp.', 'TEL T301 (4 in 1 Mega Depot) Project\r\r10A Koh Sek Lim Road\r\rSingapore 485980\r\r\r\rAttn : Ms Gin Tan\r\rProcurement Officer\r\rHP : 9871 9240\r\rEmail : gin.tan@gsconst.co.kr', 'sg_65_9871 9240', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(967, 'Singapore Pools (Private) Ltd', '210 Middle Road #01-01\r\rSingapore Pools Building\r\rSingapore 188994\r\r\r\rAttn : Mr Richard Tan\r\rHP : 93867949\r\rEmail : richardtan@sgpoolz.com.sg', 'sg_65_93867949', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(968, 'ID21 Pte Ltd', '1 Kim Seng Promenade\r\rGreat World City East Tower\r\rUnit 05-01/01A/01B\r\rSingapore 237994\r\r\r\rAttn : Mr Stanley Chang\r\rProject Manager    \r\rDID : +65 6716 0749\r\rMobile : +65 9648 2122 \r\rEmail : chang@id21.com.sg', 'sg_65_9648 2122', '', 'sg_65_6270 0306', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(969, 'ONE15 Marina Club', '#01-01, 11 Cove Drive\r\rSentosa Cove\r\rSingapore 098497\r\r\r\rAttn : Ms Pei Fen\r\rTel : 6309 2443\r\rEmail : pfliew@one15marine.com', 'sg_65_6309 2443', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(970, 'Semba Singapore Pte Ltd', 'No. 73A Tanjong Pagar Road, \r\rSingapore 088494\r\r\r\rAttn : Ms Karen\r\rHP : 9627 4230\r\rTel : 6339 1318\r\rFax : 6339 0136\r\rEmail : karen.fampo@semba.com.sg', 'sg_65_9627 4230', '', 'sg_65_6339 0136', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(971, 'All Link (21) Engineering Pte Ltd', 'Attn : Mr Terence\r\rHP : 9362 8029\r\rEmail : terence.chan@alllink.com.sg', 'sg_65_93628029', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(972, 'Rye & Pint Brewery Pte Ltd', '27 Tuas Bay Walk\r\r#03-09 Westview Food Factory\r\rSingapore 637127\r\r\r\rAttn : Mr Luther Goh\r\rHP : 90221517\r\rEmail : luther@ryepint.com', 'sg_65_90221517', '', 'sg_65_62623164', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(973, 'Onui Pte Ltd', '27 West Coast Highway    \r\r#01-18/19 YESS CENTER    \r\rSingapore 117861 \r\r\r\rAttn : Ms Yuseon Jeong\r\rHP : 81392852\r\rEmail : westjushinjung@yahoo.com', 'sg_65_81392852', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(974, 'Guston Pte Ltd', 'Sofitel Singapore City Center\r\r20 Collyer Quay \r\r#15-02\r\rSingapore 049319\r\rAttn :  Mr Richard KOH \r\rhp   :  97463024\r\rpu.sofitelscc@gmail.com', 'sg_65_97463024', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(975, 'Valencia Design Pte Ltd', '35 Tannery Road\r\r#06-05 Ruby Industrial Complex\r\rSingapore 347740\r\r\r\rAttn : Ms Kim Pang \r\rHP : 9739 1292   \r\rTel : 6345 7897 \r\rFax : 6345 1209\r\rEmail : kim_pang@valhouse.com.sg', 'sg_65_9739 1292', '', 'sg_65_6345 1209', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(976, 'Timworx Associates Pte Ltd', '3 Changi South Lane\r\r#01-06 Singapore 486118\r\rAttn : Mr Jonathan Toh\r\rTel: 6542 4833\r\rMobile : 9062 4649\r\rFax : 6542 7353', 'sg_65_9062 4649', '', 'sg_65_6542 7353', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(977, 'Hwa Hing Trading Pte Ltd', '15 Yishun Industrial Street 1\r\r#06-29 Win 5\r\rSingapore 768091\r\r\r\rAttn : Ms Myrak\r\rHP : 9112 4877\r\rEmail : hwahing@singnet.com.sg', 'sg_65_9112 4877', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(978, 'Fast Track Events Pte Ltd', 'Attn : Ms Cherissa Souw\r\rHP : 9758 0278\r\rTel : 6759 1897\r\rEmail : cherissa@fastrackevents.com', 'sg_65_9758 0278', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(979, 'Left Leg Innovation Pte Ltd', '186 Toa Payoh Central\r\r#01-432 \r\rSingapore 310186\r\r\r\rAttn : Mr Fei Hong\r\rHP : 8188 2539\r\rEmail : feihong87yan@gmail.com', 'sg_65_8188 2539', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(980, 'Keppel Logistics Pte Ltd', '7 Gul Circle\r\rSingapore 629563	 \r\r\r\rAttn : Ms Chhan Siew Wai\r\rTel : 6861 1911\r\rFax : 6861 2168\r\rDID : 6868 2375\r\rHP : 9223 8519\r\rEmail : chhan.siewwai@keppellog.com', 'sg_65_9223 8519', '', 'sg_65_6861 2168', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(981, 'DINING INNOVATION Asia-Pacific Pte.Ltd.', '175A Bencoolen st. #12-09/10 \r\rBurlington Square\r\rSingapore 189650\r\rTel : +65 6336 4211\r\rFax : +65 6336 7335 \r\rHP  : +65 9781 5418\r\rasaoka@dining-innovation.com', 'sg_65_9781 5418', '', 'sg_65_6336 7335', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(982, 'KRD Enterprise Pte Ltd', '81 Tagore Lane, \r\r#04-06 TAG A,\r\rSingapore 787502\r\r\r\rAttn : Mr Anand.V\r\rProject Manager\r\rHP: 8290 7730\r\rEmail : anand@krdenterprise.com', 'sg_65_8290 7730', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056809, 0, 1),
(983, 'Ethan K Pte Ltd', '23 New Industrial Road                                                  \r\r#02-02/03 Solstice Business Centre                           \r\rSingapore 536209                                                          \r\r\r\rAttn : Mr Jackson Ong\r\rHP : 9152 4237\r\rEmail : jackson.ong@ethan-k.com', 'sg_65_9152 4237', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(984, 'GSK Construction & Engineering Pte Ltd', '52 Ubi Ave 3\r\r#03-32 Frontier\r\rSingapore 408867\r\r\r\rAttn : Mr Ian\r\rTel : 6746 2766\r\rFax : 6746 6525\r\rMobile : 9047 2177\r\rEmail : ianpehgsk@gmail.com', 'sg_65_9047 2177', '', 'sg_65_6746 6525', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(985, 'Ann Consulting Pte Ltd', 'Blk 1003 Bukit Merah Central \r\r#04-18 Inno Centre\r\rSingapore 159836\r\r\r\rAttn :   Mr Mark You \r\rHP   :   82337060\r\rm@replyx.com.sg', 'sg_65_82337060', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(986, 'Delcon Technology (S) Pte Ltd', '2 Tukang Innovation Grove\r\r#09-05 JTC Medtech Hub\r\rSingapore 618305\r\r\r\rAttn : Mr KN Lee\r\rHP : 9004 0013\r\rEmail : delcon@singnet.com.sg', 'sg_65_9004 0013', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(987, 'HENG XU ZHAO YAN (xiao chu) PTE LTD', 'No 5 mosque street \r\rSingapore 059485 \r\r\r\rAttn : Ms Sherry Shi \r\rTel : 91143993\r\rEmail : sherryshi3993@gmail.com', 'sg_65_91143993', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(988, 'Eurokars Group', 'Eurokars Centre \r\r12 Sungei Kadut Avenue \r\rSingapore 729648\r\rTel :  6363 3003\r\rFax :  6369 3003\r\rmichaelleong@eurokars.com.sg', 'sg_65_96826821', '', 'sg_65_63693003', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(989, 'Gateway Resource Pte Ltd', '3615 Jalan Bukit Merah, \r\rGateway Theatre, \r\rSingapore 159461\r\r\r\rAttn : Mr Eddie Pang\r\rOperations Manager\r\rDID : 6424 9424\r\rMobile : 9827 4837\r\rEmail : eddie.pang@fcbc.org.sg', 'sg_65_9827 4837', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(990, 'Groz-Beckert East Asia LLP', '29 Ubi Road 4  \r\r#03-01 Wangi Industrial Building \r\rSingapore 408619 \r\r\r\rAttn  : Mr Meivin \r\rHP : 9766 5334\r\rEmail : melvin.goh@groz-beckert.com', 'sg_65_9766 5334', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(991, 'BINJAI INTERIOR DESIGN PTE LTD', 'Blk 4008 Ang Mo Kio Avenue 10 #02-11 TECHPLACE 1\r\rSingapore 569625\r\rAttn :  Ms Angie Ng \r\rhp   :  97973733\r\renquiry@binjai.com.sg', 'sg_65_97973733', '', 'sg_65_6221 6931', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(992, 'ST DIVERS TECHNICS PTE LTD', '50 Tuas Avenue 11, \r\r#02-22, Tuas Lot\r\rSingapore 639107\r\r\r\rAttn : Mr Simon Tee\r\rHP  : 9626 9203\r\rTel : 6748 7337\r\rEmail : simon@stdivers.com.sg', 'sg_65_96269203', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(993, 'Milan Decoration & Construction Pte Ltd', '53 Ubi Avenue 1, \r\r#05-20 Paya Ubi Industrial Park, \r\rSingapore 408934\r\r\r\rAttn : Mr Vincent\r\rTel : +65 6745 1508\r\rFax : +65 6745 1533\r\rHp : +65 8309 7076\r\rEmail:  Vincent.liew@milandec.com.sg', 'sg_65_8309 7076', '', 'sg_65_6745 1533', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(994, 'Gaga Holdings Pte Ltd', 'Zara Liat Tower\r\r541 Orchard Road\r\rLiat Tower #02-02\r\rSingapore 238801\r\rAttn : Mr William Oh\r\rMobile : 8699 5340', 'sg_65_8699 5340', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(995, 'FC Retail Trustee Pte Ltd (as trustee manager of Sapphire Star Trust)', '83 Punggol Central\r\r#B2-34 Waterway Point\r\rSingapore 828761\r\r\r\rAttn : Mr See Lye Kiat\r\rHP : 9770 4669\r\rTel : 6812 7300\r\rFax : 6385 9031\r\rEmail : lyekiat.see@fraserscentrepoint.com', 'sg_65_97704669', '', 'sg_65_63859031', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(996, 'The Warehouse Hotel', 'I Hotel Pte Ltd \r\r( c/o The Warehouse Hotel ) \r\r10 Anson Road\r\r#09-12\r\rSingapore 079903\r\r\r\rAttn   : Mr Tarun Karlra\r\rGeneral Manager\r\r\r\rMobile :  +659180 1992\r\rOffice :  +656338 8035 \r\rtarun.kalra@thewarehousehotel.com', 'sg_65_9180 1992', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(997, 'ISS Catering Services Pte Ltd', 'Raffles Hospital\r\rLevel 3 Kitchen\r\r585 North Bridge Road\r\rSingapore 188770\r\r\r\rAttn : Mr Richard Chong\r\rHP : 9008 6906\r\rTel : 6311 1398\r\rFax : 6311 2393\r\rEmail : foodservice@rafflesmedical.com', 'sg_65_9008 6906', '', 'sg_65_6311 2393', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(998, 'Eastern Holdings Company Pte Ltd', 'Blk 78B Telok Blangah St 32 \r\r#03-15, Singapore 102708\r\r\r\rAttn : Mr Darren Bell\r\rProjects Coordinator\r\rMobile : +65 9247 0384\r\rEmail: darren.bell@anytimefitness.sg', 'sg_65_9247 0384', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(999, 'Four Leaves Pte Ltd', '11 Enterprise Road \r\rSingapore 629823\r\rAttn :  Mr Jimmy Tan \r\rTel  :   62687944\r\rhp   :   91092416\r\rjimmytan@fourleaves.com.sg', 'sg_65_91092416', '', 'sg_65_62665470', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1000, 'Shell Paya Lebar PIE', '98 Paya Lebar Road\r\rSingapore 409008\r\rTel: 67480560\r\r\r\rAttn : Mr Melvin\r\rHP : 9661 5676\r\rEmail : melyhl38@gmail.com', 'sg_65_9661 5676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1001, 'Shell Macpherson', '259 Macpherson Road\r\rSingapore 348584\r\rTel: 62884573\r\r\r\rAttn : Mr Melvin\r\rHP : 9661 5676\r\rEmail : melyhl38@gmail.com', 'sg_65_9661 5676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1002, 'Shell Bukit Batok Road', '11 Bukit Batok West Ave 3\r\rSingapore 659166\r\rTel: 65696343\r\r\r\rAttn : Mr Melvin\r\rHP : 9661 5676\r\rEmail : melyhl38@gmail.com', 'sg_65_9661 5676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1003, 'Shell Boon Lay', '2 Boon Lay Ave\r\rSingapore 649960\r\rTel: 62681944\r\r\r\rAttn : Mr Melvin\r\rHP : 9661 5676\r\rEmail : melyhl38@gmail.com', 'sg_65_9661 5676', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1004, 'Batam Fast Ferry Pte Ltd', '1, Maritime Square\r\rHarbourFront Centre #11-20\r\rSingapore 099253\r\r\r\rAttn : Mr Richard Goh\r\rTel : 96527650\r\rEmail : it.support@batamfast.com', 'sg_65_96527650', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1005, 'Singapore MY OUTLETS PTE LTD', '21 Woodlands Close, \r\rPrimz Bizhub, #01-19/29 \r\rSingapore 737854.\r\r\r\rAttn : Mr Daniel Tang\r\rAssistant Supermarket Manager\r\rMobile : +65 9151 5145\r\rOffice : +65 6250 2360\r\rEmail : daniel@myoutlets.com.sg', 'sg_65_9151 5145', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1006, 'TDB International Pte Ltd', '18 Kaki Bukit Road 3\r\r#04-04 Entrepreneur Business Centre\r\rSingapore 415978\r\r\r\rAttn : Mr Joe Lai\r\rProject Coordinator\r\rHP : 9813 5028\r\rTel : 6744 0966\r\rFax : 6842 7585\r\rEmail : joelai@tdb-int.com.sg', 'sg_65_9813 5028', '', 'sg_65_6842 7585', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1007, 'JC Connections Pte Ltd', '47 Kallang Pudding Road\r\r#05-09/10 Crescent @ Kallang\r\rSingapore 349318\r\r\r\rAttn : Mr Anthony Low\r\rHP : 9101 2552\r\rTel : 6747 4822\r\rFax : 6748 8340\r\rEmail : anthony@jc-net.com.sg', 'sg_65_9101 2552', '', 'sg_65_6748 8340', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1008, 'Beacon International Management Group', 'Block 105 Sims Avenue\r\r#05-13 Chancerlodge Complex\r\rSingapore 387429\r\r\r\rAttn : Mr Tan\r\rHP : 90024887\r\rEmail : mgg.kayden@gmail.com', 'sg_65_90024887', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1009, 'Oceanic Underwater Services (Pte) Ltd', 'Blk M, No. 95 Pandan Loop\r\rSingapore 126888\r\r\r\rAttn : Mr Lionel Ng\r\rHP : 9815 4366\r\rTel : 6779 7719\r\rFax : 6777 6363\r\rEmail : oceanicus@singnet.com.sg / oceanic@starhub.net.sg', 'sg_65_9815 4366', '', 'sg_65_6777 6363', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1010, 'AJ Hackett Sentosa', '30 Siloso Beach Walk\r\rSentosa Island\r\rSingapore 099007\r\r\r\rAttn : Mr Stefan Roos\r\rGeneral Manager\r\rHP : 9100 8562 \r\rEmail: stefan.roos@ajhackett.com', 'sg_65_9100 8562', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1011, 'ZY International Pte Ltd', '175 Bencoolen Street, \r\r# 01-57 Burlington Square\r\rSingapore 189649\r\r\r\rAttn : Mr Derrick\r\rHP : 8481 7996\r\rEmail : xudeli@hotmail.com', 'sg_65_84817996', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1012, 'Singapore Navy RSS Resolution', 'Attn : Mr Kelvin\r\rHP : 9270 1636\r\rFax : 6544 4378', 'sg_65_9270 1636', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1013, 'MSTS Asia (S) Pte Ltd', '#67 Tuas South Ave 1, Seatown Industrial Centre, Singapore 637579', 'sg_65_6515 8193', '', 'sg_65_6515 8198', 'Initial upload imported from Old system', 1, 1, '44', 192, '', '', '', 'http://www.msts-my.org', '', 1, 8, 1477056810, 1477379066, 1),
(1014, 'Eurofragance Asia Pacific Pte Ltd', '56 Loyang Way\r\rLoyang Enterprise\r\r#01-09/11\r\rSingapore 508775\r\r\r\rAttn :  Mr Lim Fong Hoong   \r\r        Plant Manager\r\rTel  : 6734 7328\r\rFax  : 6734 4223\r\rhp   : 9662 0295\r\rfhoong@eurofragance.com', 'sg_65_96620295', '', 'sg_65_67344223', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1015, 'Ballyhoo Exhibitions & Events', '2 Jalan Rajah\r\r#02-12 Golden Wall Flatted Factory\r\rSingapore 329134\r\r\r\rAttn : Mr Eugene Aw\r\rSenior Account Manager\r\rExhibitions & Events\r\rHP : 8322 1261  \r\rTel : 6258 1696   \r\rFax : 6254 0266\r\rEmail : eugene@ballyhoo.com.sg', 'sg_65_8322 1261', '', 'sg_65_6254 0266', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1016, 'Demax Design Pte Ltd', '60 Kaki Bukit Place\r\r#08-07 Eunos Techpark\r\rSingapore 415979\r\r\r\rAttn: Ms Melodie \r\rTel : 6296 3639\r\rHP :  9820 5705\r\rEmail: melodie@demaxdesign.com', 'sg_65_98205705', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1017, 'MRI Diagnostics Pte Ltd', '8 Sinaran Drive\r\r#05-02 Novena Specialist Centre\r\rSingapore 307470\r\r\r\rAttn : Ms Sharon Low\r\rHP : 9769 0670\r\rTel: 6694 3431\r\rFax: 6694 1431\r\rEmail : enquiry@novenamri.com / sharonlow@novenamri.com', 'sg_65_9769 0670', '', 'sg_65_6694 1431', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1018, 'Sync Cycle Pte Ltd', 'Attn : Ms Jas\r\rHP : 93366966\r\rEmail : hello@synccycle.com.sg', 'sg_65_93366966', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1019, 'eWerkz Projects', 'M38 @ 38 Jalan Pemimpin,\r\r#07-01/02, Singapore 577178\r\r\r\rAttn : Ms Wee Yi Ling \r\rEvent Executive\r\rDID : 6804 5165 \r\rHp  : 9831 4631\r\rTel : 6804 5160 \r\rFax : 6804 5176\r\rEmail : yilingwee@ewerkzprojects.com', 'sg_65_9831 4631', '', 'sg_65_6804 5176', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1020, 'Xtra Designs Pte Ltd', '6 Raffles Boulevard\r\r#02-240 Marina Square\r\rSingapore 039594\r\r\r\rAttn : Mr Nick Tan\r\rT : +65 6336 4664\r\rF : +65 6338 4664\r\rM : +65 9830 0856\r\rE : nick@xtra.com.sg', 'sg_65_9830 0856', '', 'sg_65_6338 4664', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1021, 'HERITANCE RESOURCES PTE LTD', 'Attn : Mr Joe Yeo TH\r\rMobile: +65 84186291\r\rEmail: joe@heritance.com.sg', 'sg_65_84186291', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1022, 'Mercure Singapore Bugis', '122 Middle Road\r\rSingapore 188973\r\r\r\rAttn : Ms Tan Geok Mei\r\rPurchasing Executive\r\rDID : 6521 6021\r\rFax : 6822 8901\r\rHP  : 9831 0359\r\rEmail : HA0D7-PU@accor.com', 'sg_65_98310359', '', 'sg_65_6822 8901', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1023, 'JTC Corporation', 'The JTC Summit\r\r8 Jurong Town Hall Road\r\rSingapore 609434\r\r\r\rAttn : Mr Lee Chun Kiat\r\rHP : 9178 3503\r\rDID : 6883 3355\r\rEmail : lee_chun_kiat@jtc.gov.sg', 'sg_65_91783503', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1024, 'Herim Himer Pte Ltd', '8 Eu Tong Sen Street, \r\rThe Central, \r\rSingapore 059818\r\r\r\rAttn : Mr Leon \r\rHP : 97228129\r\rEmail : leon.ccw@hotmail.com', 'sg_65_97228129', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1);
INSERT INTO `account` (`id`, `name`, `address`, `contact_no`, `email`, `fax`, `remark`, `country_id`, `branch_id`, `industries`, `physical_country`, `physical_city`, `company_name`, `company_address`, `company_website`, `company_email`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(1025, 'Teppei Pte Ltd', '99B Tanjong Pagar\r\rSingapore 088520\r\r\r\rAttn : Ms Serena Ong\r\rHP : 9744 1217\r\rEmail : serenaong_1217@yahoo.com.sg', 'sg_65_97441217', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1026, 'RoStac Pte Ltd', 'Attn : Ms Samantha Woodward \r\rHP: +65 9002 1189\r\rEmail : Samantha.Woodward@anytimefitness.sg', 'sg_65_9002 1189', '', '', 'Initial upload imported from Old system', 1, 1, '70', 192, '', '', '', '', '', 1, 0, 1477056810, 0, 1),
(1027, 'Pet Far Eastern (M) Sdn Bhd', 'No 69, Jalan Persiaran Cyber ,\r\nKawasan Perindustrian Senai 3,\r\n81400 Senai Johor\r\n\r\n', 'my_60_7-598 2175', 'wctan@petfar.com', '', '', 2, 2, '37', 129, 'Senai', '', '', '', '', 12, 14, 1477293606, 1477301154, 1),
(1028, 'Account', 'Address', 'sg_65_6666 6666', '', 'sg_65_6666 6666', '', 1, 1, '70', 192, '', '', '', '', '', 8, 0, 1477303514, 0, 1),
(1029, 'Nakano Construction Sdn. Bhd.', '12a & 14a, Jalan Bertam 14,\r\nTaman Daya, \r\n81100 Johor Bahru, Johor.', 'my_60_73546461', 'zakaria@nakano-const.com', '', '', 2, 2, '10', 129, 'Johor Bahru', '', '', '', '', 14, 14, 1477361315, 1477366700, 1),
(1030, 'Brady Corporation Asia Pte Ltd', '1 Kaki Bukit Crescent, Singapore 416236', 'sg_65_6477 7261', '', 'sg_65_6748 8857', '', 1, 1, '47', 192, '', '', '', 'www.bradyid.com.sg', '', 8, 0, 1477365059, 0, 1),
(1031, 'Institute of Technical Education', 'ITE College Central, 2 Ang Mo Kio Drive, Singapore 567720', 'sg_65_6331 7912', '', 'sg_65_6580 5300', '', 1, 1, '58', 192, '', '', '', 'www.ite.edu.sg', '', 8, 0, 1477365500, 0, 1),
(1032, 'I Space Furniture Dot Com', '9,Jalan Canggih 4, \r\nTaman Perindustrian Cemerlang, \r\n81800 Ulu Tiram , Johor Bahru. ', 'my_60_07-861 7622', '', 'my_60_07-861 8622', '', 2, 2, '24', 129, 'Johor Bahru', '', '', '', '', 14, 0, 1477366448, 0, 1),
(1033, 'RICE Consultancy', '', 'sg_65_98888888', '', '', '', 2, 2, '70', 192, '', '', '', '', '', 20, 0, 1477367716, 0, 1),
(1034, '  HILTON SINGAPORE', '581 Orchard Road, Singapore 238883   \r\n', 'sg_65_6730 3490', '', 'sg_65_6732 0139 ', '', 1, 1, '30', 192, '', '', '', 'singapore.hilton.com', '', 8, 0, 1477369738, 0, 1),
(1035, 'Sergent Services Pte Ltd', '1 Ubi View, #04-23 Focus One, Singapore 408555', 'sg_65_65706733', '', '', '\r\n', 1, 1, '44', 192, '', '', '', '', '', 8, 0, 1477377864, 0, 1),
(1036, 'Singapore Anglican Community Services', 'Blk 534 Pasir Ris Drive #01-266 Singapore 510534', 'sg_65_6584 4633', '', 'sg_65_6581 7614', '', 1, 1, '41', 192, '', '', '', 'www.sacs.org.sg ', '', 8, 0, 1477378577, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '1',
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(150) NOT NULL,
  `phoneNumber` varchar(15) NOT NULL,
  `cellNumber` varchar(15) NOT NULL,
  `address` text NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `regions` varchar(100) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `profileImage` varchar(100) NOT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedDate` datetime NOT NULL,
  `lastLoginDate` datetime NOT NULL,
  `verifiedNumber` varchar(100) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`userId`, `role_id`, `firstName`, `lastName`, `email`, `password`, `phoneNumber`, `cellNumber`, `address`, `latitude`, `longitude`, `city`, `regions`, `country_code`, `postal_code`, `profileImage`, `gender`, `status`, `createdDate`, `updatedDate`, `lastLoginDate`, `verifiedNumber`) VALUES
(1, 1, 'pramod', 'jain', 'pramod.jain@gyrix.in', 'n/e3d1Zhsqes3/jpVKk5J7C3UD+/YMgVlJgGoQ69CpDIrPj9Oqcgw5BYM0VGm+EizScIJghwcK04PneMqlBNCQ==', '9981462821', '073150405152', 'scheme no 114 vijay nagar indore', '22.7640507', '75.88623819999998', 'Indore', 'MP', 'IN', '452010', '1.jpg', 'Male', 'Active', '2015-08-13 08:59:13', '2015-08-14 03:46:46', '2016-01-13 01:29:00', ''),
(2, 2, 'pramod', 'jain', 'pramod.jn2@gmail.com', 'RNfaK+1eH2d1pYyByCQPoo0Tw+gBCnd7djvLjQxGAA9xXXLVFKa/EI77et7AQw6wOcYTs1pmuPKJKI5Qabv/Xg==', '9179148852', '9179148851', 'indore', '', '', '', '', '', '', '2.jpg', 'Male', 'Active', '2016-01-22 00:43:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `administrator_role`
--

CREATE TABLE IF NOT EXISTS `administrator_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `administrator_role`
--

INSERT INTO `administrator_role` (`role_id`, `role_name`, `status`, `createdDate`) VALUES
(1, 'Admin', 'Active', '2016-01-20 05:35:48'),
(2, 'Subscriber', 'Active', '2016-01-22 00:42:42');

-- --------------------------------------------------------

--
-- Table structure for table `admin_main_menu`
--

CREATE TABLE IF NOT EXISTS `admin_main_menu` (
  `page_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_key` varchar(50) NOT NULL,
  `menu_title` varchar(100) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `menu_level` int(1) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`page_menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `admin_main_menu`
--

INSERT INTO `admin_main_menu` (`page_menu_id`, `parent_id`, `menu_key`, `menu_title`, `icon_class`, `menu_level`, `menu_order`, `createdDate`, `status`) VALUES
(1, 0, 'setting', 'Setting', 'clip-settings', 1, 3, '2016-01-20 06:40:14', 'Active'),
(2, 0, 'navigation', 'Backend Navigation', 'clip-menu-4', 1, 4, '2016-01-20 06:42:03', 'Active'),
(3, 0, 'emailtemplate', 'Email template', 'fa fa-envelope-o', 1, 6, '2016-01-20 07:33:56', 'Active'),
(4, 0, 'messages', 'Message', 'fa fa-envelope', 1, 7, '2016-01-21 02:00:35', 'Active'),
(5, 0, 'homeslider', 'Home slider', 'clip-images-2', 1, 5, '2016-01-21 07:09:31', 'Active'),
(7, 0, 'faq', 'Manage FAQ', 'clip-question-2', 1, 8, '2016-01-23 02:08:42', 'Active'),
(8, 0, 'newsletter', 'Newsletter', 'clip-users', 1, 9, '2016-01-23 04:10:53', 'Active'),
(9, 0, 'manage_menus', 'Manage Menus', 'clip-list', 1, 1, '2016-06-09 06:19:47', 'Active'),
(10, 0, 'manage_content', 'Manage Content', 'clip-stack', 1, 2, '2016-06-09 06:21:45', 'Active'),
(11, 0, 'frontend', 'Frontend Navigation', 'clip-menu-4', 1, 0, '2016-10-20 04:22:03', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `admin_sub_menu`
--

CREATE TABLE IF NOT EXISTS `admin_sub_menu` (
  `page_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_key` varchar(50) NOT NULL,
  `menu_title` varchar(100) NOT NULL,
  `icon_class` varchar(50) NOT NULL,
  `menu_level` int(1) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`page_menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `admin_sub_menu`
--

INSERT INTO `admin_sub_menu` (`page_menu_id`, `parent_id`, `menu_key`, `menu_title`, `icon_class`, `menu_level`, `menu_order`, `createdDate`, `status`) VALUES
(1, 2, 'navigation/mainMenu', 'Main Menu', '', 2, 1, '2016-01-20 06:42:42', 'Active'),
(2, 2, 'navigation/subMenu', 'Sub Menu', '', 2, 2, '2016-01-20 06:43:11', 'Active'),
(3, 2, 'navigation/setPermission', 'Set Permission', '', 2, 5, '2016-01-20 06:56:34', 'Active'),
(4, 1, 'setting/websetting', 'Setting', '', 2, 1, '2016-01-20 06:58:14', 'Active'),
(5, 1, 'setting/adminrole', 'Admin Roles', '', 2, 4, '2016-01-20 07:14:54', 'Active'),
(6, 3, 'emailtemplate/template', 'Template', '', 2, 0, '2016-01-20 07:35:07', 'Active'),
(7, 4, 'messages/index', 'Inbox', '', 2, 2, '2016-01-21 02:01:30', 'Active'),
(8, 4, 'messages/sentbox', 'Sent Message ', '', 2, 3, '2016-01-21 02:02:17', 'Active'),
(9, 4, 'messages/composeMail', 'Compose Message', '', 2, 1, '2016-01-21 02:03:00', 'Active'),
(10, 4, 'messages/trash', 'Trashed Message ', '', 2, 4, '2016-01-21 02:03:49', 'Active'),
(11, 1, 'setting/sitelogo', 'Site Logo', '', 2, 2, '2016-01-21 05:57:23', 'Active'),
(12, 5, 'homeslider/index', 'Home slider', '', 2, 0, '2016-01-21 07:10:34', 'Active'),
(13, 1, 'setting/adminusers', 'Admin Users', '', 2, 5, '2016-01-22 00:25:19', 'Active'),
(19, 7, 'faq/faqSection', 'FAQ Section ', '', 2, 1, '2016-01-23 02:09:38', 'Active'),
(20, 7, 'faq/faqCategory', 'FAQ Category ', '', 2, 2, '2016-01-23 02:10:47', 'Active'),
(21, 7, 'faq/faqList', 'FAQ List ', '', 2, 3, '2016-01-23 02:11:54', 'Active'),
(22, 8, 'newsletter/index', 'Newsletter Subscribe ', '', 2, 0, '2016-01-23 04:12:07', 'Active'),
(23, 2, 'navigation/menuorder', 'menu order', '', 2, 3, '2016-01-25 06:59:17', 'Active'),
(24, 1, 'setting/siteMaintenance', 'Site Maintenance', '', 2, 3, '2016-02-11 13:00:45', 'Active'),
(25, 9, 'manage_menus/menus', 'Menu', '', 2, 1, '2016-06-09 06:20:40', 'Active'),
(26, 9, 'manage_menus/menu_items', 'Menu Items', '', 2, 2, '2016-06-09 06:21:02', 'Active'),
(27, 10, 'manage_content/section', 'Section', '', 2, 2, '2016-06-09 06:22:18', 'Active'),
(28, 10, 'manage_content/category', 'Category', '', 2, 1, '2016-06-09 06:22:43', 'Active'),
(29, 10, 'manage_content/content', 'Content', '', 2, 3, '2016-06-09 06:23:12', 'Active'),
(31, 9, 'manage_menus/pageorder/', 'Menu Items Order', '', 2, 3, '2016-06-15 05:25:56', 'Active'),
(32, 2, 'navigation/sub_menu_order', 'Sub Menu Order', '', 2, 4, '2016-06-23 08:07:53', 'Active'),
(33, 2, 'navigation/actionpermission', 'Action Permission', '', 2, 6, '2016-09-15 07:24:02', 'Active'),
(34, 11, 'frontend/classes', 'Main Class', '', 2, 1, '2016-10-20 04:30:09', 'Active'),
(35, 11, 'frontend/setPermission', 'Set Permission', '', 2, 2, '2016-10-20 04:44:06', 'Active'),
(36, 11, 'frontend/roles', 'Role', '', 2, 0, '2016-10-21 13:31:24', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `country_id` int(10) NOT NULL COMMENT 'Primary key of Country table',
  `quotation_header` text NOT NULL,
  `quotation_footer` text NOT NULL,
  `created_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `updated_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `created_date` int(10) NOT NULL,
  `updated_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: inactive,1:active',
  PRIMARY KEY (`id`),
  KEY `fk_country_id` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `country_id`, `quotation_header`, `quotation_footer`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(1, 'first Branch', 6, '', '<p>tred</p>\r\n', 2, 1, 1469714806, 1475139219, 1),
(2, 'test branch2', 5, '147668493347363.jpg', '<p>footer</p>\r\n', 2, 1, 1469859165, 1476684933, 1),
(3, 'Assigned Branch 1', 12, 'Quatation Header1', 'Quatation Footer1', 2, 1, 1470034771, 1474548303, 0),
(4, 'Assigned Branch2 ', 13, 'Quatation Header2', 'Quatation Footer2', 2, 1, 1470034771, 1474538585, 0),
(5, 'Indian Airways', 14, '147670209135140.jpg', '<p>Indian Airways</p>\r\n', 1, 2, 1471691995, 1476702091, 1),
(6, 'New Branchwith imageheader', 7, '147514392579319.jpg', '<p>trst</p>\r\n', 1, 1, 1475142782, 1475143925, 1),
(7, 'test2222', 7, '147669742854472.jpg', '<p>test</p>\r\n', 1, 1, 1475143301, 1476697428, 1),
(8, 'test17branch', 15, '147669746323209.jpg', '<p>test</p>\r\n', 1, 0, 1476697463, 0, 1),
(9, 'testbranch1717', 12, '', '<p>test</p>\r\n', 1, 1, 1476697707, 1476697727, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_category`
--

CREATE TABLE IF NOT EXISTS `cms_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_category`
--

INSERT INTO `cms_category` (`category_id`, `section_id`, `category_name`, `category_slug`, `category_desc`, `category_image`, `category_status`) VALUES
(1, 1, 'content', 'content', '<p>\r\n	content</p>\r\n', '', 'Active'),
(2, 2, 'Faq Category 1', 'faq-category-1', '<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus.</p>\r\n', '', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cms_content`
--

CREATE TABLE IF NOT EXISTS `cms_content` (
  `content_id` int(255) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `content_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `content_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unpublish_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cms_content`
--

INSERT INTO `cms_content` (`content_id`, `section_id`, `category_id`, `content_title`, `title_alias`, `content`, `content_image`, `meta_keyword`, `meta_desc`, `publish_date`, `unpublish_date`, `created`, `updated`, `content_status`) VALUES
(1, 1, 1, 'Home', 'home', '<div class="col-sm-6">\r\n	&nbsp;</div>\r\n<div class="col-sm-6">\r\n	<div class="section-content">\r\n		<h2>\r\n			Why Choose Us</h2>\r\n		<hr class="fade-right" />\r\n		<p>\r\n			Lid est laborum dolo rumes fugats untras. Etha rums ser quidem rerum facilis dolores nemis onis fugats vitaes nemo minima rerums unsers sadips amets.</p>\r\n		<p>\r\n			Ut enim ad minim veniam, quis nostrud Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci amets uns.</p>\r\n		<p>\r\n			Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips ameet quasi architecto beatae vitae dicta sunt explicabo</p>\r\n		<ul>\r\n			<li>\r\n				Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</li>\r\n			<li>\r\n				Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</li>\r\n			<li>\r\n				Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</li>\r\n			<li>\r\n				Ut enim ad minim veniam, quis nostrud Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci amets uns.</li>\r\n			<li>\r\n				Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips ameet quasi architecto beatae vitae dicta sunt explicabo.</li>\r\n		</ul>\r\n		<hr class="fade-right" />\r\n		<a class="btn btn-default" href="#">Learn more...</a></div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', '', '', '', '', '', '2016-05-21 04:25:23', '', 'Active'),
(2, 1, 1, 'About Us', 'about-us', '<p>\r\n	Welcome To Clip-One Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore dolore magnm aliquam quaerat voluptatem.</p>\r\n', '', '', '', '', '', '2016-05-24 01:31:56', '', 'Inactive'),
(3, 1, 1, 'Sitemap', 'sitemap', '<p>\r\n	SitemapSitemapSitemapSitemapSitemapSitemap</p>\r\n', '', '', '', '', '', '2016-06-10 06:47:53', '', 'Active'),
(5, 1, 1, 'Contact', 'contact', '<h4>\r\n	Get in touch</h4>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus.</p>\r\n<hr />\r\n<h4>\r\n	The Office</h4>\r\n<ul class="list-unstyled">\r\n	<li>\r\n		<strong>Address:</strong> 1234 Street Name, City Name, United States</li>\r\n	<li>\r\n		<strong>Phone:</strong> (123) 456-7890</li>\r\n	<li>\r\n		<strong>Email:</strong> <a href="mailto:mail@example.com"> mail@example.com </a></li>\r\n</ul>\r\n<hr class="right" />\r\n<h4>\r\n	Business Hours</h4>\r\n<ul class="list-unstyled">\r\n	<li>\r\n		Monday - Friday 9am to 5pm</li>\r\n	<li>\r\n		Saturday - 9am to 2pm</li>\r\n	<li>\r\n		Sunday - Closed</li>\r\n</ul>\r\n', '', '', '', '', '', '2016-07-04 12:59:12', '', 'Active');

--
-- Triggers `cms_content`
--
DROP TRIGGER IF EXISTS `menu_status_update`;
DELIMITER //
CREATE TRIGGER `menu_status_update` AFTER UPDATE ON `cms_content`
 FOR EACH ROW BEGIN
        UPDATE cms_menu_item SET mi_status = (NEW.content_status) WHERE content_id = NEW.content_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE IF NOT EXISTS `cms_menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu_status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`menu_id`, `menu_title`, `menu_desc`, `menu_status`) VALUES
(1, 'Main menu', 'main menu', 'Active'),
(2, 'Footer Menu', 'Appears in Footer Section', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cms_menu_item`
--

CREATE TABLE IF NOT EXISTS `cms_menu_item` (
  `mi_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `mi_parant_menu` int(11) NOT NULL DEFAULT '0',
  `mi_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mi_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mi_item_type` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `mi_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible_to` int(11) NOT NULL,
  `mi_target` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `mi_template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mi_order` int(11) NOT NULL,
  `mi_status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`mi_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `cms_menu_item`
--

INSERT INTO `cms_menu_item` (`mi_id`, `menu_id`, `mi_parant_menu`, `mi_title`, `mi_alias`, `mi_item_type`, `content_id`, `section_id`, `mi_link`, `visible_to`, `mi_target`, `mi_template`, `mi_order`, `mi_status`) VALUES
(1, 1, 0, 'home', 'home', 2, 0, 0, 'content', 3, '_parent', 'content', 1, 'Active'),
(3, 1, 0, 'Login', 'login', 2, 0, 0, 'login', 1, '_parent', 'content', 3, 'Active'),
(8, 1, 0, 'About', 'About', 1, 2, 0, 'about-us', 3, '_parent', 'content', 2, 'Inactive'),
(6, 1, 15, 'logout', 'logout', 2, 0, 0, 'login/logout', 2, '_parent', 'content', 5, 'Active'),
(11, 2, 0, 'Sitemap', 'Sitemap', 1, 3, 0, 'sitemap', 3, '_parent', 'content', 3, 'Active'),
(10, 2, 0, 'FAQ''s', 'faq', 4, 0, 2, 'Faq', 3, '_parent', 'faq', 1, 'Active'),
(16, 2, 0, 'Contact', 'Contact', 2, 0, 0, 'contact', 3, '_parent', 'contact', 0, 'Active'),
(15, 1, 0, 'My account', 'My-account', 2, 0, 0, 'dashboard', 2, '_parent', 'content', 5, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cms_permission`
--

CREATE TABLE IF NOT EXISTS `cms_permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `page_menu_id` int(11) NOT NULL,
  `priority` int(1) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Dumping data for table `cms_permission`
--

INSERT INTO `cms_permission` (`permission_id`, `role_id`, `page_menu_id`, `priority`) VALUES
(111, 1, 31, 5),
(110, 1, 10, 29),
(109, 1, 6, 28),
(141, 1, 33, 3),
(107, 1, 24, 27),
(106, 1, 11, 26),
(105, 1, 4, 25),
(104, 1, 3, 24),
(103, 1, 8, 23),
(102, 1, 27, 22),
(101, 1, 22, 21),
(100, 1, 2, 20),
(99, 1, 23, 19),
(98, 1, 26, 18),
(97, 1, 1, 17),
(96, 1, 25, 16),
(95, 1, 7, 15),
(94, 1, 12, 14),
(93, 1, 19, 13),
(92, 1, 21, 12),
(91, 1, 20, 11),
(90, 1, 29, 10),
(89, 1, 9, 9),
(88, 1, 28, 8),
(87, 1, 13, 7),
(86, 1, 5, 6),
(140, 2, 6, 0),
(139, 2, 9, 1),
(138, 1, 32, 4),
(142, 1, 34, 2),
(143, 1, 35, 1),
(144, 1, 36, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_permission_menu`
--

CREATE TABLE IF NOT EXISTS `cms_permission_menu` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `page_menu_id` int(11) NOT NULL,
  `permission` int(1) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `cms_permission_menu`
--

INSERT INTO `cms_permission_menu` (`permission_id`, `role_id`, `page_menu_id`, `permission`) VALUES
(105, 1, 1, 1),
(104, 1, 1, 2),
(103, 1, 1, 3),
(102, 1, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_section`
--

CREATE TABLE IF NOT EXISTS `cms_section` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `section_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `section_status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cms_section`
--

INSERT INTO `cms_section` (`section_id`, `section_name`, `section_slug`, `section_desc`, `section_status`) VALUES
(1, 'content', 'content', '<p>\r\n	content dfdfdfdfdfdfdf</p>\r\n', 'Active'),
(2, 'Faq', 'faq', '<p>\r\n	Lorem ipsum</p>\r\n', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `cms_slider`
--

CREATE TABLE IF NOT EXISTS `cms_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_name` varchar(255) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `image_title` varchar(255) NOT NULL,
  `shortcode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `mobile_number` varchar(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(5) NOT NULL,
  `currency_id` varchar(25) NOT NULL COMMENT 'Primary key of Currency table',
  `default_gst` decimal(8,2) NOT NULL,
  `created_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `updated_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `created_date` int(10) NOT NULL,
  `updated_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: inactive, 1: active',
  PRIMARY KEY (`id`),
  KEY `fk_country_courrency_id` (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `code`, `currency_id`, `default_gst`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(2, 'Bangladesh-BD', 'BD', '1,2', '44.00', 1, 0, 1468494288, 0, 1),
(3, 'Bulgaria', 'BG', '1', '34.00', 1, 1, 1468494422, 1468494655, 1),
(4, 'Burkina Faso', 'BF', '1', '45.00', 1, 0, 1468553881, 0, 1),
(5, 'Bosnia and Herzegovina', 'BA', '1', '20.33', 1, 1, 1468570740, 1468571632, 1),
(6, 'Afghanistan', 'AF', '1', '4.00', 2, 0, 1469613820, 0, 1),
(7, 'Algeria', 'DZ', '1', '6.00', 2, 0, 1469613871, 0, 1),
(8, 'American Samoa', 'AS', '1', '9.00', 2, 0, 1469613914, 0, 1),
(9, 'Company Name', 'Compa', '2', '0.00', 2, 0, 1469863269, 0, 1),
(10, 'Company Name1', 'Compa', '3', '0.00', 2, 0, 1469863406, 0, 1),
(11, 'Company Name12', 'Compa', '4', '0.00', 2, 0, 1469863406, 0, 1),
(12, 'Assigned Country1 ', 'Count', '2', '0.00', 2, 0, 1470034771, 0, 1),
(13, 'Assigned Country 2', 'Count', '3', '0.00', 2, 0, 1470034771, 0, 1),
(14, 'India', 'IN', '4', '10.15', 1, 0, 1471691995, 0, 1),
(15, 'Assigned Country 1', 'Count', '5', '0.00', 1, 0, 1474364947, 0, 1),
(16, 'Belize', 'BZ', '3,6,1,4', '10.00', 1, 0, 1476714070, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(100) NOT NULL,
  `unique_name` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`template_id`, `template_name`, `unique_name`, `subject`, `message`, `create_date`, `update_date`, `status`) VALUES
(1, 'simple_message', 'simple_message', 'simple message', '<h2>\r\n	Hi {firstname},</h2>\r\n', '2015-12-15 19:45:43', '0000-00-00 00:00:00', 'Active'),
(2, 'status_deactive', 'status_deactive', 'Account status deactive', '<h2>\n	Hi {firstname},</h2>\n<p>\n	Your account deactivated.Please contact otriga administrator</p>\n', '2015-12-16 01:02:02', '0000-00-00 00:00:00', 'Active'),
(3, 'send_message', 'send_message', 'Message', '<h2>\r\n	Hi {firstname},</h2>\r\n<p>\r\n	{sender_firstname} has sent you a message</p>\r\n<table border="0" width="50%">\r\n	<tbody>\r\n		<tr>\r\n			<td height="94" width="30%">\r\n				<div style="width: 80px; height:80px; display: inline-block; float: left;">\r\n					<a href="{sender_url}" title="{sender_fullname}"><img class="circle-img" src="{sender_profileimage}" style="border-radius: 100% 100% 100% 100%; width: 80px; height:80px;" /></a></div>\r\n			</td>\r\n			<td valign="top" width="70%">\r\n				<h4 style="margin: 10px 0px;">\r\n					<a href="{sender_url}" style="text-decoration:none;color:#000000;" title="{sender_fullname}">{sender_fullname}</a></h4>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	<b>Subject: </b>{subject}</p>\r\n<p>\r\n	<b>Message: </b>{message}</p>\r\n<p>\r\n	<a href="{reply_url}"><button class="red_button" type="button">Reply</button></a> <a href="{replytoall_url}"><button class="red_button" type="button">Reply all</button></a></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Can&#39;t see the button? Use this link: <a href="{message_url}">Click here</a></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '2015-12-16 22:39:31', '0000-00-00 00:00:00', 'Active'),
(4, 'NewLetter', 'NewLetter', 'Subscribe For News Letter', '<h1>\r\n	Hi {USERNAME}</h1>\r\n<p>\r\n	Thank you for newsletter subscription with <a href="{SITEURL}">{SITENAME}</a></p>\r\n', '2016-03-09 09:17:17', '0000-00-00 00:00:00', 'Active'),
(5, 'signup', 'signup', 'Registration', '<h1>\n	Hi {USERNAME}</h1>\n<p>\n	Thank you for registering with <a href="{SITEURL}">{SITENAME}</a></p>\n<p>\n	User Details: Email : {EMAIL}</p>\n<p>\n	Password : {PASSWORD}</p>\n<p>\n	<a href="{SITEURL}login/verification/?verifier={CODE}"><button class="red_button" type="button">Email verification</button></a></p>\n<p>\n	&nbsp;</p>\n<p>\n	Copy paste this url</p>\n<p>\n	&nbsp;</p>\n<p>\n	{SITEURL}login/verification/?verifier={CODE}</p>\n<p>\n	Can&#39;t see the button? Use this link: <a href="{SITEURL}login/verification/?verifier={CODE}">Click here</a></p>\n', '2016-03-10 05:58:15', '0000-00-00 00:00:00', 'Active'),
(6, 'email_verification', 'email_verification', 'Email is verified', '<h1>\n	Hi {USERNAME}</h1>\n<p>\n	Thank you for verify email address with <a href="{SITEURL}">{SITENAME}</a></p>\n<p>\n	User Details:</p>\n<p>\n	Email : {EMAIL}</p>\n<p>\n	<a href="{SITEURL}" target="_blank"><button class="red_button" type="button">Login</button></a></p>\n<p>\n	&nbsp;</p>\n<p>\n	Copy paste this url</p>\n<p>\n	{SITEURL}</p>\n<p>\n	Can&#39;t see the button? Use this link: <a href="{SITEURL}" target="_blank">Click here</a></p>\n', '2016-03-12 09:18:26', '0000-00-00 00:00:00', 'Active'),
(7, 'forgot_password', 'forgot_password', 'Forgot Password', '<h1>\n	Hi {USERNAME}</h1>\n\n<p>\n	 Email : {EMAIL}</p>\n<p>\n	Password : {PASSWORD}</p>\n<p>\n	<a href="{siteurl}login"><button class="red_button" type="button">Login</button></a></p>\n<p>\n	&nbsp;</p>\n<p>\n	Copy paste this url</p>\n<p>\n	&nbsp;</p>\n<p>\n	{siteurl}login</p>\n<p>\n	Can&#39;t see the button? Use this link: <a href="{siteurl}login">Click here</a></p>\n\n\n\n\n', '2016-03-16 09:32:45', '0000-00-00 00:00:00', 'Active'),
(8, 'contact_us', 'contact_us', 'Contact Us', '<h1> Hi {USERNAME}</h1>\n<p> Name : {NAME}</p>\n<p> Email : {EMAIL}</p>\n<p> Subject : {SUBJECT}</p>\n<p> Message : {MESSAGE}</p>\n\n', '2016-03-16 12:10:46', '0000-00-00 00:00:00', 'Active'),
(9, 'contact_us_thankyou', 'contact_us_thankyou', 'Contact Us', '<h1>\r\n	Hi {USERNAME}</h1>\r\n<p>\r\n	Thank you for Contact Us.{SITENAME} will contact you soon</p>\r\n', '2016-03-17 05:54:36', '0000-00-00 00:00:00', 'Active'),
(14, 'email_verification', 'email_verification', 'Email verification', '<h1>Hi {USERNAME}</h1>\r\n<p>Please click this link to verify your email</p>\r\n<p>	<a href="{SITEURL}login/verification/?verifier={CODE}"><button class="red_button" type="button">Email verification</button></a></p>\r\n<p>&nbsp;</p>\r\n<p>Copy paste this url</p>\r\n<p>&nbsp;</p>\r\n<p>{SITEURL}login/verification/?verifier={CODE}</p>\r\n<p>Can&#39;t see the button? Use this link: <a href="{SITEURL}login/verification/?verifier={CODE}">Click here</a></p>\r\n', '2016-06-20 08:40:45', '0000-00-00 00:00:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `front_actions`
--

CREATE TABLE IF NOT EXISTS `front_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_name` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `front_actions`
--

INSERT INTO `front_actions` (`id`, `action_name`, `created_date`, `status`) VALUES
(1, 'create', '2016-10-19 00:23:34', 'Active'),
(2, 'read', '2016-10-19 00:23:34', 'Active'),
(3, 'update', '2016-10-19 00:23:34', 'Active'),
(4, 'delete', '2016-10-19 00:23:34', 'Active'),
(5, 'import', '2016-10-19 00:23:34', 'Active'),
(6, 'export', '2016-10-19 00:23:34', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `front_class`
--

CREATE TABLE IF NOT EXISTS `front_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `front_class`
--

INSERT INTO `front_class` (`id`, `class_name`, `created_date`, `status`) VALUES
(1, 'Home', '2016-10-19 00:25:23', 'Active'),
(2, 'Contact', '2016-10-19 18:30:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `front_permissions`
--

CREATE TABLE IF NOT EXISTS `front_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `permission` tinyint(1) NOT NULL DEFAULT '0',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `front_permissions`
--

INSERT INTO `front_permissions` (`id`, `role_id`, `class_id`, `action_id`, `permission`, `createdDate`) VALUES
(39, 3, 1, 1, 1, '2016-10-21 13:13:08'),
(40, 3, 1, 2, 1, '2016-10-21 13:13:08'),
(41, 3, 2, 1, 1, '2016-10-21 13:13:08'),
(42, 3, 2, 2, 1, '2016-10-21 13:13:08'),
(43, 3, 2, 4, 1, '2016-10-21 13:13:08'),
(63, 1, 1, 1, 1, '2016-10-21 13:25:41'),
(64, 1, 1, 2, 1, '2016-10-21 13:25:41'),
(65, 1, 1, 3, 1, '2016-10-21 13:25:41'),
(70, 2, 1, 2, 1, '2016-10-24 06:14:05'),
(71, 2, 1, 3, 1, '2016-10-24 06:14:05'),
(72, 2, 2, 3, 1, '2016-10-24 06:14:05'),
(73, 2, 2, 4, 1, '2016-10-24 06:14:05');

-- --------------------------------------------------------

--
-- Table structure for table `front_quotation`
--

CREATE TABLE IF NOT EXISTS `front_quotation` (
  `id` int(11) NOT NULL,
  `account_name` varchar(55) NOT NULL,
  `contact_name` varchar(55) NOT NULL,
  `product_name` varchar(55) NOT NULL,
  `quotation_no` varchar(100) NOT NULL,
  `total_amount` decimal(8,2) NOT NULL,
  `created_date` int(11) NOT NULL,
  `created_by` varchar(55) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `front_quotation`
--

INSERT INTO `front_quotation` (`id`, `account_name`, `contact_name`, `product_name`, `quotation_no`, `total_amount`, `created_date`, `created_by`, `status`) VALUES
(1, 'gyrix', 'pramod', 'p1', 'GT123456', '1000.00', 1477440920, 'Vivek', 1),
(2, 'gyrix', 'Ram', 'p3', 'GT123457', '1200.55', 1477527320, 'Mohan', 1),
(3, 'gyrix', 'sonali', 'p3', 'GT123458', '451221.84', 1477613720, 'rohit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `front_roles`
--

CREATE TABLE IF NOT EXISTS `front_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `front_roles`
--

INSERT INTO `front_roles` (`id`, `role_name`, `createdDate`, `status`) VALUES
(1, 'Administrator', '2016-10-19 00:27:08', 'Active'),
(2, 'Hr', '2016-10-19 00:27:08', 'Active'),
(3, 'User Accounts', '2016-10-19 00:27:08', 'Active'),
(4, 'User Technical', '2016-11-13 18:30:00', 'Active'),
(5, 'Account Head', '2016-11-13 18:30:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `updated_by` int(10) NOT NULL COMMENT 'Primary key of User table',
  `created_date` int(10) NOT NULL,
  `updated_date` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: inactive, 1: active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=71 ;

--
-- Dumping data for table `industry`
--

INSERT INTO `industry` (`id`, `name`, `created_by`, `updated_by`, `created_date`, `updated_date`, `status`) VALUES
(1, 'Accommodation & Food Service Activities', 1, 0, 1468585214, 0, 1),
(2, 'Accounting Firms', 1, 0, 1468585214, 0, 1),
(3, 'Administrative & Support Service Activities', 1, 0, 1468585214, 0, 1),
(4, 'Advertising', 1, 0, 1468585214, 0, 1),
(5, 'Apparel & Textile Manufacturing', 1, 0, 1468585214, 0, 1),
(6, 'Arts, Entertainment, and Recreation', 1, 0, 1468585214, 0, 1),
(7, 'Automobile', 1, 0, 1468585214, 0, 1),
(8, 'Aviation, Aerospace, Airlines & Airports', 1, 0, 1468585214, 0, 1),
(9, 'Broadcasting & Media', 1, 0, 1468585214, 0, 1),
(10, 'Construction', 1, 0, 1468585214, 0, 1),
(11, 'Consultancies', 1, 0, 1468585214, 0, 1),
(12, 'Contractors', 1, 0, 1468585214, 0, 1),
(13, 'Country Clubs', 1, 0, 1468585214, 0, 1),
(14, 'Courier Services', 1, 0, 1468585214, 0, 1),
(15, 'Designers / Architects', 1, 0, 1468585214, 0, 1),
(16, 'Eateries / Restaurants', 1, 0, 1468585214, 0, 1),
(17, 'Electrical & Electronics', 1, 0, 1468585214, 0, 1),
(18, 'Energy', 1, 0, 1468585214, 0, 1),
(19, 'Facilities and Estate Management', 1, 0, 1468585214, 0, 1),
(20, 'Finance and Banking', 1, 0, 1468585214, 0, 1),
(21, 'Food and Beverage Stores & Manufacturing', 1, 0, 1468585214, 0, 1),
(22, 'Forestry and Logging', 1, 0, 1468585214, 0, 1),
(23, 'Funds, Trusts, and Other Financial Vehicles', 1, 0, 1468585214, 0, 1),
(24, 'Furniture & Furnishings', 1, 0, 1468585214, 0, 1),
(25, 'Grocery & Supermarkets', 1, 0, 1468585214, 0, 1),
(26, 'Gymn', 1, 0, 1468585214, 0, 1),
(27, 'Healthcare & Hospitals', 1, 0, 1468585214, 0, 1),
(28, 'Heavy and Civil Engineering Construction', 1, 0, 1468585214, 0, 1),
(29, 'Homes', 1, 0, 1468585214, 0, 1),
(30, 'Hospitality (Hotels, resorts and Inns)', 1, 0, 1468585214, 0, 1),
(31, 'Information & Communications', 1, 0, 1468585214, 0, 1),
(32, 'Legal Services', 1, 0, 1468585214, 0, 1),
(33, 'Leisure and entertainment', 1, 0, 1468585214, 0, 1),
(34, 'Logistics', 1, 0, 1468585214, 0, 1),
(35, 'Luxury Goods', 1, 0, 1468585214, 0, 1),
(36, 'Machinery Manufacturing', 1, 0, 1468585214, 0, 1),
(37, 'Manufacturing', 1, 0, 1468585214, 0, 1),
(38, 'Mining (except Oil and Gas)', 1, 0, 1468585214, 0, 1),
(39, 'Motor Vehicle and Parts', 1, 0, 1468585214, 0, 1),
(40, 'Museums, Historical Sites, and Similar Institution', 1, 0, 1468585214, 0, 1),
(41, 'Non-profit Organisation', 1, 0, 1468585214, 0, 1),
(42, 'Offshore & Marine', 1, 0, 1468585214, 0, 1),
(43, 'Oil and Gas & Chemicals', 1, 0, 1468585214, 0, 1),
(44, 'Others', 1, 0, 1468585214, 0, 1),
(45, 'Paper Manufacturing', 1, 0, 1468585214, 0, 1),
(46, 'Pharmaceutical', 1, 0, 1468585214, 0, 1),
(47, 'Printing and Related Support Activities', 1, 0, 1468585214, 0, 1),
(48, 'Private Education', 1, 0, 1468585214, 0, 1),
(49, 'Professional, Scientific & Technical Activities', 1, 0, 1468585214, 0, 1),
(50, 'Public Administration & Defence', 1, 0, 1468585214, 0, 1),
(51, 'Public Education', 1, 0, 1468585214, 0, 1),
(52, 'Public Sector : Governement departments and agenci', 1, 0, 1468585214, 0, 1),
(53, 'Publishing Industries (except Internet)', 1, 0, 1468585214, 0, 1),
(54, 'Real Estate', 1, 0, 1468585214, 0, 1),
(55, 'Religious Institutions', 1, 0, 1468585214, 0, 1),
(56, 'Residences', 1, 0, 1468585214, 0, 1),
(57, 'Retail Trade', 1, 0, 1468585214, 0, 1),
(58, 'Schools', 1, 0, 1468585214, 0, 1),
(59, 'Semi Conductor', 1, 0, 1468585214, 0, 1),
(60, 'Shipping', 1, 0, 1468585214, 0, 1),
(61, 'Telecommunications', 1, 0, 1468585214, 0, 1),
(62, 'Tertiary', 1, 0, 1468585214, 0, 1),
(63, 'Tourism', 1, 0, 1468585214, 0, 1),
(64, 'Transport and Logistics', 1, 0, 1468585214, 0, 1),
(65, 'Universities', 1, 0, 1468585214, 0, 1),
(66, 'Utilities', 1, 0, 1468585214, 0, 1),
(67, 'Warehousing and Storage', 1, 0, 1468585214, 0, 1),
(68, 'Waste Management and Remediation Services', 1, 0, 1468585214, 0, 1),
(69, 'Industries1', 2, 0, 1470034771, 0, 1),
(70, 'Industries2', 2, 0, 1470034771, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `libraries_demo`
--

CREATE TABLE IF NOT EXISTS `libraries_demo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aticle_title` varchar(250) NOT NULL,
  `auth_name` varchar(40) NOT NULL,
  `article_link` varchar(250) NOT NULL,
  `class_name` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `libraries_demo`
--

INSERT INTO `libraries_demo` (`id`, `aticle_title`, `auth_name`, `article_link`, `class_name`, `status`, `create_date`) VALUES
(1, 'Jquery Form Validation ', 'Pramod jain', 'js-validation.zip', 'demo/jquery_validation', 'Active', '2016-10-24 11:47:41'),
(2, 'Codeigniter Datatables', 'Pramod Jain', 'datatable.zip', 'Datatable', 'Active', '2016-10-26 18:30:00'),
(3, 'Export CSV', 'Vivek nair', 'Export_CSV.zip', 'export', 'Active', '2016-11-02 18:30:00'),
(4, 'Import CSV', 'Vivek nair', 'Import_CSV.zip', 'import', 'Active', '2016-11-02 18:30:00'),
(5, 'Report', 'Vivek nair', 'Report.zip', 'Reports', 'Active', '2016-11-02 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `manage_faq`
--

CREATE TABLE IF NOT EXISTS `manage_faq` (
  `faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` tinyint(4) NOT NULL,
  `question` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `answer` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `image` varchar(100) NOT NULL,
  `video` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `manage_faq`
--

INSERT INTO `manage_faq` (`faq_id`, `category_id`, `question`, `answer`, `image`, `video`, `created_date`, `status`) VALUES
(1, 2, 'test', '<p>\r\n	tesy</p>\r\n', '78384-chrysanthemum.jpg', '3eb84-big_buck_bunny_720p_1mb.mp4', '2016-01-23 03:18:39', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_faq_category`
--

CREATE TABLE IF NOT EXISTS `manage_faq_category` (
  `faq_category_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `faq_parent_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `description` varchar(250) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`faq_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `manage_faq_category`
--

INSERT INTO `manage_faq_category` (`faq_category_id`, `faq_parent_id`, `category_name`, `description`, `created_date`, `status`) VALUES
(1, 0, 'section1', 'test section', '2016-01-23 03:13:02', 'Active'),
(2, 1, 'catagory 1', 'catagory 1 test', '2016-01-23 03:13:31', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_home_slider`
--

CREATE TABLE IF NOT EXISTS `manage_home_slider` (
  `home_slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `home_slider_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `home_slider_image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `caption_line_1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `caption_line_2` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `caption_line_3` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `button` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Yes',
  `button_text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `button_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Inactive',
  PRIMARY KEY (`home_slider_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `manage_home_slider`
--

INSERT INTO `manage_home_slider` (`home_slider_id`, `home_slider_title`, `home_slider_image`, `caption_line_1`, `caption_line_2`, `caption_line_3`, `button`, `button_text`, `button_url`, `createdDate`, `status`) VALUES
(1, 'slider1', '9e964-13b62-3211.jpg', '', '', '', 'No', '', '', '2016-06-14 07:10:35', 'Active'),
(2, 'slide2', 'ec27b-29b08-slide1.jpg', '', '', '', 'No', '', '', '2016-06-14 07:14:41', 'Active'),
(3, 'slider3', '63b80-a2cd4-321.jpg', '', '', '', 'No', '', '', '2016-06-14 07:21:47', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `manage_menu_action`
--

CREATE TABLE IF NOT EXISTS `manage_menu_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `manage_menu_action`
--

INSERT INTO `manage_menu_action` (`id`, `name`) VALUES
(1, 'add'),
(2, 'edit'),
(3, 'read'),
(4, 'delete'),
(5, 'bulk delete'),
(6, 'bulk publish'),
(7, 'bulk unpublish');

-- --------------------------------------------------------

--
-- Table structure for table `manage_newsletter`
--

CREATE TABLE IF NOT EXISTS `manage_newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `newsletterDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`newsletter_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `manage_newsletter`
--

INSERT INTO `manage_newsletter` (`newsletter_id`, `email`, `newsletterDate`, `status`) VALUES
(4, 'pramod.jain@gyrix.in', '2016-09-11 18:30:00', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `message_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(60) NOT NULL,
  `subject` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `attachment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `attachedFilename` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `thread` int(11) NOT NULL DEFAULT '0',
  `sendDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL,
  `trash` int(1) NOT NULL DEFAULT '0',
  `message_type` enum('message') NOT NULL DEFAULT 'message',
  `message_type_id` int(11) NOT NULL,
  `sender_type` enum('user','admin') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_receiver_relation`
--

CREATE TABLE IF NOT EXISTS `message_receiver_relation` (
  `receiver_relation_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receiver_email` varchar(50) NOT NULL,
  `sender_email` varchar(100) NOT NULL,
  `message_id` bigint(20) NOT NULL,
  `send_status` enum('Sent','Failed') NOT NULL DEFAULT 'Sent',
  `read_status` tinyint(1) NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`receiver_relation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `site_logo`
--

CREATE TABLE IF NOT EXISTS `site_logo` (
  `site_logo_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_logo_name` varchar(50) NOT NULL,
  `site_logo_image` varchar(100) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`site_logo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `site_logo`
--

INSERT INTO `site_logo` (`site_logo_id`, `site_logo_name`, `site_logo_image`, `status`, `createdDate`) VALUES
(1, 'main_logo', '38bd7-download.png', 'Active', '2016-01-21 06:01:44'),
(2, 'email_logo', '03134-download.png', 'Active', '2016-01-21 06:02:15');

-- --------------------------------------------------------

--
-- Table structure for table `site_maintenance`
--

CREATE TABLE IF NOT EXISTS `site_maintenance` (
  `site_maintenance_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_maintenance_content` text NOT NULL,
  `maintenance_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`site_maintenance_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_maintenance`
--

INSERT INTO `site_maintenance` (`site_maintenance_id`, `site_maintenance_content`, `maintenance_time`, `status`) VALUES
(1, '<h2>\r\n	Sign up and we&#39;ll notify you of our launch.<br />\r\n	We&#39;ll also throw in a freebie for your effort :)</h2>\r\n', '2016-10-24 18:30:00', 'Inactive');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `role_id` tinyint(4) NOT NULL DEFAULT '1',
  `firstName` varchar(60) NOT NULL,
  `lastName` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(250) NOT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `profile_image` varchar(50) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `country_code` varchar(50) NOT NULL,
  `country` smallint(6) NOT NULL,
  `regions` smallint(6) NOT NULL,
  `city` mediumint(9) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `address` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `about_us` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `facebook_id` varchar(50) NOT NULL,
  `gmail_id` varchar(50) NOT NULL,
  `linkden_id` varchar(50) NOT NULL,
  `twitter_id` varchar(50) NOT NULL,
  `linkden_url` varchar(200) NOT NULL,
  `gmail_url` varchar(200) NOT NULL,
  `facebook_url` varchar(200) NOT NULL,
  `status` enum('Active','Inactive','Deactivate') NOT NULL DEFAULT 'Active',
  `account_status` enum('Active','Deactivate','Delete') NOT NULL DEFAULT 'Active',
  `registerDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastLoginDate` int(11) NOT NULL,
  `updatedDate` int(11) NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `activation_code` varchar(40) NOT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `verified_number` varchar(100) NOT NULL,
  `server_info` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `created_by`, `role_id`, `firstName`, `lastName`, `email`, `password`, `gender`, `profile_image`, `phone_number`, `country_code`, `country`, `regions`, `city`, `postal_code`, `latitude`, `longitude`, `address`, `about_us`, `facebook_id`, `gmail_id`, `linkden_id`, `twitter_id`, `linkden_url`, `gmail_url`, `facebook_url`, `status`, `account_status`, `registerDate`, `lastLoginDate`, `updatedDate`, `ip_address`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `last_login`, `verified_number`, `server_info`) VALUES
(1, 0, 1, 'test', 'user', 'pramod@gyrix.in', '79N0MF+m+wkcNEyr+VQ1nnmdsV+5CG537SfueRT6UrUkpMsLxZ3EXlLRH7HAVQaU4COcRjdSqjuKo+hUlVKJ9Q==', 'Female', '1.jpg', '9898989898', 'IN', 113, 0, 0, '452007', '22.7193763', '75.85696989999997', '210,mgroad,indore', 'hey I am a new user,its a wondorfull site of property', '', '', '', '', 'http://Linkdin.com', 'http://Google.com', 'http://Facebook.com', 'Active', 'Active', '2015-09-14 01:28:40', 1479112885, 1467184829, '192.168.1.12', '', NULL, NULL, NULL, NULL, '', 'Mozilla/5.0 (Windows NT 6.1; rv:47.0) Gecko/20100101 Firefox/47.0');

-- --------------------------------------------------------

--
-- Table structure for table `website_setting`
--

CREATE TABLE IF NOT EXISTS `website_setting` (
  `setting_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(100) NOT NULL,
  `setting_value` text NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `website_setting`
--

INSERT INTO `website_setting` (`setting_id`, `setting_name`, `setting_value`, `updatedDate`, `status`) VALUES
(1, 'site_name', 'Basic Admin', '2015-08-13 17:14:32', 'Active'),
(2, 'site_email', 'info@gyrix.in\r\n', '2015-08-13 17:16:23', 'Active'),
(3, 'contact_number1', '0731 403 5005', '2015-08-13 17:18:02', 'Active'),
(4, 'contact_number2', '2020202020\r\n', '2015-08-13 17:19:06', 'Active'),
(5, 'site_address', '20-21, Besides Orlando Academy, Press Complex, Behind Dainik Bhaskar Office, Near LIG Square, AB Road, Indore, Madhya Pradesh 452011', '2015-08-13 17:19:37', 'Active'),
(6, 'paypaltestmode', 'true', '2015-09-05 00:53:24', 'Active'),
(7, 'order_payee_email', 'true', '2015-09-05 00:53:24', 'Active'),
(8, 'meta_keywords', 'basic admin website', '2015-10-14 14:03:44', 'Active'),
(9, 'meta_description', 'basic admin website', '2015-10-15 05:41:35', 'Active'),
(10, 'meta_title', 'basic admin website', '2015-10-15 05:42:04', 'Active'),
(14, 'site_copyright', ' <p> © Copyright 2016 by Gyrix. All Rights Reserved. </p>', '2015-10-17 00:30:44', 'Active'),
(15, 'support_email', 'support@admin.com', '2015-11-03 00:58:16', 'Active'),
(19, 'under_construction_msg', '<h1>Your IP address has been temporarily blocked</h1>\r\n<p>IP blocks happen when you try to log into an account to many times with different passwords or try to make many accounts from the same IP address in a short time.</p>\r\n<h1>Contact our support team to get this resolved</h1>\r\n<p>For more information on</p><br/>\r\n \r\n• Why this block was enacted <br/>\r\n• Flow to further protect yourself  <br/>\r\n• How to regain access  <br/>\r\n<p><img width="300" alt="Under review" src="http://forum.fiverr.com/wp-content/themes/fiverr-forum/img/under-review.png"></p>\r\n\r\n<p>Please contact <a onclick="contactModelShow();"  href="javascript:void(0);">our support team</a></p> \r\n', '2015-11-27 01:00:05', 'Active'),
(20, 'twitter_url', 'https://twitter.com/', '2015-12-04 04:41:17', 'Active'),
(21, 'flicker_url', 'https://www.flickr.com', '2015-12-05 05:09:42', 'Inactive'),
(22, 'facebook_url', 'http://facebook.com/', '2015-12-05 05:10:26', 'Active'),
(23, 'google_url', 'http://google.com/', '2015-12-05 05:10:50', 'Active'),
(24, 'linkedin_url', 'https://www.linkedin.com/', '2015-12-05 05:11:17', 'Active'),
(25, 'pinterest_url', 'https://www.pinterest.com/', '2015-12-05 05:11:39', 'Active'),
(27, 'site_url', 'http://google.com', '2015-12-07 00:59:26', 'Active'),
(28, 'call_line_open', '<p> Lines open Monday to Saturday<br /> \r\nFrom 9am to 6pm (GMT)</p> \r\n<p>24/7 support available</p> ', '2015-12-07 01:22:19', 'Active'),
(29, 'site_maintainence', 'false', '2016-01-23 04:22:19', 'Active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

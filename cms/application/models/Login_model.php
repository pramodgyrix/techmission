<?php

class Login_model extends CI_Model {
	/**
		* GYRIX TECHNOLABS
		* USE FOR LOGIN FUNCTIONS
	**/

    var $details;
    /**
		* VALIDATION FOR USERS ACCORDING TO THE ROLE
	**/

    function validate_user($email, $password){		
		$this->db->select('a.*,at.role_name');
        $this->db->from('administrator a');
		$this->db->join('administrator_role at', 'at.role_id = a.role_id', 'LEFT');
        $this->db->where('a.email', $email);
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result();
		$result->free_result();
        if($records > 0) {			
            // check password with stored password		
			if($this->decrypt_password($recordsData[0]->password) == trim($password)){				
				$this->details = $recordsData[0];				
				$this->set_session($recordsData[0]);
				return true;
			}         
        }
        return false;
    }

 	/**
		* SET SESSION VARIBLES 		
	**/
    function set_session($userData = '') {		
		$this->details = (!isset($this->details) && empty($this->details))? $userData : $this->details;			
        $this->details = (!isset($this->details) && empty($this->details))? $userData : $this->details;			
        $this->session->set_userdata(array('userId'		=> $this->details->userId,
        	                                'user_id'		=> $this->details->userId,
											'firstName'			=> $this->details->firstName,
											'lastName'			=> $this->details->lastName,
											'email'			=> $this->details->email, 
											'status'		=> $this->details->status,
											'role_name'		=> $this->details->role_name,  
											'role_id'		=> $this->details->role_id,
											'fullName'      => $this->details->firstName.' '.$this->details->lastName,
											'profileImage' => $this->details->profileImage));
		$this->update_user_login_status($this->details->userId);
	}	
	
	/**
		* UPDATE STATUS OF LOGINNED USER	
	**/
	function update_user_login_status($userId=''){
		$timestamp = date('Y-m-d h:i:s');
		$dataArray = array('lastLoginDate' => $timestamp);
		if($user_id !=''){	
			$this->db->update('user', $dataArray, array('userId' => $userId));
		}
	}	
	
	function encrypt_password($password){
		$this->load->library('encrypt');	 
		$key = $this->config->item('encryption_key'); //'super-secret-key';
		return $this->encrypt->encode($password, $key);
	}
	 
    function decrypt_password($value){
		$this->load->library('encrypt');	 
		$key = $this->config->item('encryption_key'); //'super-secret-key';
		return $this->encrypt->decode($value, $key);		
	}	
}

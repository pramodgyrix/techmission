<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
	* GYRIX TECHNOLABS
	* USE FOR MENU FUNCTIONS
**/
class Menu_model extends CI_Model {
	/**
		* GET MENU ITEMS	
	**/
	function menu_items($menu_id){		
		$this->db->select('cmi.menu_id,cmi.mi_title,cmi.mi_id',false);
        $this->db->from('cms_menu_item cmi');
		$this->db->join('cms_menus cm', 'cm.menu_id = cmi.menu_id', 'LEFT');
        $this->db->where('cmi.menu_id', $menu_id);
		$this->db->where('cmi.mi_parant_menu', 0);
		$this->db->order_by("cmi.mi_order");
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result_array();
		$result->free_result();
        if($records > 0) {				
			return $recordsData;    
        }
        return false;
    }
	
	/**
		* GET MENUS	
	**/
	function getMenus(){		
		$this->db->select('cm.menu_title,cm.menu_id',false);
        $this->db->from('cms_menus cm');	
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result_array();
		$result->free_result();
        if($records > 0) {				
			return $recordsData;    
        }
        return false;
    }
	

	/**
		* GET ADMIN MENUS	
	**/
	function getadminMenus(){
		$this->db->select('mm.menu_title,mm.page_menu_id',false);
        $this->db->from('admin_main_menu mm');	
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result_array();
		$result->free_result();
        if($records > 0) {				
			return $recordsData;    
        }
        return false;
	}
	
	/**
		* GET ADMIN SUBMEUS	
	**/
	function getadminSubmenus($page_menu_id){
		$this->db->select('sm.parent_id,sm.menu_title,sm.page_menu_id',false);
        $this->db->from('admin_sub_menu sm');
		$this->db->join('admin_main_menu mm', 'mm.page_menu_id = sm.parent_id', 'LEFT');
        $this->db->where('sm.parent_id', $page_menu_id);
		$this->db->order_by("sm.menu_order");
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result_array();
		$result->free_result();
        if($records > 0) {				
			return $recordsData;    
        }
        return false;
	}

}

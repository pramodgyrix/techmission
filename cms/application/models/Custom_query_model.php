<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Custom_query_model extends grocery_CRUD_model {
 	/**
		* GYRIX TECHNOLABS
		* USE FOR CUSTOM QUERY FUNCTIONS
	**/
	private  $query_str = ''; 
	function __construct() {
		parent::__construct();
	}
	/**
		* GET LIST OF RECORDS OF CUSTOME QUERY
	**/
	function get_list() {
		$query = $this->db->query($this->query_str); 
		$results_array = $query->result();
		return $results_array;		
	}
 
 	/**
		* ASSIGN CUSTOM QUERY IN A OBJECT
	**/
	public function set_query_str($query_str) {
		$this->query_str = $query_str;
	}
 
 	/**
		* ADDING THIS DOES THE TRICK FOR LIMITING THE RESULTS PER PAGE
	**/
   
    function limit($value, $offset = ''){
        $this->query_str .= ' LIMIT '.($offset ? $offset.', ' : '').$value;
    }
 
    /**
    	* This method is to count the results of your query. If you don't add this
     	* the total results on 'Displaying 1 to n of x items' can be wrong, 'cause 
     	* grocery will get the total amount of rows from the table 
    **/
    function get_total_results() {
        return count($this->get_list());
    }
	
	
	
}
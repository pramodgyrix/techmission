<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Message_model extends CI_Model {
	/**
		* GYRIX TECHNOLABS
		* USE FOR MENU FUNCTIONS
	**/
	var $resultDATA = array();

	/**
		* GET MESSAGE DETAIL	
	**/
	
	function messageDetail($message_id, $dataType = 'single'){
		
		$this->db->select("m.*, (SELECT GROUP_CONCAT(mrr.receiver_email separator ',') as receiver FROM message_receiver_relation mrr where mrr.message_id = m.message_id) as allReceiver", false);
		$this->db->from('message m');
		$this->db->where('m.message_id', $message_id);
		$this->db->order_by('m.sendDate','DESC');
		
		$result = $this->db->get();	
		foreach($result->result_array() as $data){			
			$this->resultDATA[] = $data;
			if($data['thread'] > 0 && $dataType !='single'){				
				$this->messageDetail($data['thread'], 'thread');
			}
		}
		$result->free_result();
		return $this->resultDATA;
	}
	
	/**
		* GET RECENT MESSAGE DETAIL 	
	**/
	function recent_mail($receiver_email = ''){
		$this->db->select("mrr.*, m.subject, m.message, CONCAT(u.firstName,' ',lastName) as name, u.profile_image", false);
		$this->db->from('message_receiver_relation mrr');
		$this->db->join('message m', 'm.message_id = mrr.message_id', 'LEFT');		
		if('m.sender_type' == 'admin'){
		$this->db->join('administrator u', 'u.email = mrr.sender_email', 'LEFT');	
		}else{
		$this->db->join('user u', 'u.email = mrr.sender_email', 'LEFT');
		}
		$this->db->where('mrr.receiver_email', $receiver_email);
		$this->db->where('mrr.read_status', 0);
		$result = $this->db->get();
        	$sql = $this->db->last_query();
		$resultRecords = $result->num_rows();
		if($resultRecords > 0){
			$data = array();
			foreach($result->result_array() as $resultData){
				$image = array('profile_image' => display_image($resultData['profile_image'], USER_IMAGE_THUMB),
								'send_date' => get_time_ago($resultData['send_date']),
								'message' => substr(strip_tags($resultData['message']),0,100).'..');	
				$data[] = array_replace($resultData, $image);
			}
		}
		$data = array('total' => $resultRecords, 'data' => $data);
		return $data;
	}
}

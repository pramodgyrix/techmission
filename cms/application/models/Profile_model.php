<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
/**
	* GYRIX TECHNOLABS
	* USE FOR COMMON FUNCTIONS
**/

class Profile_model extends CI_Model {
	
	var $responseData = array();
	/**
		* GYRIX TECHNOLABS
		* USE FOR COMMON FUNCTIONS
	**/
	
	function get_profile_data(){
		$uid = $this->session->userdata('userId');
		$this->db->select("a.*, CONCAT(a.firstName,' ',a.lastName) as name, at.role_name", false);
		$this->db->from('administrator a');
		$this->db->join('administrator_role at', 'at.role_id = a.role_id', 'LEFT');	
		$this->db->where('a.userId',$uid);
		$this->responseData = $this->db->get()->result_array();
		return $this->responseData;
    }

	/**
		* CHECK PASSWORD IS VALID OR NOT
	**/
	function validate_password($password){
		$this->db->where(array('userId'=>$this->userID));
		$result = $this->db->get('administrator');
		$resultData=  $result->result_array();
		if($result->num_rows() == 1){
			if(decrypt_password($resultData[0]['password'])== $password)
				return true;
			else
				return false;
		}
	}
	
	/**
		* UPLOAD IMAGE
	**/
	function upload_profile_image($files, $dstPath, $fileBodyName, $size=200,$userId){		
		if(!empty($files['name'])){			
		$ext = @pathinfo($files['name'], PATHINFO_EXTENSION);
		$file_name   = $userId.'.'.$ext;			
		$file_path = $dstPath.'/'.$file_name; 		
		@move_uploaded_file($files['tmp_name'], $file_path);		
		$img = resize_images($file_path,271,221, $dstPath.'/thumb/');		
		$img = resize_images($file_path,150,150, $dstPath.'/150150/');		
		//resize_images		
		}		
		return $file_name;				
	}
	
	/**
		* UPDATE IMAGE
	**/
	function update_profile_image($userId,$table='user'){		
		$this->db->select('profileImage');
		$this->db->from($table);		
		$this->db->where(array('userId'=>$userId));
		$query = $this->db->get();
		foreach ($query->result_array() as $resultData){			
			$userMainImage = USER_IMAGE_ROOTPATH.DIRECTORY_SEPARATOR.$resultData['profileImage'];
			$userThumbImage = USER_IMAGE_ROOTPATH.DIRECTORY_SEPARATOR.'thumb'.DIRECTORY_SEPARATOR.$resultData['profileImage'];
			$userThumb150150 = USER_IMAGE_ROOTPATH.DIRECTORY_SEPARATOR.'150150'.DIRECTORY_SEPARATOR.$resultData['profileImage'];			
			if($resultData['profileImage'] !='' && is_file($userMainImage) && file_exists($userMainImage)){
				@unlink($userMainImage);
			}
			if($resultData['profileImage'] !='' && is_file($userThumbImage) && file_exists($userThumbImage)){
				@unlink($userThumbImage);
			}
			if($resultData['profileImage'] !='' && is_file($userThumb150150) && file_exists($userThumb150150)){
				@unlink($userThumb150150);
			}								  
			
		} // CLose foreach loop
		
		return true;
			
	}
	
	/**
		* GET USER DASHBOARD DATA 
	**/
	function get_mydashboard_profile_data($userID = ''){	
		$this->db->select("u.*, CONCAT(u.firstName, ' ',  u.lastName) as name, adu.*, ug.groupName,ug.description, CONCAT(crc.city , ', ' , r.region , ', ' , c.country) as location,ad.agency_name as associate_agency_name,ad.agency_address as associate_agency_address,ad.agency_email as associate_agency_email,ad.agency_phone_number as associate_agency_phone_number,ad.agency_cell_number as associate_agency_cell_number,ad.agency_website as associate_agency_website,
		 (SELECT COUNT(c.target_id) from comment c where c.target_id = u.userId  GROUP BY c.target_id) as user_review,
		  (SELECT COUNT(p.userId) from property p where p.userId = u.userId && p.status = 'Active' GROUP BY p.userId) as total_property,

		  (SELECT COUNT(p.userId) from property p where p.userId = u.userId && p.property_category = 1 && p.status = 'Active' GROUP BY p.userId) as property_sale,

		  (SELECT COUNT(p.userId) from property p where p.userId = u.userId && p.property_category = 2 && p.status = 'Active' GROUP BY p.userId) as property_rent,

		  (SELECT COUNT(p.userId) from property p where p.userId = u.userId && p.property_category = 3 GROUP BY p.property_id) as userId", false);	

		$this->db->from('user u');
		$this->db->join('agency_detail adu','adu.userId = u.group_id', 'LEFT');
		$this->db->join('user_group ug', 'ug.group_id = u.group_id', 'LEFT');
		$this->db->join('user_associated_agency  uassa', 'uassa.userId = u.userId', 'LEFT');
		$this->db->join('agency_detail ad','ad.agency_id = uassa.agency_id', 'LEFT');
		$this->db->join('country c', 'c.countryid = u.country', 'LEFT');
		$this->db->join('country_regions r', 'r.regionid = u.region', 'LEFT');
		$this->db->join('country_region_cities crc','crc.cityId = u.city', 'LEFT');
		$this->db->where('u.userId',$userID);
		$this->responseData = $this->db->get()->result_array();
		return $this->responseData;
    }	
}
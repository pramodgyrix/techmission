<?php
/* date time ago */
function ago($timestamp = ''){
 if(empty($timestamp)){
  return false;
 }
 
  $difference = time() - $timestamp;
  $periods = array('second', 'minute', 'hour', 'day', 'week', 'month', 'years', 'decade');
  $lengths = array('60', '60', '24', '7', '4.35', '12', '10');
  for($j = 0; $difference >= $lengths[$j]; $j++) $difference /= $lengths[$j];
  $difference = round($difference);
  if($difference != 1) $periods[$j] .= "s";
  return "$difference $periods[$j] ago";
}
function get_time_ago($datetime){
	
	$time = strtotime($datetime);
	$time = time() - $time; // to get the time since that moment	
	$tokens = array (
		31536000 => 'year',
		2592000 => 'month',
		604800 => 'week',
		86400 => 'day',
		3600 => 'hour',
		60 => 'minute',
		1 => 'second'
	);	
	foreach ($tokens as $unit => $text) {
		if ($time < $unit) continue;
		$numberOfUnits = floor($time / $unit);
		return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'').' ago';
	}
}
function safe_b64encode($string) {
 
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
}
 
function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
}


function encrypt_data($data){
	  $CI = & get_instance();
	  $CI->load->library('encrypt');	 
	  $key = $CI->config->item('encryption_key');
	  return  $CI->encrypt->encode($data, $key);
}
		 
function decrypt_data($value){
		$CI = & get_instance();
		$CI->load->library('encrypt');	 
		$key = $CI->config->item('encryption_key');
		return  $CI->encrypt->decode($value, $key);
}

	function seo_friendly_urls($firstName,$lastName,$user_id='')
{ 
    $text = '';
    if(!empty($firstName)){
	  $text .= $firstName;
	}
	 if(!empty($firstName)){
	  $text .= ' '.$lastName;
	}
	 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
  // trim
  $text = trim($text, '-');
  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  // lowercase
  $text = strtolower($text);
  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);
  if (empty($text))
  {
    return FALSE;
  }
  
  if(!empty($user_id)){
  
  $encode_id = safe_b64encode($user_id);
  $encode = $encode_id.'/'.$text;
  
  }else{
   $encode = $text;
  }
  return $encode;
}
/*Resize Image*/
function resize_images($path,$rs_width,$rs_height,$destinationFolder='') {

 $folder_path = dirname($path);
 $thumb_folder = $destinationFolder;
 $percent = 0.5;
 
 if (!is_dir($thumb_folder)) {

    mkdir($thumb_folder, 0777, true);
 }
 
 $name = basename($path);

 $x = getimagesize($path);            

 $width  = $x['0'];

 $height = $x['1'];

 switch ($x['mime']){

              case "image/gif":

                 $img = imagecreatefromgif($path);

                 break;

              case "image/jpeg":

                 $img = imagecreatefromjpeg($path);

                 break;
			 case "image/jpg":

                 $img = imagecreatefromjpeg($path);

                 break;

              case "image/png":

                 $img = imagecreatefrompng($path);

                 break;

  }

    $img_base = imagecreatetruecolor($rs_width, $rs_height);

    $white = imagecolorallocate($img_base, 255, 255, 255);

    imagefill($img_base, 0, 0, $white);
    
	imagecopyresized($img_base, $img, 0, 0, 0, 0, $rs_width, $rs_height, $width, $height);

    imagecopyresampled($img_base, $img, 0, 0, 0, 0, $rs_width, $rs_height, $width, $height);

    $path_info = pathinfo($path);   

    $dest = $thumb_folder.$name;

           switch ($path_info['extension']) {

              case "gif":

                 imagegif($img_base, $dest);  

                 break;

              case "jpg":

                 return imagejpeg($img_base, $dest);  

                 break;
			  case "jpeg":

                 return imagejpeg($img_base, $dest);  

                 break;

              case "png":

                return imagepng($img_base, $dest);  

                 break;

           }
}

function getMenuItemMaxOrder(){
	 $CI = & get_instance();
     $sql ="SELECT MAX(`mi_order`) as mi_order FROM `cms_menu_item`";
	 $query = $CI->db->query($sql);
	  if($query->num_rows()>0){
		 $result = $query->result_array();
         return $result[0]['mi_order'];
	   }else{
		  return 0; 
	  }
}
?>	
<?php
function dd($array){
  echo '<pre/>';
  print_r($array);
  die;	
}
	function search_array($array, $class_id, $action_id) {
	    $return = array(); 
		if(!empty($array)){  
		 foreach ($array as $k=>$subarray){  
		  	if ($subarray['class_id'] == $class_id && $subarray['action_id'] == $action_id) {
			   return true;
			}
		 }
		}
		return false;
		 
	 }
/* user details get */
function get_user_details($id = '',$table='user'){
        $CI = & get_instance();
        if(empty($id)){
		   $id = $CI->session->userdata('userId');
		}
		$CI->db->select('*');
		$CI->db->from($table);
		$CI->db->where('userId',$id);
		
		$result = $CI->db->get();
	    $resultData=  $result->result_array();
		
	    if($result->num_rows() == 1)
		{
		   return $resultData;
		}
		else{ return false;	}
        
}
/**Select Data */
function select($table, $where='',$column='*'){
	  $CI = & get_instance();			
	  $sql = "select $column from $table";
	  if(!empty($where))
	    $sql .= " $where";
	  $query = $CI->db->query($sql);
	  if($query->num_rows()>0){
		 $result = $query->result_array();
		 return $result;
	   }else{
		  return false; 
	  }
}
/* User-Profile Image */
function get_user_profile_image($image,$url){
	$filename="$url/$image";
	if(!empty($image)){
			if(@getimagesize($filename)){
			  return '/'.$image ;
			}else{
			  return '/default.png';
			}
	}else{
	   return '/default.png';
	}

}
// get admin permissions from for this group
function get_admin_permissions($roleId ){
	   $roleId = $roleId ? $roleId : 1;	
       $CI = & get_instance();  
	  	
       $CI->db->select('mm.menu_key, mm.menu_title, mm.icon_class,sm.menu_key as child_menu_key, sm.menu_title as child_menu_title');
	   $CI->db->from('admin_sub_menu sm');
	   $CI->db->join('cms_permission rp', 'sm.page_menu_id = rp.page_menu_id');
	   $CI->db->join('admin_main_menu mm', 'mm.page_menu_id = sm.parent_id');
	   // get groups
		$CI->db->where('mm.status', 'Active');
		$CI->db->where('sm.status', 'Active');
        $CI->db->where('rp.role_id', $roleId);
		$CI->db->group_by('sm.page_menu_id');
		$CI->db->order_by("mm.menu_order", "ASC");
		$CI->db->order_by("sm.menu_order", "ASC");
		$query = $CI->db->get();		
        // set permissions array and return
        if ($query->num_rows())
        {
			$permissions = array();
            foreach ($query->result_array() as $row)
            {
				$menu_key = $row['menu_key'];
               $permissions[$menu_key][] = array('category_key'=>$row['menu_key'], 
									  'category_name'=>$row['menu_title'],
									  'icon_class'=>$row['icon_class'],
									  'child_menu_key'=>$row['child_menu_key'],
									  'child_menu_title'=>$row['child_menu_title']);
            }
            return $permissions;
        }
        else
        {
            return array();
        }
		$CI->db->cache_off();
}
function check_user_login(){
	$CI = & get_instance();
	$userId = $CI->session->userdata('userId');
	if($userId ==''){
		redirect(base_url('login'), 'refresh');
	}else{
		return $userId;
	}		
}
// this function return latest picture
function get_current_profile_pic($userId='', $path='',$table='user'){
		$CI = & get_instance();
		
		$CI->db->select('profileImage');
		$CI->db->from($table); 
		$CI->db->where('userId', $userId); 
		$rslt = $CI->db->get()->result_array();	 
		
		return display_image($rslt[0]['profileImage'], $path);	
}
function display_image($imageName='', $imagePath=''){
			
		if($imageName !='' && $imagePath !=''){
			$filePath = trim($imagePath).SEPARATOR.trim($imageName);
			if(@getimagesize($filePath)){
				return $filePath;
			}else{
				return trim($imagePath).SEPARATOR.'default.png';
			}
		}
		return trim($imagePath).SEPARATOR.'default.png';
}
function get_admin_permission_check(){
	$CI = & get_instance();
	$role_id = $CI->session->userdata('role_id');
	$controller = $CI->router->class;
    $method = $CI->router->method;
	$page_menu_id = $CI->db->get_where('admin_sub_menu',array('menu_key' => $controller.'/'.$method))->result_array();
	
	if(!empty($page_menu_id)){
	    $permission_id = $CI->db->get_where('cms_permission',array('role_id' => $role_id,'page_menu_id' => $page_menu_id[0]['page_menu_id']))->result_array();
		
		if(!empty($permission_id)){
			
			$role_id = $permission_id[0]['role_id'];
			$page_menu_id = $permission_id[0]['page_menu_id'];
			$result =  get_admin_permission_menu_check($role_id, $page_menu_id);
		    return $result;
		}else{
		   redirect(base_url('permission'));
		}		
	}  	
} 

function get_admin_permission_menu_check($role_id,$page_menu_id){
   $CI = & get_instance();
   $permission_id = $CI->db->get_where('cms_permission_menu',array('role_id' => $role_id,'page_menu_id' => $page_menu_id))->result_array();
   if(!empty($permission_id)){
       return $permission_id; 
    }else{
       return false;
   	}

}
/**
   *This function is for upload image
   *Usage:-
   *    $root_path=USER_IMAGE_ROOTPATH;
   *	   $files=$_FILES['profileImage'];
   *	   uploadImage($files,$root_path);
**/
function uploadImage($files,$root_path){
	if(!empty($files['name'])){	
		$ext = @pathinfo($files['name'], PATHINFO_EXTENSION);				
		$file_name   = time().rand(1000,99999).'.'.$ext;	
		$file_path = $root_path.'/'.$file_name; 	
		@move_uploaded_file($files['tmp_name'], $file_path);
	 }
	 return $file_name;	
}
?>
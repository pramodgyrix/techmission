<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {

   var $userID;

   var $insert_permission;
   var $edit_permission;
   var $delete_permission;
   var $view_permission;
   var $publish_permission;
   var $unpublish_permission;
   var $permission_val;
  var $mts;	
	public function __construct()
	{

		parent::__construct();
		$this->load->library(array('grocery_CRUD','ajax_grocery_crud'));
		$this->userID = check_user_login();
		$this->permission_val = get_admin_permission_check();
			
	}
	
	public function setval(){
     $crud = new grocery_CRUD();
     if(!empty($this->permission_val)){
     	    foreach($this->permission_val as $key){
		      if($key['permission'] == 1){
		           $crud->unset_bulk_delete();
		       }else if($key['permission'] == 2){
		          $crud->unset_print();	
		       }else if($key['permission'] == 3){
		          $crud->unset_export();	
		       }else if($key['permission'] == 4){
		       	  $crud->unset_bulk_delete();
		       }else if($key['permission'] == 5){
		          $crud->unset_delete();	
		       }else if($key['permission'] == 6){
		          $crud->unset_read();
		       }
		       return $crud;  
		    }
	 }
	 return false;
	}
	/*   
       Special character remove 
	*/
	public function customAlpha($str){
		
		if ( !preg_replace('/[^A-Za-z0-9\-]/', '', $str) )	{
			 $this->form_validation->set_message('customAlpha', "Special character are not allowed in %s");
            return FALSE;
		}
	}
	
	/*
        View file load
	*/
	public function _example_output($output = null)
	{
		$this->load->view('setting',$output);
	}

	/*
       Bulk action : delete, publish, unpublish
	*/
	public function bulk_action(){
		$action = $this->input->post("action", TRUE);
		$table_name = $this->input->post("table_name", TRUE);
		$field_name = $this->input->post("field_name", TRUE);
		$primary_key = $this->input->post("primary_key", TRUE);
		$selection = rtrim($this->input->post("selection", TRUE), '|');
		$id_array = ($selection) ? explode("|", $selection) : '';
		
		if(isset($id_array) && $id_array != '' && $table_name !='' && $primary_key !=''){
			switch($action){
				case 'delete':
					foreach($id_array as $item):
						if($item != ''):
							$this->db->delete($table_name, array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items deleted!';
				break;
				case 'publish':
					foreach($id_array as $item):
						if($item != '' && $field_name !=''):
							$this->db->update($table_name, array($field_name => 'Active'), array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items published!';
				break;
				case 'unpublish':
					foreach($id_array as $item):
						if($item != '' && $field_name !=''):
							$this->db->update($table_name, array($field_name => 'Inactive'), array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items unpublished!';
				break;
			}
		}else{
		   echo 'Kindly Select Atleast One Item!';
		}
	}
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS	
	* USE FOR USER PROFILE
**/

class Profile extends CI_Controller { 
	var $userID;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','common'));		
		$this->load->model(array('profile_model'));
		$this->userID = check_user_login();
	}
	public function index(){
		//$data['profileData'] = $this->profile_model->getProfileData();
		$data['stylesheet'] = array("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css",
									"assets/plugins/bootstrap-social-buttons/social-buttons-3.css");
									
		$data['page_title'] = 'My Profile';
		$data['result'] = $this->profile_model->get_profile_data();		
		$this->load->view('profile/profile', $data);	
	}
	/**
		* UPDATE USER PROFILE IMAGE
	**/
	public function update_profile_image(){

		

		if($_FILES['profile_image']['name']!=''){

			$fileBodyName = strtotime(date('Y-m-d h:i:s'));				

			$imageName = $this->user_model->upload_profile_image($_FILES['profile_image'], USER_IMAGE_DIRPATH, $fileBodyName, '200');

			if($imageName != ''){

				$this->user_model->update_profile_image($this->userID);

			}												

			$imageData = array('profile_image' => $imageName);

			$result = $this->db->update('user', $imageData, array('user_id' => $this->userID));

			if($result){

				$this->message[] = array('success' => 'Your profile picture has been changed!');

			}else{

				$this->message[] = array('error' => 'Your profile picture did not changed!');

			}

			

		}else{			

			$this->message[] = array('error' => 'Kindly select Valid Image!');			

		}		

		$msg = array('message' => $this->message);

		$this->session->set_userdata($msg);

		redirect('mydashboard/profile');		

	}

	/**
		* UPDATE USER PROFILE
	**/
	public function edit(){
		$checkFormValidation = $this->__setFormRules('editProfile');
		if($checkFormValidation){			
			
			$userData = $this->input->post();

			$imgpath=USER_IMAGE_ROOTPATH;
			if($_FILES['profileImage']['name']!=''){	
			
			
						
				$fileBodyName = strtotime(date('Y-m-d h:i:s'));
				$imageName = $this->profile_model->upload_profile_image($_FILES['profileImage'], $imgpath, $fileBodyName, '200',$this->userID);
				
				
				if($imageName != ''){
					$this->profile_model->update_profile_image($this->userID,'administrator');
				}
				$imageData = array('profileImage' => $imageName);
				$userData = array_replace($userData, $imageData);		
			}
			
			$result = $this->db->update('administrator', $userData, array('userId' => $this->userID));
			
			if($result){				 
				$this->messageci->set('Your account has been updated successfully!', 'success');
			}else{
				$this->messageci->set('There is coming problem to update your profile!', 'error' );
			}
		}
		
		$data['result'] = $this->profile_model->get_profile_data($this->userID);
		$data['page_title'] = 'Edit Profile';
		$data['stylesheet'] = array('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css',
									'assets/plugins/summernote/build/summernote.css');

		$this->load->view('profile/profileEdit', $data);	

	}

	/**
		* UPDATE USER PASSWORD
	**/
	public function changePassword()
	{
		$user_details = get_user_details($this->userID,'administrator');
		$password = decrypt_data($user_details[0]['password']);
		$passChangeFormValidation = $this->__setFormRules('changePassword');	
		
		if($passChangeFormValidation){  
		   
		   $postData=$this->input->post();
		
			$oldpass = $postData['oldpass'];
			if(trim($password) ==  trim($oldpass)){
			$newpass = $postData['newpass'];
			$newPass = array('password' => encrypt_data($newpass));			
			$where = array('userId'=> $this->userID);
			
			if($this->db->update('administrator', $newPass, $where)){
				$this->messageci->set('Your Password has been changed successfully!', 'success');
			}else{
				$this->messageci->set('Password changing faild, Please try again!', 'error');
			}
		  }else{
			  $this->messageci->set('Password does not match the confirm password!', 'error');
			  }
		}
		$data['page_title'] = 'Change Password';
		$this->load->view('profile/changePassword', $data);
	}	
	
	/**
		* APPLY VALIDATION ON PASSWORD
	**/
	function validate_password($password)
	{
		if($this->profile_model->validate_password($password)){
			return true;
		}else{
			$this->form_validation->set_message('validate_password', 'The %s is not valid, kindly enter valid one!');
			return false;
		}
	}
	
	/**
		* SET RULE FOR USER INPUT ON THE SUBMIT FORM
	**/
	function __setFormRules($formName='')
	{
		switch($formName){			
			case 'editProfile':
				$this->form_validation->set_rules('firstName', 'First name', 'trim|required');
				$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
		//$this->form_validation->set_rules('email', 'Email',  'trim|required|valid_email|is_unique[administrator.email]');
				$this->form_validation->set_rules('address', 'Address', 'trim|required');
				
				$this->form_validation->set_rules('phone_number', 'phone number', 'trim|numeric');
				$this->form_validation->set_rules('cell_number', 'cell number', 'trim|numeric');
				
			break;
			case'changePassword':
			$this->form_validation->set_rules('oldpass', 'Old Password', 'trim|required');
			$this->form_validation->set_rules('newpass', 'New Password', 'trim|required|min_length[6]|matches[newpassconf]');
			$this->form_validation->set_rules('newpassconf', 'New Password Confirm', 'trim|required');
			break;			
		}

			$this->form_validation->set_rules('userImage', 'User Image', 'trim');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
			return $this->form_validation->run();
	}
	
	/**
		* UPDATE IMAGE
	**/
	public function update_image()
	{
	  if(isset($_POST))
      {

    
      	$profileimg = $_FILES['profile_image']['name']; 
		$upload_directory = config_item('root_url').'media/usersImage/';
		$temp_name = $_FILES['profile_image']['tmp_name'];
		$check = getimagesize($temp_name);
		if($check !== false){	
			  if($_FILES['profile_image']['size']< 900000) {		
				$ext = @pathinfo($profileimg, PATHINFO_EXTENSION);
				$file_name   = time().rand(1000,99999).'.'.$ext;
				$file_path = $upload_directory.$file_name; 
				@move_uploaded_file($temp_name, $file_path);
				// Check file size  				
				$filePath= $upload_directory.$file_name;					
				$img = resize_images($filePath,271,221, $upload_directory.'thumb/');
				$img = resize_images($filePath,150,150, $upload_directory.'150150/'); 	
				$uid = $this->session->userdata('user_id');
				$where = array('userId'=>$uid);
				$userdata = array('profileImage' => $file_name);
				
				$this->db->update('administrator', $userdata, $where);
			 }				
		   }// thumb
		redirect('Profile/');
		}
	}
		
}
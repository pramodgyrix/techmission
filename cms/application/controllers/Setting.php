<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS	
	* USE FOR USER PROFILE
**/
class Setting extends MY_Controller {
  

    public function index() {
        $this->_example_output((object) array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }
	
	/**
     	* Website setting
	**/	
	public function websetting(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Setting');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_bulk_delete();
        $crud->unset_texteditor('setting_value');
		      
	    $crud->set_table('website_setting');		        	
        $crud->columns('setting_name','setting_value','status');
        $crud->required_fields('setting_name' , 'setting_value', 'status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->fields('setting_name','setting_value','status');
		$crud->set_rules('setting_name' , 'setting name' , 'trim|required|is_unique[website_setting.setting_name]');
		$crud->add_fields('setting_name', 'setting_value', 'status');
    	$crud->edit_fields('setting_value', 'status');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Website Setting');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

    /*
      Admin role grid view
    */
	public function adminrole(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Role');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_bulk_delete();
		$crud->set_table('administrator_role');		        	
        $crud->columns('role_name','status');
        $crud->required_fields('setting_name' , 'setting_value', 'status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->fields('role_name','status');
		$crud->display_as('role_name', 'Role Name');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Manage Role');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

    /*
      Admin Users
    */
	public function adminusers(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Admin Users');
		$crud->unset_print();
		$crud->unset_export();
		      
	    $crud->set_table('administrator');
		$crud->set_relation('role_id','administrator_role','role_name');
	    $crud->columns('role_id','firstName' , 'lastName', 'email','status');
        $crud->required_fields('role_id','firstName' , 'lastName', 'email','password','status');
		$crud->set_rules('email' , 'Email' , 'required|valid_email');
		$crud->set_rules('password', 'password', 'trim|required|min_length[6]');
		$crud->callback_before_insert(array($this,'encrypt_password_callback'));
		$crud->callback_before_update(array($this,'encrypt_password_callback'));
		$crud->callback_edit_field('password', array($this,'decrypt_password_callback'));
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->add_fields('role_id','firstName','lastName','email','password','status');
    	$crud->edit_fields('role_id','firstName','lastName','email','password','status');
		$crud->display_as('role_id', 'Role Name');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Manage Admin Users');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

    /*
     Password encrypt
    */
	function encrypt_password_callback($post_array, $primary_key = null){
		$this->load->library('encrypt');
		$key = $this->config->item('encryption_key');
		$post_array['password'] = $this->encrypt->encode($post_array['password'], $key);
		return $post_array;
	}
    
    /*
     Password decrypt
    */
	function decrypt_password_callback($value){
		$this->load->library('encrypt');
		$key = $this->config->item('encryption_key');
		$decrypted_password = $this->encrypt->decode($value, $key);
		return "<input type='password' name='password' value='$decrypted_password' />";
	}

   
   /*
      Website logo
   */
	public function sitelogo(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Logo');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_bulk_delete();
	    $crud->set_table('site_logo');	
		$crud->set_field_upload('site_logo_image','media/siteLogo');	        	
        $crud->columns('site_logo_name' , 'site_logo_image', 'status');
        $crud->required_fields('site_logo_name' , 'site_logo_image', 'status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->fields('site_logo_name' , 'site_logo_image', 'status');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Manage Logos');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

    /*  
       Website main tenance
     */
	public function siteMaintenance(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Site Maintenance');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_bulk_delete();
	    $crud->set_table('site_maintenance');	        	
        $crud->columns('site_maintenance_content' , 'maintenance_time', 'status');
        $crud->required_fields('site_maintenance_content' , 'maintenance_time', 'status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->fields('site_maintenance_content' , 'maintenance_time', 'status');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Manage Site Maintenance');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }			
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	
	/**
		* GYRIX TECHNOLABS
		* USE FOR USER LOGIN FUNCTIONALITY
	**/
	public function __construct(){
            parent::__construct();
			
            // Your own constructor code
						
			$this->load->helper(array('form', 'url'));
			$this->load->library(array('session', 'form_validation'));			
			// Create an instance of the members model
        	$this->load->model(array('login_model'));
			
    } 
	 
	/**
		* VIEW LIGIN PAGE
	**/
	public function index(){
		
		$this->load->view('login');
	}

	/**
		* CHECK AUTHENTICATION
	**/
	public function authenticate(){
		// Grab the email and password from the form POST
		$checkValidation = $this->__setFormRules();
		if($checkValidation){
			$email = $this->input->post('email');
			$pass  = $this->input->post('password'); 
			//Ensure values exist for email and pass, and validate the user's credentials                
				 
			if($this->login_model->validate_user($email, $pass)) {
				redirect(base_url('dashboard'));		
			} else {
				// Otherwise show the login screen with an error message.					
				$data['msg'] = array('warning' => 'Incorrect Email-ID and Password');
				$this->load->view('login',$data);
			}	
		}else{
			$this->load->view('login');
		}
	}
	
	/**
		* USER SESSION LOGOUT
	**/
	public function logout(){		
		$this->session->sess_destroy();      
		$url = base_url();
		echo "<script type='text/javascript'>window.location.href = '".$url."'</script>";
        exit();
	}
	
	/**
		* SET FORM RULES
	**/
	function __setFormRules($setRulesFor = ''){
		switch($setRulesFor){
			case'forgotPassword':
				$this->form_validation->set_rules('oldpass', 'Old Password', 'trim|required');
				$this->form_validation->set_rules('newpass', 'New Password', 'trim|required|min_length[5]|matches[newpassconf]');
				$this->form_validation->set_rules('newpassconf', 'New Password Confirm', 'trim|required');
			break;
			case'changePassword_verify':
				$this->form_validation->set_rules('newpass', 'New Password','trim|required|min_length[6]|matches[newpassconf]');
				$this->form_validation->set_rules('newpassconf', 'New Password Confirm', 'trim|required');
			break;
			default:
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');
			break;
		}
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
		return $this->form_validation->run();
	
	}	
	
	
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTION MY_Controller
	* USE FOR SHOWING HOW MANY USERS ARE SUBSCRIBE FOR NEWS IN THE SITE
**/

class Newsletter extends MY_Controller {

	
	/*
      Default grid function
	*/
	public function index(){
		$crud = new grocery_CRUD();
        	$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_read();
		$crud->unset_delete();
        	$crud->unset_bulk_delete();
		$crud->unset_bulk_publish();
		$crud->unset_bulk_unpublish();

		$crud->unset_export();
		$crud->unset_jquery();
		$crud->unset_print();
		if(!empty($this->permission_val)){
	     	    foreach($this->permission_val as $key){
			      if($key['permission'] == 1){
			      	  $crud->unset_add(false);
			       }else if($key['permission'] == 2){
			           $crud->unset_edit(false);
			       }else if($key['permission'] == 3){
			           $crud->unset_read(false);	
			       }else if($key['permission'] == 4){
			       	   $crud->unset_delete(false);
			       }else if($key['permission'] == 5){
			          $crud->unset_bulk_delete(false);
			       }else if($key['permission'] == 6){
			          $crud->unset_bulk_publish(false);
			       }else if($key['permission'] == 7){
			          $crud->unset_bulk_unpublish(false);
			       }
		    }
		}


		$crud->set_table('manage_newsletter');
		$crud->set_subject('Newsletter');	
		$crud->display_as('newsletterDate', 'Date');	
		$crud->required_fields('email','newsletterDate','status');
		$crud->order_by('newsletterDate','desc');
		$crud->set_bulk_action_url(base_url('newsletter/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->add_action('Send', 'fa fa-envelope', '', 'btn btn-xs btn-default', array($this,'_custom_action'));
		$output = $crud->render();			
		$page_title = array('page_title'=>'Newsletter');
		$outputData = array_merge((array)$output, $page_title);				
		$this->_example_output($outputData);
		
	}
	
	
	/**
    	* Custom link create ex: send
    **/
	function _custom_action($primary_key , $row){   
		$email  = safe_b64encode($row->email);
		$id     = safe_b64encode($row->newsletter_id);
		return base_url('messages/composeMail?e='.$email.'&i='.$id.'&type=message');
	}
	
}
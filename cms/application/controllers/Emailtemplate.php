<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR EMAIL TEMPLATE
**/
class Emailtemplate extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->library(array( 'grocery_CRUD' , 'ajax_grocery_crud'));
		$this->userID = check_user_login();
		get_admin_permission_check();
    }
   
    public function index() {
        $this->_example_output((object) array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }
	
	/**
      		* Email template grid view
	**/
	public function template(){

        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Setting');
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_bulk_delete();
	    $crud->set_table('email_templates');		        	
        $crud->columns('unique_name','subject','message','status');
        $crud->required_fields('unique_name','subject','message','status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->fields('unique_name','subject','message','status');
	    $output = $crud->render();
        $page_title = array('page_title' => 'Manage Email Template');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }
}

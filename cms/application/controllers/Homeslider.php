<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR HOME PAGE SLIDER 
**/
class Homeslider extends MY_Controller {
	
    /**
      	* Slider grid view
	**/
	public function index(){	
		$crud = new grocery_CRUD();
		$crud->set_subject('Home Slider');	
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_read();
		
		$crud->set_table('manage_home_slider');
        $crud->set_field_upload('home_slider_image','media/homeslider');
	    $crud->columns('home_slider_title','home_slider_image' , 'status');
		$crud->required_fields('home_slider_title','home_slider_image','status');
		$crud->unset_fields('createdDate');
		
		$crud->set_bulk_action_url(base_url('homeslider/bulk_action'));	
		$crud->set_data_status_field_name('status');	
		
		$output = $crud->render();			
		$page_title = array('page_title'=>'Manage Home Slider');
		$outputData = array_merge((array)$output, $page_title);				
		$this->_example_output($outputData);
	}
	
	
	
}
<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

// Load common function MY_Controller
class Frontend extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->library(array('session' , 'form_validation' , 'grocery_CRUD' , 'ajax_grocery_crud'));
		$this->load->model(array('Menu_model'));
		$this->userID = check_user_login();
		get_admin_permission_check();
    }
     function classes(){
	    $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_subject('Classes');		
        
		$crud->set_table('front_class');
		$crud->set_bulk_action_url(base_url('navigation/bulk_action'));	
		$crud->set_data_status_field_name('status');	
        $crud->required_fields('class_name', 'created_date','status');
		
		//$crud->fields('menu_key', 'menu_title', 'icon_class', 'status', 'menu_level');
		//$crud->field_type('menu_level', 'hidden', '1');
		
        $output = $crud->render();
        $page_title = array('page_title' => 'Main Classes ');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
	   }
	   
	   
	   function roles(){
	    $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_subject('Classes');		
        
		$crud->set_table('user_group');
		$crud->set_bulk_action_url(base_url('navigation/bulk_action'));	
		$crud->set_data_status_field_name('status');	
        $crud->required_fields('class_name', 'created_date','status');
		
		$output = $crud->render();
        $page_title = array('page_title' => 'User Role');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
	   }
	
	function setPermission(){
		     
		$crud = new grocery_CRUD();
        $crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_read();
		$crud->unset_bulk_operations();
		$crud->set_subject('Set Permission');
		$crud->set_edit_url_path(base_url('frontend/updatePermission'));
		$crud->set_table('user_group');
		$crud->columns('groupName');
			
		
		$output = $crud->render();
		
		
        $page_title = array('page_title' => 'Manage Access Permission');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }
	
	  public function updatePermission(){
        $id = $this->uri->segment('3')?$this->uri->segment('3'):'';
		$data['page_title'] = 'Action permission';
		
	
	
	if($_POST){
		
		$role_id = $id; //$_POST['role_id'];
		$checkbox = @$_POST['checkbox'];
		if(!empty($checkbox)){
			$this->Common->delete('front_permissions','role_id',$role_id);
			foreach($checkbox as $key => $val){
			   $class_id = $key;
			   $permissions_data = '';
				   foreach($val as $pval){
						  $permissions_data[] = array('role_id' => $role_id,'class_id' => $class_id,'permission' => '1',
					                                    'action_id' => $pval);
					} 
				//	echo '<pre/>'; print_r($permissions_data); die;
				 $this->db->insert_batch('front_permissions', $permissions_data);
				

			}
		}
		$this->messageci->set('Permission updated successfully!', 'success');
		
	}
	
	$where = " where status = 'Active'";
	$data['action'] = $this->Common->select('front_actions',$where);
	
	$where = " where status = 'Active'";
	$data['result'] = $this->Common->select('front_class',$where);
	
	$where = " where role_id = $id";
	$data['front_permissions'] = $this->Common->select('front_permissions',$where);
	
	 $this->load->view('frontend/updatePermission',$data);
    }
	

  

   
}

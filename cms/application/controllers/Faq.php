<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTION MY_Controller
	* CONTROLLER WORK ON FAQ MODULE
**/
class Faq extends MY_Controller {

    /**
      	*Faq section grid
    **/
	public function faqSection(){

		$crud = new grocery_CRUD();
        $crud->unset_jquery();
		$crud->set_subject('Faq Section');
		$crud->set_table('manage_faq_category');
		$crud->where('manage_faq_category.faq_parent_id', '0');
		$crud->columns('category_name', 'description', 'created_date', 'status');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');
		$crud->display_as('category_name','Section Name');        
		$crud->unset_fields('created_date', 'faq_parent_id');
		$crud->required_fields('category_name','status');
		$crud->unset_print();
		$crud->unset_export();
        $output = $crud->render();
		$page_title = array('page_title' => 'Manage Faq Section');
        $outputData = array_merge((array)$output, $page_title);
        $this->_example_output($outputData);	
	}

	
   /**
      *Faq category grid
   **/
	public function faqCategory(){

		$crud = new grocery_CRUD();
        $crud->unset_jquery();
		$crud->set_subject('Faq Category');
		$crud->set_table('manage_faq_category');
        $crud->where('manage_faq_category.faq_parent_id !=', '0');
		$crud->set_relation('faq_parent_id', 'manage_faq_category', '{category_name}');
		$crud->display_as('faq_parent_id', 'Section name');
		$crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status');       
		$crud->unset_fields('created_date');
		$crud->unset_print();
		$crud->unset_export();
		$crud->required_fields('category_name','status');
        $output = $crud->render();
		$page_title = array('page_title' => 'Mange Faq Category');
        $outputData = array_merge((array)$output, $page_title);
        $this->_example_output($outputData);	
	}

	/**
       *Faq list
    **/
	public function faqList(){
		$crud = new grocery_CRUD();
        $crud->unset_jquery();				
		$crud->set_table('manage_faq');
		$crud->set_subject('Faq List');
		$crud->set_relation('category_id' , 'manage_faq_category' , 'category_name');
		$crud->display_as('category_id', 'Category');
		$crud->set_field_upload('image','media/faq/image');
		$crud->set_field_upload('video','media/faq/video');
        $crud->set_bulk_action_url(base_url('setting/bulk_action'));		
		$crud->set_data_status_field_name('status'); 
		$crud->required_fields('question', 'answer', 'status');
		$crud->unset_fields('created_date');
		$crud->unset_delete();
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_bulk_delete();
		$output = $crud->render();
		$page_title = array('page_title'=>'Manage Faq List');
		$outputData = array_merge((array)$output, $page_title);
		$this->_example_output($outputData);	
	}
}
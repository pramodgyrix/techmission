<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTION MY_Controller
	* USE FOR MANAGE MENUS
**/

class Manage_menus extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->model(array('Menu_model'));
    }
   
     /*
       Main menu grid
     */	
	public function menus(){
		$crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Manage Menus');
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_bulk_action_url(base_url('manage_menus/bulk_action'));	
		$crud->set_data_status_field_name('menu_status');
		$crud->set_table('cms_menus');
		$crud->required_fields('menu_title', 'menu_status');
		$output = $crud->render();
		$page_title = array('page_title' => 'Manage Menus');
		$outputData = array_merge((array) $output , $page_title);
		$this->_example_output($outputData);
	}
	
	/*
      Menu Item
	*/
	public function menu_items(){
		$crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->set_subject('Manage Menu Items');
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_bulk_action_url(base_url('manage_menus/bulk_action'));	
		$crud->set_data_status_field_name('mi_status');	
		$crud->set_table('cms_menu_item');
		$crud->set_relation('menu_id','cms_menus','menu_title');
		$crud->set_add_url_path(site_url('manage_menus/add_menu_items'));
		$crud->set_edit_url_path(site_url('manage_menus/edit_menu_items'));
		$crud->columns('menu_id','mi_title','mi_alias','mi_status');
		$crud->display_as('menu_id','Menu Category');
		$crud->display_as('mi_title','title');
		$crud->display_as('mi_alias','alias');
		$crud->display_as('mi_status','status');
		$output = $crud->render();
		$page_title = array('page_title' => 'Manage Category');
		$outputData = array_merge((array) $output , $page_title);
		$this->_example_output($outputData);
	}

	/*
      Add menu items
	*/	
	public function add_menu_items(){
		$order=getMenuItemMaxOrder();
		$data['page_title'] = 'Add Menu Items';
		if($this->input->post()){
			$post = $this->input->post();
			$checkFormValidation = $this->__setFormRules('addMenuItem');	
			
			if($post['mi_item_type'] == '3' && strpos($post['mi_link'], 'http://') === false){
				$link = 'http://'.$post['mi_link'];
			}else{
				$link = $post['mi_link'];
			}
			if($checkFormValidation){
				$insert_data = array(
					'menu_id' => $post['menu_id'],
					'mi_parant_menu' => $post['mi_parant_menu'],
					'mi_title' => $post['mi_title'],
					'mi_alias' => $post['mi_alias'],
					'mi_item_type' => $post['mi_item_type'],
					'content_id' => isset($post['content_id']) ? $post['content_id'] : '',
					'section_id' => isset($post['section_id']) ? $post['section_id'] : '',
					'mi_link' => $link,
					'visible_to' => $post['visible_to'],
					'mi_target' => $post['mi_target'],
					'mi_template' => $post['mi_template'],
					'mi_order'=>$order+1
				);
				$this->db->insert('cms_menu_item',$insert_data);
				$this->updateSubMenuOrder();
				redirect('manage_menus/menu_items');
			}
		}
		$this->load->view('cms/add_menu_item');
	}

	/*
	   Edit menu items
	*/	
	public function edit_menu_items($id){
		$order=getMenuItemMaxOrder();
		
		$data['page_title'] = 'Menu Items';
		
		if($this->input->post()){
			$post = $this->input->post();
			$checkFormValidation = $this->__setFormRules('editMenuItem');	
			
			if($post['mi_item_type'] == '3' && strpos($post['mi_link'], 'http://') === false){
				$link = 'http://'.$post['mi_link'];
			}else{
				$link = $post['mi_link'];
			}
			if($checkFormValidation){
				$update_data = array(
					'menu_id' => $post['menu_id'],
					'mi_parant_menu' => $post['mi_parant_menu'],
					'mi_title' => $post['mi_title'],
					'mi_alias' => $post['mi_alias'],
					'mi_item_type' => $post['mi_item_type'],
					'content_id' => isset($post['content_id']) ? $post['content_id'] : '',
					'section_id' => isset($post['section_id']) ? $post['section_id'] : '',
					'mi_link' => $link,
					'visible_to' => $post['visible_to'],
					'mi_target' => $post['mi_target'],
					'mi_template' => $post['mi_template'],
					'mi_order'=>$order+1
				);

				$this->db->where('mi_id', $post['mi_id']);
                $this->db->update('cms_menu_item', $update_data); 
				$this->updateSubMenuOrder();
				redirect('manage_menus/menu_items');
			}
		}
		$data['menu_items']=select('cms_menu_item',"where mi_id='$id'");
		$this->load->view('cms/edit_menu_item',$data);
	}

	/* 
       Validation from rules
	*/
	function __setFormRules($formName=''){
		
		switch($formName){
			case 'addMenuItem':
	$this->form_validation->set_rules('mi_alias', 'Alias',  'required|is_unique[cms_menu_item.mi_alias]|callback_customAlpha');		
				break;
				
			case 'editMenuItem':
			$this->form_validation->set_rules('mi_alias', 'Alias',  'required|callback_customAlpha');	
				break;
		}
		
		$this->form_validation->set_rules('mi_link', 'Menu Link',  'required');
		$this->form_validation->set_rules('menu_id', 'Menu',  'required');
		$this->form_validation->set_rules('mi_title', 'Menu title',  'required|callback_customAlpha');
		$this->form_validation->set_rules('mi_template', 'Template',  'required');
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
		return $this->form_validation->run();
	}

	/*
      Page order function
	*/
	public function pageorder(){
		
		$menuCatId=$this->uri->segment('3')?$this->uri->segment('3'):'';

		$data['page_title'] = 'Page Order';
		
		$data['menus'] = $this->Menu_model->getMenus();
		
		
		if(!empty($menuCatId)){
			 $menuCategoryId=$menuCatId;
			 $data['menu_category_selected']=$menuCatId;
		}else{
			$menuCategoryId=$data['menus'][0]['menu_id'];
			$data['menu_category_selected']=$menuCategoryId;
		}
		$data['menu_item'] = $this->Menu_model->menu_items($menuCategoryId);
        $this->load->view('cms/Pageorder',$data);
    }
	
	/*
      Page order save
	*/
	public function pageordersave(){
		$getData=$this->input->get(); 
		  $pageOrder = 1;

 

 			if(!empty($getData['choices'])){
					for($i=0; $i < count($getData['choices']); $i++){
					  $data= array('mi_order' => $pageOrder);
					  $where=array('mi_id' => str_replace("_","",$getData['choices'][$i]),
					               'menu_id' => $getData['menu_category_selected']);
										 $pageOrder++;
										 
                        
						$this->db->update('cms_menu_item', $data, $where); 					 
										 
					 }
				     
					 
					 	$parant_menu_data=$this->Common->select('cms_menu_item');
						if(!empty($parant_menu_data)){
							foreach($parant_menu_data as $val){
								
							 $where=array('mi_id'=>$val['mi_id']);	
							 $sub_menu_data=$this->Common->select('cms_menu_item',$where);	
							 
							 foreach($sub_menu_data as $subval){
								$where=array('mi_parant_menu'=>$subval['mi_id']);
							    $data= array('mi_order' => $subval['mi_order']);
							    $this->Common->update('cms_menu_item' ,$data, $where); 
								 
							  }
							}
						}
					 
					 
					 
				  }
                echo '<div class="alert alert-success">
										<i class="fa fa-check-circle"></i>
										Page order update successfully.<button data-dismiss="alert" class="close">×</button>
									</div>';
		}
	
	/*
      Update sub menu order
	*/	
	public function updateSubMenuOrder(){
	     $parant_menu_data=$this->Common->select('cms_menu_item');
						if(!empty($parant_menu_data)){
							foreach($parant_menu_data as $val){
								
							 $where=array('mi_id'=>$val['mi_id']);	
							 $sub_menu_data=$this->Common->select('cms_menu_item',$where);	
							 
							 foreach($sub_menu_data as $subval){
								$where=array('mi_parant_menu'=>$subval['mi_id']);
							    $data= array('mi_order' => $subval['mi_order']);
							    $this->Common->update('cms_menu_item' ,$data, $where); 
								 
							  }
							}
		}	
		return;
	}
}
<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS	
	* USE FOR NAVIGATION OF THE PAGE
**/

class Navigation extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->library(array('session' , 'form_validation' , 'grocery_CRUD' , 'ajax_grocery_crud'));
		$this->load->model(array('Menu_model'));
		$this->userID = check_user_login();
		get_admin_permission_check();
    }
    public function _example_output($output = null) {
        $this->load->view('setting' , $output);
    }

   
	public function bulk_action(){
				
		$action = $this->input->post("action", TRUE);
		$table_name = $this->input->post("table_name", TRUE);
		$field_name = $this->input->post("field_name", TRUE);
		$primary_key = $this->input->post("primary_key", TRUE);
		
		$selection = rtrim($this->input->post("selection", TRUE), '|');
		$id_array = ($selection) ? explode("|", $selection) : '';
		
		if($id_array != '' && $table_name !='' && $primary_key !=''){
			switch($action){
				case 'delete':
					foreach($id_array as $item):
						if($item != ''):
							$this->db->delete($table_name, array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items deleted!';
				break;
				case 'publish':
					foreach($id_array as $item):
						if($item != '' && $field_name !=''):
							$this->db->update($table_name, array($field_name => 'Active'), array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items published!';
				break;
				case 'unpublish':
					foreach($id_array as $item):
						if($item != '' && $field_name !=''):
							$this->db->update($table_name, array($field_name => 'Inactive'), array($primary_key => $item));
						endif;
					endforeach;
					echo count($id_array).' Items unpublished!';
				break;	
			}
		}else{
		   echo 'Kindly Select Atleast One Item!';
		}
	}
    public function index() {
        $this->_example_output((object) array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }
	
	
	public function main_menu(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_subject('Main Menu');		
        $crud->set_table('admin_main_menu');
		$crud->where(array('admin_main_menu.parent_id'=>'0', 'admin_main_menu.menu_level'=>'1'));
		
		$crud->columns('menu_key', 'menu_title', 'icon_class', 'status');
		$crud->set_bulk_action_url(base_url('navigation/bulk_action'));	
		$crud->set_data_status_field_name('status');	
        $crud->required_fields('menu_key', 'menu_title', 'status');
		$crud->fields('menu_key', 'menu_title', 'icon_class', 'status', 'menu_level');
		$crud->field_type('menu_level', 'hidden', '1');
		
        $output = $crud->render();
        $page_title = array('page_title' => 'Main Menu');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }
	public function subMenu(){
        $crud = new grocery_CRUD();
		$crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->set_subject('Sub Menu');		
		
		
		$crud->set_table('admin_sub_menu');
		
		
		$crud->set_relation('parent_id', 'admin_main_menu', 'menu_title');
		$crud->columns('parent_id', 'menu_key', 'menu_title', 'status');		
		$crud->display_as('parent_id', 'parent menu');
				
		$crud->set_bulk_action_url(base_url('navigation/bulk_action'));	
		$crud->set_data_status_field_name('status');
        $crud->required_fields('parent_id', 'menu_key', 'menu_title', 'status');
		$crud->fields('parent_id', 'menu_key', 'menu_title', 'icon_class', 'status', 'menu_level');	
		$crud->field_type('menu_level', 'hidden', '2');
		
		
		
		
        $output = $crud->render();
        $page_title = array('page_title' => 'Sub Menu');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }
	function setPermission(){
		     
		$crud = new grocery_CRUD();
        $crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_bulk_operations();
		$crud->set_subject('Set Permission');
		
		$crud->where('administrator_role.status', 'Active');
      
	    $crud->set_table('administrator_role');
		
		$crud->set_relation_n_n('permission_category','cms_permission', 'admin_sub_menu','role_id','page_menu_id', 'menu_title', 'priority', array('admin_sub_menu.status'=>'Active'));
			
		$crud->columns('role_name', 'permission_category', 'status');
		$crud->display_as('permission_category','Page permission');	
		$crud->display_as('role_name', 'Role Name');
		$crud->field_type('role_name', 'hidden');
		$crud->edit_fields('role_name', 'permission_category');	        
        $output = $crud->render();
        $page_title = array('page_title' => 'Manage Permission');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

     public function actionpermission(){
        $crud = new grocery_CRUD();
        $crud->unset_jquery();
		$crud->unset_print();
		$crud->unset_export();
		$crud->unset_delete();
		$crud->unset_add();
		$crud->unset_bulk_operations();
		$crud->set_subject('Set Permission');
		$crud->where('administrator_role.status', 'Active');
        $crud->set_table('administrator_role');
		$crud->set_relation_n_n('permission_category','cms_permission', 'admin_sub_menu','role_id','page_menu_id', 'menu_title', 'priority', array('admin_sub_menu.status'=>'Active'));
		$crud->columns('role_name', 'permission_category', 'status');
		$crud->display_as('permission_category','Page permission');	
		$crud->display_as('role_name', 'Role Name');
		$crud->field_type('role_name', 'hidden');
		$crud->edit_fields('role_name', 'permission_category');	   

		//$crud->set_add_url_path(base_url('properties/add'));
		$crud->set_edit_url_path(base_url('navigation/editAction'));
		//$crud->set_read_url_path(base_url('properties/view'));

        $output = $crud->render();
        $page_title = array('page_title' => 'Action Permission');
        $outputData = array_merge((array) $output , $page_title);
        $this->_example_output($outputData);
    }

    public function editAction(){
        $id = $this->uri->segment('3')?$this->uri->segment('3'):'';
		$data['page_title'] = 'Action permission';
		
	$data['menu_action'] = $this->Common->select("manage_menu_action");
	$sql = "SELECT i.* FROM cms_permission as p join admin_sub_menu as i on p.page_menu_id = i.page_menu_id where p.role_id = $id";
		$query = $this->db->query($sql);
        if($query->num_rows()>0){
            $data['result'] =  $query->result_array();
        }
        
        $this->load->view('navigation/editAction',$data);
    }
	
	public function menuorder(){
		
		$data['page_title'] = 'Menu Order';
		
		$where = " where status = 'Active' ORDER BY menu_order ASC";
		$data['menu_item'] = $this->Common->select("admin_main_menu",$where);
		
		//echo '<pre/>';
		//print_r($data['menu_item']); 
        $this->load->view('navigation/Pageorder',$data);
    }
	
	public function navigationordersave(){
	  $getData=$this->input->get();
	  $pageOrder = 1;
 			if(!empty($getData['choices'])){
					for($i=0; $i < count($getData['choices']); $i++){
					  $data= array('menu_order' => $pageOrder);
					  
					  $where=array('page_menu_id' => str_replace("_","",$getData['choices'][$i]));
					  //print_r($where);
						$pageOrder++;
						$this->db->update('admin_main_menu', $data, $where); 					 
					 }
				  }
                echo '<div class="alert alert-success">
										<i class="fa fa-check-circle"></i>
										Page navigation update successfully.
									</div>';
		}	
		
		
	public function sub_menu_order(){
		
		$menuCatId=$this->uri->segment('3')?$this->uri->segment('3'):'';

		$data['page_title'] = 'Sub-menu Order';
		
		$data['menus'] = $this->Menu_model->getadminMenus();
		
		
		if(!empty($menuCatId)){
			 $menuCategoryId=$menuCatId;
			 $data['menu_category_selected']=$menuCatId;
		}else{
			$menuCategoryId=$data['menus'][0]['page_menu_id'];
			$data['menu_category_selected']=$menuCategoryId;
		}
		$data['menu_item'] = $this->Menu_model->getadminSubmenus($menuCategoryId);
        $this->load->view('navigation/sub_menu_order',$data);
    }
	
	public function sub_menu_order_save(){
	  $getData=$this->input->get();
	  $pageOrder = 1;
 			if(!empty($getData['choices'])){
					for($i=0; $i < count($getData['choices']); $i++){
					  $data= array('menu_order' => $pageOrder);
					  
					  $where=array('page_menu_id' => str_replace("_","",$getData['choices'][$i]));
					  //print_r($where);
						$pageOrder++;
						$this->db->update('admin_sub_menu', $data, $where); 					 
					 }
				  }
                echo '<div class="alert alert-success">
										<i class="fa fa-check-circle"></i>
										Page navigation update successfully.
									</div>';
		}	
}

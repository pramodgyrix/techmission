<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
	/**
	 	* GYRIX TECHNOLABS
	 	* THIS IS DEFAULT CONTROLLER USE FOR CALANDER
	**/
class Dashboard extends CI_Controller {	 
	var $userID;
	function __construct()
    {
		parent::__construct();
		/* Your own constructor code */
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('session', 'form_validation'));
		$this->userID = check_user_login();
    }
    
    /**
		* SHOWING CALENDAR ON DASHBOARD
    **/
	public function index()
	{		
		$data['stylesheet'] = array('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css');
		$data['page_title'] = 'Dashboard';
		$this->load->view('dashboard', $data);
	}	
}

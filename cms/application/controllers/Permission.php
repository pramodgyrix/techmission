<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Permission extends CI_Controller {
	public function __construct(){
            parent::__construct();
    } 
	 
	public function index(){
		$data['page_title']='Permission';
		$this->load->view('permission',$data);
	}
}
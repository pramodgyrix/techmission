<?php defined('BASEPATH') OR exit('No direct script access allowed');
   $websetting = $this->session->userdata('websetting');
   $site_name =  $websetting['site_name'];
   
   
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title><?php if(!empty($site_name)){echo $site_name;}else{ echo config_item('site_name');}?>- <?php echo $page_title;?></title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="shortcut icon" href="<?php echo config_item('site_url');?>favicon.ico" type="image/x-icon" />
		<link href="<?php echo config_item('site_url');?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/iCheck/skins/all.css">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<!--<link rel="stylesheet/less" type="text/css" href="<?php echo config_item('site_url');?>assets/css/styles.less">-->
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/css/theme_navy.css" type="text/css" id="skin_color">
		<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: EXTRA CSS REQUIRED FOR THIS PAGE ONLY -->
		 <?php
        	if(isset($stylesheet) && !empty($stylesheet)){
				$i=0;
				foreach($stylesheet as $value){$i++;								
					$tab = ($i!=1)?"\t\t ":"";
					echo $tab.'<link rel="stylesheet" href="'.config_item('site_url').$value.'">'."\n";
				}
			}
			if(isset($style) && !empty($style)){
				foreach($style as $value){
					echo $value;
				}
			}
		?>
		<!-- end: EXTRA CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="shortcut icon" href="<?=base_url();?>favicon.ico" type="image/x-icon" />
	<script>var base_url = '<?php echo config_item('base_url');?>';</script>
	<script>var site_url = '<?php echo config_item('site_url');?>';</script>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
<?php
 $websetting = $this->session->userdata('websetting');
 $Weblogos=$this->session->userdata('Weblogos');
 $main_logo = display_image($Weblogos['main_logo'], SITE_LOGO.'/');
?>
<!-- start: HEADER -->

<div class="navbar navbar-inverse navbar-fixed-top"> 
  <!-- start: TOP NAVIGATION CONTAINER -->
  <div class="container">
    <div class="navbar-header"> 
      <!-- start: RESPONSIVE MENU TOGGLER -->
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- end: RESPONSIVE MENU TOGGLER --> 
      <!-- start: LOGO --> 
      <!-- <a class="navbar-brand" href="<?php echo base_url('dashboard');?>">
                 OTRIGA 
                                       
            </a>--> 
      
      <a href="<?php echo base_url('dashboard');?>" class="navbar-brand logo_inner" style="font-size: 26px;" title="<?php if(!empty($websetting['site_name'])){echo $websetting['site_name'];}else{ echo config_item('site_name');} ?>">
         
           <img src="<?=$main_logo;?>" style="width: 94px;height: 50px;
    margin-top: -20px;"/>
          
      </a> 
      
      <!-- end: LOGO --> 
    </div>
    <div class="navbar-tools"> 
      <!-- start: TOP NAVIGATION MENU -->
      <ul class="nav navbar-right">
        <!-- start: TO-DO DROPDOWN --> 
        
        <!-- end: NOTIFICATION DROPDOWN --> 
        <!-- start: MESSAGE DROPDOWN -->
        <li class="dropdown navbar-tools msg_nav" id="messageNavigation"> <a class="dropdown-toggle" data-close-others="true"  data-toggle="dropdown" href="javascript:void(0)"> <i class="fa fa-envelope-o"></i> <span class="badge total_badge_mail" id="total_badge_mail">0</span> </a>
          <ul class="dropdown-menu posts pull-right">
            <li> <span class="dropdown-menu-title"> You have <span class="total_badge_mail">0</span> messages</span> </li>
            <li>
              <div class="drop-down-wrapper panel-scroll ps-container recently_unread_mail" id="recently_unread_mail">
                <ul>
                  <li>
                    <div class="dont_have_recent_activity">You don't have recent message!</div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="view-all"> <a href="<?=base_url('messages');?>"> See all messages <i class="fa fa-arrow-circle-o-right"></i> </a> </li>
          </ul>
        </li>
        <!-- end: MESSAGE DROPDOWN --> 
        <!-- start: USER DROPDOWN -->
        <?php 
                $userId = $this->session->userdata('userId');
				$userData=select('administrator',"where userId='$userId'",'firstName,lastName,profileImage');
				$profilePic = display_image($userData[0]['profileImage'], USER_IMAGE_150150);
				
                ?>
        <li class="dropdown current-user"> <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)"> <img src="<?=$profilePic?>" style="width:20px;height:20px;" class="circle-img"> <span class="username"><?php echo ucfirst($userData[0]['firstName'].' '.$userData[0]['lastName']);?></span> <i class="clip-chevron-down"></i> </a>
          <ul class="dropdown-menu">
            <li> <a href="<?php echo config_item('site_url');?>" target="_blank"> <i class="fa fa-mail-forward"></i> &nbsp;Visit Frontend </a> </li>
            <li> <a href="<?php echo base_url();?>profile"> <i class="clip-user-2"></i> &nbsp;My Profile </a> </li>
            <li> <a href="<?php echo base_url();?>profile/edit"> <i class="clip-pencil-2"></i> &nbsp;Edit Profile </a> </li>
            <li> <a href="<?=base_url();?>profile/changePassword"> <i class="fa fa-refresh"></i> &nbsp;Change Password </a> </li>
            <li> <a href="<?php echo base_url('login/logout');?>"> <i class="clip-exit"></i> &nbsp;Log Out </a> </li>
          </ul>
        </li>
        <!-- end: USER DROPDOWN -->
      </ul>
      <!-- end: TOP NAVIGATION MENU --> 
    </div>
  </div>
  <!-- end: TOP NAVIGATION CONTAINER --> 
</div>
<!-- end: HEADER -->
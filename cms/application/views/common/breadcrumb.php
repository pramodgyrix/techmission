<?php
// load Breadcrumbs
$CI =& get_instance();
$CI->load->library('breadcrumbs');
$CI->breadcrumbs->push('<i class="clip-home-3"></i> Home', '/');

$segs = $this->uri->segment_array();
$href = '';
foreach ($segs as $segment)
{
	$href .= $segment.'/';
	
	$segment = preg_replace('/(?<!\ )[A-Z]/', ' $0', $segment);
	
   $CI->breadcrumbs->push(ucfirst($segment), $href);	
}
echo $CI->breadcrumbs->show();
?>
<?php $this->load->view('common/notification_alert');?>
<?php
 $websetting = $this->session->userdata('websetting');
 $site_address =  $websetting['site_address'];
 $site_copyright =  $websetting['site_copyright'];
?>		
		<div class="footer clearfix">
			<div class="footer-inner">
				<?=$site_copyright.' '.str_replace("<br/>","",$site_address);?>
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
        
        
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="<?php echo config_item('site_url');?>assets/plugins/respond.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/js/main.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/js/custom/recent_activity_admin.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
        
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <?php 
        	if(isset($scriptsrc) && !empty($scriptsrc)){
				$i=0;
				foreach($scriptsrc as $value){$i++;
					$tab = ($i!=1)?"\t\t ":"";
					echo $tab.'<script src="'.config_item('site_url').$value.'" type="text/javascript"></script>'."\n";
				}
			}
			if(isset($script) && !empty($script)){
				foreach($script as $value){
					echo $value;
				}
			}
		?>		
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		
		
		<script>
	jQuery(document).ready(function() {
		Main.init();
	});
</script>
<?php
$controller = $this->router->class;
$method = $this->router->method;
$role_name = $this->session->userdata('role_name');
$role_id = $this->session->userdata('role_id');

?>
<div class="main-navigation navbar-collapse collapse"> 
  <!-- start: MAIN MENU TOGGLER BUTTON -->
  <div class="navigation-toggler"> <i class="clip-chevron-left"></i> <i class="clip-chevron-right"></i> </div>
  <!-- end: MAIN MENU TOGGLER BUTTON --> 
  <!-- start: MAIN NAVIGATION MENU -->
  <ul class="main-navigation-menu">
    <li<?php if($controller=='dashboard'){?> class="active open"<?php }?>> <a href="<?php echo base_url('dashboard');?>"><i class="fa fa-tachometer"></i> <span class="title"> Dashboard </span><span class="selected"></span> </a> </li>
   <?php /*?> <li<?php if($controller=='setting'){?> class="active open"<?php }?>> <a href="<?php echo base_url('setting/websetting');?>"><i class="clip-settings"></i> <span class="title"> Setting </span><span class="selected"></span> </a> </li>
    	  

 <li <?php if($controller=='navigation'){?> class="active open"<?php }?>><a href="javascript:void(0)"> <i class="clip-user-5"></i> <span class="title">Manage Navigation</span> <i class="icon-arrow"></i> <span class="selected"></span> </a>
      <ul class="sub-menu">
        <li><a <?php if($method=='menuCategory' && $controller=='navigation'){?> class="active"<?php }?> href="<?=base_url('navigation/menuCategory');?>"><span class="title"> Menu Category </span></a></li>
        <li><a <?php if($method=='menuPage' && $controller=='navigation'){?> class="active"<?php }?> href="<?=base_url('navigation/menuPage');?>"><span class="title"> Menu Page </span></a></li>
        <li><a <?php if($method=='setPermission' && $controller=='navigation'){?> class="active"<?php }?> href="<?=base_url('navigation/setPermission');?>"><span class="title"> Set Permission </span></a></li>
      </ul>
    </li><?php */?>
    
    
    
    
     <?php  
	$permitNavigation = get_admin_permissions($role_id);
	
	if(isset($permitNavigation) && !empty($permitNavigation)){
			foreach($permitNavigation as $val){
		
			$icon_class= $val[0]['icon_class'];
			$category_name = $val[0]['category_name'];
			
			$menu= '<li'; 
				if($controller == $val[0]['category_key']){ 
					$menu .= ' class="active open"';
				} else if ($controller == 'auctionproperty' && $val[0]['category_key'] == 'properties') {
					$menu .= ' class="active open"';
				}
				$menu .= '>';
			
			
			
			$menu.='<a href="javascript:void(0)">
<i class="'.$icon_class.'"></i>
<span class="title">'.ucwords($category_name).'</span>
<i class="icon-arrow"></i>
<span class="selected"></span> 
</a> ';
			
			$menu .= '<ul class="sub-menu">';
			foreach($val as $value){
			
			$st = $value['child_menu_key'];

            $pieces = @explode("/", $st);

            $mymethod=end($pieces);
			
				$menu .= '<li'; 
				if($controller == $val[0]['category_key'] && $method == $mymethod && $controller != 'auctionproperty'){ 
					$menu .= ' class="active"';
				} else if ($controller == 'auctionproperty' && $st == 'auctionproperty') {
					$menu .= ' class="active"';
				}
				$menu .= '>';
				$menu .= '<a href="'.base_url($value['child_menu_key']).'"><span class="title"> '.ucwords($value['child_menu_title']).' </span></a>';
				$menu .= '</li>';
			}
			$menu .= '</ul>';
            echo $menu .= '</li>';
			
            
		}
		}		
		?>
    
    
 
 
</div>


<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>        
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
	<div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php $this->load->view('common/left_navigation'); ?>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">				
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">							
                    <!-- start: PAGE TITLE & BREADCRUMB -->							
                    <?php $this->load->view('common/breadcrumb'); ?>
                    <div class="page-header">
                        <h1><?php echo $page_title;?> <small>overview &amp; stats </small></h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            
			<!-- start: PAGE CONTENT -->
			<div class="row">
				<div class="col-lg-12">
                	<div class="tabbable">	
						<?php $this->load->view('message/navigation');?>
						<div class="tab-content-static">
                            <div id="panel_overview" class="tab-pane in active">
                                <!-- start: COMPOSE PANEL -->
                                <?php echo validation_errors();?>
                                <?php 
									foreach($result as $messageData);
									$attachment ='';
									$text = 'Re:';
									if($mailType=='reply'){
										$to = $messageData['sender_email'];										
									}else if($mailType=='replyToAll'){
										$to = str_replace($my_email, '', $messageData['allReceiver']);
										$to = ($to=='' || $to==',')? $messageData['sender_email'] : $messageData['sender_email'].', '.$to;
									}else{
										$to = '';
										$text = 'Fr:';
										$path = set_value('attachment', $messageData['attachment']);
										$file = set_value('attachedFilename', $messageData['attachedFilename']);
										$attachment = '<input type="hidden" name="attachment" value="'.$path.'">'."\n\n";
										$attachment .= '<input type="hidden" name="attachedFilename" value="'.$file.'">';
									}
									
								?>
                                <form role="form" id="messageComposeForm" class="form-horizontal" 
                                action="<?php echo base_url('messages/'.$mailType);?>" method="post" enctype="multipart/form-data">
                                    
                                    <?=$attachment?>
                                    <input type="hidden" name="message_id" value="<?=set_value('message_id', $messageData['message_id']);?>">
									<br/>
                                    <div class="form-group">
                                    	<label class="col-sm-2 control-label">Recipient email <span class="symbol required"></span></label>
                                        <div class="col-md-9">
                                        	<input id="tags_1" type="text" name="recipienties" class="form-control tags"
                                            value="<?=set_value('recipienties', $to);?>">
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"> 
											Subject <span class="symbol required"></span> 
                                        </label>
                                        <div class="col-md-9">
                                        	<input type="text" id="msgsubj" name="subject" class="form-control" placeholder="Subject" value="<?=$text.set_value('subject', $messageData['subject']);?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                    	<label class="col-sm-2 control-label">Message Body <span class="symbol required"></span> </label>
                                        <div class="col-md-10">
                                        	 <textarea class="form-control" name="message" id="msgbody" placeholder="Message Body" rows="10"><?=set_value('message');?></textarea>
                                        </div>
                                    </div>
                                    <style>
									.button-text{padding-top: 5px;padding-left:5px;vertical-align:middle;}
									.progress-wrap{margin-top:10px;margin-bottom:10px;}
									#errormsg{}
									#attachmentbox{padding-top:0px;padding-bottom:10px;}
									</style>
                                    <div class="form-group">
                                    	<label class="col-sm-2 control-label"></label>
                                        <div class="col-md-9">
                                            <div class="content-box">           
                                                <input type="button" value="Choose file" class="btn btn-large clearfix" id="upload-btn">
                                                <span class="button-text">Attach a file</span>
                                                <div class="clearfix redtext" id="errormsg"></div>	              
                                                <div class="progress-wrap" id="pic-progress-wrap"></div>	
                                                <div class="clear" id="attachmentbox"></div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>                              
                                        <div class="clear"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-2">
                                            <button class="btn btn-light-grey" type="button" onclick="location.href = '<?=base_url('messages');?>'"><i class="fa fa-arrow-circle-left"></i> <?php echo 'cancel';?></button>
                                            <button type="submit" class="btn btn-dark-grey">SEND<i class="fa fa-arrow-circle-right"></i></button>
                                        </div>
                                    </div>     
                                    
								<input type="hidden" value="<?php echo !empty($result->message_id) ? $result->message_id : ''; ?>" name="thread"/>
										                                
                              </form>                                
                			</div>
              			</div>
              			<!-- end: PAGE CONTENT--> 
            		</div>
          		</div>
          <!-- end: PAGE --> 
			</div>
		</div>
	</div>
</div>
<!-- end: MAIN CONTAINER --> 
<?php $this->load->view('common/footer_content'); ?>
<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo config_item('site_url');?>assets/plugins/select2/select2.min.js"></script>
<script src="<?php echo config_item('site_url');?>assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
<script src="<?php echo config_item('site_url');?>assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo config_item('site_url');?>assets/grocery_crud/js/jquery_plugins/config/jquery.ckeditor.config.js"></script>
<script src="<?php echo config_item('site_url');?>assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js"></script>
<script src="<?php echo config_item('site_url');?>assets/js/custom/compose-message-validator.js"></script>
<script src="<?php echo config_item('site_url');?>assets/js/ajaxUploader.js"></script>
<script>
	
	//function to initiate jquery.tagsinput
	jQuery(document).ready(function() {
		messageFormValidator.init();
		$('#tags_1').tagsInput({
			defaultText:'Email ID',
            width: 'auto'
        });
						
		$(".search-select").select2({
		placeholder: "Select a record",
		allowClear: true
	});
	var btn = document.getElementById('upload-btn'),
	wrap = document.getElementById('pic-progress-wrap'),
	picBox = document.getElementById('attachmentbox'),
	errBox = document.getElementById('errormsg');
	var file_count = 0;
	var uploader = new ss.SimpleUpload({
		button: btn,
		url: base_url+'messages/uploadAttachment', // server side handler
		progressUrl: 'uploadProgress.php', // enables cross-browser progress support (more info below)
		responseType: 'json',
		name: 'uploadfile',
		multiple: true,
		maxUploads: 10,
		maxSize: 51200,
		queue: false,
		allowedExtensions: ['jpg', 'jpeg', 'png', 'gif', 'pdf', 'doc', 'docx', 'txt', 'zip', 'mp4', 'csv', 'xls', 'xlsx', 'mp3'],
		accept: '*',
		debug: true,
		hoverClass: 'btn-hover',
		focusClass: 'active',
		disabledClass: 'disabled',
		responseType: 'json',
		onSubmit: function(filename, ext) {
		   var prog = document.createElement('div'),
			   outer = document.createElement('div'),
			   bar = document.createElement('div'),
			   size = document.createElement('div'),
			   self = this;
			   prog.className = 'prog';
			size.className = 'size';
			outer.className = 'progress';
			bar.className = 'bar';
			outer.appendChild(bar);
			prog.appendChild(size);
			prog.appendChild(outer);
			wrap.appendChild(prog); // 'wrap' is an element on the page
			self.setProgressBar(bar);
			self.setProgressContainer(prog);
			self.setFileSizeBox(size);
			errBox.innerHTML = '';
			btn.value = 'Choose another file';
		  },
		  onSizeError: function() {
			errBox.innerHTML = 'Files may not exceed 500K.';
		  },
		  onExtError: function() {
			  errBox.innerHTML = 'Invalid file type. Please select a PNG, JPG, GIF image.';
		  },
		  onComplete: function(file, response) {
			if (!response) {
			  errBox.innerHTML = 'Unable to upload file';
			}
			if (response.success === true) {
				file_count++;
				//picBox.innerHTML = '<img src=\"' + response.file + '\">';
			  jQuery('#attachmentbox').append('<div class=\"attachment-container\" id=\"attachment'+file_count+'\"><span class=\"attachment-icon\"><i class=\"fa fa-paperclip\"></i></span><span>'+response.filename+'</span><a href=\"javascript:void(0)\" class=\"close fileupload-exists\" onclick=\"jQuery(\'#attachment'+file_count+'\').remove();\">×</a><input type=\"hidden\" value=\"' + response.file + '\" name=\"attachment[]\"><input type=\"hidden\" value=\"' + response.filename + '\" name=\"attachedFilename[]\"></div>');
			} else {
			  if (response.msg)  {
				errBox.innerHTML = response.msg;
			  } else {
				errBox.innerHTML = 'Unable to upload file';
			  }
			}
		  }
		});
	});
</script>
<script type="text/javascript">
// CKEDITOR.replace( 'msgbody' );
</script> 
<style>
#tags_1_tag{
width:100% !important;
}
.close{
font-size: 18px !important;
}
</style>
<?php $this->load->view('common/footer'); ?>

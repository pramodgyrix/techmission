<?php $this->load->view('common/header'); ?>

<?php foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>        
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->
<div class="main-container">
	<div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php $this->load->view('common/left_navigation'); ?>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">				
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">							
                    <!-- start: PAGE TITLE & BREADCRUMB -->							
                    <?php $this->load->view('common/breadcrumb'); ?>
                    <div class="page-header">
                        <h1><?php echo $page_title;?> <small>overview &amp; stats </small></h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            
			<!-- start: PAGE CONTENT -->
			<div class="row">
				<div class="col-md-12">
                	<div class="tabbable">
 						<?php $this->load->view('message/navigation');?>
                        <div class="tab-content-static">
                            <div id="panel_overview" class="tab-pane in active">
								<?php echo $output; ?>
                            </div>
                       </div>
                   </div>					
				</div>
			</div>                    
			<!-- end: PAGE CONTENT-->
            
		</div>
	</div>
	<!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?> 
	   
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?> 
 <?php
	if(isset($dropdown_setup)) {
		$this->load->view('dependent_dropdown', $dropdown_setup);
	}
 ?>
			
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/header'); ?>

<!-- Start: HEADER -->

<?php $this->load->view('common/header_content'); ?>        

<!-- end: HEADER -->

<style>

.read_msg{background-color:#CCCCCC;}

</style>

<!-- start: MAIN CONTAINER -->

<div class="main-container">

	<?php $this->load->view('common/left_navigation'); ?>

	<!-- start: PAGE -->

	<div class="main-content">				

		<div class="container">

			<!-- start: PAGE HEADER -->

			<?php $this->load->view('common/breadcrumb'); ?>

			<!-- end: PAGE HEADER -->

			<!-- start: PAGE CONTENT -->

			<div class="row">

				<div class="col-lg-12">

                	<div class="tabbable">	

                    							

						<?php $this->load->view('message/navigation');?>

                        <div class="tab-content-static">

                            <div id="panel_overview" class="tab-pane in active">

                                <!-- start: INBOX PANEL -->

                                <div class="panel panel-default">                                            

                                    <div class="panel-body messages">

                                        <ul class="messages-list">

                                            <li class="messages-search">

                                                <form action="#" class="form-inline">

                                                    <div class="input-group">

             <input id="search_name" type="text" class="form-control" placeholder="Search messages...">

                                                        <div class="input-group-btn">

                                   <button id="serach_outbox" class="btn btn-primary" type="button">

                                                                <i class="fa fa-search"></i>

                                                            </button>

                                                        </div>

                                                    </div>

                                                </form>

                                            </li>

											<div id="show_search_data">

											<?php 

											if(!empty($outboxmsg)){

				          foreach($outboxmsg as $inbxdata){

					  	  $sender_user    = getUserName($inbxdata['sender']);	

						  $subject        = $inbxdata['subject'];

						  $message        = strip_tags($inbxdata['message']);

						  

						 $rec_data1 = unserialize($inbxdata['receiver']);

						 $rec_size = sizeof($rec_data1);

						  if($rec_size ==1)

						  {

						   $rec_id = $rec_data1[0];

						   $user_data = get_user_details($rec_id);

						   

						   $urls = config_item('site_url').'applicationMediaFiles/usersImage';

						   $chk_img   = get_user_profile_image($user_data[0]['profile_image'],$urls);
							
						  $chk_img = display_image($user_data[0]['profile_image'], USER_IMAGE_THUMB);
							
						  }else{
							  $chk_img = '/default.png';
						  }

						 ?>

			<a href="javascript:void(0);" onclick="viewMessage('<?php echo $inbxdata['message_id'];?>');" style="text-decoration:none;">

                            <li class="messages-item">

        <!-- <span title="Mark as starred" class="messages-item-star"><i class="fa fa-star"></i></span>-->

     <img alt="" src="<?=$chk_img;?>" class="messages-item-avatar">

                        <span class="messages-item-from">

			

						<?php 
					
						$rec_data = unserialize($inbxdata['receiver']);

						$rec_user = '';
						if(is_array($rec_data)){
							foreach($rec_data as $rec_val){
								if(!empty($rec_val)){
									$rec_user .= ucfirst(getUserName($rec_val));
								}
								$rec_user .= ',';
							}
						}

					   

					   echo dataLimit(trim($rec_user, ','),'10');

					   

						?>

						</span>

                                  <div class="messages-item-time">

                                         <span class="text"><?php echo ago($inbxdata['date_time']);?></span>

                                             

											 

													

                                                </div>

                         <span class="messages-item-subject"><?php echo dataLimit($subject,'26');?></span>

				         <span class="messages-item-preview"><?php echo dataLimit($message,'72');?></span>

                                            
										<div class="clear"></div>
											</li>
                                            <div class="clear"></div>
                                            </a>

											<?php } 
											//echo $paging; 
											}else{

										  	echo '<div class="alert alert-danger">'.$lang_data['message_no_found'].'</div>';

											}

											?>

										 </div><!-- show_search_data -->

										   </ul>

										   

										<?php if(!empty($outboxmsg)){ ?>

                                        <div class="messages-content" id="dynamic-content">

                                            

                                        </div>

										<?php }else{

										echo '<div class="alert alert-danger">'.$lang_data['message_no_found'].'</div>';

									}?>

									

                                    </div>

                                </div>

                                <!-- end: INBOX PANEL -->

                            </div>									

                        </div>

                    </div>	  

				</div>

			</div>                    

			<!-- end: PAGE CONTENT-->

		</div>

	</div>

	<!-- end: PAGE -->

</div> 

<!-- end: MAIN CONTAINER -->



<?php $this->load->view('common/footer_content'); ?>



<link type="text/css" rel="stylesheet" href="<?php echo config_item('site_url'); ?>assets/css/pagination.css" />

<script src="<?=config_item('site_url').'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' ?>"></script>

<script src="<?=config_item('site_url').'assets/plugins/jquery.pulsate/jquery.pulsate.min.js' ?>"></script>

<script src="<?=config_item('site_url').'assets/js/pages-user-profile.js' ?>"></script>

<script>

	 jQuery(document).ready(function(e){

		PagesUserProfile.init();

	 });			

</script>

			

<!-- start: FOOTER -->

<?php $this->load->view('common/footer'); ?>

<script>

var viewMessage=function(id){

var uid = id;

var url = base_url+'mymessage/viewOutboxMessage';	

$.ajax({

	 type:"POST",

	 url:url, 

	 data: {id: id},

	 success: function(result)

	{ 

		$('#dynamic-content').html(result);

		//alert(result);

		

	}





 });

}



/* trash message */

var deleteMessage=function(id){

var msgid = id;

var url = base_url+'mymessage/trashMsg';



$.ajax({

	 type:"POST",

	 url:url, 

	 data: {msgid: msgid},

	 success: function(result)

	 {window.location.href = '<?php echo config_item('base_url');?>messages/sentbox';

	 //alert(result);

	 }





 });

}



$('#serach_outbox').click(function(){

  var searchdata = $('#search_name').val();

  var url = base_url+'mymessage/searchoutboxdata';

  

		$.ajax({

			type: "POST",

			url: url,

			data: {searchdata:searchdata},

			success:function(result){

			$('#show_search_data').html(result);

			

			}

		});

    });

	

	$(window).load(function() {

$("#show_search_data a:first" ).trigger( "click" );

});

</script>


<?php $this->load->view('common/header'); ?>

<!-- Start: HEADER -->

<?php $this->load->view('common/header_content'); ?>        

<!-- end: HEADER -->

<style>








.read_msg{background-color:#CCCCCC;}

</style>

<!-- start: MAIN CONTAINER -->

<div class="main-container">

	<?php $this->load->view('common/left_navigation'); ?>

	<!-- start: PAGE -->

	<div class="main-content">				

		<div class="container">

			<!-- start: PAGE HEADER -->

			<?php $this->load->view('common/breadcrumb'); ?>

			<!-- end: PAGE HEADER -->

			<!-- start: PAGE CONTENT -->

			<div class="row">

				<div class="col-lg-12">

                	<div class="tabbable">	

                    							

						<?php $this->load->view('message/navigation');?>

                                                

                        <div class="tab-content-static">

                            <div id="panel_overview" class="tab-pane in active">

                                <!-- start: INBOX PANEL -->

                                <div class="panel panel-default">                                            

                                    <div class="panel-body messages">

                                        <ul class="messages-list">

                                            <li class="messages-search">

                                                <form action="#" class="form-inline">

                                                    <div class="input-group">

         <input id="search_name" type="text" class="form-control" placeholder="Search messages...">

                                                        <div class="input-group-btn">

                                      <button id="serach_trash" class="btn btn-primary" type="button">

                                                                <i class="fa fa-search"></i>

                                                            </button>

                                                        </div>

                                                    </div>

                                                </form>

                                            </li>

											<div id="show_search_data">

											<?php 

											//echo '<pre/>'; print_r($trashmsg);

											if(!empty($trashmsg)){

				          					foreach($trashmsg as $trash){

					  	  					$receiver_user    = getUserName($trash['sender']);	

						  					$subject        = $trash['subject'];

						  					$message        = strip_tags($trash['message']);

											$date_time      = ago($trash['date_time']);

											

							 $uid = $this->session->userdata('userId');

							  /* inbox section */

							  $urls = config_item('site_url').'applicationMediaFiles/usersImage';

							  if($trash['sender']!= $uid)

							  {

								$sid =  $trash['sender'];

								$user_info = selectData('user',' where userId = '.$sid);  

								//$chk_img   = get_user_profile_image($user_info[0]['profile_image'],$urls);
								$chk_img = display_image($user_info[0]['profile_image'], USER_IMAGE_THUMB);
							  }/* outbox section */

							  else

							  {

								$select_user = unserialize($trash['receiver']);  

								$size_user = count($select_user);

								if($size_user ==1)

						        {

						          $rec_id = $select_user[0];

						          $user_data = get_user_details($rec_id);

						          //$chk_img   = get_user_profile_image($user_data[0]['profile_image'],$urls);
								  $chk_img = display_image($user_data[0]['profile_image'], USER_IMAGE_THUMB);
						        }

						         else{$chk_img = '/default.png';}

							  }

/*

							  $user_pics = unserialize($trash['receiver']);

						      $user_pics_size = sizeof($user_pics);

							  $urls = config_item('site_url').'applicationMediaFiles/usersImage';

						      if($user_pics_size ==1)

						      {

						       $rec_id = $user_pics[0];

						       $user_data = get_user_details($rec_id);

						       $chk_img   = get_user_profile_image($user_data[0]['profile_image'],$urls);

						      }

						       else{$chk_img = '/default.png';}

	*/			        ?>

                                        
                                        <li class="messages-item" onclick="viewMessage('<?php echo $trash['message_id'];?>');" style="cursor:pointer;">
                                        <span title="Mark as starred" class="messages-item-star"><i class="fa fa-star"></i></span>
                                        <img alt="" src="<?=$chk_img;?>" class="messages-item-avatar">
                                        <span class="messages-item-from">
                                        <?php 
                                        
                                        if($trash['sender'] == $this->session->userdata('userId')){
                                        
                                        $rec_data = unserialize($trash['receiver']);
                                        
                                        $rec_user = '';
                                        
                                        foreach($rec_data as $rec_val){
                                        
                                        if(!empty($rec_val))
                                        
                                        {
                                        
                                        $rec_user .= ucfirst(getUserName($rec_val));
                                        
                                        }
                                        
                                        $rec_user .= ',';
                                        
                                        }echo dataLimit(trim($rec_user, ','),'10');
                                        
                                        }else{
                                        
                                        echo ucfirst($receiver_user);}
                                        
                                        ?>
                                        </span>
                                        <div class="messages-item-time">
                                        
                                        <span class="text"><?php echo $date_time;?></span>                          
                                        
                                        <div class="messages-item-actions">
                                        <a data-toggle="dropdown" title="Reply" href="wsdindex.html"><i class="fa fa-mail-reply"></i></a>
                                        
                                        <div class="dropdown">
                                        
                                        <a data-toggle="dropdown" title="Move to folder" href="wsdindex.html#"><i class="fa fa-folder-open"></i></a>
                                        
                                        <ul class="dropdown-menu pull-right">
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#">
                                        
                                        <i class="fa fa-pencil"></i>
                                        
                                        Mark as Read
                                        
                                        </a>
                                        
                                        </li>
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#">
                                        
                                        <i class="fa fa-ban"></i>
                                        
                                        Spam
                                        
                                        </a>
                                        
                                        </li>
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#">
                                        
                                        <i class="fa fa-trash-o"></i>
                                        
                                        Delete
                                        
                                        </a>
                                        
                                        </li>
                                        
                                        </ul>
                                        
                                        </div>
                                        
                                        <div class="dropdown">
                                        
                                        <a data-toggle="dropdown" title="Attach to tag" href="wsdindex.html#"><i class="fa fa-tag"></i></a>
                                        
                                        <ul class="dropdown-menu pull-right">
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#"><i class="tag-icon red"></i>Important</a>
                                        
                                        </li>
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#"><i class="tag-icon teal"></i>Work</a>
                                        
                                        </li>
                                        
                                        <li>
                                        
                                        <a href="wsdindex.html#"><i class="tag-icon green"></i>Home</a>
                                        
                                        </li>
                                        
                                        </ul>
                                        
                                        </div>
                                        
                                        </div>
                                        
                                        </div>
                                        <span class="messages-item-subject"><?php echo dataLimit($subject,'26');?></span>
                                        <span class="messages-item-preview"><?php echo dataLimit($message,'72');?></span>
                                        </li>
                                       

                                            <?php } 
											//echo $paging; 
											}else{

											echo '<div class="alert alert-danger">'.$lang_data['message_no_found'].'</div>';

										}

											?>

											<!-- user list end -->

                                            </div> <!--show_search_data  -->

                                        </ul>

										

                                        <div class="messages-content" id="dynamic-content">

										</div>

									</div>

                                </div>

                                <!-- end: INBOX PANEL -->

                            </div>									

                        </div>

                    </div>	  

				</div>

			</div>                    

			<!-- end: PAGE CONTENT-->

		</div>

	</div>

	<!-- end: PAGE -->

</div>

<!-- end: MAIN CONTAINER -->



<?php $this->load->view('common/footer_content'); ?>

<link type="text/css" rel="stylesheet" href="<?=base_url(); ?>assets/css/pagination.css" />	
<script src="<?=base_url().'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js' ?>"></script>
<script src="<?=base_url().'assets/plugins/jquery.pulsate/jquery.pulsate.min.js' ?>"></script>
<!-- start: FOOTER -->

<?php $this->load->view('common/footer'); ?>

<script>

var viewMessage=function(id){

var url = base_url+'mymessage/viewTrashMessage';

$.ajax({

	 type:"POST",

	 url:url, 

	 data: {msgid: id},

	 success: function(result){ 

	 $('#dynamic-content').html(result);

	 //alert(result);

	 }

 });

}



/* paramanent delete message */

var deleteMessage=function(id){

var msgid = id;

var url = base_url+'mymessage/delete';

$.ajax({

	 type:"POST",

	 url:url, 

	 data: {msgid: msgid},

	 success: function(result)

	 {window.location.href = base_url+'mymessage';}

 });

 }

/* paramanent delete message resetMessage */

 /* paramanent delete message */

var resetMessage=function(id){

var msgid = id;

var url = base_url+'mymessage/resetMessage';

$.ajax({

	 type:"POST",

	 url:url, 

	 data: {msgid: msgid},

	 success: function(result)

	 {window.location.href = base_url+'mymessage/trash';}

 });

 }





$('#serach_trash').click(function(){

  var searchdata = $('#search_name').val();

  var url = base_url+'mymessage/searchtrashdata';

  

  

		$.ajax({

			type: "POST",

			url: url,

			data: {searchdata:searchdata},

			success:function(result){

			$('#show_search_data').html(result);

			//alert(result);

			}

		});

    });

$(window).load(function() {

$("#show_search_data a:first" ).trigger( "click" );

});



</script>


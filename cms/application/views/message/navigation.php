<?php $menu = $this->router->method; ?>
<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
    <li<?php if($menu=='index' || $menu=='messageDetail'){?> class="active"<?php }?>>
        <a href="<?php echo base_url('messages');?>">Inbox</a>
    </li>
    <li<?php if($menu=='sentbox' || $menu=='sentMsgDetail'){?> class="active"<?php }?>>
        <a href="<?php echo base_url('messages/sentbox');?>">Sentbox</a>
    </li>    
    <li<?php if($menu=='composeMail'){?> class="active"<?php }?>>
        <a href="<?php echo base_url('messages/composeMail');?>">Compose Message</a>
    </li> 
    <li<?php if($menu=='trash' || $menu=='trashMsgDetail'){?> class="active"<?php }?>>
        <a href="<?php echo base_url('messages/trash');?>">Trash</a>
    </li>
</ul>

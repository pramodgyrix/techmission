<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>        
<!-- end: HEADER -->
<style>
.messages-content{
	margin-left:0px !important;
}
</style>
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php $this->load->view('common/left_navigation'); ?>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">				
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">							
                    <!-- start: PAGE TITLE & BREADCRUMB -->							
                    <?php $this->load->view('common/breadcrumb'); ?>
                    <div class="page-header">
                        <h1><?php echo $page_title;?> <small>overview &amp; stats </small></h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            
			<!-- start: PAGE CONTENT -->
			<div class="row">
				<div class="col-lg-12">
                	<div class="tabbable">
 						<?php $this->load->view('message/navigation');?>
                        <div class="tab-content-static">
                            <div id="panel_overview" class="tab-pane in active"></div> 
                                <!-- start: INBOX PANEL -->
                                <div class="panel panel-default">
                                    <div class="panel-body messages">
                                    	<?php
										$i=0;					
                                        foreach($result as $messageDate){ $i++;
										?>
										<div class="messages-content">
                                            <div class="message-header">
                                                <div class="message-time">
												<?php
                                                 $attachedFiles = unserialize($messageDate['attachment']);
                                                 if(isset($attachedFiles) && count($attachedFiles) > 0 && $attachedFiles !=''){
                                                    echo '<i class="clip-attachment"></i>';
                                                  }
                                                 ?>
												<?=date('d M Y, h:i A',strtotime($messageDate['sendDate']));?>
                                                </div>
                                                <div class="message-from">
                                                    From: &nbsp; <?=$messageDate['sender_email']?>
                                                </div>
                                                <div class="message-to">
                                                    To: &nbsp; <?=$messageDate['allReceiver']?>
                                                </div>
                                                <div class="message-subject">
                                                   <?=$messageDate['subject']?>
                                                </div>
                                                <div class="message-actions">
                                                <?php if($this->router->method == 'sentMsgDetail'){?>
                                                    <?php /*?><a title="Move to trash" href="<?=base_url('messages/sentItemDelete/'.$messageDate['message_id'])?>"><i class="fa fa-trash-o"></i></a> <?php */?>                                                   
                                                    <a title="Forward" href="<?=base_url('messages/forward/'.$messageDate['message_id'])?>"><i class="fa fa-long-arrow-right"></i></a>
                                                 <?php }else{ ?>
                                                 	<a title="Move to trash" href="<?=base_url('messages/moveInTrash/'.$messageDate['message_id'])?>" onclick="return confirm('Are you sure to perform this action?')"><i class="fa fa-trash-o"></i></a>
                                                    <a title="Reply" href="<?=base_url('messages/reply/'.$messageDate['message_id'])?>"><i class="fa fa-reply"></i></a>
                                                    <a title="Reply to all" href="<?=base_url('messages/replyToAll/'.$messageDate['message_id'])?>"><i class="fa fa-reply-all"></i></a>
                                                    <a title="Forward" href="<?=base_url('messages/forward/'.$messageDate['message_id'])?>"><i class="fa fa-long-arrow-right"></i></a>
                                                 <?php } ?>
                                                </div>
                                            </div>
                                            <div class="message-content"<?=($i>1) ? ' style="display:none;"' : '';?>>
                                                <p>
                                                    <?=nl2br($messageDate['message'])?>
                                                </p>
                                                <?php									
                                                	$attachedFiles = unserialize($messageDate['attachment']);													
													if(isset($attachedFiles) && count($attachedFiles) > 0 && $attachedFiles !=''){
														echo '<h5><strong>Attached Files:</strong></h5><p>';
														for($i=0; $i < count($attachedFiles);$i++){
															//print_r($attachedFiles);
															$fileData = explode('/', $attachedFiles[$i]);											
															$fileName = preg_replace("/%20/", "-", end($fileData));
															echo '<p><a download="download" href="'.$attachedFiles[$i].'">'.$fileName.'</a></p>';
														}
														echo '</p>';
													}
												?>	
                                            </div>
                                        </div>
										<?php }?>
                                        
									</div>
                                </div>
                                <!-- end: INBOX PANEL -->
                            </div>									
                        </div>
                    </div>	  
				</div>
			</div>                    
			<!-- end: PAGE CONTENT-->
		</div>
	</div>
	<!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<?php $this->load->view('common/footer_content'); ?>
<script>
	$('.message-header').on('click', function(){
		if($(this).next().is(":visible")){
			$(this).next().hide();
		}else{
			$(this).next().show();
		}
	})
</script>
<?php $this->load->view('common/footer'); ?>
<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title;?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      <div class="row">
        <div class="col-lg-12">
          <div class="tabbable">
            <?php //$this->load->view('profile/navigation');?>
            <div id="feedback"></div>
            <div class="tab-content-static">
              <div id="panel_edit_profile" class="tab-pane in active">
                <div class="row">
                 
               
  <form action="" method="post">               
<?php 
  if(!empty($result)){
		foreach($result as $val){
		echo '<div class="panel panel-default"><div class="panel-heading">';
		echo $val['class_name'];
		echo '</div>';
		$id = $val['id'];
			if(!empty($action)){
			echo '<div class="panel-body">';
				foreach($action as $key){
					 $st = search_array($front_permissions,$id,$key['id']);
					 $chk = '';
					 if(!empty($st))
					   $chk = 'checked';
				echo '<div class="col-md-2"><label class="checkbox-inline">';
				echo '<input type="checkbox" name="checkbox['.$id.'][]" '.$chk.' value="'.$key['id'].'" class="grey">';
				echo $key['action_name']; 
				echo '</label></div>';
				}  
			echo '</div>';
			}
		echo '</div>';
		}
	}
?>
<div class="form-group">
													<div class="col-sm-2 col-sm-offset-10">
														<button type="submit" name="submit" class="btn btn-blue next-step btn-block">
															Update 
														</button>
													</div>
												</div>
               
   </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<script>
  $(function() {
   
   
	
	
  });
</script> 
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

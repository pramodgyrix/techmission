<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title;?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      <div class="row">
        <div class="col-lg-12">
          <div class="tabbable">
            <div id="feedback"></div>
            <div class="tab-content-static">
              <div id="panel_edit_profile" class="tab-pane in active">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <div class="col-md-2"> &nbsp; </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-8">
                        <div class="alert alert-warning"> <span class="label label-warning">NOTE!</span> <span> Click on and drag an element to a new spot within the list.</span> </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-2"> &nbsp; </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Navigation Items <span class="symbol required"></span></label>
                      <div class="col-md-6">
                        <?php
if(!empty($menu_item)){
	echo '<ul id="sortable">';
	$i=1;
	foreach($menu_item as $val){
?>
                        <li id="<?=$val['page_menu_id'].'_'.$i;?>" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                          <?=ucwords($val['menu_title']);?>
                        </li>
                        <?php
	$i++;}
	echo '</ul>';
	}else{
	   echo '<div class="alert alert-danger">
										<i class="fa fa-times-circle"></i>
										Sorry no Menu-items found.<a href="'.base_url('navigation/main_menu').'">Click here</a> to create Menu category
									</div>';	
	}
?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<link rel="stylesheet" href="<?php echo config_item('site_url').'assets/css/ui_css/jquery-ui.min.css';?>">
<link rel="stylesheet" href="<?php echo config_item('site_url').'assets/css/ui_css/jquery-ui.structure.min.css';?>">
<link rel="stylesheet" href="<?php echo config_item('site_url').'assets/css/ui_css/jquery-ui.theme.min.css';?>">
<script src="<?php echo config_item('site_url').'assets/js/ui_js/jquery-ui.min.js';?>"></script>
<style>
  #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 40px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }
  </style>
<script>
  var menu_category_selected='<?=$menu_category_selected;?>';
  $(function() {
    $( "#sortable" ).sortable({
  create: function( event, ui ) {
	 // console.log(event); console.log(ui);
	  }
} );
    $( "#sortable" ).disableSelection();
	
	$( "#sortable" ).on( "sortout", function( event, ui ) {
		// console.log(event);
		//  console.log(ui); console.log('ui');
		   var schoices = $( "#sortable" ).sortable( "serialize",
        { key: "choices[]", expression: /(.+)(.+)/} );
  console.log(schoices);
  $( "#feedback" ).load(base_url+'navigation/navigationordersave/?&'+ schoices+'&menu_category_selected='+menu_category_selected);
  
  
		} );
		
		/*
		expression: /(.+)[=_](.+)/
		*/
		
  });
  var changeCatagory=function(menuCatId){
	 //window.location(base_url+'sitemenu/pageorder/'+menuCatId); 
	 location.href=base_url+'sitemenu/pageorder/'+menuCatId;
	 
  }
  </script> 

<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

<?php $this->load->view('common/header'); ?>
<?php foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->
<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title;?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      <div class="row">
        <div class="col-lg-12"> <?php echo $output; ?> </div>
      </div>
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php //$this->load->view('common/footer_content'); ?>

<?php
 $websetting = $this->session->userdata('websetting');
 $site_address =  $websetting['site_address'];
 $site_copyright =  $websetting['site_copyright'];
?>		
		<div class="footer clearfix">
			<div class="footer-inner">
				<?=$site_copyright.' '.str_replace("<br/>","",$site_address);?>
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
        
        
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="<?php echo config_item('site_url');?>assets/plugins/respond.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/excanvas.min.js"></script>
		<![endif]-->
        <?php
        $segment = $this->uri->segment('3');
		if($segment == 'edit' || $segment == 'add'){
			echo '<script src="'.config_item('site_url').'assets/grocery_crud/js/jquery-1.11.1.min.js"></script>';
		}else{
			echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>';
		}
		?>
		
		<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/js/main.js"></script>
		<script src="<?php echo config_item('site_url');?>assets/js/custom/recent_activity_admin.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
        
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <?php 
        	if(isset($scriptsrc) && !empty($scriptsrc)){
				$i=0;
				foreach($scriptsrc as $value){$i++;
					$tab = ($i!=1)?"\t\t ":"";
					echo $tab.'<script src="'.config_item('site_url').$value.'" type="text/javascript"></script>'."\n";
				}
			}
			if(isset($script) && !empty($script)){
				foreach($script as $value){
					echo $value;
				}
			}
		?>		
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		
		
		<script>
			jQuery(document).ready(function() {
				Main.init();
			});
		</script>

<?php
$requestUrl = $_SERVER['REQUEST_URI'];
/*if(!empty($requestUrl)){
  $count_requestUrl = @explode("/",$requestUrl);
 // echo '--'.count($count_requestUrl);
  if(count($count_requestUrl) > 5){
	  echo $tab.'<script src="'.config_item('site_url').'assets/grocery_crud/js/jquery-1.11.1.min.js"></script>'."\n";
	 
	  }
}*/

 foreach($js_files as $file): ?>
<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php
			if(isset($dropdown_setup)) {
				$this->load->view('dependent_dropdown', $dropdown_setup);
			}
			
	/* $url = explode('/', $_SERVER['REQUEST_URI']);
	  if(count($url) > 2){
		if($url[3] == 'myabroadproperties'){
		     echo '<script src="'.config_item('site_url').'assets/js/custom/myabroadproperties.js'.'"></script>';
		  }
	  }*/
		 ?>

<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

<?php
$websetting = $this->session->userdata('websetting');
$site_name =  $websetting['site_name'];
$Weblogos=$this->session->userdata('Weblogos');
$main_logo = display_image($Weblogos['main_logo'], SITE_LOGO.'/');
?>

<!DOCTYPE html>

<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->

<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->

<!--[if !IE]><!-->

<html lang="en" class="no-js">

<!--<![endif]-->

<!-- start: HEAD -->

<head>
<title>Admin-Login</title>

<!-- start: META -->

<meta charset="utf-8" />

<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->

<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="" name="description" />
<meta content="" name="author" />

<!-- end: META -->

<!-- start: MAIN CSS -->

<link rel="shortcut icon" href="<?php echo config_item('site_url');?>favicon.ico" type="image/x-icon" />
<link href="<?php echo config_item('site_url');?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">

<!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">-->

<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/fonts/style.css">
<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/css/backend_main.css">
<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">

<!--<link rel="stylesheet/less" type="text/css" href="<?php echo config_item('site_url');?>assets/css/styles.less">-->

<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/css/theme_navy.css" type="text/css" id="skin_color">

<!--[if IE 7]>

		<link rel="stylesheet" href="<?php echo config_item('site_url');?>assets/plugins/font-awesome/css/font-awesome-ie7.min.css">

		<![endif]-->

<!-- end: MAIN CSS -->

<!-- start: EXTRA CSS REQUIRED FOR THIS PAGE ONLY -->

<?php

        	if(isset($stylesheet) && !empty($stylesheet)){

				$i=0;

				foreach($stylesheet as $value){$i++;								

					$tab = ($i!=1)?"\t\t ":"";

					echo $tab.'<link rel="stylesheet" href="'.base_url().$value.'">'."\n";

				}

			}

			if(isset($style) && !empty($style)){

				foreach($style as $value){

					echo $value;

				}

			}

		?>

<!-- end: EXTRA CSS REQUIRED FOR THIS PAGE ONLY -->

<link rel="shortcut icon" href="<?=base_url();?>favicon.ico" type="image/x-icon" />
<head>


<body class="login example2">
<div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
  <div class="logo">
  <img src="<?=$main_logo;?>" /><br/>
  Admin-Login</div>
  
  <!-- start: LOGIN BOX -->
  
  <div class="box-login">
    <h3>Sign in to your account</h3>
    <p> Please enter your Email and password to log in. </p>
    
    <!--<form class="form-login" action="index.html">-->
    
    <p>
      <?php if(!empty($msg)){

					         if($msg['warning']){

							   echo '<div class="alert alert-danger"><i class="fa fa-times-circle"></i> <strong>Warning!</strong> '.$msg['warning'].'</div>'; 

							}else if($msg['success'])

							   echo '<div class="alert alert-success"><i class="fa fa-times-circle"></i><strong>Well done!</strong> '.$msg['success'].'</div>';

					   } ?>
    </p>
    <?php echo form_open('login/authenticate','class="form-login"') ?> <?php echo validation_errors();?>
    <div class="errorHandler alert alert-danger no-display"> <i class="fa fa-remove-sign"></i> You have some form errors. Please check below. </div>
    <fieldset>
      <div class="form-group"> <span class="input-icon">
        <input type="text" class="form-control" name="email" placeholder="Email" value="<?=set_value('email');?>">
        <i class="fa fa-user"></i> </span> 
        
        <!-- To mark the incorrectly filled input, you must add the class "error" to the input --> 
        
        <!-- example: <input type="text" class="login error" name="login" value="Username" /> --> 
        
      </div>
      <div class="form-group form-actions"> <span class="input-icon">
        <input type="password" class="form-control password" name="password" placeholder="Password" value="<?=set_value('password');?>">
        <i class="clip-key"></i> <a class="forgot" href="#">I forgot my password</a> </span> </div>
      <div class="form-actions"> 
        
        <!--<label for="remember" class="checkbox-inline">

								<input type="checkbox" class="grey remember" id="remember" name="remember">

								Keep me signed in

							</label>-->
        
        <button type="submit" class="btn red_button pull-right"> Login <i class="fa fa-arrow-circle-right"></i> </button>
      </div>
    </fieldset>
    </form>
  </div>
  
  <!-- end: LOGIN BOX -->
  
  <div class="box-forgot" style="display:none;">
    <h3>Forget Password?</h3>
    <p>Enter your e-mail address below to reset your password.</p>
    <?php echo validation_errors();?>
    <form class="form-forgot" action="<?=base_url('login/forgetPassword');?>" method="post">
      <div class="errorHandler alert alert-danger no-display"> <i class="fa fa-remove-sign"></i> You have some form errors. Please check below. </div>
      <fieldset>
        <div class="form-group"> <span class="input-icon">
          <input type="email" class="form-control" name="email" placeholder="Email" value="<?=set_value('email');?>">
          <i class="fa fa-envelope"></i> </span> </div>
        <div class="form-actions">
          <button class="btn btn-light-grey go-back"> <i class="fa fa-circle-arrow-left"></i> Back </button>
          <button type="submit" class="btn red_button pull-right"> Submit <i class="fa fa-arrow-circle-right"></i> </button>
        </div>
      </fieldset>
    </form>
  </div>
</div>

<!-- start: MAIN JAVASCRIPTS --> 

<!--[if lt IE 9]>

		<script src="<?php echo config_item('site_url');?>assets/plugins/respond.min.js"></script>

		<script src="<?php echo config_item('site_url');?>assets/plugins/excanvas.min.js"></script>

		<![endif]--> 

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/blockUI/jquery.blockUI.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/iCheck/jquery.icheck.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/less/less-1.5.0.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-cookie/jquery.cookie.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/js/main.js"></script> 

<!-- end: MAIN JAVASCRIPTS --> 

<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY --> 

<script src="<?php echo config_item('site_url');?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script> 
<script src="<?php echo config_item('site_url');?>assets/js/login.js"></script> 

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY --> 

<script>

			jQuery(document).ready(function() {

				Main.init();

				Login.init();

			});

		</script> 

<!-- start: FOOTER -->

<?php $this->load->view('common/footer'); ?>

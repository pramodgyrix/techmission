<?php $menu = $this->router->method; 
$groupName = $this->session->userdata('groupName');
?>
<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue" id="myTab4">
   <li<?php if($menu=='index' || $menu=='profile'){?> class="active"<?php }?>>
        <a href="<?php echo base_url();?>profile">
            My-profile
        </a>
    </li>
    <li<?php if($menu=='edit'){?> class="active"<?php }?>>
        <a href="<?php echo base_url();?>profile/edit">
            Edit Profile
        </a>
    </li>    
    <li<?php if($menu=='changePassword'){?> class="active"<?php }?>>
        <a href="<?php echo base_url();?>profile/changePassword">
            Change Password
        </a>
    </li>        
</ul>

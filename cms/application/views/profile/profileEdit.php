<?php $this->load->view('common/header'); ?>

<!-- Start: HEADER -->

<?php $this->load->view('common/header_content'); 

foreach($result as $userData);



?>

<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">

  <div class="navbar-content">

    <!-- start: SIDEBAR -->

    <?php $this->load->view('common/left_navigation'); ?>

    <!-- end: SIDEBAR --> 

  </div>

  

  <!-- start: PAGE -->

  

  <div class="main-content">

    <div class="container"> 

      

      <!-- start: PAGE HEADER -->

      

      <div class="row">

        <div class="col-sm-12"> 

          

          <!-- start: PAGE TITLE & BREADCRUMB -->

          

          <?php $this->load->view('common/breadcrumb'); ?>

          <div class="page-header">

            <h1><?php echo $page_title;?></h1>

          </div>

          

          <!-- end: PAGE TITLE & BREADCRUMB --> 

          

        </div>

      </div>

      

      <!-- end: PAGE HEADER --> 

      

      <!-- start: PAGE CONTENT -->

      

      <div class="row">

        <div class="col-lg-12">

          <div class="tabbable">

            <?php $this->load->view('profile/navigation');?>

            <div class="tab-content-static">

              <div id="panel_edit_profile" class="tab-pane in active">

                <form action="<?=base_url('profile/edit')?>" method="post" role="form" id="profileForm" enctype="multipart/form-data">

                    <div class="row">

                        <div class="col-md-12">                        	

                        	<?php echo validation_errors();?>

                            <div class="errorHandler alert alert-danger no-display">

                                <i class="fa fa-times-sign"></i> You have some form errors. Please check below.

                            </div>

                            <div class="successHandler alert alert-success no-display">

                                <i class="fa fa-ok"></i> Your form validation is successful!

                            </div>

                        </div>

                        <div class="col-md-12">

                            <h3>Account Info</h3>

                            <hr>

                        </div>

                        <div class="col-md-6">                        	

                            <div class="form-group">

                                <label class="control-label">

                                    Gender <span class="symbol required"></span>

                                </label>

                                <div>

                                    <label class="radio-inline">

                                        <input <?=(set_value('gender', $userData['gender'])=='Female')?'checked="checked"':''?> type="radio" class="grey" value="Female" name="gender"> Female

                                    </label>

                                    <label class="radio-inline">

                                        <input <?=(set_value('gender', $userData['gender'])=='Male')?'checked="checked"':''?> type="radio" class="grey" value="Male" name="gender"> Male

                                    </label>

                                </div>

                            </div>

                            

                            <div class="form-group connected-group">

                                <label class="control-label">

                                    Full Name <span class="symbol required"></span>

                                </label>

                                <div class="row">

                                    <div class="col-md-6 col-sm-6 col-xs-6">

                                        <input type="text" placeholder="First Name" class="form-control" id="firstName" name="firstName" value="<?=set_value('firstName', $userData['firstName']);?>">

                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-6">

                                        <input type="text" placeholder="Last Name" class="form-control" id="lastName" name="lastName" value="<?=set_value('lastName', $userData['lastName']);?>">

                                    </div>                                    

                                </div>

                            </div>

                            <div class="form-group">

                                <label class="control-label">

                                    Email Address <span class="symbol required"></span>

                                </label>

                                <input readonly="readonly" type="email" placeholder="Email Address" class="form-control" id="email" name="email" value="<?=set_value('email', $userData['email']);?>">

                            </div>

                            <div class="form-group">

                                <label class="control-label">

                                    Phone number <span class="symbol required"></span>

                                </label>

                                <input type="text" placeholder="Phone Number" class="form-control" id="phoneNumber" name="phoneNumber" value="<?=set_value('phoneNumber', $userData['phoneNumber']);?>">

                            </div>   

                            <div class="form-group">

                                <label class="control-label">

                                    Cell number <span class="symbol required"></span>

                                </label>

                                <input type="text" placeholder="Cell Number" class="form-control" id="cellNumber" name="cellNumber" value="<?=set_value('cellNumber', $userData['cellNumber']);?>">

                            </div>   

                                              

                        </div>

                        

                        <div class="col-md-6">                        	

                            <div class="form-group">

                                <div class="col-sm-12">

                                    <label>Image Upload</label>

                                    <div class="fileupload fileupload-new" data-provides="fileupload">

                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">

                                        <?php

                                        	$profilePic = display_image($userData['profileImage'], USER_IMAGE.'/thumb');

										?>

                                        <img src="<?php echo $profilePic;?>" alt=""/>

                                        </div>

                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                                        <div>

                                            <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> Select image</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> Change</span>

                                                <input name="profileImage" value="" type="file">

                                            </span>

                                            <a href="wsdindex.html#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">

                                                <i class="fa fa-times"></i> Remove

                                            </a>

                                        </div>

                                    </div>

                                    

                                </div>

                            </div> 

                            <div class="form-group">

                                <label class="control-label">

                                    Address <span class="symbol required"></span>

                                </label>

                                <input type="text" placeholder="address" class="form-control" id="address" name="address" value="<?=set_value('address', $userData['address']);?>">
                                
                                
                                
                                <input id="latitude"  name="latitude" class="form-control" type="hidden" placeholder="lat" value="<?=set_value('latitude', $userData['latitude']);?>">

                                <input id="longitude"  name="longitude" class="form-control" type="hidden" placeholder="lng" value="<?=set_value('longitude', $userData['longitude']);?>">
                                
                                <input type="hidden" id="city" name="city" class="locality" value="<?=set_value('city', $userData['city']);?>"/>
                                
                                <input type="hidden" id="regions" name="regions" class="administrative_area_level_1" value="<?=set_value('regions', $userData['regions']);?>"/>
                                
                                <input type="hidden" id="country" name="country_code" class="country" value="<?=set_value('country_code', $userData['country_code']);?>"/>
                                
                                <input type="hidden" id="postal_code" name="postal_code" class="postal_code" value="<?=set_value('postal_code', $userData['postal_code']);?>"/>
                                                                

                            </div>

                             <div class="form-group">
                                <div id="map-canvas" style="height:250px; width:100%;"> Map</div>
                             </div>  
                              <span class="help-block"><small><i class="fa fa-info-circle"></i> You can also get your address by dragging of marker on the map</small></span>                                                   

                        </div>

                    </div> 

                    

                    <div class="row">

                        <div class="col-md-12">

                            <div>

                                <span class="symbol required"></span>Required Fields

                                <hr>

                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-8">

                           

                        </div>

                        <div class="col-md-4">
						    <div class="col-md-4"> 
                           <a class="btn btn-light-grey" href="<?=base_url('profile');?>"> <i class="fa fa-arrow-circle-left"></i>&nbsp;Back </a>


                            </div>
                            <div class="col-md-8"> 
                            <button class="btn btn-dark-grey pull-right" type="submit">

                                Update Profile <i class="fa fa-arrow-circle-right"></i>

                            </button>

                            </div>
							
						
                    </div>

                </form>

              </div>

            </div>

          </div>

        </div>

      </div>

      

      <!-- end: PAGE CONTENT--> 

      

    </div>

  </div>

  

  <!-- end: PAGE --> 

  

</div>



<!-- end: MAIN CONTAINER -->



<?php $this->load->view('common/footer_content'); ?>

<script src="<?=config_item('site_url').'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js';?>"></script> 
<script src="<?=config_item('site_url').'assets/plugins/jquery-validation/dist/jquery.validate.min.js';?>"></script>
<script src="<?=config_item('site_url').'assets/js/custom/admin_profile.js';?>"></script> 
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDmS8jRSpSNi4itEnZXL4YpqImWMt-028A&sensor=false&libraries=places"></script> 
<script src="<?=config_item('site_url').'assets/js/gmap/country_region_change_map_property.js';?>"></script>


<script>

jQuery(document).ready(function(e){

	ProfileFormValidator.init();

});

</script> 



<!-- start: FOOTER -->



<?php $this->load->view('common/footer'); ?>


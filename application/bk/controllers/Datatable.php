<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datatable extends CI_Controller {

	function __construct()
    {
		parent::__construct();	
		$this->load->helper(array('cms'));
		$this->load->library(array('pagination','encryption'));
		$this->load->model(array('Datatables_Model'));
		//$this->output->enable_profiler(TRUE);
		//$this->load->model('user_model');
		//$this->load->database();
    }
	
	private function column_name($name){
		 switch($name)
       	 {
		  case 'name':
		  return 'name';
		  break; 
		
	      case 'contact':
		  return 'contact_no';
		  break; 
		  
		   case 'email':
		  return 'email';
		  break; 
		  
		    case 'country':
		  return 'country_name';
		  break; 
		  
		   case 'branch':
		  return 'branch_name';
		  break; 
		  
		   case 'industry':
		  return 'industry_name';
		  break; 
	  
	     default :
	     return 'id';
	   }
	}
	
	public function index($alias=''){
		//$this->output->enable_profiler(TRUE);
		//dd($_SESSION);
		$limit = 6;
		$display = 1;
		$display_from = $limit;
		$offset = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$data['offset'] = $offset;
		if(!empty($offset)){
			if($offset != 1){
			  $display =  (($offset-1) * $limit);	
			}else{
				$display = $limit;
			}
		 	$offset  = (($offset-1) * $limit); 
			$display_from = $offset + $limit ;
		}
		
		 $order_column_name = ($this->uri->segment(4))? $this->uri->segment(4) : NULL;
		 $order_type = ($this->uri->segment(5))? $this->uri->segment(5) : ''; 
		 
          
		  $data['order_column'] =  trim($order_column_name);
		  $data['order_class'] = 'sorting';
		  if($order_type == 'asc'){
		  $data['order_class'] = 'sorting_asc';
		  }else if($order_type == 'desc'){
		  $data['order_class'] = 'sorting_desc';
		  }
		 
		$order_column = $this->column_name($order_column_name); 
	    $new_order = ($order_type == 'asc' ? 'desc' : 'asc');
		$data['new_order'] = $new_order;
		
		if(@$_POST['btn_search'] == 'search'){
			$this->session->unset_userdata('search');
		}
		$post = $this->input->post('s',true);
		$post = trim($post);
		$data['s'] = $post;
		
		//dd($_POST);
		
		$reset_search = $this->input->post('reset_search',true);
		if($reset_search == 1) $this->session->unset_userdata('search');
		

		$where = '';
		$result = $this->Datatables_Model->select_data($where,$limit, $offset,$post,$order_column, $order_type);
		$data['result_count'] = $this->Datatables_Model->select_data_count($where,$post);
		
		
		if($new_order == 'asc'){
		$result = $this->array_msort($result, array($order_column=>SORT_ASC));	
		}else{
		$result = $this->array_msort($result, array($order_column=>SORT_DESC));
		}
		$data['result'] = $result;
		
		$config["base_url"] = base_url() . "Datatable/index";
		$config["total_rows"] =  $data['result_count'];
		$config["per_page"] = $limit;
		$config["uri_segment"] = 3;
		$config['num_links'] = 1; 
		$config['display_pages'] = TRUE;  
		$config = array_merge($config, $this->config->item('pagingConfig'));
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		
		if(@$_POST['btn_search'] == 'search'){
			$this->session->unset_userdata('search');
		}
		$this->load->view('datatables_view',$data);
		
	}//end of function
	
	
	
	function array_msort($array, $cols)
	{
		$colarr = array();
		foreach ($cols as $col => $order) {
			$colarr[$col] = array();
			foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
		}
		$eval = 'array_multisort(';
		foreach ($cols as $col => $order) {
			$eval .= '$colarr[\''.$col.'\'],'.$order.',';
		}
		$eval = substr($eval,0,-1).');';
		eval($eval);
		$ret = array();
		foreach ($colarr as $col => $arr) {
			foreach ($arr as $k => $v) {
				$k = substr($k,1);
				if (!isset($ret[$k])) $ret[$k] = $array[$k];
				$ret[$k][$col] = $array[$k][$col];
			}
		}
		return $ret;
	}

	

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR CONTACT US MODULE
**/
class Contact extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();	
		$this->load->library(array('parser','recaptcha'));
		$this->lang->load('recaptcha');
	}
	

	/**
		* CALL METHOD ID USER WANT TO CONTACT US BY EMAIL
		* EMAIL SEND TO ADMIN FOR CONTACT US REQUEST AND TO USER ALSO FOR ACKNOWLEDGED
	**/
	public function index()
	{
		if($_POST){
		   $checkFormValidation = $this->__setFormRules('contact');	
		   $postData=$this->input->post();
		   if($checkFormValidation){
			   $insert_array=array('name'=>$postData['name'],
							 'email'=>$postData['email'],
							 'mobile_number'=>$postData['mobile_number'],
							 'subject'=>$postData['subject'],
							 'message'=>nl2br($postData['message'])
							 ); 
			   $insert_id=$this->Common->data_insert('contact_us',$insert_array);				 
			   if($insert_id>0){
				   /*Admin Email Start*/
					$websetting = $this->session->userdata('websetting');
                    $site_name =  $websetting['site_name'];
					$data['username'] = 'Laundry Administrator';
					$data['siteurl'] = base_url();
					$data['sitename'] = $site_name;
					$data['name'] = $postData['name'];
					$data['email'] = $postData['email'];
					$data['subject'] = $postData['subject'];

					$data['msg'] = $postData['message'];
					$data['data']['title']	= "Enquiry";
					
					$where = "WHERE template_id = 8";
					$data['welcome_user'] = $this->Common->select('email_templates',$where);
					$message = $this->parser->parse('mail_template/contact_us', $data, TRUE);
					
					$toEmail = $postData['email'];
					$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
					$subject = $site_name.' - '.$postData['subject'];
					$attachment = array();
					$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
					/*Admin Email End*/
					
					/*User Email Start*/
					$where = "WHERE template_id = 9";
					$data['welcome_user'] = $this->Common->select('email_templates',$where);
					$data['username'] = $postData['name'];
					$data['sitename'] = $site_name;
					$message = $this->parser->parse('mail_template/contact_us_thank_you', $data, TRUE);
					$toEmail = $postData['email'];
					$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
					$subject = $site_name.' - '.$postData['subject'];
					$attachment = array();
					$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
					/*User Email End*/
				   
				   
				   $this->session->set_flashdata('message_success', 'Your message has been sent successfully');
				   redirect(base_url('contact'));
			   }else{
				   $this->session->set_flashdata('message_failed', 'Message sending failed');
				   redirect(base_url('contact'));
			   }
		  }
		}
		$data['page_title']='Contact';
		$data['recaptcha']=$this->recaptcha->get_html();
		$this->load->view('contact',$data);
	}
	


/**
	* SET RULES FOR INPUT POST DATA	
**/
	function __setFormRules($setRulesFor = ''){
		switch($setRulesFor){				
			case'contact':
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
				$this->form_validation->set_rules('name', 'Name', 'trim|required');
				$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
				$this->form_validation->set_rules('message', 'Message', 'trim|required');
				$this->form_validation->set_rules('recaptcha_response_field', 'lang:recaptcha_field_name', 'required|callback_check_captcha');
			break;
			
			default:
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			break;
		}
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
				
		return $this->form_validation->run();
	}
	

	/**
		* CHECK CAPTCHA IS CORRECT OR NOT
	**/
	function check_captcha($val)
	{
	  	if ($this->recaptcha->check_answer($this->input->ip_address(), $this->input->post('recaptcha_challenge_field'), $val))
		{
	    	return TRUE;
	  	}
		
	    $this->form_validation->set_message('check_captcha', $this->lang->line('recaptcha_incorrect_response'));
	    return FALSE;
	}
}

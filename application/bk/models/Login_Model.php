<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* USE FOR LOGIN FUNCTIONS
**/

class Login_Model extends CI_Model
{
	var $details;
	/**
		* CHECK AUTHENTICATION OF THE USERS
        **/
	function authenticate_user($email, $type = 'email', $password){

	    $this->db->select('*');
        $this->db->from('user');
		
		if($email !='' && $type == 'email'){
        	$this->db->where('email', $email);	
		}else{
			$this->db->where('user_id', $email);
		}	
		
		
        $result = $this->db->get();
		$records = $result->num_rows();
		$recordsData = $result->result();
		
        if($records > 0) {	
						
			if($email !='' && $type=='email' && $password !='')
			{	
			//190215
			//echo decrypt($recordsData[0]->password); die;
			
				if(decrypt($recordsData[0]->password) == trim($password)){
					
					if($recordsData[0]->status == 'Inactive'){
						//echo "<pre>"; print_r($recordsData[0]->user_id); die;
						$msg = 'Your account is inactive. Please verify email after that login.';
						$this->email_verification($recordsData[0]->user_id);
						return $msg;	  
			        }
						
					if($recordsData[0]->account_status == 'Active'){
					   $this->details = $recordsData[0];				
					   $this->set_session($recordsData[0]);
					   //return true;
					   return 1;
					}/*else if($recordsData[0]->status == 'Inactive'){
						$msg = 'Your account is inactive. Please verify email after login.For Verification <a href="'.base_url('user/mailverify/'.$recordsData[0]->user_id).'" >Click here</a>';
						return $msg;	  
					}*/else if($recordsData[0]->account_status == 'Deactivate'){
						$msg = 'Your A/C has been deactivated, Kindly contact to wed-admin <a href="'.site_url('pages/contact_us').'" >Click here</a>';
						return $msg;	
					}
				}else{
					$msg = 'Incorrect password';
					return $msg;
				}
			}else{				
			
				$this->details = $recordsData[0];				
             	$this->set_session($recordsData[0]);
				return 1;				
			}
        }
		$msg = 'This email is not registered with us.';
		return $msg;
    }
	
	/**
		* SET SESSION VALUES
    **/
	
    function set_session($userData = '')
	{
		$this->details = (!isset($this->details) && empty($this->details))? $userData : $this->details;
		$user_data = array('user_id' => $this->details->user_id,
						'name'			=> $this->details->firstName,
						'lastName'			=> $this->details->lastName,
						'phone_number'			=> $this->details->phone_number,
						
						'fullName'		=> $this->details->firstName.' '.$this->details->lastName,
						'email'			=> $this->details->email, 
						'group_id'		=> $this->details->group_id,
						'status'		=> $this->details->status,
						'registerDate' =>$this->details->registerDate,
						'profile_image' => $this->details->profile_image);
		$this->session->set_userdata('logged_in',$user_data);
		$this->session->set_userdata('user_id', $this->details->user_id);
		$this->update_user_login_status($this->details->user_id);
	}

	/**
		* UPDATE LAST LOGIN TIME
    **/
 
	public function update_user_login_status($user_id=''){
		$timestamp = strtotime(date('Y-m-d h:i:s'));
		$dataArray = array('lastLoginDate' => $timestamp);
		if($user_id !=''){	
		$this->db->update('user', $dataArray, array('user_id' => $user_id));
		}
	}
	
	/**
		* USER EMAIL VARIFICATION
    **/
	protected function email_verification($user_id){
		
		$websetting = $this->session->userdata('websetting');		
		$result= $this->Common->select('user',"where user_id='$user_id'");	
		
		$site_name =  $websetting['site_name'];			
		$data['username'] = ucwords($result[0]['firstName'].' '.$result[0]['lastName']);
		$data['email'] = $result[0]['email'];
		$data['code'] = $result[0]['activation_code'];
		$data['siteurl'] = base_url();
		$data['sitename'] = $site_name;
		$data['data']['title']	= "Email verification";
		
		
		$where = "WHERE template_id = 14";
		$data['welcome_user'] = $this->Common->select('email_templates',$where);
		
		$message = $this->parser->parse('mail_template/mail_verify_message', $data,true);
		$toEmail = $result[0]['email'];
		$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
		$subject = 'Email verification';
		$subject = $site_name.' - '.$subject;
		$attachment = array();
		$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
		return true;
	}
	
	
}

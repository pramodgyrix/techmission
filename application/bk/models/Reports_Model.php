<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports_Model extends CI_Model 
{
	public function select_data($tablename, $where = NULL)
	{
		$this->db->select('*');
		$this->db->from($tablename);

		if($where != NULL)
		{
			$this->db->where($where);
		}

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			$result =  $query->result_array();
			$query->free_result();
			return $result;
		}
		else
		{
			return false;
		}
	}

	public function dataExist($table, $where ='')
    {	
		$sql = "SELECT * FROM ".$table;
		if($where!='')
		{
		 $sql .= " ".$where;
		}
		$query = $this->db->query($sql);
		return $query->num_rows();		
	}
	
	public function data_insert($table, $data)
	{
		$query = $this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
}
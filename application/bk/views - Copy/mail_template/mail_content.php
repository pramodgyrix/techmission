
<h2>Don't miss out!</h2>
<p>You've created an account with the email address:<a href="#"> demo.demo@gmail.com</a></p>
<p>Click 'confirm' to verify the email address and unlock your full account.</p>
<p>We'll also import any bookings you've made with that address.</p>
<br>

<p>
    Can't see the button? Use this link: <a href="#">link here</a>
</p>
    

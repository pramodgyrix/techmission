<?php $slider = select('manage_home_slider', "where status='Active'");?>

<!-- start: REVOLUTION SLIDERS -->
<?php if(!empty($slider)): ?>

<section class="fullwidthbanner-container">
  <div class="fullwidthabnner">
    <ul>
      <?php foreach($slider as $slideimg){?>
      <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" > <img src="<?php echo base_url('media/homeslider/'.$slideimg["home_slider_image"]);?>"  style="background-color:rgb(246, 246, 246)" alt="<?=$slideimg['home_slider_title']?>"  data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
        <?php 
				 $data_y=90;
				 $data_start=1500;
				 if(!empty($slideimg['caption_line_1'])){
				 ?>
        <div class="caption lft slide_title slide_item_left"
                data-x="0"
                data-y="<?=$data_y?>"
                data-speed="400"
                data-start="<?=$data_start?>"
                data-easing="easeOutExpo">
          <?=$slideimg['caption_line_1']?>
        </div>
        <?php
					
					$data_y +=50;
				 	$data_start +=500;
					
				 } if(!empty($slideimg['caption_line_2'])){?>
        <div class="caption lft slide_subtitle slide_item_left"
                data-x="0"
                data-y="<?=$data_y?>"
                data-speed="400"
                data-start="<?=$data_start?>"
                data-easing="easeOutExpo">
          <?=$slideimg['caption_line_2']?>
        </div>
        <?php 
					
					$data_y +=50;
				 	$data_start +=500;
					
				} if(!empty($slideimg['caption_line_3'])){			
				?>
        <div class="caption lfr slide_desc slide_item_left"
                data-x="0"
                data-y="<?=$data_y?>"
                data-speed="400"
                data-start="<?=$data_start?>"
                data-easing="easeOutExpo">
          <?=$slideimg['caption_line_3']?>
        </div>
        <?php 
					
					$data_y +=50;
				 	$data_start +=500;
					
				} if($slideimg['button'] == 'Yes' && $slideimg['button_text'] !=''){
							
				?>
        <a class="caption lft btn btn-green slide_btn slide_item_left" href="<?=$slideimg['button_url']?>"
                data-x="0"
                data-y="<?=$data_y?>"
                data-speed="400"
                data-start="<?=$data_start?>"
                data-easing="easeOutExpo">
        <?=$slideimg['button_text']?>
        </a>
        <?php }?>
      </li>
      <?php }?>
    </ul>
  </div>
</section>
<?php endif;?>
<!-- end: REVOLUTION SLIDERS -->
<?php
$websetting = $this->session->userdata('websetting');
?>
<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <section class="page-top">
    <div class="container">
      <div class="col-md-4 col-sm-4">
        <h1><?php echo @$content['content_title']?@$content['content_title']:@$page_title; ?></h1>
      </div>
      <div class="col-md-8 col-sm-8">
        
      </div>
    </div>
  </section>
  <section class="wrapper no-padding">
    <div id="map"></div>
  </section>
  <section class="wrapper padding50">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div id="contactSuccess" class="alert alert-success hidden"> <strong>Success!</strong> Your message has been sent to us. </div>
          <div id="contactError" class="alert alert-error hidden"> <strong>Error!</strong> There was an error sending your message. </div>
           
                 <?php echo validation_errors(); ?>
          <h2>Contact us</h2>
          <form id="contactForm" action="<?=base_url('contact');?>" novalidate method="post">
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                  <label> Your name <span class="symbol required"></span> </label>
                  <input type="text" id="name" name="name" class="form-control" maxlength="40" data-msg-required="Please enter your name." value="<?=set_value('name');?>">
                </div>
                <div class="col-md-6">
                  <label> Your email address <span class="symbol required"></span> </label>
                  <input type="email" id="email" name="email" class="form-control" maxlength="40" data-msg-email="Please enter a valid email address." data-msg-required="Please enter your email address." value="<?=set_value('email');?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-12">
                  <label> Mobile Number </label>
                  <input type="text" id="mobile_number" name="mobile_number" class="form-control" maxlength="11" value="<?=set_value('mobile_number');?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-12">
                  <label> Subject <span class="symbol required"></span> </label>
                  <input type="text" id="subject" name="subject" class="form-control" value="<?=set_value('subject');?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-12">
                  <label> Message <span class="symbol required"></span> </label>
                  <textarea id="message" name="message" class="form-control" rows="10" data-msg-required="Please enter your message." maxlength="5000"><?=set_value('message');?></textarea>
                </div>
              </div>
            </div>
            <?php
            echo $recaptcha;
			?>
            <div class="row">
              <div class="col-md-12">
                <input type="submit" data-loading-text="Loading..." class="btn btn-main-color" value="Send Message">
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-6">
        
         <?php
		 
		 
		 if(isset($content)){
			preg_match_all('/{(.*?)}/', $content['content'], $matches);
			
			 if(isset($matches[1]) && !empty($matches[1])){
				 $slider = '';
				 foreach($matches[1] as $shortcode){
					 $slider = get_slider("{".$shortcode."}");
					 
					 $data[$shortcode] = $slider;
				 }
				 
				 $data['content'] = $content['content'];
				 echo $this->parser->parse('parse_content', $data, TRUE);
			 }else{
			
			 	echo $content['content'];
			 }
		 }
		 ?>
        
          <!--<h4>Get in touch</h4>
          <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. </p>
          <hr>
          <h4>The Office</h4>
          <ul class="list-unstyled">
            <li> <i class="icon icon-map-marker"></i><strong>Address:</strong> 1234 Street Name, City Name, United States </li>
            <li> <i class="icon icon-phone"></i><strong>Phone:</strong> (123) 456-7890 </li>
            <li> <i class="icon icon-envelope"></i><strong>Email:</strong> <a href="mailto:mail@example.com"> mail@example.com </a> </li>
          </ul>
          <hr class="right">
          <h4>Business Hours</h4>
          <ul class="list-unstyled">
            <li> <i class="icon icon-time"></i> Monday - Friday 9am to 5pm </li>
            <li> <i class="icon icon-time"></i> Saturday - 9am to 2pm </li>
            <li> <i class="icon icon-time"></i> Sunday - Closed </li>
          </ul>-->
        </div>
      </div>
    </div>
  </section>
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); 

   /*     $latlang=address_to_latlng_detail($websetting['site_address']);
		
		$latitude=$latlang['latitude'][0];
        $longitude=$latlang['longitude'][0];*/
		
		$latitude=22.75;
        $longitude= 75.5;

?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDmS8jRSpSNi4itEnZXL4YpqImWMt-028A&sensor=true"></script> 
<script src="<?=base_url('assets/plugins/gmaps/gmaps.js');?>"></script> 
<script>
			jQuery(document).ready(function() {
				
				ContactFormValidator.init();
				
				map = new GMaps({
					div: '#map',
					lat: <?=$latitude;?>,
					lng: <?=$longitude;?>
				});
				
				map.addMarker({
					lat: <?=$latitude;?>,
					lng: <?=$longitude;?>,
					title: 'Address',
					infoWindow: {
						content: '<p><?=$websetting['site_address'];?></p>'
					}
				});
			});
		</script> 
<script src="<?=base_url('assets/plugins/jquery-validation/dist/jquery.validate.min.js');?>"></script>

<script src="<?=base_url('assets/js/custom/contact_us.js');?>"></script> 
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

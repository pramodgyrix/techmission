<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>        
<!-- end: HEADER -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container">

			<!-- start: REVOLUTION SLIDERS -->
			   <?php $this->load->view('common/revolution_sliders'); ?>  
			<!-- end: REVOLUTION SLIDERS -->
			
			<section class="wrapper wrapper-grey padding50">
				<!-- start: WORKS CONTAINER -->
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="center">Take a look at some of our work</h2>
							<hr>
							<p class="center">
								Nulla consequat massa quis enim. <strong>Donec pede justo</strong> fringilla vel, aliquet nec, vulputate eget, arcu.
								<br>
								In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.
							</p>
							<div class="grid-container animate-group" data-animation-interval="100">
								<div class="col-md-3 col-sm-6">
									<div class="grid-item animate">
										<a href="#">
											<div class="grid-image">
												<img src="<?php echo base_url();?>assets/images/image01.jpg" class="img-responsive"/>
												<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
											</div>
											<div class="grid-content">
												4 Columns Portfolio
											</div>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="grid-item animate">
										<a href="#">
											<div class="grid-image">
												<img src="<?php echo base_url();?>assets/images/image02.jpg" class="img-responsive"/>
												<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
											</div>
											<div class="grid-content">
												3 Columns Portfolio
											</div>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="grid-item animate">
										<a href="#">
											<div class="grid-image">
												<img src="<?php echo base_url();?>assets/images/image03.jpg" class="img-responsive"/>
												<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
											</div>
											<div class="grid-content">
												2 Columns Portfolio
											</div>
										</a>
									</div>
								</div>
								<div class="col-md-3 col-sm-6">
									<div class="grid-item animate">
										<a href="#">
											<div class="grid-image">
												<img src="<?php echo base_url();?>assets/images/image04.jpg" class="img-responsive"/>
												<span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
											</div>
											<div class="grid-content">
												Single Project
											</div>
										</a>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<!-- end: WORKS CONTAINER -->
			</section>
			<section class="wrapper padding50">
				<!-- start: ABOUT US CONTAINER -->
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<h2 style="text-align: right;">About Us</h2>
							<hr class="fade-left">
							<p style="text-align: right;">
								Lorem ipsum dolor sit amet, consectetuer <strong>adipiscing elit</strong>. Aenean commodo ligula eget dolor. Aenean massa.
							</p>
							<p style="text-align: right;">
								Nulla consequat massa quis enim.
								<br>
								Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
								<br>
								In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.
								<br>
								Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.
								<br>
								Aenean <strong style="text-align: right;">vulputate</strong> eleifend tellus.
							</p>
							<hr class="fade-left">
							<a href="#" class="btn btn-default pull-right"><i class="fa fa-info"></i> Learn more...</a>
						</div>
						<div class="col-sm-6">
							<ul class="icon-list animate-group">
								<li>
									<div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300"}'></div>
									<i class="clip-stack-empty circle-icon circle-teal animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
									<div class="icon-list-content">
										<h4>HTML5 / CSS3 / JS</h4>
										<p>
											Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in.
										</p>
									</div>
								</li>
								<li>
									<div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300", "delay": "300"}'></div>
									<i class="clip-paperplane circle-icon circle-green animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
									<div class="icon-list-content">
										<h4>Awesome Sliders</h4>
										<p>
											Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in.
										</p>
									</div>
								</li>
								<li>
									<div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300", "delay": "300"}'></div>
									<i class="clip-droplet circle-icon circle-bricky animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
									<div class="icon-list-content">
										<h4>Clean Design</h4>
										<p>
											Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in.
										</p>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: ABOUT US CONTAINER -->
			</section>
			
			<section class="wrapper padding50">
				<!-- start: CLIENTS CONTAINER -->
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="center">Our Happy Clients</h2>
							<h4 class="center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>
						</div>
						<div class="flexslider col-md-12 animate-group" data-plugin-options='{"directionNav":false, "animation":"slide", "slideshow": false}' data-animation-interval="100">
							<ul class="slides">
								<li>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-1.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-2.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-3.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-4.png" alt="">
									</div>
								</li>
								<li>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/logo-4.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/logo-2.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-5.png" alt="">
									</div>
									<div class="col-md-3 col-sm-3 col-xs-6">
										<img class="img-responsive animate" data-animation-options='{"animation":"fadeIn", "duration":"300"}' src="<?php echo base_url();?>assets/images/logo-6.png" alt="">
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: CLIENTS CONTAINER -->
			</section>
			<section class="wrapper wrapper-grey">
			
			<section class="wrapper" style="min-height:400px; background-image: url('<?php echo base_url();?>assets/images/photodune-4043508-3d-modern-office-room-l.jpg')" data-stellar-background-ratio="0.8" data-stellar-vertical-offset="-750">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2 class="center white"> WE WORK HARD FOR YOU</h2>
							<p class="center white">
								Try new wxperience with Clip-One
							</p>
						</div>
					</div>
					<div class="row animate-group">
						<div class="col-sm-4">
							<div class="icon-box animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'>
								<i class="icon-box-icon fa fa-pencil"></i>
								<h3 class="icon-box-title">Design</h3>
								<div class="icon-box-content">
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
										<br>
										<a href="#">
											Learn more
										</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="icon-box animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'>
								<i class="icon-box-icon fa fa-code"></i>
								<h3 class="icon-box-title">Develop</h3>
								<div class="icon-box-content">
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
										<br>
										<a href="#">
											Learn more
										</a>
									</p>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="icon-box animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'>
								<i class="icon-box-icon fa fa-bug"></i>
								<h3 class="icon-box-title">Support</h3>
								<div class="icon-box-content">
									<p>
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
										<br>
										<a href="#">
											Learn more
										</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="wrapper padding50">
				<!-- start: BLOG POSTS AND COMMENTS CONTAINER -->
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Latest Blog Posts</h2>
							<div class="row recent-posts">
								<div class="flexslider" data-plugin-options='{"directionNav":false, "animation":"slide", "slideshow": false}'>
									<ul class="slides">
										<li>
											<div class="col-md-12">
												<article>
													<span><i class="fa fa-calendar"></i> January 11, 2014 </span>
													<h4>
													<a href="blog_post.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</a></h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.
													</p>
													<div class="post-meta">
														<span><i class="fa fa-user"></i> By
															<a href="#">
																Peter Clark
															</a> </span>
														<span><i class="fa fa-tag"></i>
															<a href="#">
																Design
															</a>,
															<a href="#">
																Lifestyle
															</a> </span>
														<span><i class="fa fa-comments"></i>
															<a href="#">
																36 Comments
															</a></span>
													</div>
													<a href="blog_post.html" class="btn btn-xs btn-main-color">
														Read more...
													</a>
												</article>
											</div>
										</li>
										<li>
											<div class="col-md-12">
												<article>
													<span><i class="fa fa-calendar"></i> January 11, 2014 </span>
													<h4>
													<a href="blog_post.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</a></h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.
													</p>
													<div class="post-meta">
														<span><i class="fa fa-user"></i> By
															<a href="#">
																Peter Clark
															</a> </span>
														<span><i class="fa fa-tag"></i>
															<a href="#">
																Design
															</a>,
															<a href="#">
																Lifestyle
															</a> </span>
														<span><i class="fa fa-comments"></i>
															<a href="#">
																36 Comments
															</a></span>
													</div>
													<a href="blog_post.html" class="btn btn-xs btn-primary">
														Read more...
													</a>
												</article>
											</div>
										</li>
										<li>
											<div class="col-md-12">
												<article>
													<span><i class="fa fa-calendar"></i> January 11, 2014 </span>
													<h4>
													<a href="blog_post.html">
														Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</a></h4>
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero.
													</p>
													<div class="post-meta">
														<span><i class="fa fa-user"></i> By
															<a href="#">
																Peter Clark
															</a> </span>
														<span><i class="fa fa-tag"></i>
															<a href="#">
																Design
															</a>,
															<a href="#">
																Lifestyle
															</a> </span>
														<span><i class="fa fa-comments"></i>
															<a href="#">
																36 Comments
															</a></span>
													</div>
													<a href="blog_post.html" class="btn btn-xs btn-primary">
														Read more...
													</a>
												</article>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<h2>What Client’s Say</h2>
							<div class="row">
								<div class="flexslider">
									<ul class="slides">
										<li>
											<div class="col-md-12">
												<blockquote class="testimonial">
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.
													</p>
												</blockquote>
												<div class="testimonial-arrow-down"></div>
												<div class="testimonial-author">
													<div class="img-thumbnail img-thumbnail-small">
														<img src="<?php echo base_url();?>assets/images/avatar-1.jpg" alt="">
													</div>
													<p>
														<strong>Peter Clark</strong><span>UI Designer - Cliptheme</span>
													</p>
												</div>
											</div>
										</li>
										<li>
											<div class="col-md-12">
												<blockquote class="testimonial">
													<p>
														Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
													</p>
												</blockquote>
												<div class="testimonial-arrow-down"></div>
												<div class="testimonial-author">
													<div class="img-thumbnail img-thumbnail-small">
														<img src="<?php echo base_url();?>assets/images/avatar-2.jpg" alt="">
													</div>
													<p>
														<strong>Nicole Bell</strong><span>Content Designer - Cliptheme</span>
													</p>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end: BLOG POSTS AND COMMENTS CONTAINER -->
			</section>
		</div>
		<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>



			
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>
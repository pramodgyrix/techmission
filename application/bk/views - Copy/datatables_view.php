<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); 


?>
<?php 				  
  $page_num = (int)$this->uri->segment(3);
  if($page_num==0) $page_num=1;
  $order_seg = $this->uri->segment(5,"asc"); 
  if($order_seg == "asc") $order = "desc"; else $order = "asc";
  ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <section class="wrapper wrapper-grey padding50"> 
    <!-- start: GENERIC CONTENT CONTAINER -->
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <form action="<?php echo base_url('Datatable'); ?>" method="post" >
            <div class="col-md-6">
              <input class="form-control" id="s" name="s" placeholder="Search for Name..." type="text" value="<?php echo  $this->session->userdata('search'); ?>" />
            </div>
            <div class="col-md-1"> 
              <!--  <input id="btn_search" name="btn_search" type="submit" class="btn btn-green" value="Search" />-->
              
              <button type="submit" value="search" class="btn btn-info" id="btn_search" name="btn_search"> <span class="glyphicon glyphicon-search"></span> Search </button>
            </div>
          </form>
          <form action="<?php echo base_url('Datatable'); ?>" method="post" >
            <button id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="reset" style="margin-left: 14px;" />
            <span class="glyphicon glyphicon-refresh"></span> Reset
            </button>
            <input type="hidden" name="reset_search" value="1">
          </form>
        </div>
        <br/>
        <br/>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <table id="myTable" class="table table-striped table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th >
                <a class="" href="<?php echo base_url('Datatable/index/'.$offset.'/name/'.$new_order); ?>">Contact Name
                <img class="<?php if($order_column == 'name') { echo $order_class; }else{ echo 'sorting'; } ?>"  >
               
                </a>
              </th>
                <th><a href="<?php echo base_url('Datatable/index/'.$offset.'/contact/'.$new_order); ?>">Mobile Contact 
                <img class="<?php if($order_column == 'contact') { echo $order_class; }else{ echo 'sorting'; } ?>"  ></a></th>
                
                <th><a href="<?php echo base_url('Datatable/index/'.$offset.'/email/'.$new_order); ?>">Contact Email
                <img class="<?php if($order_column == 'email') { echo $order_class; }else{ echo 'sorting'; } ?>"  ></a></th>
                <th>
                <a  href="<?php echo base_url('Datatable/index/'.$offset.'/branch/'.$new_order); ?>">Branch
                <img class="<?php if($order_column == 'branch') { echo $order_class; }else{ echo 'sorting'; } ?>" >
                </a></th>
                <th><a  href="<?php echo base_url('Datatable/index/'.$offset.'/industry/'.$new_order); ?>">Industry
                <img class="<?php if($order_column == 'industry') { echo $order_class; }else{ echo 'sorting'; } ?>">
                </a></th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($result)) { 
						  foreach($result as $val){
							  //dd($val);
							 
						 ?>
              <tr>
                <td><?php echo $val['name']; ?></td>
                <td><?php echo $val['contact_no']; ?></td>
                <td><?php echo $val['email']; ?></td>
                <td><?php echo $val['branch_name']; ?></td>
                <td><?php echo $val['industry_name']; ?></td>
              </tr>
              <?php } 
						
						}?>
            </tbody>
          </table>
          <div class="row">
            <div id="ajax_paging" class="col-md-12"><?php echo $links; ?></div>
          </div>
        </div>
      </div>
    </div>
    <!-- end: GENERIC CONTENT CONTAINER --> 
  </section>
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>

<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>
<link  rel="stylesheet" href="<?php echo base_url('assets/css/pagination.css'); ?>" >
<link  rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" >
<!--<script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> 
<script src=" https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"></script> --> 
<script>
$(document).ready(function(){
   // $('#myTable').DataTable();
});
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR HOME PAGE
**/
class Dashboard extends MY_Controller {
	
	
	var $userID;
	function __construct()
    {
		parent::__construct();
		/* Your own constructor code */
		$this->userID = check_user_login();
    }
    
    /**
		* SHOWING CALENDAR ON DASHBOARD
    **/
	public function index()
	{		
		$data['stylesheet'] = array('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css');
		$data['page_title'] = 'Dashboard';
		$this->load->view('dashboard', $data);
	}	
}

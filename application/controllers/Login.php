<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR LOGIN PAGE 
**/ 

class Login extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie'));
		$this->load->library(array('parser','form_validation'));	
	}

	/**
	 	* CALL FOR USER LOGIN
	**/
	
	public function index(){
		
		$data['page_title']='Login';
		$this->load->model('Login_Model');
		if($_POST){			
			$post = $this->input->post();
			$email = $post['email'];
		 	$password  = $post['password'];
			$remember = @$post['remember'];
			
	     	$responce = $this->Login_Model->authenticate_user($email, 'email' , $password);
			
			if($responce == 1) {    
    		  // If the user is valid, redirect to the main view
               
			   $url = base_url();
				   if($remember){    
					 $cokiesYear = time() + 60*60*24*30;
					  $cookies = array('name'   => 'userID',
									   'value'  => $this->session->userdata('user_id'),
									   'expire'   => $cokiesYear);
					 $this->input->set_cookie($cookies);
					 
					}else{
						   delete_cookie("userID");
				         }
				   
			    $responce_array[] = array(
				    'message' => 'you have login successfully', 
			        'message_div_id' => 'sign_in_message', 
			        'message_class' => 'alert alert-success',
			        'url_full' => 1,
			        'redirect' =>$url);
					
					if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
						echo json_encode($responce_array); die;
						}else{
						//redirect($url);	
						redirect('dashboard');	
						} 
			    	
			    }else {
			    // Otherwise show the login screen with an error message. 
			    //$data['msg']['warning'] = 'Incorrect Email-ID and Password';
			    //login_attempt_count($email);
			    $data['msg']['warning'] = $responce;
			    $responce_array[] = array('message' => $responce, 
					'message_div_id' => 'sign_in_message', 
					'message_class' => 'alert alert-danger');
					
					if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
					echo json_encode($responce_array); die;
					}else{
					$data['msg'] = $responce;
				    $data['class'] = 'alert alert-danger';	
					
					
					}
			}
		}	
		
		$this->load->view('login',$data);
	}
	
	/**
	 	* CALL FOR USER REGISTRATION
	**/
	public function Signup(){

		$websetting = $this->session->userdata('websetting');
		if($_POST)
		{
			$post = $this->input->post();
			$firstName = $post['first_name'];
			$email = $post['email'];
			$group_id = 1;
			$password = encrypt($post['password']);
			$activation_code = mt_rand();
			
			// 1) unique email check
			/*$where = " where email = '$email'"; Shifted to helper
			$result = $this->Common->select('user',$where);*/
			$result = check_user_from_email($email);
			
			
			
			if(empty($result)){
				
				$data = array('firstName' => $post['first_name'],
							  'email' => $email,
							  'password' => $password,
							  'phone_number'=>$post['phone_number'],
							  'group_id' => $group_id,
							  'status' => 'Inactive',
							  'activation_code' => $activation_code);
		 
				$result = $this->Common->data_insert('user', $data);
			 	
				if($result){
                    $site_name =  $websetting['site_name'];
					$data['username'] = ucwords($firstName);
					$data['siteurl'] = base_url();
					$data['sitename'] = $site_name;
					$data['email']	= $email;
					$data['password'] = $post['password'];
					$data['code'] = $activation_code;
					$data['data']['title']	= "Successfully Sign Up";
					
					$where = "WHERE template_id = 5";
					$data['welcome_user'] = $this->Common->select('email_templates',$where);
					$message = $this->parser->parse('mail_template/sign_up', $data, TRUE);
					$toEmail = $email;
					$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
					$subject = 'Thank you for registering';
					$subject = $site_name.' - '.$subject;
					$attachment = array();
					$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
					
					// An email has been sent to your mail address. please check your mail and use the enclosed link to finish registration
					$responce_array[] = array('message' => 'Thank you for registeration. An email has been sent to your mail address. please check your mail and use the enclosed link to finish registration.', 
										  'message_div_id' => 'signup_message', 
										  'message_class' => 'alert alert-success',
										  'form_id' => 'form-register',
										  'form_reset' => 1,
										  'hide_div' => 'box-register',
										  'show_div' => 'box-login'
										  );	
										  
										 				  				  
										  
				}else{
					$responce_array[] = array('message' => 'Email confirmation message sending failed to your email, please enter email id', 
										  'message_div_id' => 'signup_message', 
										  'message_class' => 'alert alert-success',
										  'form_id' => 'form-register',
										  'form_reset' => 1);				  
					}
			}else{
				    $msg['type'] = 'alert alert-danger';
					$msg['reset'] = 0;
					$msg['redirect'] = '';
					$msg['form_id'] = 0;
					$msg['hide_div'] = 0;
					$msg['msg'] = "This e-mail address already exists.";
					$responce_array[] = array('message' => 'This e-mail address already exists', 
										  'message_div_id' => 'signup_message', 
										  'message_class' => 'alert alert-danger',
										  'form_id' => 'form-register',
										  'form_reset' => 0);
				}
			
			echo json_encode($responce_array); die;
		}else{
			$data['page_title'] = 'Login';
			$this->load->view('login',$data);
		}
	}
	

	/**
	 	* CALL FOR VARIFICATION IF USER REGISTERED
	**/
	public function verification(){
		
		$verifier = @$_POST['verifier'] ? @$_POST['verifier'] : @$_GET['verifier'];
		if($verifier){
		
		        $where = " where activation_code = '$verifier'"; 
				$result = $this->Common->select('user',$where);
				if(!empty($result)){
					
					$ver_user_id = $result[0]['user_id'];
					$where = array('user_id' => $ver_user_id);	
					$update_data = array('activation_code' => '',
					                     'status' => 'Active');
				 					 
				    $this->Common->update('user',$update_data,$where);
                    
					$websetting = $this->session->userdata('websetting');
                    $site_name =  $websetting['site_name'];
                    
					$email = $result[0]['email'];
					$data['email'] = $email;
					$data['username'] = ucwords($result[0]['firstName'].' '.$result[0]['lastName']);
					$data['sitename'] = $site_name;
					$data['siteurl'] = base_url();
					$data['data']['title'] = 'Email Verification';
					
					$where = "WHERE template_id = 6";
					$data['welcome_user'] = $this->Common->select('email_templates',$where);
					$message = $this->parser->parse('mail_template/email_verification', $data, TRUE);
					$toEmail = $email;
					$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
					$subject = $site_name.' Thank you for email verification';
					$attachment = array();
					$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
					
					$user_id = $this->session->userdata('user_id');
					if(!empty($user_id)){
					$data['msg'] = 'Well done! You have successfully verified your email address';	
					}else{
					$data['msg'] = 'Well done! You have successfully verified your email address Please login <a href="'.base_url('login').'">Sign In</a>.';
					}
				    $data['class'] = 'alert alert-success';
				}else{
					$data['msg'] = 'Invalid verification code.';
				    $data['class'] = 'alert alert-danger';
					}
	      	}else{
				$data['msg'] = 'Please Insert verification code.';
				$data['class'] = 'alert alert-danger';
			}
			$this->load->view('login',$data);
			
	}
	
	/**
	 	* CALL IF USER FORGOT THE PASSWORD OF THEIR LOGIN ACCOUNT 
	**/
	public function forgotpassword(){
		$userData = $this->input->post();
		$submittype = @$_POST['submittype'];
		$uCheck = $this->db->get_where('user', array('email' => $userData['email']))->result_array();
		if(!empty($uCheck)){            
			$websetting = $this->session->userdata('websetting');
            $site_name =  $websetting['site_name'];
		    $Password = mt_rand(100000, 999999);
			$this->db->update('user', array('password' => encrypt($Password)),array('user_id' => $uCheck[0]['user_id']));           
			
			// Email Configuration
			
		    $subject=$site_name.' Forgot Password';
				$data['username'] = ucwords($uCheck[0]['firstName'].' '.$uCheck[0]['lastName']);
				$data['siteurl'] = base_url();
				$data['sitename'] =$site_name;
				$data['email']	= $uCheck[0]['email'];
				$data['PASSWORD']	= $Password;
			
                $data['data']	= '';
			
			$data['firstname'] = $uCheck[0]['firstName'];
			$data['profile_image'] = $uCheck[0]['profile_image'];
			
			$username=ucwords($uCheck[0]['firstName'].' '.$uCheck[0]['lastName']);
            $seousername = str_replace('&nbsp;', '-', $username);
            $data['recieverseo'] = seo_friendly_urls($seousername,'',$uCheck[0]['user_id']);

			$where = "WHERE template_id = 7";
			$data['welcome_user'] = $this->Common->select('email_templates',$where);
			$message = $this->parser->parse('mail_template/forgotPassword_message', $data, TRUE);
			$toEmail = $uCheck[0]['email'];
			$fromEmail = array('email' => config_item('no_reply'),'name' => config_item('site_name'));
			$subject = config_item('site_name').' Forgot password';
			$attachment = array();
			$result = send_user_email_ci($toEmail, $fromEmail, $subject, $message, $attachment);
			  
			if(!empty($submittype)){
			  	$responce_array[] = array('message' => 'Thank you ! your request has been processed a new password is emailed to you.', 
			              
						   'message_div_id' => 'forgetpassword_verification_message', 
							
							'message_class' => 'alert alert-success',
							'hide_div' => 'box-verification',
							'tab_id' => 'tab_signin_tabs'
							);	
			}else{
				$responce_array[] = array('message' => 'Thank you ! your request has been processed a password is emailed to you.', 
										  'message_div_id' => 'forgot_message',
										  'message_class' => 'alert alert-success',
										  'hide_div' => 'box-forgot',
										  'show_div' => 'box-login',
										  'form_reset' => 1,
										  'form_id' => 'form-forgot'
										  );
			}
			echo json_encode($responce_array); die;
		}else{		     
			$responce_array[] = array('message' => 'No account found with that email address.', 
										  'message_div_id' => 'forgot_message', 
										  'message_class' => 'alert alert-danger',
										   'hide_div' => 'box-login',
										  'show_div' => 'box-forgot'
										  );
			echo json_encode($responce_array); die;
										  
		}		
	}
	
	/**
	 	* CALL FOR USER LOGOUT 
	 	* SESSION WILL DISTROY
	 	* DELETE USER COOKIE
	 	* REDIRECT TO LOGIN PAGE AFTER LOGOUT
	**/
	public function logout(){
		$this->session->sess_destroy();
      	delete_cookie("userID");
		redirect(base_url('login'));		
	}
	
	
}

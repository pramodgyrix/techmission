<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR HOME PAGE
**/
class Home extends MY_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->helper('download');
	}
	
	public function index()
	{
		$this->load->view('home');
	}
	
	
	
	
	public function create()
	{  
	  //  check_function_permission($this->permision,$this->className,$this->methodName);
	 	$this->load->view('home');
	}
	
	public function read()
	{
		//check_function_permission($this->permision,$this->className,$this->methodName);
		$this->load->view('home');
	}
	
	public function delete()
	{
		
		//check_function_permission($this->permision,$this->className,$this->methodName);
		$this->load->view('home');
	   
	}
	
	
	
	public function update()
	{
		$this->load->view('home');
	}

	
}

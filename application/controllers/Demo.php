<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends MY_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->helper('download');
	}
	
	public function index()
	{
		$data['result'] = $this->Common->select('libraries_demo');
		$this->load->view('libraries_demo',$data);
	}
	
	
	public function jquery_validation(){
		$this->load->view('demo/jquery_validation/index');
	}
	
	

	
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS	
	* MANAGE ALL TYPE EMPLOYEE
**/

class Employee extends MY_Controller { 
	var $userID;

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','encryption'));
		$this->userID = check_user_login();
		//$this->output->enable_profiler(TRUE);
	}

/**
    * [ employee grid view display ]
    * @param  {[type]} $post [string search form parameter ]
    * @return {[type]}       [view page]
*/ 	
	public function index(){
		$this->load->model(array('Employee_Model'));
		$data['stylesheet'] = array("assets/css/pagination.css");
		
		$limit = 10;
		$display = 1;
		$display_from = $limit;
		$offset = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
		$data['offset'] = $offset;
		if(!empty($offset)){
			if($offset != 1){
			  $display =  (($offset-1) * $limit);	
			}else{
				$display = $limit;
			}
		 	$offset  = (($offset-1) * $limit); 
			$display_from = $offset + $limit ;
		}
		
		 $order_column_name = ($this->uri->segment(4))? $this->uri->segment(4) : NULL;
		 $order_type = ($this->uri->segment(5))? $this->uri->segment(5) : ''; 
		  $data['order_column'] =  trim($order_column_name);
		  $data['order_class'] = 'sorting';
		  if($order_type == 'asc'){
		  $data['order_class'] = 'sorting_asc';
		  }else if($order_type == 'desc'){
		  $data['order_class'] = 'sorting_desc';
		  }
		 
		$order_column = $this->column_name_emp($order_column_name); 
	    $new_order = ($order_type == 'asc' ? 'desc' : 'asc');
		$data['new_order'] = $new_order;
		if(@$_POST['btn_search'] == 'search'){
			$this->session->unset_userdata('search');
		}
		$post = $this->input->post('s',true);
		$post = trim($post);
		$data['s'] = $post;
		$reset_search = $this->input->post('reset_search',true);
		if($reset_search == 1) $this->session->unset_userdata('search');
		$where = '';
		$result = $this->Employee_Model->select_data($where,$limit, $offset,$post,$order_column, $order_type);
		$data['result_count'] = $this->Employee_Model->select_data_count($where,$post);
		if($new_order == 'asc'){
		$result = array_msort($result, array($order_column=>SORT_ASC));	
		}else{
		$result = array_msort($result, array($order_column=>SORT_DESC));
		}
		$data['result'] = $result;
		$config["base_url"] = base_url() . "employee/index";
		$config["total_rows"] =  $data['result_count'];
		$config["per_page"] = $limit;
		$config["uri_segment"] = 3;
		$config['num_links'] = 1; 
		$config['display_pages'] = TRUE;  
		$config = array_merge($config, $this->config->item('pagingConfig'));
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		if(@$_POST['btn_search'] == 'search'){
			$this->session->unset_userdata('search');
		}
		$this->load->view('user/employee', $data);	
	}

/** end */	
/**
    * [ short column name ]
    * @param  {[type]} $name [string column name]
    * @return {[type]}       [ailish name]
*/ 
	private function column_name_emp($name){
		 switch($name)
       	 {
		  case 'name':
		  return 'firstName';
		  break; 
		
	      case 'groupName':
		  return 'groupName';
		  break; 
		  
		  case 'email':
		  return 'email';
		  break; 
		  
		  case 'status':
		  return 'status';
		  break; 
		  
		 default :
	     return 'user_id';
	   }
	}

/** end */
/**
    * [ employee add page ]
    * @param  {[type]} $post [form post varameter ]
    * @return {[type]}       [view page]
*/ 	
	public function Add(){
	   
	   if($this->input->post()){
		   $formValidation = $this->__setFormRules('employeeAdd');
		   if($formValidation){  
				$post = $this->input->post();
				$fullName = $post['fullName'];
				$email = $post['email'];
				$groupName = $post['userGroup'];
				$password = encrypt($post['password']);
				
				$data = array('firstName' => $fullName,
				              'email' => $email,
							  'role_id' => $groupName,
							  'password' => $password,
							  'updatedDate' => time()
				             );
				
			if($this->Common->data_insert('user',$data)){
				$this->messageci->set('Employee account create successfully!', 'success');
				redirect('employee');
			}else{
			    $this->messageci->set('Some errors occurred, Please try again!', 'error');
			}
		   }
				
		}
		$data['scriptsrc'] = array('assets/plugins/jquery-validation/dist/jquery.validate.min.js');
		$data['page_title'] = 'Employee Add';
		$this->load->view('user/employeeAdd', $data);
	}
/** end */


/**
    * [ employee edit page ]
    * @param  {[type]} $post [form post varameter ]
    * @param  {[type]} $id [user id ]
    * @return {[type]}       [view page]
*/ 	
	public function Edit($id){
    
		check_function_permission($this->permision,$this->className,'update');
		
		if(empty($id)){
		$this->messageci->set('Some errors occurred, Please try again!', 'error');
		redirect('Employee');
		}
		$data['id'] = $id;
		$id = safe_b64decode($id);
		$where = "where user_id = $id";
		$data['result'] = $this->Common->select('user',$where);
		
	   
	   if($this->input->post()){
		   $formValidation = $this->__setFormRules('employeeEdit');
		   if($formValidation){  
				$post = $this->input->post();
				$fullName = $post['fullName'];
				$groupName = $post['userGroup'];
				$password = encrypt($post['password']);
				$userStatus = $post['userStatus'];
				
				$data = array('firstName' => $fullName,
				              'role_id' => $groupName,
							  'password' => $password,
							  'status' => $userStatus,
							  'updatedDate' => time()
				             );
							 
							 
			
			$where = array('user_id' => $id);
			if($this->Common->update('user',$data,$where)){
				$this->messageci->set('Employee account update successfully!', 'success');
				redirect('employee');
			}else{
			    $this->messageci->set('Some errors occurred, Please try again!', 'error');
			}
		   }
				
		}
		$data['scriptsrc'] = array('assets/plugins/jquery-validation/dist/jquery.validate.min.js');
		$data['page_title'] = 'Employee Edit';
		$this->load->view('user/employeeEdit', $data);
	}
/** end */

/**
	* [ unique email check]
    * @param  {[type]} $email [email id]
    * @return {[type]}       [message condition according]
**/
 function uniqueEmailCheck($email){
    	    $sql = select('user');
            $uemail = array();
             if($sql != ''){
                  foreach($sql as $val){
                      array_push($uemail,$val['email']);
                  }
             }else{
             	return true;
             }
           
            if (in_array($email, $uemail)) {
                  $this->form_validation->set_message('email_check', "The $email already exists.");
                  return FALSE;
            }else{
             	return true;
             }
         
    }

/* end */
/**
	* [ validation function SET RULE FOR USER INPUT ON THE SUBMIT FORM]
    * @param  {[type]} $post [form post varameter ]
    * @return {[type]}       [view page]
**/
	function __setFormRules($formName='')
	{
		switch($formName){	
		   case 'employeeAdd':
				$this->form_validation->set_rules('fullName', 'Full Name', 'trim|required|min_length[2]');
				//$this->form_validation->set_rules('email', 'Email Id', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('email', 'Email Id', 'callback_uniqueEmailCheck');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('userGroup', 'User Group', 'trim|required');
				$this->form_validation->set_message('is_unique', 'This %s already Exists!');
			    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			break;	
			case 'employeeAdd':
				$this->form_validation->set_rules('fullName', 'Full Name', 'trim|required|min_length[2]');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
				$this->form_validation->set_rules('userGroup', 'User Group', 'trim|required');
		    break;
		 }

			$this->form_validation->set_rules('userImage', 'User Image', 'trim');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close">×</button><i class="fa fa-times-circle"></i> ', '</div>');
			return $this->form_validation->run();
	}
	/* end */
		
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
	* GYRIX TECHNOLABS
	* LOAD COMMON FUNCTIONS FROM MY_Controller
	* USE FOR SITE MAINTENANCE 
**/ 
class SiteMaintenance extends CI_Controller {
	public function index()
	{
		$this->db->select('site_maintenance_content,maintenance_time,status');
		$this->db->from('site_maintenance'); 
		$this->db->where('site_maintenance_id', '1'); 
		$data['site_maintenance_data'] = $this->db->get()->result_array();	 
		
		$mdate = $data['site_maintenance_data'][0]['maintenance_time'];
		$mdate = strtotime($mdate);
		$cdate = time();
            if($mdate < $cdate){
		    $udate = array('status' =>  'Inactive');
			$where = array('site_maintenance_id' => 1);
			$this->Common->update('site_maintenance',$udate);
			  redirect('Home');	
			}
		//dd($data['site_maintenance_data'][0]['maintenance_time']);
		$this->load->view('SiteMaintenance',$data);
	}
}

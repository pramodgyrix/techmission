<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
	* GYRIX TECHNOLABS	
	* MANAGE ALL TYPE CONTACT
**/

class Contact extends MY_Controller { 
	var $userID;

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('pagination','encryption'));
		$this->userID = check_user_login();
		//$this->output->enable_profiler(TRUE);
	}

/**
    * [ employee grid view display ]
    * @param  {[type]} $post [string search form parameter ]
    * @return {[type]}       [view page]
*/ 	
	public function index(){
		$this->load->model(array('Employee_Model'));
		$data['stylesheet'] = array("assets/css/pagination.css");
		
		$data['scriptsrc'] = array('assets/plugins/jquery-validation/dist/jquery.validate.min.js','assets/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
			'assets/js/form-wizard.js');
		$this->load->view('contact/contact_view.php', $data);	
	}

/** end */	


		
}
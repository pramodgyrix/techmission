<?php 
/**
Site Maintenance class *
Manages active users *
@package CodeIgniter
@subpackage Libraries
@author Shobhit Yadav
*/ 

class SiteMaintenance { 
	public $CI;
	function SiteMaintenance(){
		$CI =& get_instance();
		$this->CI = $CI;
		$this->CI->db->select('site_maintenance_content,maintenance_time,status');
		$this->CI->db->from('site_maintenance'); 
		$this->CI->db->where('site_maintenance_id', '1'); 
		$result = $this->CI->db->get()->result_array();	 
		
		if($result[0]['status']=='Active'){
		 redirect('SiteMaintenance');	
		}else{
		 return;	
		}
	}
			
}
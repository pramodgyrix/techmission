<?php 
/**
Weblogos Online class *
Manages active logos *
@package CodeIgniter
@subpackage Libraries
@category Add-Ons
@author Leonardo Monteiro Bersan de Araujo
@link hhttp://codeigniter.com/wiki/Library: Weblogos */ 

class Weblogos { 
	public $data, $CI;
	function Weblogos(){
		$CI =& get_instance();
		$this->CI = $CI;
		$class_session_loaded='';
		//Loads the USER_AGENT class if it's not loaded yet 
		if(!isset($this->CI->session)) { 
			$this->CI->load->library('session');
			$class_session_loaded = true; 
		}				
		$Weblogos = $this->CI->session->userdata('Weblogos');
		$this->data = $this->getDataFromDB();
			$this->CI->session->set_userdata(array('Weblogos' => $this->data));
		if(!isset($Weblogos) && count($Weblogos) < 1){			
			$this->data = $this->getDataFromDB();
			$this->CI->session->set_userdata(array('Weblogos' => $this->data));
		}else{
			$this->data = $this->CI->session->userdata('Weblogos');
		}
		/*$this->data = $this->getDataFromDB();
			$this->CI->session->set_userdata(array('Weblogos' => $this->data));*/
		
		if($class_session_loaded){ 
			unset($class_session_loaded, $this->CI->session); 
		}
	}
	
	function getDataFromDB(){
		$class_db_loaded='';
		//Loads the USER_AGENT class if it's not loaded yet 
		if(!isset($this->CI->db)) { 
			$this->CI->load->database;
			$class_db_loaded = true; 
		}
		
		$this->CI->db->where('status', 'Active');
		$result = $this->CI->db->get('site_logo');
		$dataResult = array();
		foreach($result->result_array() as $value){
			$dataResult = array_merge($dataResult , array($value['site_logo_name'] => $value['site_logo_image']));
		}
		
		if($class_db_loaded){ 
			unset($class_db_loaded, $this->CI->db); 
		}
				
		return $dataResult;
	}
	
	function getSetting($setting_name = ''){
		$this->data = $this->CI->session->userdata('Weblogos');
		if(isset($setting_name) && $setting_name !=''){
			return $this->data[$setting_name];
		}else{
			return $this->data;
		}
	}
	function unsetSetting(){
		$this->CI->session->unset_userdata('Weblogos');
	}
			
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
	var $permision;
	var $className;
	var $methodName;
	
	function __construct(){
		parent::__construct();	
		
		$this->className  =  $this->router->fetch_class();
        $this->methodName =  $this->router->fetch_method();	
		$this->permision = user_permission($this->className);
		//dd($this->permision);
	 }
	

}
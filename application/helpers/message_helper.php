<?php
/**
 * Function:	send_user_email_ci
 * params:	
 * 				$to			can be string, array or comma saparated value, 
 * 				$from 		array('name'=>'', email=>''), 
 * 				$subject 	string, 
 * 				$body 		string, 
 * 				$attachment array
 */
function send_user_email_ci($to, $from, $subject = '', $body = '', $attachments = array(),$filePath='',$emailtype=''){
	$CI = & get_instance();
	$config = array();
	$config['useragent']	= "CodeIgniter";
	$config['mailpath'] 	= "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
	$config['protocol'] 	= "smtp";
	$config['smtp_host']	= "localhost";
	$config['smtp_port']	= "25";
	$config['mailtype']		= 'html';
	$config['charset'] 		= 'utf-8';
	$config['newline']  	= "\r\n";
	$config['wordwrap'] 	= TRUE;
	$CI->load->library('email');
	$CI->email->initialize($config);
	
	$websetting = $CI->session->userdata('websetting');
	if(!empty($websetting)){
		$site_email = $websetting['site_email']; 
		$support_email = $websetting['support_email']; 
		$site_name = $websetting['site_name']; 
		
	}
	
	if(empty($emailtype)){
	
	$CI->email->from($site_email, $site_name);
	
	}else{
	
		if($emailtype=='support_email'){
		    $CI->email->from($support_email, $site_name);
		}
	}
	
	/*if(is_array($from)){
		if($from['email'] == $site_email){
			$CI->email->from($site_email, $site_name);
		}else{
			$CI->email->from($from['email'], $site_name);
		}
	}else{
		if($from == $site_email){
			$CI->email->from($site_email, $site_name);
		}else{
			$CI->email->from($from, $site_name);
		}
	}*/
	
	
	$CI->email->to($to);
	$CI->email->subject($subject);
	$CI->email->message($body);
	if(!empty($attachments))
	{
		foreach($attachments as $attachment){
			$file_path = $filePath ? $filePath : config_item('root_url');
			$CI->email->attach($file_path.$attachment);
		}
	}
	
	$result = $CI->email->send();
	if($result){
	  return $result;
	}else{
	 return false;
	}	
}
?>
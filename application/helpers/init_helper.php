<?php

function inAarray($array,$string){
if (in_array($string, $array)) {
    return $string;
}
return false;	
}
/* end */
/**
    * [ get user status ]
   * @param  {[type]} $selVal [string selected id]
    * @return {[type]}       [group array]
*/
function getUserStatus($selVal){
	  $CI = & get_instance();
	  $result = array('Active','Inactive');
	  if(!empty($result)){
		 $list = "";
		 foreach($result as $val){
			 $sel = '';
			 if($val == $selVal){
			  $sel = 'selected';
			 }
			 
			$list .= "<option ".$sel." value='".$val."'>".$val."</option>"; 
			}
			return $list;
	   }else{
		   $list = '<option value="">No status found.</option>';
		  return false; 
	  }
	
}
/* end */
/**
    * [ get user group ]
   * @param  {[type]} $selVal [string selected id]
    * @return {[type]}       [group array]
*/
function getUserGroup($selVal){
	
	  $CI = & get_instance();
	  $sql = "select * from user_group where status = 'Active'";
	  $query = $CI->db->query($sql);
	  if($query->num_rows()>0){
		 $result = $query->result_array();
		
		 $list = "";
		 foreach($result as $val){ 
			 $sel = '';
			 if($val['group_id'] == $selVal){
			  $sel = 'selected';
			 }
			 
			$list .= "<option ".$sel." value='".$val['group_id']."'>".$val['groupName']."</option>"; 
			}
			 //$list = "</div>";
			return $list;
	   }else{
		   $list = '<option value="">No group found.</option>';
		  return false; 
	  }
	
}
/* end */
/**
    * [ multiple array short ]
    * @param  {[type]} $array [array val]
	* @param  {[type]} $cols [string column name]
    * @return {[type]}       [short array]
*/ 
function array_msort($array, $cols)
	{
		$colarr = array();
		foreach ($cols as $col => $order) {
			$colarr[$col] = array();
			if(!empty($array)){
			foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
			}
		}
		$eval = 'array_multisort(';
		foreach ($cols as $col => $order) {
			$eval .= '$colarr[\''.$col.'\'],'.$order.',';
		}
		$eval = substr($eval,0,-1).');';
		eval($eval);
		$ret = array();
		foreach ($colarr as $col => $arr) {
			foreach ($arr as $k => $v) {
				$k = substr($k,1);
				if (!isset($ret[$k])) $ret[$k] = $array[$k];
				$ret[$k][$col] = $array[$k][$col];
			}
		}
		return $ret;
	}

/* end */
	
/* Dump and die */
function dd($array){
	echo '<pre/>';
	print_r($array);
	die;
}
/* get front user permission */
function user_permission($class, $method = ''){

	 if(empty($class)) 
	 return false;
	 
	 $class =  strtolower($class);
	 $CI = & get_instance();
	// $CI->output->enable_profiler(TRUE);
	 $role_id = $CI->session->userdata('group_id');; // group_id session role id  $this->session->userdata('role_id');
	 $CI->db->select('a.action_name');
	 $CI->db->from('front_permissions p');
     $CI->db->join('front_class c', 'c.id = p.class_id');
     $CI->db->join('front_actions a', 'a.id = p.action_id', 'LEFT'); 
	 $CI->db->where('c.class_name', $class);
     $CI->db->where('p.role_id', $role_id);
     $query = $CI->db->get();
	  if($query->num_rows()>0){
		 $result = $query->result_array();
		  $query->free_result();
		 return $result;
	   }else{
		  return false; 
	  }
	}
	
function check_function_permission($array,$className,$methodName){
	if(empty($array))
		return false;
	
	$CI = & get_instance();
	$methodName = strtolower($methodName);
	foreach($array as $val){
		if($val['action_name'] == $methodName){
			return true;
		}
	}
	$className = $className ? $className : 'Home';   
	$CI->session->set_flashdata('alert alert-warning', 'You do not have permission to access this page!', 20);
	redirect(base_url($className));
	
	}
/**	
	* user details get 
**/
function get_user_details($id = '',$table='user'){
        $CI = & get_instance();
        if(empty($id)){
		   $id = $CI->session->userdata('user_id');
		}
		$CI->db->select('*');
		$CI->db->from($table);
		$CI->db->where('user_id',$id);
		
		$result = $CI->db->get();
	    $resultData=  $result->result_array();
		
	    if($result->num_rows() == 1)
		{
		   return $resultData;
		}
		else{ return false;	}
        
}
/**
	*Select Data 
**/
function select($table, $where='',$column='*'){
	  $CI = & get_instance();		
	  $sql = "select $column from $table";
	  if(!empty($where))
	    $sql .= " $where";
	  $query = $CI->db->query($sql);
	  if($query->num_rows()>0){
		 $result = $query->result_array();
		 return $result;
	   }else{
		  return false; 
	  }
}
/**
	* User-Profile Image 
**/
function get_user_profile_image($image,$url){
	$filename="$url/$image";
	if(!empty($image)){
			if(@getimagesize($filename)){
			  return '/'.$image ;
			}else{
			  return '/default.png';
			}
	}else{
	   return '/default.png';
	}
}

/**
	* User-Profile Image 
**/
function check_user_login(){
	$CI = & get_instance();
	$user_id = $CI->session->userdata('user_id');
	if($user_id ==''){
		redirect(base_url('login'), 'refresh');
	}else{
		return $user_id;
	}		
}
// this function return latest picture
function get_current_profile_pic($userId='', $path='',$table='user'){
		$CI = & get_instance();
		//$CI->load->database();
		$CI->db->select('profileImage');
		$CI->db->from($table); 
		$CI->db->where('userId', $userId); 
		$rslt = $CI->db->get()->result_array();	 
		//echo $rslt[0]['profile_image'];
		return display_image($rslt[0]['profileImage'], $path);	
}
function display_image($imageName='', $imagePath=''){
			
		if($imageName !='' && $imagePath !=''){
			$filePath = trim($imagePath).SEPARATOR.trim($imageName);
			if(@getimagesize($filePath)){
				return $filePath;
			}else{
				return trim($imagePath).SEPARATOR.'default.png';
			}
		}
		return trim($imagePath).SEPARATOR.'default.png';
}
function address_to_latlng_detail($address){
  

  $address = urlencode($address);
  $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";
  $xml = simplexml_load_file($request_url);
  $status = $xml->status;
  if ($status=="OK") {
      $Lat = $xml->result->geometry->location->lat;
      $Lon = $xml->result->geometry->location->lng;
      
 $data = array('latitude' => $Lat,
                'longitude' => $Lon);

   return $data;
  }
  return true;
}

function contact_us_subject(){
  $contact_array=array('1'=>'General customer service',
          '2'=>'Suggestions',
		  '3'=>'Service  related support',
		  '4'=>'Order related support',
		  '5'=>'Payment related support',
		  '6'=>'Price related support',
		  '7'=>'Package related support'
          );
  return $contact_array;		  	
}
function get_package_variants($package_id){
		$CI = & get_instance();
		$CI->db->select("package_variants.variant_id,package_variants.package_price variant_price,package_variants.package_duration variant_duration,package_variants.duration_unit variant_unit");
		return $CI->db->get_where("package_variants",array("package_id" =>  $package_id))->result_array();
		
}
function seo_friendly_urls($firstName,$lastName,$user_id=''){ 
    $text = '';
    if(!empty($firstName)){
	  $text .= $firstName;
	}
	 if(!empty($firstName)){
	  $text .= ' '.$lastName;
	}
	 
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
  // trim
  $text = trim($text, '-');
  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  // lowercase
  $text = strtolower($text);
  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);
  if (empty($text))
  {
    return FALSE;
  }
  
  if(!empty($user_id)){
  
  $encode_id = safe_b64encode($user_id);
  $encode = $encode_id.'/'.$text;
  
  }else{
   $encode = $text;
  }
  return $encode;
}
function safe_b64encode($string) {
 
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
}

function safe_b64decode($string) {
	$data = str_replace(array('-','_'),array('+','/'),$string);
	$mod4 = strlen($data) % 4;
	if ($mod4) {
		$data .= substr('====', $mod4);
	}
	return base64_decode($data);
}

function encrypt($string, $key='')
{
	$CI = & get_instance();	
	$CI->load->library('encrypt');	 
	$key = (isset($key) && $key !='')? $key : $CI->config->item('encryption_key');
	return $CI->encrypt->encode($string, $key);
	
}
 
function decrypt($string, $key='')
{
	$CI = & get_instance();	
	$CI->load->library('encrypt');	 
	$key = (isset($key) && $key !='')? $key : $CI->config->item('encryption_key');
	return $CI->encrypt->decode($string, $key);		
}
	
		/*Resize Image*/
function resize_images($path,$rs_width,$rs_height,$destinationFolder='') {

 $folder_path = dirname($path);
 $thumb_folder = $destinationFolder;
 $percent = 0.5;
 
 if (!is_dir($thumb_folder)) {

    mkdir($thumb_folder, 0777, true);
 }
 
 $name = basename($path);

 $x = getimagesize($path);            

 $width  = $x['0'];

 $height = $x['1'];

 switch ($x['mime']){

              case "image/gif":

                 $img = imagecreatefromgif($path);

                 break;

              case "image/jpeg":

                 $img = imagecreatefromjpeg($path);

                 break;
			 case "image/jpg":

                 $img = imagecreatefromjpeg($path);

                 break;

              case "image/png":

                 $img = imagecreatefrompng($path);

                 break;

  }

    $img_base = imagecreatetruecolor($rs_width, $rs_height);

    $white = imagecolorallocate($img_base, 255, 255, 255);

    imagefill($img_base, 0, 0, $white);
    
	imagecopyresized($img_base, $img, 0, 0, 0, 0, $rs_width, $rs_height, $width, $height);

    imagecopyresampled($img_base, $img, 0, 0, 0, 0, $rs_width, $rs_height, $width, $height);

    $path_info = pathinfo($path);   

    $dest = $thumb_folder.$name;

           switch ($path_info['extension']) {

              case "gif":

                 imagegif($img_base, $dest);  

                 break;

              case "jpg":

                 return imagejpeg($img_base, $dest);  

                 break;
			  case "jpeg":

                 return imagejpeg($img_base, $dest);  

                 break;

              case "png":

                return imagepng($img_base, $dest);  

                 break;

           }
}

function media_url($suffix = '', $isrootpath = false){
	if ($isrootpath){
		$url = config_item('media_path');
	} else {
		$url = config_item('media_url');
	}
	
	$url.= $suffix;
	return $url;
}




/* user details get */


function check_user_from_email($email = ''){
	$CI = & get_instance();
	$where = " where email = '$email'"; 
	$result = $CI->Common->select('user',$where);
	return $result;

}

function get_week_dates($year, $week, $start=true) {
    $from = date("Y-m-d", strtotime("{$year}-W{$week}-1"));
    $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));
 
 	$array = array(
		'startdate' => $from,
		'enddate' => $to
	);
	
	return $array;
}
/*
   This function is for upload image
   Usage:-
       $root_path=USER_IMAGE_ROOTPATH;
	   $files=$_FILES['profileImage'];
	   upload_image($files,$root_path);
*/
function upload_image($files,$root_path){
	if(!empty($files['name'])){	
		$ext = @pathinfo($files['name'], PATHINFO_EXTENSION);				
		$file_name   = time().rand(1000,99999).'.'.$ext;	
		$file_path = $root_path.'/'.$file_name; 	
		@move_uploaded_file($files['tmp_name'], $file_path);
	 }
	 return $file_name;	
}

/*
  encrupt data
*/
function encrypt_data($data){
	  $CI = & get_instance();
	  $CI->load->library('encrypt');	 
	  $key = $CI->config->item('encryption_key');
	  return  $CI->encrypt->encode($data, $key);
}
		
/* 
  decrypt data
*/ 
function decrypt_data($value){
		$CI = & get_instance();
		$CI->load->library('encrypt');	 
		$key = $CI->config->item('encryption_key');
		return  $CI->encrypt->decode($value, $key);
}

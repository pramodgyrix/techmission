<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Datatables_Model extends CI_Model {
	
 
public function select_data($where = NULL,$limit, $offset,$post = NULL,$order_column = NULL, $order_type = NULL){

$this->db->select('a.id, a.name,a.contact_no, a.email, c.name as country_name, b.name as branch_name, i.name as industry_name');
	$this->db->from('account a');
	$this->db->join('country c', 'c.id = a.country_id', 'LEFT');
	$this->db->join('branch b', 'b.id = a.branch_id', 'LEFT');
	$this->db->join('industry i', 'i.id = a.industries', 'LEFT');        
	$this->db->where('a.status', 1);
	
	if($where != ''){
	$this->db->where($where);
	}
	
	if(empty($post)){
	$post =	$this->session->userdata('search');
	$post = trim($post);
	}
	
	$post = array('search' => $post);
	$this->session->set_userdata($post);
		
	if($post){
		$post_data = explode(" ",$post['search']);
		if(!empty($post_data)){
		  foreach($post_data as $val){
			 $this->db->where("(a.name LIKE '%$val%' OR a.contact_no LIKE '%$val%' OR a.email LIKE '%$val%' OR c.name LIKE '%$val%' OR b.name LIKE '%$val%' OR i.name LIKE '%$val%')");
		  }
		}
    }

	$this->db->limit($limit, $offset);   
	$query = $this->db->get();
	if($query->num_rows()>0){
	$result =  $query->result_array();
	$query->free_result();
	return $result;
	}else{
	return false;
	}
	
}  


public function select_data_count($where = NULL,$post = NULL){

$this->db->select('a.id, a.name,a.contact_no, a.email, c.name as country_name, b.name as branch_name, i.name as industry_name');
	$this->db->from('account a');
	$this->db->join('country c', 'c.id = a.country_id', 'LEFT');
	$this->db->join('branch b', 'b.id = a.branch_id', 'LEFT');
	$this->db->join('industry i', 'i.id = a.industries', 'LEFT');        
	$this->db->where('a.status', 1);
	
	
	if($where != ''){
	$this->db->where($where);
	}
	
	if(empty($post)){
	$post =	$this->session->userdata('search');
	$post = trim($post);
	}
	
	$post = array('search' => $post);
	$this->session->set_userdata($post);
	
	if($post){
		$post_data = explode(" ",$post['search']);
		if(!empty($post_data)){
		  foreach($post_data as $val){
			 $this->db->where("(a.name LIKE '%$val%' OR a.contact_no LIKE '%$val%' OR a.email LIKE '%$val%' OR c.name LIKE '%$val%' OR b.name LIKE '%$val%' OR i.name LIKE '%$val%')");
		  }
		}
    }
	
	
	
	$query = $this->db->get();
	if($query->num_rows()>0){
	$result =  count($query->result_array());
	$query->free_result();
	return $result;
	}else{
	return false;
	}
	
}  
  
}
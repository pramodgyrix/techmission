<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Employee_Model extends CI_Model {
	
 
public function select_data($where = NULL,$limit, $offset,$post = NULL,$order_column = NULL, $order_type = NULL){

     $this->db->select('u.user_id, u.firstName, u.lastName,u.email, u.status, g.groupName');
	$this->db->from('user u');
	$this->db->join('user_group g', 'g.group_id = u.role_id', 'LEFT');
	
	
	if($where != ''){
	$this->db->where($where);
	}
	
	if(empty($post)){
	$post =	$this->session->userdata('search');
	$post = trim($post);
	}
	
	$post = array('search' => $post);
	$this->session->set_userdata($post);
		
	if($post){
		$post_data = explode(" ",$post['search']);
		if(!empty($post_data)){
		  foreach($post_data as $val){
			  if(!empty($val)){
			   $this->db->where("(u.firstName LIKE '%$val%' OR u.lastName LIKE '%$val%' OR g.groupName LIKE '%$val%')");
			  }
		  }
		}
    }

    $this->db->order_by("u.user_id", "desc");
	$this->db->limit($limit, $offset);   
	$query = $this->db->get();
	if($query->num_rows()>0){
	$result =  $query->result_array();
	$query->free_result();
	return $result;
	}else{
	return false;
	}
	
}  


public function select_data_count($where = NULL,$post = NULL){

     $this->db->select('u.firstName, u.lastName,u.email, u.status, g.groupName');
	$this->db->from('user u');
	$this->db->join('user_group g', 'g.group_id = u.role_id', 'LEFT');
	
	
	if($where != ''){
	$this->db->where($where);
	}
	
	if(empty($post)){
	$post =	$this->session->userdata('search');
	$post = trim($post);
	}
	
	$post = array('search' => $post);
	$this->session->set_userdata($post);
	
	if($post){
		$post_data = explode(" ",$post['search']);
		if(!empty($post_data)){
		  foreach($post_data as $val){
			 if(!empty($val)){
			 $this->db->where("(u.firstName LIKE '%$val%' OR u.lastName LIKE '%$val%' OR g.groupName LIKE '%$val%')");
			 }
		  }
		}
    }
	
	
	
	$query = $this->db->get();
	if($query->num_rows()>0){
	$result =  count($query->result_array());
	$query->free_result();
	return $result;
	}else{
	return false;
	}
	
}  
  
}
<?php
$websetting = $this->session->userdata('websetting');
$site_name =  $websetting['site_name'];
$site_copyright=$websetting['site_copyright'];

?>



<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title><?php if(!empty($site_name)){echo $site_name;}else{ echo config_item('site_name');}?></title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/theme_light.css" id="skin_color">
		<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="coming-soon">
		<!-- start: PAGE -->
		<div class="logo">
			<?php if(!empty($site_name)){echo $site_name;}else{ echo config_item('site_name');}?>
		</div>
		<div class="timer-area">
			<h1>We're coming soon...</h1>
			<ul id="countdown">
				
			</ul>
		</div>
        
         <div class="col-sm-12 no-padding">
               
                <div id="source_date" style="display:none;"><?php echo date('Y/m/d H:i:s',strtotime($site_maintenance_data[0]['maintenance_time']));?></div>
                <!--<div id="endtime" class="col-sm-4">END TIME: <span>11:59:40</span></div>-->
                <div class="clear"></div>
              </div>
              
        
		<div class="container">
			<?php echo $site_maintenance_data[0]['site_maintenance_content'];?>
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="enter your email address">
						<span class="input-group-btn">
							<button class="btn btn-teal" type="button">
								NOTIFY ME
							</button> </span>
					</div>
				</div>
			</div>
			<div class="row">
				<div id="disclaimer" class="col-md-12">
					<p>
						<span>*</span> We won't use your email for spam, just to notify you of our launch.
					</p>
				</div>
			</div>
		</div>
		<div class="copyright">
			<?php if(!empty($site_copyright)){echo $site_copyright;}else{ echo config_item('site_copyright');}?>
		</div>
		<!-- end: PAGE -->
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="<?php echo base_url();?>assets/plugins/respond.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo base_url();?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url();?>assets/js/utility-coming-soon.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			
			jQuery(document).ready(function() {
				//Main.init();
				//ComingSoon.init();
			});
		</script>
        <script>
        function calculateremaingtime(){
	var dateFuture = new Date(jQuery('#source_date').html());
	//var dateNow = Date();*/

	//var dateFuture = new Date(new Date().getFullYear() +1, 0, 1);
	var dateNow = new Date();	
	
	var seconds = Math.floor((dateFuture - (dateNow))/1000);
	var minutes = Math.floor(seconds/60);
	var hours = Math.floor(minutes/60);
	var days = Math.floor(hours/24);

	hours = hours-(days*24);
	minutes = minutes-(days*24*60)-(hours*60);
	seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);


	
	jQuery('#countdown').html('<li><span class="days">' + days + '</span><p class="timeRefDays">days</p></li>'+
				'<li><span class="hours">' + hours + '</span><p class="timeRefHours">hours</p></li>'+
				'<li><span class="minutes">' + minutes + '</span><p class="timeRefMinutes">minutes</p></li>'+
				'<li><span class="seconds">' + seconds + '</span><p class="timeRefSeconds">seconds</p></li>');
	
	
	
	
	
	
}
jQuery(document).ready(function(e) {
    calculateremaingtime();
});
setInterval(calculateremaingtime, 500);
        </script>
        <style>
        #countdown li span{
		   color:#FFF !important;	
		}
        </style>
	</body>
	<!-- end: BODY -->
</html>
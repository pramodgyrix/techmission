<?php
 $websetting = $this->session->userdata('websetting');
 $Weblogos=$this->session->userdata('Weblogos');
 $main_logo = display_image($Weblogos['main_logo'], SITE_LOGO.'/');

?>
<!-- start: HEADER -->

<div class="navbar navbar-inverse navbar-fixed-top"> 
  <!-- start: TOP NAVIGATION CONTAINER -->
  <div class="container">
    <div class="navbar-header"> 
      <!-- start: RESPONSIVE MENU TOGGLER -->
      <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- end: RESPONSIVE MENU TOGGLER --> 
      <!-- start: LOGO --> 
      <!-- <a class="navbar-brand" href="<?php echo base_url('dashboard');?>">
                  
                                       
            </a>--> 
      
      <a href="<?php echo base_url('dashboard');?>" class="navbar-brand logo_inner" style="font-size: 26px;" title="<?php if(!empty($websetting['site_name'])){echo $websetting['site_name'];}else{ echo config_item('site_name');} ?>"> <img src="<?=$main_logo;?>" style="width: 94px;height: 50px;
    margin-top: -20px;"/> </a> 
      
      <!-- end: LOGO --> 
    </div>
    <div class="navbar-tools"> 
      <!-- start: TOP NAVIGATION MENU -->
      <ul class="nav navbar-right">
        <!-- start: USER DROPDOWN -->
        <?php 
                $userId = $this->session->userdata('user_id');
				$userData = select('user',"where user_id='$userId'",'email,firstName,lastName,profile_image as profileImage');
				$profilePic = display_image($userData[0]['profileImage'], USER_IMAGE_150150);
				
				
                ?>
        <li class="dropdown current-user"> <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)"> <img src="<?=$profilePic?>" style="width:30px;height:30px;" class="circle-img" title="<?php echo ucwords($userData[0]['firstName'].' '.$userData[0]['lastName']);?>"> <span class="username"><?php echo ucwords($userData[0]['firstName'].' '.$userData[0]['lastName']);?></span> <i class="clip-chevron-down"></i> </a>
          <ul class="dropdown-menu">
          <li class="text-center"><a><b><?php echo $userData[0]['email']; ?> </b></a></li>
            <li> <a href="<?php echo base_url();?>profile"> <i class="clip-user-2"></i> &nbsp;My Profile </a> </li>
            <li> <a href="<?php echo base_url();?>profile/edit"> <i class="clip-pencil-2"></i> &nbsp;Edit Profile </a> </li>
            <li> <a href="<?=base_url();?>profile/changePassword"> <i class="fa fa-refresh"></i> &nbsp;Change Password </a> </li>
            <li> <a href="<?php echo base_url('login/logout');?>"> <i class="clip-exit"></i> &nbsp;Log Out </a> </li>
          </ul>
        </li>
        <!-- end: USER DROPDOWN -->
      </ul>
      <!-- end: TOP NAVIGATION MENU --> 
    </div>
  </div>
  <!-- end: TOP NAVIGATION CONTAINER --> 
</div>
<!-- end: HEADER -->
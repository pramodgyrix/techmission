<?php
$controller = $this->router->class;
$method = $this->router->method;
$role_name = $this->session->userdata('role_name');
$role_id = $this->session->userdata('role_id');

/**
pslug : is a array use active navigation class
category_name : parant category name

slug : is a array use clild class navigation active
child_menu_key : this parameter linking class
child_menu_title : child class name 

*/
$permitNavigation = array(
              array('pslug' => array('user','employee','contact'),'category_key' => 'user','icon_class' => 'clip-users','category_name'=> 'Master Entry', 
			  
			  'setting' => array(
			   
			   array('slug' => array('index','employee','Add','Edit'),'selected_menu_key' => 'user/employee','child_menu_key' => 'employee','child_menu_title' => 'Employee', 'id' => 'user'),

			   array(array('pslug' => array('')),'child_menu_key' => '','child_menu_title' => 'Supplier', 'id' => 'user'),
			   array('slug' => array(''),'child_menu_key' => '','child_menu_title' => 'Account', 'id' => 'user'),
			  
			   array('slug' => array('index','contact','Add','Edit'), 'selected_menu_key' => 'user/contact','child_menu_key' => 'contact','child_menu_title' => 'Contact', 'id' => 'user')
			  
			  )),
			 
			  array(array('pslug' => array('')),'category_key' => 'trasaction','icon_class' => 'clip-menu-4','category_name'=> 'Trasaction', 
			  'setting' => array(array('slug' => array(''),'child_menu_key' => '','child_menu_title' => 'User Account', 'id' => 'user'))),
			  
			  array(array('pslug' => array('')),'category_key' => 'report','icon_class' => 'clip-stack','category_name'=> 'Reports', 
			  'setting' => array(array('slug' => array(''),'child_menu_key' => '','child_menu_title' => 'User Account', 'id' => 'user', 'id' => 'user'))),
			  
			  
			    array(array('pslug' => array('')),'category_key' => '','icon_class' => '','category_name'=> 'Material Movement', 
			  ),


);

?>
<div class="main-navigation navbar-collapse collapse"> 
  <!-- start: MAIN MENU TOGGLER BUTTON -->
  <div class="navigation-toggler"> <i class="clip-chevron-left"></i> <i class="clip-chevron-right"></i> </div>
  <!-- end: MAIN MENU TOGGLER BUTTON --> 
  <!-- start: MAIN NAVIGATION MENU -->
  <ul class="main-navigation-menu">
    <li<?php if($controller=='dashboard'){?> class="active open"<?php }?>> <a href="<?php echo base_url('dashboard');?>"><i class="fa fa-tachometer"></i> <span class="title"> Dashboard </span><span class="selected"></span> </a> </li>
   
     <?php  
	if(isset($permitNavigation) && !empty($permitNavigation)){
			foreach($permitNavigation as $val){
				
			$icon_class= $val['icon_class'];
			$category_name = $val['category_name'];
			
            $menu= '<li'; 
			
			if(!empty($val['pslug'])){
				 $category_key = inAarray($val['pslug'],$controller); 
			}else{
				 //$category_key = $val['category_key'];
				 $category_key = '';
			}


			
				if($controller == $category_key){ 
					$menu .= ' class="active open"';
				} 
				$menu .= '>';
				
			$menu.='<a href="javascript:void(0)"><i class="'.$icon_class.'"></i>
					<span class="title">'.ucwords($category_name).'</span>
					<i class="icon-arrow"></i><span class="selected"></span></a>';
			
			//dd($val['setting']);
			if(!empty($val['setting'])){
			$menu .= '<ul class="sub-menu">';
			
			foreach($val['setting'] as $value){
			    $st = $value['child_menu_key'];
                $pieces = @explode("/", $st);
               
               $mymethod = '';
           if(!empty($value['slug']) && ($value['child_menu_key'] == $controller)){
				$mymethod = inAarray($value['slug'],$method);
			}
			
				$menu .= '<li'; 
				//$method == $mymethod
				if($controller == $category_key && !empty($mymethod)){ 
					$menu .= ' class="active"';
				} 
				$menu .= '>';
				$menu .= '<a href="'.base_url($value['child_menu_key']).'"><span class="title"> '.ucwords($value['child_menu_title']).' </span></a>';
				$menu .= '</li>';
			}
			$menu .= '</ul>';
			}
            echo $menu .= '</li>';
			
            
		}
		}		
		?>
    
    
 
 
</div>

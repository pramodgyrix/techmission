<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>        
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->
<div class="main-container">
	<div class="navbar-content">
		<!-- start: SIDEBAR -->
		<?php $this->load->view('common/left_navigation'); ?>
		<!-- end: SIDEBAR -->
	</div>
	<!-- start: PAGE -->
	<div class="main-content">				
		<div class="container">
			<!-- start: PAGE HEADER -->
			<div class="row">
				<div class="col-sm-12">							
					<!-- start: PAGE TITLE & BREADCRUMB -->							
					<?php $this->load->view('common/breadcrumb'); ?>
					<div class="page-header">
						<h1><?php echo $page_title;?></h1>
					</div>
					<!-- end: PAGE TITLE & BREADCRUMB -->
				</div>
			</div>
			<!-- end: PAGE HEADER -->
			<!-- start: PAGE CONTENT -->
			<div class="row">
				<div class="col-lg-12">
                	<div class="tabbable">								
						<?php $this->load->view('profile/navigation');?>
                        <div class="tab-content-static">
                            <div id="panel_edit_profile" class="tab-pane in active">
								<form action="<?=base_url('profile/changePassword');?>" id="changePassword" method="post" class="form-horizontal">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Change Password</h3>
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <?php echo validation_errors();?>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">														
                                                <label class="col-sm-2 control-label">Old Password <span class="symbol required"></span></label>
                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="oldpass" id="oldpass" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">														
                                                <label class="col-sm-2 control-label">New Password <span class="symbol required"></span></label>
                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="newpass" id="newpass" value="">
                                                </div>
                                            </div>	
                                            <div class="form-group">														
                                                <label class="col-sm-2 control-label">Confirm Password <span class="symbol required"></span></label>
                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" name="newpassconf" id="newpassconf" value="">
                                                </div>
                                            </div>
                                        </div>												
                                    </div>
                                    <div class="row">

                                        <label class="col-sm-2 control-label"></label>

                                        <div class="col-md-6">

										    <a class="btn btn-light-grey" href="<?php echo config_item('base_url');?>profile">

														<i class="fa fa-arrow-circle-left"></i>&nbsp;Back 

					 </a>

                                            <button class="btn btn-dark-grey" type="submit">

                                                Update <i class="fa fa-arrow-circle-right"></i>

                                            </button>

                                        </div>

                                    </div>
                                </form>                                                                                                 
                            </div>									
                        </div>
                    </div>	  
				</div>
			</div>                    
			<!-- end: PAGE CONTENT-->
		</div>
	</div>
	<!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<script src="<?php echo base_url('assets/js/custom/changePasswordValidation.js')?>"></script>
<script>
jQuery(document).ready(function(e){
	FormValidatorChangePassword.init();
});			
</script>
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>
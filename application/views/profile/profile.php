<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title;?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      
      
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
          <div class="tabbable">
            <?php $this->load->view('profile/navigation');?>
            <div class="tab-content-static">
              <div id="panel_edit_profile" class="tab-pane in active">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="user-left">
                      <div class="center">
                        <h4><?php echo ucfirst($result[0]['name']);?></h4>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                          <div class="user-image"id="userImage">
                            <div class="fileupload-new thumbnail">
                              <?php														
								$profile_img = display_image($result[0]['profileImage'], USER_IMAGE);
                              ?>
                              <img src="<?php echo $profile_img;?>" alt="<?php echo ucfirst($result[0]['name']);?>" style="width:200px;"> </div>
                            <form id="imageform" method="post" enctype="multipart/form-data" action='<?=config_item('base_url');?>Profile/update_image'>
                              <div class="user-image-buttons"> <span class="btn btn-teal btn-file btn-sm"><span class="fileupload-new"><i class="fa fa-pencil"></i></span><span class="fileupload-exists"><i class="fa fa-pencil"></i></span>
                                <input type="file" name="profile_image" id="profileImage" />
                                </span> <a href="#" class="btn fileupload-exists btn-bricky btn-sm" data-dismiss="fileupload"> <i class="fa fa-times"></i> </a> </div>
                            </form>
                          </div>
                        </div>
                        <hr>
                      </div>
                      <table class="table table-condensed table-hover">
                        <thead>
                          <tr>
                            <th colspan="3">Profile Contact Information</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Gender</td>
                            <td><?php echo $result[0]["gender"];?></td>
                          </tr>
                          <tr>
                            <td>Email</td>
                            <td><?php echo $result[0]["email"];?></td>
                          </tr>
                          <tr>
                            <td>Phone Number</td>
                            <td><?php echo $result[0]["phoneNumber"];?></td>
                          </tr>
                          <tr>
                            <td>Cell Number</td>
                            <td><?php echo $result[0]["cellNumber"];?></td>
                          </tr>
                          <tr>
                            <td>Address</td>
                            <td><?php echo $result[0]["address"];?></td>
                          </tr>
                          <tr>
                            <td>Created Date</td>
                            <td><?php echo $result[0]["createdDate"];?></td>
                          </tr>
                          <tr>
                            <td>Modify Date</td>
                            <td><?php echo $result[0]["updatedDate"];?></td>
                          </tr>
                          <tr>
                            <td>Login Date</td>
                            <td><?php echo $result[0]["lastLoginDate"];?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<script src="<?=config_item('site_url').'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js';?>"></script> 
<script src="<?=config_item('site_url').'assets/plugins/jquery.pulsate/jquery.pulsate.min.js';?>"></script> 
<script>	
$('#profileImage').on('change', function(){
	$("#imageform").submit();
});	 
</script> 

<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

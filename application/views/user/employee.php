<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->
<?php 	
$emailTags = array();
$emailJson = '';
if(!empty($result)) { 
  foreach($result as $val){
	  $emailTags[] =  $val['firstName'];
	  $emailTags[] =  $val['email'];
	  
  }
  $emailJson = json_encode($emailTags);
}
  $page_num = (int)$this->uri->segment(3);
  if($page_num==0) $page_num=1;
  $order_seg = $this->uri->segment(5,"asc"); 
  if($order_seg == "asc") $order = "desc"; else $order = "asc";
  ?>

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="col-md-4">
            <div class="core-box">
              <div class="heading"> <i class="clip-user-4 circle-icon circle-green"></i>
                <h2>Manage Employee</h2>
              </div>
            </div>
          </div>
          <div class="col-md-8"> <a class="pull-right btn btn-dark-grey" href="<?php echo base_url('employee/Add'); ?>"> <i class="fa fa-user fa fa-white"></i> Add Employee</a> </div>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-sm-12">
        
        
         <div class="col-md-10">
          <form action="<?php echo base_url('employee'); ?>" method="post" >
            <div class="col-md-5"></div>
            <div class="col-md-6">
              <input class="form-control" id="s" name="s" placeholder="Search for Name..." type="text" value="<?php echo  $this->session->userdata('search'); ?>" />
            </div>
            <div class="col-md-1"> 
             <button type="submit" value="search" class="btn btn-info" id="btn_search" name="btn_search"> <span class="glyphicon glyphicon-search"></span> Search </button>
            </div>
          </form>
          </div>
          <div class="col-md-2">
          <form action="<?php echo base_url('employee'); ?>" method="post" >
            <button id="btn_search" name="btn_search" type="submit" class="btn btn-danger" value="reset" style="margin-left: 8px;" />
           <span class="glyphicon glyphicon-refresh"></span> Reset
            </button>
            <input type="hidden" name="reset_search" value="1">
          </form>
          </div>
        </div>
        <br/>
        <br/>
      </div>
      
      <div class="row">
        <div class="col-md-12 table-responsive">
        
       
         
          <table id="myTable" class="table table-striped table-bordered" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th > <a class="" href="<?php echo base_url('employee/index/'.$offset.'/name/'.$new_order); ?>">Contact Name <img class="<?php if($order_column == 'name') { echo $order_class; }else{ echo 'sorting'; } ?>"  > </a> </th>
                <th><a href="<?php echo base_url('employee/index/'.$offset.'/email/'.$new_order); ?>">Email <img class="<?php if($order_column == 'email') { echo $order_class; }else{ echo 'sorting'; } ?>"  ></a></th>
                <th><a href="<?php echo base_url('employee/index/'.$offset.'/groupName/'.$new_order); ?>">Group Name <img class="<?php if($order_column == 'groupName') { echo $order_class; }else{ echo 'sorting'; } ?>"  ></a></th>
                <th><a href="<?php echo base_url('employee/index/'.$offset.'/status/'.$new_order); ?>">Status <img class="<?php if($order_column == 'status') { echo $order_class; }else{ echo 'sorting'; } ?>"  ></a></th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($result)) { 
						  foreach($result as $val){
							$id = safe_b64encode($val['user_id']);
							$status = trim($val['status']);
							 
						 ?>
              <tr>
                <td><?php echo ucwords($val['firstName'].' '.$val['lastName']); ?></td>
                <td><?php echo $val['email']; ?></td>
                <td><?php echo $val['groupName']; ?></td>
                <td class="<?php if($status == 'Active'){ echo ''; } ?>"><?php echo $val['status']; ?></td>
                <td  ><a title="Edit Employee" href="<?php echo base_url('employee/Edit/'.$id); ?>">Edit</a></td>
              </tr>
              <?php } 
						
						}else{
							echo '<tr><td colspan="5" class="text-center alert alert-danger">'.NOFOUND.'</td></tr>';
							}?>
            </tbody>
          </table>
          <div class="row">
            <div id="ajax_paging" class="col-md-12"><?php echo $links; ?></div>
          </div>
          
         
          
        </div>
      </div>
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  
<script type="text/javascript">
var result = eval('<?php print_r($emailJson); ?>');
$( "#s" ).autocomplete({
      source: result
    });
</script>

<style type="text/css"></style>
<!-- start: FOOTER -->
<?php $this->load->view('common/footer'); ?>

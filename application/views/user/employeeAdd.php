<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title; ?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> </div>
            <div class="col-md-12"> <?php echo validation_errors();?></div>
                     
            <div class="panel-body">
              <form role="form" class="form-horizontal" action="<?php echo base_url('employee/Add'); ?>" method="post" id="employeeAdd">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-4"> User Group </label>
                  <div class="col-sm-9">
                    <select name="userGroup" id="userGroup" class="form-control">
                      <?php echo getUserGroup(set_value('userGroup'));?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Full Name </label>
                  <div class="col-sm-9">
                    <input placeholder="Full Name Ex: John" id="fullName" name="fullName" class="form-control" type="text" value="<?php echo set_value('fullName'); ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-2"> Email Id </label>
                  <div class="col-sm-9">
                    <input placeholder="Email Ex: test@gmail.com" id="email" name="email" class="form-control" type="text" value="<?php echo set_value('email'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-3"> Password </label>
                  <div class="col-sm-9">
                    <input placeholder="Password Ex: 123456" id="password" name="password" class="form-control" type="password" value="<?php echo set_value('pasword'); ?>">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-7"> </div>
                  <div class="col-md-4">
                    <div class="col-md-9 col-xs-6"> <a class="btn btn-light-grey  pull-right" href="<?php echo base_url('employee'); ?>"> <i class="fa fa-arrow-circle-left"></i>&nbsp;Back </a> </div>
                    <div class="col-md-3 col-xs-6">
                      <button class="btn btn-dark-grey pull-right" type="submit"> Save <i class="fa fa-arrow-circle-right"></i> </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<!-- start: FOOTER -->

<script src="<?php echo base_url('assets/js/custom/employee.js'); ?>"></script>
<script>
jQuery(document).ready(function(){
	employeeValidator.init();
});
</script> 

<?php $this->load->view('common/footer'); ?>

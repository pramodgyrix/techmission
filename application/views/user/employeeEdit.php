<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<?php 
$fullName =  (set_value('fullName')) ? set_value('fullName') : $result['firstName'];
$email =  (set_value('email')) ? set_value('email') : $result['email'];
$groupName =  (set_value('groupName')) ? set_value('groupName') : $result['role_id'];
$password =  (set_value('password')) ? set_value('password') : decrypt($result['password']);
$userStatus =  (set_value('userStatus')) ? set_value('userStatus') : $result['status'];
?>

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php echo $page_title; ?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> </div>
            <div class="col-md-12"> <?php echo validation_errors();?></div>
            <div class="panel-body">
              <form role="form" class="form-horizontal" action="<?php echo base_url('employee/Edit/'.$id); ?>" method="post" id="employeeAdd">
              
              <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-2"> Email Id </label>
                  <div class="col-sm-9">
                    <input placeholder="Email Ex: test@gmail.com" id="email" name="email" class="form-control" type="text" value="<?php echo $email; ?>" disabled>
                    <span class="help-block"><i class="fa fa-info-circle"></i> You can not change this email id.</span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-4"> User Group </label>
                  <div class="col-sm-9">
                    <select name="userGroup" id="userGroup" class="form-control">
                      <?php echo getUserGroup($groupName);?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-1"> Full Name </label>
                  <div class="col-sm-9">
                    <input placeholder="Full Name Ex: John" id="fullName" name="fullName" class="form-control" type="text" value="<?php echo $fullName; ?>">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-3"> Password </label>
                  <div class="col-sm-9">
                    <input placeholder="Password Ex: 123456" id="password" name="password" class="form-control" type="password" value="<?php echo $password; ?>">
                  </div>
                </div>
                
                
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="form-field-3"> Status </label>
                  <div class="col-sm-9">
                    <select name="userStatus" id="userStatus" class="form-control">
                      <?php echo getUserStatus($userStatus);?>
                    </select>
                  </div>
                </div>
                
                
                <div class="form-group">
                  <div class="col-md-7"> </div>
                  <div class="col-md-4">
                    <div class="col-md-8 col-xs-6"> <a class="btn btn-light-grey  pull-right" href="<?php echo base_url('employee'); ?>"> <i class="fa fa-arrow-circle-left"></i>&nbsp;Back </a> </div>
                    
                    <div class="col-md-4 col-xs-6">
                      <button class="btn btn-dark-grey pull-right" type="submit"> Update <i class="fa fa-arrow-circle-right"></i> </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<!-- start: FOOTER --> 

<script src="<?php echo base_url('assets/js/custom/employee.js'); ?>"></script> 
<script>
jQuery(document).ready(function(){
	employeeValidator.init();
});
</script>
<?php $this->load->view('common/footer'); ?>

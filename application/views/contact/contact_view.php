<?php $this->load->view('common/header'); ?>
<!-- Start: HEADER -->
<?php $this->load->view('common/header_content'); ?>
<!-- end: HEADER -->

<!-- start: MAIN CONTAINER -->

<div class="main-container">
  <div class="navbar-content"> 
    <!-- start: SIDEBAR -->
    <?php $this->load->view('common/left_navigation'); ?>
    <!-- end: SIDEBAR --> 
  </div>
  <!-- start: PAGE -->
  <div class="main-content">
    <div class="container"> 
      <!-- start: PAGE HEADER -->
      <div class="row">
        <div class="col-sm-12"> 
          <!-- start: PAGE TITLE & BREADCRUMB -->
          <?php $this->load->view('common/breadcrumb'); ?>
          <div class="page-header">
            <h1><?php //echo $page_title; ?></h1>
          </div>
          <!-- end: PAGE TITLE & BREADCRUMB --> 
        </div>
      </div>
      <!-- end: PAGE HEADER --> 
      <!-- start: PAGE CONTENT -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading"> <i class="fa fa-external-link-square"></i> </div>
            <div class="col-md-12"> <?php //echo validation_errors();?></div>
                     
            <div class="panel-body">
             <form action="" role="form" class="smart-wizard form-horizontal" id="form" method="post" novalidate="novalidate">
          <!--    <form role="form" class="form-horizontal" action="<?php echo base_url('employee/Add'); ?>" method="post" id="employeeAdd"> -->
                    <div id="wizard" class="swMain">
                      <ul>
                        <li>
                          <a href="#step-1">
                            <div class="stepNumber">
                              1
                            </div>
                            <span class="stepDesc"> Step 1
                              <br />
                              <small>Account Information</small> </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-2">
                            <div class="stepNumber">
                              2
                            </div>
                            <span class="stepDesc"> Step 2
                              <br />
                              <small>Billing Information</small> </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-3">
                            <div class="stepNumber">
                              3
                            </div>
                            <span class="stepDesc"> Step 3
                              <br />
                              <small>Additional Information</small> </span>
                          </a>
                        </li>
                        <li>
                          <a href="#step-4">
                            <div class="stepNumber">
                              4
                            </div>
                            <span class="stepDesc"> Step 4
                              <br />
                              <small>Step 4 description</small> </span>
                          </a>
                        </li>
                      </ul>
                      <div class="progress progress-striped active progress-sm">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success step-bar">
                          <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                      </div>
                      <div id="step-1">
                        <h2 class="StepTitle">Step 1 Account Information</h2>                       
                        <div class="row">

                          <div class="col-sm-12">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                  Account Owner <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="accountOwner" name="accountOwner" placeholder="Account Owner">
                                </div>
                              </div>

                               <div class="form-group">
                                <label class="col-sm-3 control-label">
                                  Parent Account <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="parentAccount" name="parentAccount" placeholder="Parent Account">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Phone <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                                </div>
                              </div>

                               <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Type <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <select class="form-control" id="type" name="type">
                                      <option value="">&nbsp;</option>
                                      <option value="Type 1">Type 1</option>
                                      <option value="Type 2">Type 2</option>
                                      <option value="Type 3">Type 3</option>
                                  </select>
                                </div>
                              </div>

                               <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Employee <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="employee" name="employee" placeholder="Employee">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Description <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                <textarea class="form-control" id="description" name="description"></textarea>                               
                                </div>
                              </div>

                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                  Account Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="accountName" name="accountName" placeholder="Account Name">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Website <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="website" name="website" placeholder="Website">
                                </div>
                              </div>

                               <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Fax <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="fax" name="fax" placeholder="Fax">
                                </div>
                              </div> 

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Industry <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <select class="form-control" id="industry" name="industry">
                                    <option value="">&nbsp;</option>
                                    <option value="Industry 1">Industry 1</option>
                                    <option value="Industry 2">Industry 2</option>
                                    <option value="Industry 3">Industry 3</option>
                                  </select>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Annual Revenue<span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="annualRevenue" name="annualRevenue" placeholder="Annual Revenue">
                                </div>
                              </div>
                             

                            </div>
                             <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-8">
                                  <button class="btn btn-blue next-step btn-block">
                                    Next <i class="fa fa-arrow-circle-right"></i>
                                  </button>
                                </div>
                              </div>
                          </div>  
                                           
                        </div>
                      </div>
                      <div id="step-2">
                        <h2 class="StepTitle">Step 2 Billing Information</h2>

                          <div class="row">
                            <div class="col-sm-12">
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                   Billing Street  <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <textarea class="form-control" id="billingStreet" name="billingStreet"></textarea>
                                   
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Billing City <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="billingCity" name="billingCity" placeholder="Billing City">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Billing State/Province <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="billingStateProvince" name="billingStateProvince" placeholder="Billing State Province">
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Billing Zip/Postal Code <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="billingZipPostalCode" name="billingZipPostalCode" placeholder="Billing Zip Postal Code">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Billing Country <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="billingCountry" name="billingCountry" placeholder="Billing Country">
                                  </div>
                                </div>

                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                   Shipping Street <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                  <textarea class="form-control" id="shippingStreet" name="shippingStreet"></textarea>
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Shipping City <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="shippingCity" name="shippingCity" placeholder="Shipping City">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Shipping State/Province <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="shippingStateProvince" name="shippingStateProvince" placeholder="Shipping State/Province">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Shipping Zip/Postal Code <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="shippingZipPostalCode" name="shippingZipPostalCode" placeholder="Shipping Zip Postal Code">
                                  </div>
                                </div>

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Shipping Country <span class="symbol required"></span>
                                  </label>
                                  <div class="col-sm-7">
                                    <input type="text" class="form-control" id="shippingCountry" name="shippingCountry" placeholder="Shipping Country">
                                  </div>
                                </div>

                              </div>
                            </div> 
                            <div class="form-group">
                              <div class="col-sm-2 col-sm-offset-3">
                                <button class="btn btn-light-grey back-step btn-block">
                                  <i class="fa fa-circle-arrow-left"></i> Back
                                </button>
                              </div>
                              <div class="col-sm-2 col-sm-offset-3">
                                <button class="btn btn-blue next-step btn-block">
                                  Next <i class="fa fa-arrow-circle-right"></i>
                                </button>
                              </div>
                            </div>                           
                            </div>


                        
                        
                  
                      </div>
                      <div id="step-3">
                        <h2 class="StepTitle">Step 3 Additional Information</h2>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Account No. <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="accountNo" name="accountNo" placeholder="Account No">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Branch Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="branchName" name="branchName" placeholder="Branch Name">
                                </div>
                              </div> 

                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Bank Name <span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="bankName" name="bankName" placeholder="Bank Name">
                                </div>
                              </div>

                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 IFC Code<span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="ifcCode" name="ifcCode" placeholder="IFC Code">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 PAN Number<span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="panNumber" name="panNumber" placeholder="Pan Number">
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">
                                 Tin Number<span class="symbol required"></span>
                                </label>
                                <div class="col-sm-7">
                                  <input type="text" class="form-control" id="tinNumber" name="tinNumber" placeholder="Tin Number">
                                </div>
                              </div>

                            </div>
                          </div>                              
                        </div>

                        


                       
                      
                        <div class="form-group">
                          <div class="col-sm-2 col-sm-offset-3">
                            <button class="btn btn-light-grey back-step btn-block">
                              <i class="fa fa-circle-arrow-left"></i> Back
                            </button>
                          </div>
                          <div class="col-sm-2 col-sm-offset-3">
                            <button class="btn btn-blue next-step btn-block">
                              Next <i class="fa fa-arrow-circle-right"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div id="step-4">
                        <h2 class="StepTitle">Step 4 Summary</h2>
                        <h3>Opportunity Information</h3>
                            <div class="col-sm-12">
                              <div class="col-sm-6">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Account Owner:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="accountOwner"></p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Account Name:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="accountName"></p>
                                    </div>
                                  </div>                     
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Parent Account:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="parentAccount"></p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Type:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="type"></p>
                                    </div>
                                  </div>
                                 
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Employee:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="employee"></p>
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Description:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="description"></p>
                                    </div>
                                  </div>
                              </div>
                              <div class="col-sm-6">
                               <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Phone:
                                  </label>
                                  <div class="col-sm-7">
                                    <p class="form-control-static display-value" data-display="phone"></p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Fax:
                                  </label>
                                  <div class="col-sm-7">
                                    <p class="form-control-static display-value" data-display="fax"></p>
                                  </div>
                                </div>                     
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">
                                    Website:
                                  </label>
                                  <div class="col-sm-7">
                                    <p class="form-control-static display-value" data-display="website"></p>
                                  </div>
                                </div>
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Industry:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="industry"></p>
                                    </div>
                                  </div>
                                   <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                      Annual Revenue:
                                    </label>
                                    <div class="col-sm-7">
                                      <p class="form-control-static display-value" data-display="annualRevenue"></p>
                                    </div>
                                  </div>
                              
                              </div>
                            </div>
                       <h3>Billing Information</h3>
                       <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Billing Street:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="billingStreet"></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Billing City:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display=" billingCity"></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Billing State/Province:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="billingStateProvince"></p>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Billing Zip/Postal Code:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="billingZipPostalCode"></p>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Billing Country:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="billingCountry"></p>
                          </div>
                        </div>
                        </div>

                        <div class="col-sm-6">
                         <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Shipping Street:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="shippingStreet"></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Shipping City:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display=" shippingCity"></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Shipping State/Province:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="shippingStateProvince"></p>
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Shipping Zip/Postal Code:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="shippingZipPostalCode"></p>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-3 control-label">
                            Shipping Country:
                          </label>
                          <div class="col-sm-7">
                            <p class="form-control-static display-value" data-display="shippingCountry"></p>
                          </div>
                        </div>

                        </div>
                      </div>

                      <h3>Billing Information</h3>
                      <div class="col-sm-12">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              Account No:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="accountNo"></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              Bank Name:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="bankName"></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              Branch Name:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="branchName"></p>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              IFC Code:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="ifcCode"></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              Pan Number:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="panNumber"></p>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">
                              Tin Number:
                            </label>
                            <div class="col-sm-7">
                              <p class="form-control-static display-value" data-display="tinNumber"></p>
                            </div>
                          </div>
                        </div>
                      </div>


                        
                        
                        <div class="form-group">
                          <div class="col-sm-2 col-sm-offset-8">
                            <button class="btn btn-success finish-step btn-block">
                              Finish <i class="fa fa-arrow-circle-right"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
            </div>
          </div>
        </div>
      </div>
      
      <!-- end: PAGE CONTENT--> 
    </div>
  </div>
  <!-- end: PAGE --> 
</div>
<!-- end: MAIN CONTAINER -->

<?php $this->load->view('common/footer_content'); ?>
<!-- start: FOOTER -->

<!-- <script src="<?php echo base_url('assets/js/custom/contact.js'); ?>"></script> -->
<script>
jQuery(document).ready(function(){   
    FormWizard.init();
    //contactValidator.init();
});
</script> 

<?php $this->load->view('common/footer'); ?>

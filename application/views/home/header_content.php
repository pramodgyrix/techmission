<?php
$websetting = $this->session->userdata('websetting');
$Weblogos=$this->session->userdata('Weblogos');
$main_logo = display_image($Weblogos['main_logo'], SITE_LOGO.'/');	
?>
<!-- start: HEADER -->

<header>

<?php $this->load->view('home/sliding_bar'); ?>   
  <div role="navigation" class="navbar navbar-default navbar-fixed-top space-top"> 
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
      <div class="navbar-header"> 
        <!-- start: RESPONSIVE MENU TOGGLER -->
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <!-- end: RESPONSIVE MENU TOGGLER --> 
        <!-- start: LOGO --> 
        <a class="navbar-brand" href="<?=base_url();?>" title="<?=$websetting['site_name'];?>"> <img src="<?=$main_logo;?>" alt="<?=$websetting['site_name'];?>" height="85"/> </a> 
        <!-- end: LOGO --> 
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <?php 
$navs = get_navigation_from_CMS('1');




if(!empty($navs)){
	 $cycle = 0;
	 $group_navs = _group_by_array_key($navs,'groupID');
	 
	 //echo '<pre/>';
	// dd($group_navs);
	
	foreach($group_navs as $navs){
		$s = 1;
		$arrayCount = count($navs);
		foreach($navs as $nav){
			if($nav['mi_item_type'] == 3){
				$link = $nav['mi_link'];
			}elseif($nav['mi_item_type'] == 2){
				$link = base_url($nav['mi_link']);
			}else{
				$link = base_url('page/'.$nav['mi_alias']);
			}
			if(count($navs) > 1){
				$class = 'class="dropdown hidden-xs"';
				$attr = 'data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle"';
				$icon = '<b class="caret"></b>';
				if($nav['mi_parant_menu'] == 0){
					
					if($nav['visible_to'] == 1 && !$this->session->userdata('user_id')){
					
					echo '<li '.$class .'"><a '.$attr.' href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).$icon.' </a>';
					}else if($nav['visible_to'] == 2 && $this->session->userdata('user_id')){
					echo '<li '.$class .'"><a '.$attr.' href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).$icon.' </a>';	
					}if($nav['visible_to'] == 3){
					echo '<li '.$class .'"><a '.$attr.' href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).$icon.' </a>';	
					}
				 $s++;	
				}else{
					if($s == 2){
						echo '<ul class="dropdown-menu">';
					}
					echo '<li><a href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).'</a></li>';
					
					if($arrayCount  == $s){
						echo '</ul>';
					echo '</li>';
					
					}
				  $s++;
				}
			}else{
				if($nav['mi_title'] != 'home'){
					if($nav['visible_to'] == 1 && !$this->session->userdata('user_id')){
						echo '<li><a href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).' </a>';
					}else if($nav['visible_to'] == 2 && $this->session->userdata('user_id')){
						echo '<li><a href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).' </a>';
					}
					if($nav['visible_to'] == 3){
						echo '<li><a href="'.$link.'" target="'.$nav['mi_target'].'">'.ucfirst($nav['mi_title']).' </a>';
					}
				}
				
			}
		
		}
	}
}

?>
        </ul>
      </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER --> 
    
  </div>
</header>
<!-- end: HEADER -->
<div class="clearfix"></div>

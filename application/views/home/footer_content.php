<?php
$websetting = $this->session->userdata('websetting');
if($this->session->flashdata('message_success')){
	 $msg=$this->session->flashdata('message_success');
}elseif($this->session->flashdata('message_failed')){
	 $msg=$this->session->flashdata('message_failed');
} 
?>
<!-- start: FOOTER -->
		<footer id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-10">
						<div class="contact-details">
							<h4>Contact Us</h4>
							<ul class="contact">
								
								<li>
									<p>
										<i class="fa fa-phone"></i><strong>Phone:</strong> <?=$websetting['contact_number1'];?>
									</p>
								</li>
								<li>
									<p>
										<i class="fa fa-envelope"></i><strong>Email:</strong> <a href="mailto:<?=$websetting['site_email'];?>"><?=$websetting['site_email'];?></a>
									</p>
								</li>
                                
                                <li>
									<p>
										<i class="fa fa-map-marker"></i><strong>Address:</strong> <?=$websetting['site_address'];?>
									</p>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-2">
						<h4>Follow Us</h4>
						<div class="social-icons">
							<ul>
								<?php
 if(!empty($websetting) && !empty($websetting['twitter_url'])){
	$twitter_url =  $websetting['twitter_url'];
	echo '<li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom"> <a target="_blank" href="'.$twitter_url.'"> Twitter </a> </li>';
}

 if(!empty($websetting) && !empty($websetting['flicker_url'])){
	$flicker_url =  $websetting['flicker_url'];
	echo '<li class="social-flicker tooltips" data-original-title="Flicker" data-placement="bottom"> <a target="_blank" href="'.$flicker_url.'"> Flicker </a> </li>';
}

 if(!empty($websetting) && !empty($websetting['facebook_url'])){
	$facebook_url =  $websetting['facebook_url'];
	echo '<li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom"> <a target="_blank" href="'.$facebook_url.'"> Facebook </a> </li>';
}



 if(!empty($websetting) && !empty($websetting['google_url'])){
	$google_url =  $websetting['google_url'];
	echo '<li class="social-google tooltips" data-original-title="Google" data-placement="bottom"> <a target="_blank" href="'.$google_url.'"> Google+ </a> </li>';
}

 if(!empty($websetting) && !empty($websetting['linkedin_url'])){
	$linkedin_url =  $websetting['linkedin_url'];
	echo '<li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom"> <a target="_blank" href="'.$linkedin_url.'"> LinkedIn </a> </li>';
}

if(!empty($websetting) && !empty($websetting['pinterest_url'])){
	$pinterest_url =  $websetting['pinterest_url'];
	echo ' <li class="social-pinterest tooltips" data-original-title="Pinterest" data-placement="bottom"> <a target="_blank" href="'.$pinterest_url.'"> Pinterest </a> </li>';
}
?>

							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12">
							<p>
								<?=$websetting['site_copyright'];?>
							</p>
						</div>
						
					</div>
				</div>
			</div>
		</footer>
		<a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>
		<!-- end: FOOTER -->
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="<?php echo config_item('base_url');?>assets/plugins/respond.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/excanvas.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/html5shiv.js"></script>
		<script type="text/javascript" src="<?php echo config_item('base_url');?>assets/plugins/jQuery-lib/1.10.2/jquery.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo config_item('base_url');?>assets/plugins/jQuery-lib/2.0.3/jquery.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo config_item('base_url');?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/jquery.transit/jquery.transit.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/js/front_end/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo config_item('base_url');?>assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/flex-slider/jquery.flexslider.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/stellar.js/jquery.stellar.min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/plugins/colorbox/jquery.colorbox-min.js"></script>
		<script src="<?php echo config_item('base_url');?>assets/js/front_end/index.js"></script>
        <script src="<?=base_url('assets/plugins/gritter/js/jquery.gritter.min.js')?>"></script>
        <script src="<?=base_url('assets/js/custom/email-subscribe.js');?>"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
		    var msg='<?=!empty($msg)?$msg:'';?>';
			jQuery(document).ready(function() {
				Main.init();
				Index.init();
				$.stellar();
				if(msg != '') {
		        showNewNotificationAlert(msg);
				}
				
			});
			function showNewNotificationAlert(msg){
			

					 $.gritter.add({
					// (string | mandatory) the heading of the notification
						title: 'Success',
						// (string | mandatory) the text inside the notification
						text: msg,
						// (string | optional) the image to display on the left
						image: base_url+'assets/images/notification_icon.png',
						// (bool | optional) if you want it to fade out on its own or just sit there
						sticky: false,
						// (int | optional) the time you want it to be alive for before fading out
						time: ''
					});
				
		 }
		</script>
        
        <?php 
        	if(isset($scriptsrc) && !empty($scriptsrc)){
				$i=0;
				foreach($scriptsrc as $value){$i++;
					$tab = ($i!=1)?"\t\t ":"";
					echo $tab.'<script src="'.base_url($value).'" type="text/javascript"></script>'."\n";
				}
			}
			if(isset($script) && !empty($script)){
				foreach($script as $value){
					echo $value;
				}
			}
		?>	
        
        <style>
      /*  #footer{
    background: #255986 url("http://webdeveloperbareilly.in/img/map.png") repeat scroll center top / cover ;
   padding: 0px;
   color:#FFF;
}
 #footer > .contact p strong{
	 color:#FFF !important;
	 
	 }*/
        </style>
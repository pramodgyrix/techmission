	<?php  $websetting = $this->session->userdata('websetting');
		$contact_number = $websetting['contact_number1'];
		$site_email = $websetting['site_email'];
		$site_address = $websetting['site_address'];
	?>
    <!-- start: SLIDING BAR (SB) -->
			<div id="slidingbar-area">
				<div id="slidingbar">
					<!-- start: SLIDING BAR FIRST COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>About Techmission</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
							Nulla consequat massa quis enim.
							Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
							In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
						</p>
					</div>
					<!-- end: SLIDING BAR FIRST COLUMN -->
					<!-- start: SLIDING BAR SECOND COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>Recent Works</h2>
						<div class="blog-photo-stream margin-bottom-30">
							<ul class="list-unstyled">
								<li>
									<a href="#"><img alt="" src="assets/images/image01.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image02.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image03.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image04.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image05.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image06.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image07.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image08.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image09.jpg"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="assets/images/image10.jpg"></a>
								</li>
							</ul>
						</div>
					</div>
					<!-- end: SLIDING BAR SECOND COLUMN -->
					<!-- start: SLIDING BAR THIRD COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>Contact Us</h2>
						<address class="margin-bottom-40">
							<?php echo $site_address; ?>
							<br>
							P: <?php echo $contact_number; ?>
							<br>
							Email:
							<a href="mailto:<?php echo $site_email; ?>">
                            <?php echo $site_email; ?>
								
							</a>
						</address>
					</div>
					<!-- end: SLIDING BAR THIRD COLUMN -->
				</div>
				<!-- start: SLIDING BAR TOGGLE BUTTON -->
				<a href="javascript:void(0);" class="sb_toggle">
				</a>
				<!-- end: SLIDING BAR TOGGLE BUTTON -->
			</div>
			<!-- end: SLIDING BAR -->
         	<!-- start: TOP BAR -->
			<div class="clearfix " id="topbar">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<!-- start: TOP BAR CALL US -->
							<div class="callus">
								Call Us: <?php echo $contact_number; ?> - Mail:
								<a href="mailto:<?php echo $site_email; ?>">
									<?php echo $site_email; ?>
								</a>
							</div>
							<!-- end: TOP BAR CALL US -->
						</div>
						<div class="col-sm-6">
							<!-- start: TOP BAR SOCIAL ICONS -->
							<div class="social-icons">
								<ul>
									<li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
										<a target="_blank" href="http://www.twitter.com">
											Twitter
										</a>
									</li>
									<li class="social-dribbble tooltips" data-original-title="Dribbble" data-placement="bottom">
										<a target="_blank" href="http://dribbble.com">
											Dribbble
										</a>
									</li>
									<li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
										<a target="_blank" href="http://facebook.com">
											Facebook
										</a>
									</li>
									<li class="social-google tooltips" data-original-title="Google" data-placement="bottom">
										<a target="_blank" href="http://google.com">
											Google+
										</a>
									</li>
									<li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
										<a target="_blank" href="http://linkedin.com">
											LinkedIn
										</a>
									</li>
									<li class="social-youtube tooltips" data-original-title="YouTube" data-placement="bottom">
										<a target="_blank" href="http://youtube.com/">
											YouTube
										</a>
									</li>
									<li class="social-rss tooltips" data-original-title="RSS" data-placement="bottom">
										<a target="_blank" href="#" >
											RSS
										</a>
									</li>
								</ul>
							</div>
							<!-- end: TOP BAR SOCIAL ICONS -->
						</div>
					</div>
				</div>
			</div>
			<!-- end: TOP BAR -->
            <style type="text/css">
			#topbar {
    background-color: #FF4500;
    color: #fff !important;
}
            #topbar .callus{
				color:#fff !important;
			}
			#topbar .callus a{
				color:#fff !important;
			}
			.social-icons ul li a::before{
			    color:#fff !important;
			}
			#slidingbar{
			    color:#fff !important;
			}
			
            </style>
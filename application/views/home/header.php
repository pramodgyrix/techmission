<?php
$websetting = $this->session->userdata('websetting');
$className = $this->router->fetch_class();
$methodName = $this->router->fetch_method();
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
<title>
<?=$websetting['site_name'];?>
</title>
<!-- start: META -->
<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- end: META -->
<!-- start: MAIN CSS -->
<link href="<?php echo config_item('base_url');?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/fonts/style.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/plugins/animate.css/animate.min.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/css/front_end/main.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/css/front_end/main-responsive.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/css/front_end/theme_blue.css" type="text/css" id="skin_color">
<!-- end: MAIN CSS -->
<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/plugins/revolution_slider/rs-plugin/css/settings.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/plugins/flex-slider/flexslider.css">
<link rel="stylesheet" href="<?php echo config_item('base_url');?>assets/plugins/colorbox/example2/colorbox.css">
<link rel="stylesheet" href="<?=base_url('assets/plugins/gritter/css/jquery.gritter.css')?>">
<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: HTML5SHIV FOR IE8 -->
<!--[if lt IE 9]>
		<script src="<?php echo config_item('base_url');?>assets/plugins/html5shiv.js"></script>
		<![endif]-->
<!-- end: HTML5SHIV FOR IE8 -->

<?php
        	if(isset($stylesheet) && !empty($stylesheet)){
				$i=0;
				foreach($stylesheet as $value){$i++;								
					$tab = ($i!=1)?"\t\t ":"";
					echo $tab.'<link rel="stylesheet" href="'.base_url($value).'">'."\n";
				}
			}
			if(isset($style) && !empty($style)){
				foreach($style as $value){
					echo $value;
				}
			}
		?>
<script> 
		var base_url = "<?php echo base_url();?>";
		var site_url = "<?php echo site_url('/');?>";
		var session_user_id = '<?php echo $this->session->userdata('user_id'); ?>';
        </script>
</head>
<!-- end: HEAD -->
<body>
<?php if($this->session->flashdata('alert alert-warning')){ ?>
<div class="alert alert-warning" style="font-size: 16px; text-align: center; width: 100%; margin: 0px;"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Warning!</strong> <?php echo $this->session->flashdata('alert alert-warning'); ?> </div>
<?php } ?>
